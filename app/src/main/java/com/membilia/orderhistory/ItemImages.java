
package com.membilia.orderhistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemImages {

    @SerializedName("original_1")
    @Expose
    private String original1;

    public String getOriginal1() {
        return original1;
    }

    public void setOriginal1(String original1) {
        this.original1 = original1;
    }

}
