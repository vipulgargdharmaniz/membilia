
package com.membilia.orderhistory;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemObject {

    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("last_used")
    @Expose
    private String lastUsed;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("isAutographed")
    @Expose
    private boolean isAutographed;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("charity")
    @Expose
    private boolean charity;
    @SerializedName("charity_name")
    @Expose
    private String charityName;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("delivery_type")
    @Expose
    private boolean deliveryType;
    @SerializedName("item_name")
    @Expose
    private String itemName;
    @SerializedName("item_story")
    @Expose
    private String itemStory;
    @SerializedName("sport")
    @Expose
    private String sport;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("isFavorite")
    @Expose
    private boolean isFavorite;
    @SerializedName("user_images")
    @Expose
    private UserImages userImages;
    @SerializedName("pawned_date")
    @Expose
    private String pawnedDate;
    @SerializedName("tags")
    @Expose

    private List<Object> tags = null;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("item_images")
    @Expose
    private ItemImages itemImages;
    @SerializedName("item_video")
    @Expose
    private ItemVideo itemVideo;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(String lastUsed) {
        this.lastUsed = lastUsed;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isIsAutographed() {
        return isAutographed;
    }

    public void setIsAutographed(boolean isAutographed) {
        this.isAutographed = isAutographed;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isCharity() {
        return charity;
    }

    public void setCharity(boolean charity) {
        this.charity = charity;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(boolean deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemStory() {
        return itemStory;
    }

    public void setItemStory(String itemStory) {
        this.itemStory = itemStory;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public UserImages getUserImages() {
        return userImages;
    }

    public void setUserImages(UserImages userImages) {
        this.userImages = userImages;
    }

    public String getPawnedDate() {
        return pawnedDate;
    }

    public void setPawnedDate(String pawnedDate) {
        this.pawnedDate = pawnedDate;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ItemImages getItemImages() {
        return itemImages;
    }

    public void setItemImages(ItemImages itemImages) {
        this.itemImages = itemImages;
    }

    public ItemVideo getItemVideo() {
        return itemVideo;
    }

    public void setItemVideo(ItemVideo itemVideo) {
        this.itemVideo = itemVideo;
    }

}
