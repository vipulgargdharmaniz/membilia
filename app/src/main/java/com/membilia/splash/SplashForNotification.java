package com.membilia.splash;


import android.os.Bundle;
import com.membilia.BaseActivity;
import com.membilia.R;

/*
 * Created by dharmaniapps on 24/1/18.
 */

public class SplashForNotification  extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}
