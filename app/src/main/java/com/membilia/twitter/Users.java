package com.membilia.twitter;

/**
 * Created by dharmaniapps on 19/1/18.
 */

public class Users {
    private String location;

    private String default_profile;

    private String profile_background_tile;

    private String statuses_count;

    private String live_following;

    private String lang;

    private String profile_link_color;

    private String profile_banner_url;

    private String id;

    private String following;

    private String muting;

    //private String protected;

    private String favourites_count;

    private String profile_text_color;

    private String description;

    private String verified;

    private String contributors_enabled;

    private String profile_sidebar_border_color;

    private String name;

    private String profile_background_color;

    private String created_at;

    private String is_translation_enabled;

    private String default_profile_image;

    private String followers_count;

    private String has_extended_profile;

    private String profile_image_url_https;

    private String geo_enabled;

    //private Status status;

    private String profile_background_image_url;

    private String profile_background_image_url_https;

    private String follow_request_sent;

    //private Entities entities;

    private String url;

    private String utc_offset;

    private String time_zone;

    private String blocking;

    private String notifications;

    private String profile_use_background_image;

    private String friends_count;

    private String blocked_by;

    private String profile_sidebar_fill_color;

    private String screen_name;

    private String id_str;

    private String profile_image_url;

    private String listed_count;

    private String is_translator;
}