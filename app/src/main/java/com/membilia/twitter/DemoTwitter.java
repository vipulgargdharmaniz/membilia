package com.membilia.twitter;

/*
 * Created by dharmaniapps on 1/2/18.
 */


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.membilia.R;
import com.membilia.activities.MyTwitterApiClient;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;


public class DemoTwitter extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TAG = "DemoTwitter";
    TwitterLoginButton loginButton;
    LinearLayout twitterFriendsLL;
    TwitterLoginButton twitter_login_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        twitterFriendsLL = (LinearLayout) findViewById(R.id.twitterFriendsLL);
        twitterFriendsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                twitter();
                twitter_login_button.performClick();
            }
        });
        twitter_login_button = findViewById(R.id.twitter_login_button);
    }

    private void twitter() {
        twitter_login_button.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.e(TAG, "success: "+result );
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                getUserData(result);
            }


            @Override
            public void failure(TwitterException exception) {
                Log.e("TwitterKit", "Login with Twitter failure", exception);
            }
        });
    }

    public void getUserData(final Result<TwitterSession> result) {

        final TwitterSession session = result.data;
        Call<User> userResult = TwitterCore.getInstance().getApiClient(session).getAccountService().verifyCredentials(true, false, false);
        userResult.enqueue(new Callback<User>() {
            @Override
            public void failure(TwitterException e) {
                Log.e(TAG, "******ERROR******" + e.toString());


            }

            @Override
            public void success(Result<User> userResult) {
                Log.e(TAG, "userResult: " + userResult);
                User user = userResult.data;
                MyTwitterApiClient apiClient = new MyTwitterApiClient(session);
                getverify(session);
            }
        });
    }

    private void getverify(TwitterSession session) {
        DemoMyTwitterApiClient myTwitterApiClient = new DemoMyTwitterApiClient(session);
        myTwitterApiClient.getCustomService().list(session.getUserId()).enqueue(new Callback<JSONObject>() {
            @Override
            public void success(Result<JSONObject> result) {

                try {
                    Log.e(TAG, "success: "+result.response.body() );
                    Log.e(TAG, "success: "+result.response.message());
                    Log.e(TAG, "success: "+result.data.getString("users"));
                    String a = result.data.getString("users");
                    Log.e(TAG, "success: "+a );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        final TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
        if (twitterAuthClient.getRequestCode() == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }
}
