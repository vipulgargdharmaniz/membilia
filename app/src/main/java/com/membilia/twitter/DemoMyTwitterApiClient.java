package com.membilia.twitter;

/*
 * Created by dharmaniapps on 1/2/18.
 */

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class DemoMyTwitterApiClient extends TwitterApiClient {
    public DemoMyTwitterApiClient(TwitterSession session) {
        super(session);
    }

    /**
     * Provide CustomService with defined endpoints
     */
    public GetFriends getCustomService() {
        return getService(GetFriends.class);
    }
}

// example users/show service endpoint
 interface GetFriends {
    @GET("/1.1/followers/list.json?cursor=-1&skip_status=true&include_user_entities=false")
    Call<JSONObject> list(@Query("user_id") long userId);
}

