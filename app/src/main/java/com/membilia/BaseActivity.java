package com.membilia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/*
 * Created by dharmaniz on 24/5/17.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpViews();
        setUpClickListeners();
    }

    protected void setUpViews() {
    }

    protected void setUpClickListeners() {
    }


}
