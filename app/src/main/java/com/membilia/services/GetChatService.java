package com.membilia.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.Util.DatabaseHandler;
import com.membilia.Util.WebServicesConstants;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniapps on 22/2/18.
 */

public class GetChatService extends IntentService {

    String token;
    private static final String TAG = "GetChatService";
    public  DatabaseHandler db;


    public GetChatService(String name) {
        super(name);
    }
    public GetChatService() {
        super("GetChatService");
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
            token = intent.getExtras().getString("Token");
        Log.e(TAG, "onHandleIntent: "+token );
        getAllMessages();

    }

    private void getAllMessages() {
        String strUrl = WebServicesConstants.MessageHistory + "perPage=" + "20" + "&page_no=" + "1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", token);
                return header;
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.MessageHistory);
    }
}
