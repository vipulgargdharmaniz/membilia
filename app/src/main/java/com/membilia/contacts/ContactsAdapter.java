package com.membilia.contacts;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.membilia.R;

import java.util.ArrayList;
import java.util.List;

public class ContactsAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    public Resources res;
    Contact tempValues = null;
    int i = 0;
    /*********** Declare Used Variables *********/
    private Activity activity;
    private List<Contact> picList = null;

    private ArrayList<Contact> listpicOrigin;

    /*************  CustomAdapter Constructor *****************/
    public ContactsAdapter(Activity a, ArrayList d, Resources resLocal)
    {

        /********** Take passed values **********/
        activity = a;
        picList = d;
        res = resLocal;
        /***********  Layout inflator to call external xml layout () ***********/
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listpicOrigin = new ArrayList<Contact>();
        listpicOrigin.addAll(picList);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount()
    {

        if (picList.size() <= 0)
            return 1;
        return picList.size();
    }

    public Object getItem(int position)
    {
        return position;
    }

    public long getItemId(int position)
    {
        return position;
    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(int position, View convertView, ViewGroup parent)
    {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null)
        {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.adapter_contact_item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.tvName = (TextView) vi.findViewById(R.id.tvName);
            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        }
        else
            holder = (ViewHolder) vi.getTag();


        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (Contact) picList.get(position);

        /************  Set Model values in Holder elements ***********/
        holder.tvName.setText(tempValues.name);
        return vi;
    }

    /**
     * Filter
     */
    public void filter(String charText)
    {
        charText = charText.toLowerCase();
        picList.clear();
        if (charText.length() == 0)
        {
            picList.addAll(listpicOrigin);

        }
        else
        {
            for (Contact pic : listpicOrigin)
            {
                if (pic.name.toLowerCase().contains(charText))
                {
                    picList.add(pic);

                }

            }

        }
        notifyDataSetChanged();

    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder
    {
        TextView tvName;
    }

}
