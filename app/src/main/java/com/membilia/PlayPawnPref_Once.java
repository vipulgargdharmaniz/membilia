package com.membilia;

import android.content.Context;
import android.content.SharedPreferences;
/*
 * Created by dharmaniapps on 4/12/17.
 */

public class PlayPawnPref_Once {

    public static String FirstTimeSplash = "firstTimeSplash";
    public static String FirstTimeItemSplash = "firstTimeItemSplash";
    public static String FirstForCharity = "firstCharity";

    public static String ApplyasFan = "ApplyasFan";

    public static String FirstShipping = "firstShipping";
    public static String FirstPersonal = "firstPersonal";

    private static final String PREF_NAME = "firstTimePref_New";
    private static final int MODE = Context.MODE_PRIVATE;



    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }


    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }
}
