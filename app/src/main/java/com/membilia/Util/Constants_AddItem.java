package com.membilia.Util;

import com.membilia.volley_models.ImagesItemModel;

import java.util.ArrayList;

/*
 * Created by dharmaniapps on 7/12/17.
 */

public class Constants_AddItem {
    public static String ITEM_ADDER_ID = "";
    public static String ITEM_ID = "";
    public static String ITEM_NAME = "";
    public static String ITEM_PRICE = "";
    public static String ITEM_WEIGHT = "";
    public static String ITEM_CAT = "";
    public static String ITEM_GENRE = "";
    public static String ITEM_GENREVALUE = "";
    public static String ITEM_STORY = "";
    public static String TAGS = "";
    public static String ITEM_DELVIERYTYPE = "";
    public static String ITEM_PERSONAL_TRANSFER = "";
    public static String ITEM_LOCATION = "";
    public static String ITEM_AUTO = "";
    public static String ITEM_CHARITY = "";
    public static String ITEM_CHARITYNAME = "";
    public static String ITEM_LASTUSED = "";

    public static String USER_ID = "";
    public static String SELLER_ID = "";
    public static String CREATEDAT = "";

    public static String ITEM_COUNTRY = "";

    public static byte[] VideoDAta = null;
    public static String imagepath1 = "";
    public static String imagepath2 = "";
    public static String imagepath3 = "";
    public static String imagepath4 = "";
    public static String imagepath5 = "";
    public static String imagepath6 = "";

    public static String videopath = "";
    public static String videoimage = "";

    public static String User_Profile_Image = "";


    public static ArrayList<ImagesItemModel> ImageVideoArraylist;
    public static String BITMAP1 = "";
    public static String BITMAP2 = "";
    public static String BITMAP3 = "";
    public static String BITMAP4 = "";
    public static String BITMAP5 = "";
    public static String BITMAP6 = "";
    public static String BITMAP7 = "";

    public static String AdminArea = "";
    public static String AdminCountry = "";
    public static String AdminLocatity = "";
    public static String AdminPostalCode = "";
    public static String AdminCountryCode = "";
    public static String AdminLat = "";
    public static String AdminLng = "";

    public static boolean FilterPrize = true;
    public static boolean FilterDistance = true;
    public static boolean FiterDate = true;
    public static boolean FilterDeliveryType = true;
    public static int FilterRange=100000;
    public static int FilterPawncast=1;


    public static boolean FilterThingsWorn=true;
    public static boolean FilterAccess=true;
    public static boolean FilterExperince=true;
    public static boolean FilterBallBat=true;
    public static boolean FilterThingsMove=true;
    public static boolean FilterMiscl=true;
    public static boolean FilterFootwear=true;
    public static boolean FilterFashion=true;
    public static boolean FilterAutograph=true;
}
