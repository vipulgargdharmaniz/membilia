package com.membilia.Util;

/**
 * Created by dharmaniz on 19/7/17.
 */

public class Countries {

    public static String COUNTRIES_INFO = "[\n" +
            "  {\n" +
            "    \"name\": \"United States\",\n" +
            "    \"nameImage\": \"United States\",\n" +
            "    \"dial_code\": \"+1\",\n" +
            "    \"code\": \"US\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"United Kingdom\",\n" +
            "    \"nameImage\": \"United Kingdom\",\n" +
            "    \"dial_code\": \"+44\",\n" +
            "    \"code\": \"GB\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Afghanistan (افغانستان\u200E)\",\n" +
            "    \"nameImage\": \"Afghanistan\",\n" +
            "    \"dial_code\": \"+93\",\n" +
            "    \"code\": \"AF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Albania (Shqipëri)\",\n" +
            "    \"nameImage\": \"Albania\",\n" +
            "    \"dial_code\": \"+355\",\n" +
            "    \"code\": \"AL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Algeria (الجزائر\u200E)\",\n" +
            "    \"nameImage\": \"Algeria\",\n" +
            "    \"dial_code\": \"+213\",\n" +
            "    \"code\": \"DZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"American Samoa\",\n" +
            "    \"nameImage\": \"American Samoa\",\n" +
            "    \"dial_code\": \"+1684\",\n" +
            "    \"code\": \"AS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Andorra\",\n" +
            "    \"nameImage\": \"Andorra\",\n" +
            "    \"dial_code\": \"+376\",\n" +
            "    \"code\": \"AD\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Angola\",\n" +
            "    \"nameImage\": \"Angola\",\n" +
            "    \"dial_code\": \"+244\",\n" +
            "    \"code\": \"AO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Anguilla\",\n" +
            "    \"nameImage\": \"Anguilla\",\n" +
            "    \"dial_code\": \"+1264\",\n" +
            "    \"code\": \"AI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Antigua and Barbuda\",\n" +
            "    \"nameImage\": \"Antigua and Barbuda\",\n" +
            "    \"dial_code\": \"+1268\",\n" +
            "    \"code\": \"AG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Argentina\",\n" +
            "    \"nameImage\": \"Argentina\",\n" +
            "    \"dial_code\": \"+54\",\n" +
            "    \"code\": \"AR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Armenia (Հայաստան)\",\n" +
            "    \"nameImage\": \"Armenia\",\n" +
            "    \"dial_code\": \"+374\",\n" +
            "    \"code\": \"AM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Aruba\",\n" +
            "    \"nameImage\": \"Aruba\",\n" +
            "    \"dial_code\": \"+297\",\n" +
            "    \"code\": \"AW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Australia\",\n" +
            "    \"nameImage\": \"Australia\",\n" +
            "    \"dial_code\": \"+61\",\n" +
            "    \"code\": \"AU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Austria (Österreich)\",\n" +
            "    \"nameImage\": \"Austria\",\n" +
            "    \"dial_code\": \"+43\",\n" +
            "    \"code\": \"AT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Azerbaijan (Azərbaycan)\",\n" +
            "    \"nameImage\": \"Azerbaijan\",\n" +
            "    \"dial_code\": \"+994\",\n" +
            "    \"code\": \"AZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bahamas\",\n" +
            "    \"nameImage\": \"Bahamas\",\n" +
            "    \"dial_code\": \"+1242\",\n" +
            "    \"code\": \"BS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bahrain (البحرين\u200E)\",\n" +
            "    \"nameImage\": \"Bahrain\",\n" +
            "    \"dial_code\": \"+973\",\n" +
            "    \"code\": \"BH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bangladesh (বাংলাদেশ)\",\n" +
            "    \"nameImage\": \"Bangladesh\",\n" +
            "    \"dial_code\": \"+880\",\n" +
            "    \"code\": \"BD\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Barbados\",\n" +
            "    \"nameImage\": \"Barbados\",\n" +
            "    \"dial_code\": \"+1246\",\n" +
            "    \"code\": \"BB\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Belarus (Беларусь)\",\n" +
            "    \"nameImage\": \"Belarus\",\n" +
            "    \"dial_code\": \"+375\",\n" +
            "    \"code\": \"BY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Belgium (België)\",\n" +
            "    \"nameImage\": \"Belgium\",\n" +
            "    \"dial_code\": \"+32\",\n" +
            "    \"code\": \"BE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Belize\",\n" +
            "    \"nameImage\": \"Belize\",\n" +
            "    \"dial_code\": \"+501\",\n" +
            "    \"code\": \"BZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Benin (Bénin)\",\n" +
            "    \"nameImage\": \"Benin\",\n" +
            "    \"dial_code\": \"+229\",\n" +
            "    \"code\": \"BJ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bermuda\",\n" +
            "    \"nameImage\": \"Bermuda\",\n" +
            "    \"dial_code\": \"+1441\",\n" +
            "    \"code\": \"BM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bhutan (འབྲུག)\",\n" +
            "    \"nameImage\": \"Bhutan\",\n" +
            "    \"dial_code\": \"+975\",\n" +
            "    \"code\": \"BT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bolivia\",\n" +
            "    \"nameImage\": \"Bolivia\",\n" +
            "    \"dial_code\": \"+591\",\n" +
            "    \"code\": \"BO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bosnia and Herzegovina (Босна и Херцеговина)\",\n" +
            "    \"nameImage\": \"Bosnia and Herzegovina\",\n" +
            "    \"dial_code\": \"+387\",\n" +
            "    \"code\": \"BA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Botswana\",\n" +
            "    \"nameImage\": \"Botswana\",\n" +
            "    \"dial_code\": \"+267\",\n" +
            "    \"code\": \"BW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Brazil (Brasil)\",\n" +
            "    \"nameImage\": \"Brazil\",\n" +
            "    \"dial_code\": \"+55\",\n" +
            "    \"code\": \"BR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"British Indian Ocean Territory\",\n" +
            "    \"nameImage\": \"British Indian Ocean Territory\",\n" +
            "    \"dial_code\": \"+246\",\n" +
            "    \"code\": \"IO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"British Virgin Islands\",\n" +
            "    \"nameImage\": \"British Virgin Islands\",\n" +
            "    \"dial_code\": \"+1284\",\n" +
            "    \"code\": \"VG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Brunei\",\n" +
            "    \"nameImage\": \"Brunei\",\n" +
            "    \"dial_code\": \"+673\",\n" +
            "    \"code\": \"BN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Bulgaria (България)\",\n" +
            "    \"nameImage\": \"Bulgaria\",\n" +
            "    \"dial_code\": \"+359\",\n" +
            "    \"code\": \"BG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Burkina Faso\",\n" +
            "    \"nameImage\": \"Burkina Faso\",\n" +
            "    \"dial_code\": \"+226\",\n" +
            "    \"code\": \"BF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Burundi (Uburundi)\",\n" +
            "    \"nameImage\": \"Burundi\",\n" +
            "    \"dial_code\": \"+257\",\n" +
            "    \"code\": \"BI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Cambodia (កម្ពុជា)\",\n" +
            "    \"nameImage\": \"Cambodia\",\n" +
            "    \"dial_code\": \"+855\",\n" +
            "    \"code\": \"KH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Cameroon (Cameroun)\",\n" +
            "    \"nameImage\": \"Cameroon\",\n" +
            "    \"dial_code\": \"+237\",\n" +
            "    \"code\": \"CM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Canada\",\n" +
            "    \"nameImage\": \"Canada\",\n" +
            "    \"dial_code\": \"+1\",\n" +
            "    \"code\": \"CA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Cape Verde (Kabu Verdi)\",\n" +
            "    \"nameImage\": \"Cape Verde\",\n" +
            "    \"dial_code\": \"+238\",\n" +
            "    \"code\": \"CV\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Caribbean Netherlands\",\n" +
            "    \"nameImage\": \"Caribbean Netherlands\",\n" +
            "    \"dial_code\": \"+599\",\n" +
            "    \"code\": \"BQ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Cayman Islands\",\n" +
            "    \"nameImage\": \"Cayman Islands\",\n" +
            "    \"dial_code\": \"+1345\",\n" +
            "    \"code\": \"KY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Central African Republic (République centrafricaine)\",\n" +
            "    \"nameImage\": \"Central African Republic\",\n" +
            "    \"dial_code\": \"+236\",\n" +
            "    \"code\": \"CF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Chad (Tchad)\",\n" +
            "    \"nameImage\": \"Chad\",\n" +
            "    \"dial_code\": \"+235\",\n" +
            "    \"code\": \"TD\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Chile\",\n" +
            "    \"nameImage\": \"Chile\",\n" +
            "    \"dial_code\": \"+56\",\n" +
            "    \"code\": \"CL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"China (中国)\",\n" +
            "    \"nameImage\": \"China\",\n" +
            "    \"dial_code\": \"+86\",\n" +
            "    \"code\": \"CN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Colombia\",\n" +
            "    \"nameImage\": \"Colombia\",\n" +
            "    \"dial_code\": \"+57\",\n" +
            "    \"code\": \"CO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Comoros (جزر القمر\u200E)\",\n" +
            "    \"nameImage\": \"Comoros\",\n" +
            "    \"dial_code\": \"+269\",\n" +
            "    \"code\": \"KM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)\",\n" +
            "    \"nameImage\": \"Congo (DRC)\",\n" +
            "    \"dial_code\": \"+243\",\n" +
            "    \"code\": \"CD\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Congo (Republic) (Congo-Brazzaville)\",\n" +
            "    \"nameImage\": \"Congo (Republic)\",\n" +
            "    \"dial_code\": \"+242\",\n" +
            "    \"code\": \"CG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Cook Islands\",\n" +
            "    \"nameImage\": \"Cook Islands\",\n" +
            "    \"dial_code\": \"+682\",\n" +
            "    \"code\": \"CK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Costa Rica\",\n" +
            "    \"nameImage\": \"Costa Rica\",\n" +
            "    \"dial_code\": \"+506\",\n" +
            "    \"code\": \"CR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Côte d’Ivoire\",\n" +
            "    \"nameImage\": \"Côte d’Ivoire\",\n" +
            "    \"dial_code\": \"+225\",\n" +
            "    \"code\": \"CI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Croatia (Hrvatska)\",\n" +
            "    \"nameImage\": \"Croatia\",\n" +
            "    \"dial_code\": \"+385\",\n" +
            "    \"code\": \"HR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Cuba\",\n" +
            "    \"nameImage\": \"Cuba\",\n" +
            "    \"dial_code\": \"+53\",\n" +
            "    \"code\": \"CU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Curaçao\",\n" +
            "    \"nameImage\": \"Curaçao\",\n" +
            "    \"dial_code\": \"+599\",\n" +
            "    \"code\": \"CW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Cyprus (Κύπρος)\",\n" +
            "    \"nameImage\": \"Cyprus\",\n" +
            "    \"dial_code\": \"+357\",\n" +
            "    \"code\": \"CY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Czech Republic (Česká republika)\",\n" +
            "    \"nameImage\": \"Czech Republic\",\n" +
            "    \"dial_code\": \"+420\",\n" +
            "    \"code\": \"CZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Denmark (Danmark)\",\n" +
            "    \"nameImage\": \"Denmark\",\n" +
            "    \"dial_code\": \"+45\",\n" +
            "    \"code\": \"DK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Djibouti\",\n" +
            "    \"nameImage\": \"Djibouti\",\n" +
            "    \"dial_code\": \"+253\",\n" +
            "    \"code\": \"DJ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Dominica\",\n" +
            "    \"nameImage\": \"Dominica\",\n" +
            "    \"dial_code\": \"+1767\",\n" +
            "    \"code\": \"DM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Dominican Republic (República Dominicana)\",\n" +
            "    \"nameImage\": \"Dominican Republic\",\n" +
            "    \"dial_code\": \"+1\",\n" +
            "    \"code\": \"DO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Ecuador\",\n" +
            "    \"nameImage\": \"Ecuador\",\n" +
            "    \"dial_code\": \"+593\",\n" +
            "    \"code\": \"EC\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Egypt (مصر\u200E)\",\n" +
            "    \"nameImage\": \"Egypt\",\n" +
            "    \"dial_code\": \"+20\",\n" +
            "    \"code\": \"EG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"El Salvador\",\n" +
            "    \"nameImage\": \"El Salvador\",\n" +
            "    \"dial_code\": \"+503\",\n" +
            "    \"code\": \"SV\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Equatorial Guinea (Guinea Ecuatorial)\",\n" +
            "    \"nameImage\": \"Equatorial Guinea\",\n" +
            "    \"dial_code\": \"+240\",\n" +
            "    \"code\": \"GQ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Eritrea\",\n" +
            "    \"nameImage\": \"Eritrea\",\n" +
            "    \"dial_code\": \"+291\",\n" +
            "    \"code\": \"ER\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Estonia (Eesti)\",\n" +
            "    \"nameImage\": \"Estonia\",\n" +
            "    \"dial_code\": \"+372\",\n" +
            "    \"code\": \"EE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Ethiopia\",\n" +
            "    \"nameImage\": \"Ethiopia\",\n" +
            "    \"dial_code\": \"+251\",\n" +
            "    \"code\": \"ET\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Falkland Islands (Islas Malvinas)\",\n" +
            "    \"nameImage\": \"Falkland Islands\",\n" +
            "    \"dial_code\": \"+500\",\n" +
            "    \"code\": \"FK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Faroe Islands (Føroyar)\",\n" +
            "    \"nameImage\": \"Faroe Islands\",\n" +
            "    \"dial_code\": \"+298\",\n" +
            "    \"code\": \"FO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Fiji\",\n" +
            "    \"nameImage\": \"Fiji\",\n" +
            "    \"dial_code\": \"+679\",\n" +
            "    \"code\": \"FJ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Finland (Suomi)\",\n" +
            "    \"nameImage\": \"Finland\",\n" +
            "    \"dial_code\": \"+358\",\n" +
            "    \"code\": \"FI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"France\",\n" +
            "    \"nameImage\": \"France\",\n" +
            "    \"dial_code\": \"+33\",\n" +
            "    \"code\": \"FR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"French Guiana (Guyane française)\",\n" +
            "    \"nameImage\": \"French Guiana\",\n" +
            "    \"dial_code\": \"+594\",\n" +
            "    \"code\": \"GF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"French Polynesia (Polynésie française)\",\n" +
            "    \"nameImage\": \"French Polynesia\",\n" +
            "    \"dial_code\": \"+689\",\n" +
            "    \"code\": \"PF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Gabon\",\n" +
            "    \"nameImage\": \"Gabon\",\n" +
            "    \"dial_code\": \"+241\",\n" +
            "    \"code\": \"GA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Gambia\",\n" +
            "    \"nameImage\": \"Gambia\",\n" +
            "    \"dial_code\": \"+220\",\n" +
            "    \"code\": \"GM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Georgia (საქართველო)\",\n" +
            "    \"nameImage\": \"Georgia\",\n" +
            "    \"dial_code\": \"+995\",\n" +
            "    \"code\": \"GE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Germany (Deutschland)\",\n" +
            "    \"nameImage\": \"Germany\",\n" +
            "    \"dial_code\": \"+49\",\n" +
            "    \"code\": \"DE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Ghana (Gaana)\",\n" +
            "    \"nameImage\": \"Ghana\",\n" +
            "    \"dial_code\": \"+233\",\n" +
            "    \"code\": \"GH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Gibraltar\",\n" +
            "    \"nameImage\": \"Gibraltar\",\n" +
            "    \"dial_code\": \"+350\",\n" +
            "    \"code\": \"GI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Greece (Ελλάδα)\",\n" +
            "    \"nameImage\": \"Greece\",\n" +
            "    \"dial_code\": \"+30\",\n" +
            "    \"code\": \"GR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Greenland (Kalaallit Nunaat)\",\n" +
            "    \"nameImage\": \"Greenland\",\n" +
            "    \"dial_code\": \"+299\",\n" +
            "    \"code\": \"GL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Grenada\",\n" +
            "    \"nameImage\": \"Grenada\",\n" +
            "    \"dial_code\": \"+1473\",\n" +
            "    \"code\": \"GD\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Guadeloupe\",\n" +
            "    \"nameImage\": \"Guadeloupe\",\n" +
            "    \"dial_code\": \"+590\",\n" +
            "    \"code\": \"GP\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Guam\",\n" +
            "    \"nameImage\": \"Guam\",\n" +
            "    \"dial_code\": \"+1671\",\n" +
            "    \"code\": \"GU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Guatemala\",\n" +
            "    \"nameImage\": \"Guatemala\",\n" +
            "    \"dial_code\": \"+502\",\n" +
            "    \"code\": \"GT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Guinea (Guinée)\",\n" +
            "    \"nameImage\": \"Guinea\",\n" +
            "    \"dial_code\": \"+224\",\n" +
            "    \"code\": \"GN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Guinea-Bissau (Guiné Bissau)\",\n" +
            "    \"nameImage\": \"Guinea-Bissau\",\n" +
            "    \"dial_code\": \"+245\",\n" +
            "    \"code\": \"GW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Guyana\",\n" +
            "    \"nameImage\": \"Guyana\",\n" +
            "    \"dial_code\": \"+592\",\n" +
            "    \"code\": \"GY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Haiti\",\n" +
            "    \"nameImage\": \"Haiti\",\n" +
            "    \"dial_code\": \"+509\",\n" +
            "    \"code\": \"HT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Honduras\",\n" +
            "    \"nameImage\": \"Honduras\",\n" +
            "    \"dial_code\": \"+504\",\n" +
            "    \"code\": \"HN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Hong Kong (香港)\",\n" +
            "    \"nameImage\": \"Hong Kong\",\n" +
            "    \"dial_code\": \"+852\",\n" +
            "    \"code\": \"HK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Hungary (Magyarország)\",\n" +
            "    \"nameImage\": \"Hungary\",\n" +
            "    \"dial_code\": \"+36\",\n" +
            "    \"code\": \"HU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Iceland (Ísland)\",\n" +
            "    \"nameImage\": \"Iceland\",\n" +
            "    \"dial_code\": \"+354\",\n" +
            "    \"code\": \"IS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"India (भारत)\",\n" +
            "    \"nameImage\": \"India\",\n" +
            "    \"dial_code\": \"+91\",\n" +
            "    \"code\": \"IN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Indonesia\",\n" +
            "    \"nameImage\": \"Indonesia\",\n" +
            "    \"dial_code\": \"+62\",\n" +
            "    \"code\": \"ID\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Iran (ایران\u200E)\",\n" +
            "    \"nameImage\": \"Iran\",\n" +
            "    \"dial_code\": \"+98\",\n" +
            "    \"code\": \"IR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Iraq (العراق\u200E)\",\n" +
            "    \"nameImage\": \"Iraq\",\n" +
            "    \"dial_code\": \"+964\",\n" +
            "    \"code\": \"IQ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Ireland\",\n" +
            "    \"nameImage\": \"Ireland\",\n" +
            "    \"dial_code\": \"+353\",\n" +
            "    \"code\": \"IE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Israel (ישראל\u200E)\",\n" +
            "    \"nameImage\": \"Israel\",\n" +
            "    \"dial_code\": \"+972\",\n" +
            "    \"code\": \"IL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Italy (Italia)\",\n" +
            "    \"nameImage\": \"Italy\",\n" +
            "    \"dial_code\": \"+39\",\n" +
            "    \"code\": \"IT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Jamaica\",\n" +
            "    \"nameImage\": \"Jamaica\",\n" +
            "    \"dial_code\": \"+1876\",\n" +
            "    \"code\": \"JM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Japan (日本)\",\n" +
            "    \"nameImage\": \"Japan\",\n" +
            "    \"dial_code\": \"+81\",\n" +
            "    \"code\": \"JP\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Jordan (الأردن\u200E)\",\n" +
            "    \"nameImage\": \"Jordan\",\n" +
            "    \"dial_code\": \"+962\",\n" +
            "    \"code\": \"JO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Kazakhstan (Казахстан)\",\n" +
            "    \"nameImage\": \"Kazakhstan\",\n" +
            "    \"dial_code\": \"+7\",\n" +
            "    \"code\": \"KZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Kenya\",\n" +
            "    \"nameImage\": \"Kenya\",\n" +
            "    \"dial_code\": \"+254\",\n" +
            "    \"code\": \"KE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Kiribati\",\n" +
            "    \"nameImage\": \"Kiribati\",\n" +
            "    \"dial_code\": \"686\",\n" +
            "    \"code\": \"KI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Kuwait (الكويت\u200E)\",\n" +
            "    \"nameImage\": \"Kuwait\",\n" +
            "    \"dial_code\": \"+965\",\n" +
            "    \"code\": \"KW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Kyrgyzstan (Кыргызстан)\",\n" +
            "    \"nameImage\": \"Kyrgyzstan\",\n" +
            "    \"dial_code\": \"+996\",\n" +
            "    \"code\": \"KG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Laos (ລາວ)\",\n" +
            "    \"nameImage\": \"Laos\",\n" +
            "    \"dial_code\": \"+856\",\n" +
            "    \"code\": \"LA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Latvia (Latvija)\",\n" +
            "    \"nameImage\": \"Latvia\",\n" +
            "    \"dial_code\": \"+371\",\n" +
            "    \"code\": \"LV\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Lebanon (لبنان\u200E)\",\n" +
            "    \"nameImage\": \"Lebanon\",\n" +
            "    \"dial_code\": \"+961\",\n" +
            "    \"code\": \"LB\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Lesotho\",\n" +
            "    \"nameImage\": \"Lesotho\",\n" +
            "    \"dial_code\": \"+266\",\n" +
            "    \"code\": \"LS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Liberia\",\n" +
            "    \"nameImage\": \"Liberia\",\n" +
            "    \"dial_code\": \"+231\",\n" +
            "    \"code\": \"LR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Libya (ليبيا\u200E)\",\n" +
            "    \"nameImage\": \"Libya\",\n" +
            "    \"dial_code\": \"+218\",\n" +
            "    \"code\": \"Ly\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Liechtenstein\",\n" +
            "    \"nameImage\": \"Liechtenstein\",\n" +
            "    \"dial_code\": \"+423\",\n" +
            "    \"code\": \"LI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Lithuania (Lietuva)\",\n" +
            "    \"nameImage\": \"Lithuania\",\n" +
            "    \"dial_code\": \"+370\",\n" +
            "    \"code\": \"LT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Luxembourg\",\n" +
            "    \"nameImage\": \"Luxembourg\",\n" +
            "    \"dial_code\": \"+352\",\n" +
            "    \"code\": \"LU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Macau (澳門)\",\n" +
            "    \"nameImage\": \"Macau\",\n" +
            "    \"dial_code\": \"+853\",\n" +
            "    \"code\": \"MO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Macedonia (FYROM) (Македонија)\",\n" +
            "    \"nameImage\": \"Macedonia\",\n" +
            "    \"dial_code\": \"+389\",\n" +
            "    \"code\": \"MK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Madagascar (Madagasikara)\",\n" +
            "    \"nameImage\": \"Madagascar\",\n" +
            "    \"dial_code\": \"+261\",\n" +
            "    \"code\": \"MG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Malawi\",\n" +
            "    \"nameImage\": \"Malawi\",\n" +
            "    \"dial_code\": \"+265\",\n" +
            "    \"code\": \"MW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Malaysia\",\n" +
            "    \"nameImage\": \"Malaysia\",\n" +
            "    \"dial_code\": \"+60\",\n" +
            "    \"code\": \"MY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Maldives\",\n" +
            "    \"nameImage\": \"Maldives\",\n" +
            "    \"dial_code\": \"+96\",\n" +
            "    \"code\": \"MV\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Mali\",\n" +
            "    \"nameImage\": \"Mali\",\n" +
            "    \"dial_code\": \"+223\",\n" +
            "    \"code\": \"ML\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Malta\",\n" +
            "    \"nameImage\": \"Malta\",\n" +
            "    \"dial_code\": \"+356\",\n" +
            "    \"code\": \"MT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Marshall Islands\",\n" +
            "    \"nameImage\": \"Marshall Islands\",\n" +
            "    \"dial_code\": \"+692\",\n" +
            "    \"code\": \"MH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Martinique\",\n" +
            "    \"nameImage\": \"Martinique\",\n" +
            "    \"dial_code\": \"+596\",\n" +
            "    \"code\": \"MQ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Mauritania (موريتانيا\u200E)\",\n" +
            "    \"nameImage\": \"Mauritania\",\n" +
            "    \"dial_code\": \"+222\",\n" +
            "    \"code\": \"MR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Mauritius (Moris)\",\n" +
            "    \"nameImage\": \"Mauritius\",\n" +
            "    \"dial_code\": \"+230\",\n" +
            "    \"code\": \"MU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Mexico (México)\",\n" +
            "    \"nameImage\": \"Mexico\",\n" +
            "    \"dial_code\": \"+52\",\n" +
            "    \"code\": \"MX\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Micronesia\",\n" +
            "    \"nameImage\": \"Micronesia\",\n" +
            "    \"dial_code\": \"+691\",\n" +
            "    \"code\": \"MF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Moldova (Republica Moldova)\",\n" +
            "    \"nameImage\": \"Moldova\",\n" +
            "    \"dial_code\": \"+373\",\n" +
            "    \"code\": \"MD\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Monaco\",\n" +
            "    \"nameImage\": \"Monaco\",\n" +
            "    \"dial_code\": \"+377\",\n" +
            "    \"code\": \"MC\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Mongolia (Монгол)\",\n" +
            "    \"nameImage\": \"Mongolia\",\n" +
            "    \"dial_code\": \"+976\",\n" +
            "    \"code\": \"MN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Montenegro (Crna Gora)\",\n" +
            "    \"nameImage\": \"Montenegro\",\n" +
            "    \"dial_code\": \"+382\",\n" +
            "    \"code\": \"ME\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Montserrat\",\n" +
            "    \"nameImage\": \"Montserrat\",\n" +
            "    \"dial_code\": \"+1664\",\n" +
            "    \"code\": \"MS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Morocco (المغرب\u200E)\",\n" +
            "    \"nameImage\": \"Morocco\",\n" +
            "    \"dial_code\": \"+212\",\n" +
            "    \"code\": \"MA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Mozambique (Moçambique)\",\n" +
            "    \"nameImage\": \"Mozambique\",\n" +
            "    \"dial_code\": \"+258\",\n" +
            "    \"code\": \"MZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Myanmar (Burma) (မြန်မာ)\",\n" +
            "    \"nameImage\": \"Myanmar\",\n" +
            "    \"dial_code\": \"+95\",\n" +
            "    \"code\": \"MM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Namibia (Namibië)\",\n" +
            "    \"nameImage\": \"Namibia\",\n" +
            "    \"dial_code\": \"+264\",\n" +
            "    \"code\": \"NA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Nauru\",\n" +
            "    \"nameImage\": \"Nauru\",\n" +
            "    \"dial_code\": \"+674\",\n" +
            "    \"code\": \"NR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Nepal (नेपाल)\",\n" +
            "    \"nameImage\": \"Nepal\",\n" +
            "    \"dial_code\": \"+977\",\n" +
            "    \"code\": \"NP\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Netherlands (Nederland)\",\n" +
            "    \"nameImage\": \"Netherlands\",\n" +
            "    \"dial_code\": \"+31\",\n" +
            "    \"code\": \"NL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"New Caledonia (Nouvelle-Calédonie)\",\n" +
            "    \"nameImage\": \"New Caledonia\",\n" +
            "    \"dial_code\": \"+687\",\n" +
            "    \"code\": \"NC\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"New Zealand\",\n" +
            "    \"nameImage\": \"New Zealand\",\n" +
            "    \"dial_code\": \"+64\",\n" +
            "    \"code\": \"NZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Nicaragua\",\n" +
            "    \"nameImage\": \"Nicaragua\",\n" +
            "    \"dial_code\": \"+505\",\n" +
            "    \"code\": \"NI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Niger (Nijar)\",\n" +
            "    \"nameImage\": \"Niger\",\n" +
            "    \"dial_code\": \"+227\",\n" +
            "    \"code\": \"NE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Nigeria\",\n" +
            "    \"nameImage\": \"Nigeria\",\n" +
            "    \"dial_code\": \"+234\",\n" +
            "    \"code\": \"NG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Niue\",\n" +
            "    \"nameImage\": \"Niue\",\n" +
            "    \"dial_code\": \"+683\",\n" +
            "    \"code\": \"NU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Norfolk Island\",\n" +
            "    \"nameImage\": \"Norfolk Island\",\n" +
            "    \"dial_code\": \"+672\",\n" +
            "    \"code\": \"NF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"North Korea (조선 민주주의 인민 공화국)\",\n" +
            "    \"nameImage\": \"North Korea\",\n" +
            "    \"dial_code\": \"+850\",\n" +
            "    \"code\": \"KP\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Northern Mariana Islands\",\n" +
            "    \"nameImage\": \"Northern Mariana Islands\",\n" +
            "    \"dial_code\": \"+1670\",\n" +
            "    \"code\": \"MP\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Norway (Norge)\",\n" +
            "    \"nameImage\": \"Norway\",\n" +
            "    \"dial_code\": \"+47\",\n" +
            "    \"code\": \"NO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Oman (عُمان\u200E)\",\n" +
            "    \"nameImage\": \"Oman\",\n" +
            "    \"dial_code\": \"+968\",\n" +
            "    \"code\": \"OM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Pakistan (پاکستان\u200E)\",\n" +
            "    \"nameImage\": \"Pakistan\",\n" +
            "    \"dial_code\": \"+92\",\n" +
            "    \"code\": \"PK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Palau\",\n" +
            "    \"nameImage\": \"Palau\",\n" +
            "    \"dial_code\": \"+680\",\n" +
            "    \"code\": \"PW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Palestine\",\n" +
            "    \"nameImage\": \"Palestine\",\n" +
            "    \"dial_code\": \"+970\",\n" +
            "    \"code\": \"PS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Panama (Panamá)\",\n" +
            "    \"nameImage\": \"Panama\",\n" +
            "    \"dial_code\": \"+507\",\n" +
            "    \"code\": \"PA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Papua New Guinea\",\n" +
            "    \"nameImage\": \"Papua New Guinea\",\n" +
            "    \"dial_code\": \"+675\",\n" +
            "    \"code\": \"PG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Paraguay\",\n" +
            "    \"nameImage\": \"Paraguay\",\n" +
            "    \"dial_code\": \"+595\",\n" +
            "    \"code\": \"PY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Peru (Perú)\",\n" +
            "    \"nameImage\": \"Peru\",\n" +
            "    \"dial_code\": \"+51\",\n" +
            "    \"code\": \"PE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Philippines\",\n" +
            "    \"nameImage\": \"Philippines\",\n" +
            "    \"dial_code\": \"+63\",\n" +
            "    \"code\": \"PH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Poland (Polska)\",\n" +
            "    \"nameImage\": \"Poland\",\n" +
            "    \"dial_code\": \"+48\",\n" +
            "    \"code\": \"PL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Portugal\",\n" +
            "    \"nameImage\": \"Portugal\",\n" +
            "    \"dial_code\": \"+351\",\n" +
            "    \"code\": \"PT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Puerto Rico\",\n" +
            "    \"nameImage\": \"Puerto Rico\",\n" +
            "    \"dial_code\": \"+1\",\n" +
            "    \"code\": \"PR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Qatar (قطر\u200E)\",\n" +
            "    \"nameImage\": \"Qatar\",\n" +
            "    \"dial_code\": \"+974\",\n" +
            "    \"code\": \"QA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Réunion (La Réunion)\",\n" +
            "    \"nameImage\": \"Réunion\",\n" +
            "    \"dial_code\": \"+262\",\n" +
            "    \"code\": \"RE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Romania (România)\",\n" +
            "    \"nameImage\": \"Romania\",\n" +
            "    \"dial_code\": \"+40\",\n" +
            "    \"code\": \"RO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Russia (Россия)\",\n" +
            "    \"nameImage\": \"Russia\",\n" +
            "    \"dial_code\": \"+7\",\n" +
            "    \"code\": \"RU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Rwanda\",\n" +
            "    \"nameImage\": \"Rwanda\",\n" +
            "    \"dial_code\": \"+250\",\n" +
            "    \"code\": \"RW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saint Barthélemy (Saint-Barthélemy)\",\n" +
            "    \"nameImage\": \"Saint Barthélemy\",\n" +
            "    \"dial_code\": \"+590\",\n" +
            "    \"code\": \"BL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saint Helena\",\n" +
            "    \"nameImage\": \"Saint Helena\",\n" +
            "    \"dial_code\": \"+290\",\n" +
            "    \"code\": \"SH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saint Kitts and Nevis\",\n" +
            "    \"nameImage\": \"Saint Kitts and Nevis\",\n" +
            "    \"dial_code\": \"+1869\",\n" +
            "    \"code\": \"KN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saint Lucia\",\n" +
            "    \"nameImage\": \"Saint Lucia\",\n" +
            "    \"dial_code\": \"+1758\",\n" +
            "    \"code\": \"LC\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saint Martin (Saint-Martin (partie française))\",\n" +
            "    \"nameImage\": \"Saint Martin\",\n" +
            "    \"dial_code\": \"+590\",\n" +
            "    \"code\": \"MF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)\",\n" +
            "    \"nameImage\": \"Saint Pierre and Miquelon\",\n" +
            "    \"dial_code\": \"+508\",\n" +
            "    \"code\": \"PM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saint Vincent and the Grenadines\",\n" +
            "    \"nameImage\": \"Saint Vincent and the Grenadines\",\n" +
            "    \"dial_code\": \"+1784\",\n" +
            "    \"code\": \"VC\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Samoa\",\n" +
            "    \"nameImage\": \"Samoa\",\n" +
            "    \"dial_code\": \"+685\",\n" +
            "    \"code\": \"WS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"San Marino\",\n" +
            "    \"nameImage\": \"San Marino\",\n" +
            "    \"dial_code\": \"+378\",\n" +
            "    \"code\": \"SM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"São Tomé and Príncipe (São Tomé e Príncipe)\",\n" +
            "    \"nameImage\": \"São Tomé and Príncipe\",\n" +
            "    \"dial_code\": \"+239\",\n" +
            "    \"code\": \"ST\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Saudi Arabia (المملكة العربية السعودية\u200E)\",\n" +
            "    \"nameImage\": \"Saudi Arabia\",\n" +
            "    \"dial_code\": \"+966\",\n" +
            "    \"code\": \"SA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Senegal (Sénégal)\",\n" +
            "    \"nameImage\": \"Senegal\",\n" +
            "    \"dial_code\": \"+221\",\n" +
            "    \"code\": \"SN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Serbia (Србија)\",\n" +
            "    \"nameImage\": \"Serbia\",\n" +
            "    \"dial_code\": \"+381\",\n" +
            "    \"code\": \"RS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Seychelles\",\n" +
            "    \"nameImage\": \"Seychelles\",\n" +
            "    \"dial_code\": \"+248\",\n" +
            "    \"code\": \"SC\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Sierra Leone\",\n" +
            "    \"nameImage\": \"Sierra Leone\",\n" +
            "    \"dial_code\": \"+232\",\n" +
            "    \"code\": \"SL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Singapore\",\n" +
            "    \"nameImage\": \"Singapore\",\n" +
            "    \"dial_code\": \"+65\",\n" +
            "    \"code\": \"SG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Sint Maarten\",\n" +
            "    \"nameImage\": \"Sint Maarten\",\n" +
            "    \"dial_code\": \"+1721\",\n" +
            "    \"code\": \"SX\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Slovakia (Slovensko)\",\n" +
            "    \"nameImage\": \"Slovakia\",\n" +
            "    \"dial_code\": \"+421\",\n" +
            "    \"code\": \"SK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Slovenia (Slovenija)\",\n" +
            "    \"nameImage\": \"Slovenia\",\n" +
            "    \"dial_code\": \"+386\",\n" +
            "    \"code\": \"SI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Solomon Islands\",\n" +
            "    \"nameImage\": \"Solomon Islands\",\n" +
            "    \"dial_code\": \"+252\",\n" +
            "    \"code\": \"SB\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Somalia (Soomaaliya)\",\n" +
            "    \"nameImage\": \"Somalia\",\n" +
            "    \"dial_code\": \"+27\",\n" +
            "    \"code\": \"SO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"South Africa\",\n" +
            "    \"nameImage\": \"South Africa\",\n" +
            "    \"dial_code\": \"+82\",\n" +
            "    \"code\": \"ZA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"South Korea (대한민국)\",\n" +
            "    \"nameImage\": \"South Korea\",\n" +
            "    \"dial_code\": \"+211\",\n" +
            "    \"code\": \"KR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"South Sudan (جنوب السودان\u200E)\",\n" +
            "    \"nameImage\": \"South Sudan\",\n" +
            "    \"dial_code\": \"+34\",\n" +
            "    \"code\": \"SS\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Spain (España)\",\n" +
            "    \"nameImage\": \"Spain\",\n" +
            "    \"dial_code\": \"+94\",\n" +
            "    \"code\": \"ES\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Sri Lanka (ශ්\u200Dරී ලංකාව)\",\n" +
            "    \"nameImage\": \"Sri Lanka\",\n" +
            "    \"dial_code\": \"+249\",\n" +
            "    \"code\": \"LK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Sudan (السودان\u200E)\",\n" +
            "    \"nameImage\": \"Sudan\",\n" +
            "    \"dial_code\": \"\",\n" +
            "    \"code\": \"SD\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Suriname\",\n" +
            "    \"nameImage\": \"Suriname\",\n" +
            "    \"dial_code\": \"+597\",\n" +
            "    \"code\": \"SR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Swaziland\",\n" +
            "    \"nameImage\": \"Swaziland\",\n" +
            "    \"dial_code\": \"+268\",\n" +
            "    \"code\": \"SZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Sweden (Sverige)\",\n" +
            "    \"nameImage\": \"Sweden\",\n" +
            "    \"dial_code\": \"+46\",\n" +
            "    \"code\": \"SE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Switzerland (Schweiz)\",\n" +
            "    \"nameImage\": \"Switzerland\",\n" +
            "    \"dial_code\": \"+41\",\n" +
            "    \"code\": \"CH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Syria (سوريا\u200E)\",\n" +
            "    \"nameImage\": \"Syria\",\n" +
            "    \"dial_code\": \"+963\",\n" +
            "    \"code\": \"SY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Taiwan (台灣)\",\n" +
            "    \"nameImage\": \"Taiwan\",\n" +
            "    \"dial_code\": \"+886\",\n" +
            "    \"code\": \"TW\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Tajikistan\",\n" +
            "    \"nameImage\": \"Tajikistan\",\n" +
            "    \"dial_code\": \"+992\",\n" +
            "    \"code\": \"TJ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Tanzania\",\n" +
            "    \"nameImage\": \"Tanzania\",\n" +
            "    \"dial_code\": \"+255\",\n" +
            "    \"code\": \"TZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Thailand (ไทย)\",\n" +
            "    \"nameImage\": \"Thailand\",\n" +
            "    \"dial_code\": \"+66\",\n" +
            "    \"code\": \"TH\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Timor-Leste\",\n" +
            "    \"nameImage\": \"Timor-Leste\",\n" +
            "    \"dial_code\": \"+670\",\n" +
            "    \"code\": \"TL\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Togo\",\n" +
            "    \"nameImage\": \"Togo\",\n" +
            "    \"dial_code\": \"+228\",\n" +
            "    \"code\": \"TG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Tokelau\",\n" +
            "    \"nameImage\": \"Tokelau\",\n" +
            "    \"dial_code\": \"+690\",\n" +
            "    \"code\": \"TK\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Tonga\",\n" +
            "    \"nameImage\": \"Tonga\",\n" +
            "    \"dial_code\": \"+676\",\n" +
            "    \"code\": \"TO\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Trinidad and Tobago\",\n" +
            "    \"nameImage\": \"Trinidad and Tobago\",\n" +
            "    \"dial_code\": \"+1868\",\n" +
            "    \"code\": \"TT\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Tunisia (تونس\u200E)\",\n" +
            "    \"nameImage\": \"Tunisia\",\n" +
            "    \"dial_code\": \"+216\",\n" +
            "    \"code\": \"TN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Turkey (Türkiye)\",\n" +
            "    \"nameImage\": \"Turkey\",\n" +
            "    \"dial_code\": \"+90\",\n" +
            "    \"code\": \"TR\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Turkmenistan\",\n" +
            "    \"nameImage\": \"Turkmenistan\",\n" +
            "    \"dial_code\": \"+993\",\n" +
            "    \"code\": \"TM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Turks and Caicos Islands\",\n" +
            "    \"nameImage\": \"Turks and Caicos Islands\",\n" +
            "    \"dial_code\": \"+1649\",\n" +
            "    \"code\": \"TC\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Tuvalu\",\n" +
            "    \"nameImage\": \"Tuvalu\",\n" +
            "    \"dial_code\": \"+688\",\n" +
            "    \"code\": \"TV\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"U.S. Virgin Islands\",\n" +
            "    \"nameImage\": \"U S Virgin Islands\",\n" +
            "    \"dial_code\": \"+1340\",\n" +
            "    \"code\": \"VI\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Uganda\",\n" +
            "    \"nameImage\": \"Uganda\",\n" +
            "    \"dial_code\": \"+256\",\n" +
            "    \"code\": \"UG\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Ukraine (Україна)\",\n" +
            "    \"nameImage\": \"Ukraine\",\n" +
            "    \"dial_code\": \"+380\",\n" +
            "    \"code\": \"UA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"United Arab Emirates (الإمارات العربية المتحدة\u200E)\",\n" +
            "    \"nameImage\": \"United Arab Emirates\",\n" +
            "    \"dial_code\": \"+971\",\n" +
            "    \"code\": \"AE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"United Kingdom\",\n" +
            "    \"nameImage\": \"United Kingdom\",\n" +
            "    \"dial_code\": \"+44\",\n" +
            "    \"code\": \"GB\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"United States\",\n" +
            "    \"nameImage\": \"United States\",\n" +
            "    \"dial_code\": \"+1\",\n" +
            "    \"code\": \"US\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Uruguay\",\n" +
            "    \"nameImage\": \"Uruguay\",\n" +
            "    \"dial_code\": \"+598\",\n" +
            "    \"code\": \"UY\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Uzbekistan (Oʻzbekiston)\",\n" +
            "    \"nameImage\": \"Uzbekistan\",\n" +
            "    \"dial_code\": \"+998\",\n" +
            "    \"code\": \"UZ\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Vanuatu\",\n" +
            "    \"nameImage\": \"Vanuatu\",\n" +
            "    \"dial_code\": \"+678\",\n" +
            "    \"code\": \"VU\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Vatican City (Città del Vaticano)\",\n" +
            "    \"nameImage\": \"Vatican City\",\n" +
            "    \"dial_code\": \"+39\",\n" +
            "    \"code\": \"VA\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Venezuela\",\n" +
            "    \"nameImage\": \"Venezuela\",\n" +
            "    \"dial_code\": \"+58\",\n" +
            "    \"code\": \"VE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Vietnam (Việt Nam)\",\n" +
            "    \"nameImage\": \"Vietnam\",\n" +
            "    \"dial_code\": \"+84\",\n" +
            "    \"code\": \"VN\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Wallis and Futuna\",\n" +
            "    \"nameImage\": \"Wallis and Futuna\",\n" +
            "    \"dial_code\": \"+681\",\n" +
            "    \"code\": \"WF\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Yemen (اليمن\u200E)\",\n" +
            "    \"nameImage\": \"Yemen\",\n" +
            "    \"dial_code\": \"+967\",\n" +
            "    \"code\": \"YE\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Zambia\",\n" +
            "    \"nameImage\": \"Zambia\",\n" +
            "    \"dial_code\": \"+260\",\n" +
            "    \"code\": \"ZM\",\n" +
            "    \"Language\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Zimbabwe\",\n" +
            "    \"nameImage\": \"Zimbabwe\",\n" +
            "    \"dial_code\": \"+263\",\n" +
            "    \"code\": \"ZW\",\n" +
            "    \"Language\": \"\"\n" +
            "  }\n" +
            "]";
}
