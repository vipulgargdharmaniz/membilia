package com.membilia.Util;

/*
 * Created by dharmaniz on 15/6/17.
 */

public class WebServicesConstants {
//    public static String BASE_URL = "http://35.166.11.148/playPawn/api/";
//    public static String BASE_URL = "http://35.166.11.148/staging/api/";

    public static String BASE_URL = "http://membiliaapp.com/staging/api/";
    public static String FACEBOOK = BASE_URL + "facebook.php";
    public static String FACEBOOK_LOGIN = BASE_URL + "auth/facebook";
    public static String TWITTER = BASE_URL + "twitter.php";
    public static String TWITTER_LOGIN = BASE_URL + "auth/twitter";
    //    public static String SIGN_UP = BASE_URL + "auth/email/signup";
    public static String SIGN_UP = BASE_URL + "user_signup.php";
    //http://35.166.11.148/staging/api/auth/email/signup
    public static String SIGN_IN = BASE_URL + "auth/email/signin";//http://35.166.11.148/staging/api/auth/email/signup
    public static String FORGOT_PASSWORD = BASE_URL + "forgotpassword.php";
    public static String UPLOAD_IMAGE_ON_SERVER = BASE_URL + "image/cache/user";
    public static String EXPLORE_ITEMS = BASE_URL + "explore/items";
    public static String USERS_LISTING = BASE_URL + "user/listing";
    //  public static String ITEM_DETAILS = BASE_URL + "items/listing";
//  http://35.166.11.148/staging/api/items/969playpawn
    public static String APPLY_AS_SELLER = BASE_URL + "access/seller";
    public static String USER_PROFILE = BASE_URL + "user/profile/";
    public static String AVAILABLE_ITEMS = BASE_URL + "available/item";
    public static String FAVROITE_ITEMS = BASE_URL + "item/fav/";
    //http://35.166.11.148/staging/api/item/fav/203playpawn&perPage=1&page_no=1
    public static String PAWNED_ITEMS = BASE_URL + "item/Pawned";
    public static String USER_REVIEW = BASE_URL + "user/review/";
    //    public static String CATEGORIES_ITEMS = BASE_URL + "item/search/cat";
    public static String ACTIVITY_FRAGMENTS = BASE_URL + "user/activity?";//http://35.166.11.148/staging/api/user/activity?user_id=12&perPage=10&page_no=1
    public static String ALL_ADDRESSES = BASE_URL + "user/addresses/";//http://35.166.11.148/staging/api/user/addresses/
    public static String SAVE_USER_ADDRESS = BASE_URL + "user/address";
    public static String CHARITY = BASE_URL + "Charity";//http://35.166.11.148/staging/api/Charity
    public static String Follower = BASE_URL + "user/followers/";
    public static String Following = BASE_URL + "user/following/";
    public static String ITEM_SEARCH = BASE_URL + "item/search?";
    public static String USER_SEARCH = BASE_URL + "user/search?";
    public static String UPDATE_USER = BASE_URL + "updateuser.php";
    public static String ITEM_DETAILS_ALONE = BASE_URL + "items/";
    public static String GET_CATEGORIES = BASE_URL + "app/itemCategories";
    public static String GET_GENRE_VALUE = "http://35.166.11.148/staging/api/GetGenreWithValues.php";
    public static String ADDITEM = BASE_URL + "TestItemAdd.php";
    public static String DeleteItem = BASE_URL + "item/delete/";
    public static String UPDATEITEM = BASE_URL + "item/";
    public static String MultipleImages = BASE_URL + "multipleImageItem.php";
    //  public static String AddImages = BASE_URL + "image/item/";
    public static String DeleteVideo = BASE_URL + "video/delete/item/";
    public static String VideoUpload = BASE_URL + "videoitem.php";
    //  public static String DeleteImages = BASE_URL + "image/delete/item/";
    public static String DefaultAddress = BASE_URL + "defalut_address.php?address_id=";
    public static String OrderHistroy = BASE_URL + "order_history.php?";
    public static String Review = BASE_URL + "user/";
    public static String Feedback = BASE_URL + "feedback.php";
    public static String Refund = BASE_URL + "refund.php";
    public static String Payment = BASE_URL + "item/payment";
    public static String ChangePassword = BASE_URL + "changepassword.php";
    public static String NotificationOn = BASE_URL + "user/notification";
    public static String TransactionHistory = BASE_URL + "stats.php";
    public static String PayPalAdd = BASE_URL + "addpaypal.php";
    public static String PayPalDelete = BASE_URL + "deletepaypal.php";
    public static String PayPalDeleteNew = BASE_URL + "DeletePaypalAndroid.php";
    public static String PayPalGet = BASE_URL + "paypal/get";
    public static String PayPalEdit = BASE_URL + "editpaypal.php";
    public static String GetAllBankAccount = BASE_URL + "getallpayment.php";
    public static String AddBankAccount = BASE_URL + "addpayment.php";
    public static String DeleteAccount = BASE_URL + "payments/ac/";
    public static String EditBackAccount = BASE_URL + "payment/ac/";
    public static String EditMultiImages = BASE_URL + "EditMultipleImages.php?item_id=";
    public static String MakePaymentDefault = BASE_URL + "make_payment_defualt.php";
    public static String GetPaymentDefault = BASE_URL + "get_payment_defualt.php?user_id=";
    public static String MakeBankAccountDefault = BASE_URL + "account/defulta";
    public static String PaymentHistroy = BASE_URL + "paymentHistory.php";
    public static String MessageHistory = BASE_URL + "lastchat/?";
    public static String GetMessage = BASE_URL + "chat/";
    public static String Chat = BASE_URL + "chat.php";
    public static String NotificationStatus = BASE_URL + "notification/status";

    public static String NotVerifiedUsers = BASE_URL + "filteritemNew.php";
    public static String BadgeCount = BASE_URL + "badgeCount.php";
    public static String SerchTag = BASE_URL + "item/search/tags?";

    public static String CheckItem = BASE_URL + "checkItemStatus.php";
    public static String HoldItem = BASE_URL + "itemHold.php";
    public static String OrderHistoryApi = BASE_URL + "purchasedetail.php";
}
