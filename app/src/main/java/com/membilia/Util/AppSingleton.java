package com.membilia.Util;

import com.membilia.volley_models.CategoriesModel;
import com.membilia.volley_models.FbFriendsModel;
import com.membilia.volley_models.GenreModel;
import com.membilia.volley_models.ImagesItemModel;
import com.membilia.volley_models.ItemsModel;
import com.membilia.volley_models.UsersModel;

import java.util.ArrayList;

/**
 * Created by Dharmani Apps on 6/27/2017.
 */

public class AppSingleton {
    private static AppSingleton myObj;
    ArrayList<CategoriesModel> categoriesArrayList = new ArrayList<CategoriesModel>();
    ArrayList<ItemsModel> itemsArrayList = new ArrayList<ItemsModel>();
    ArrayList<FbFriendsModel> fbFriendsArrayList = new ArrayList<FbFriendsModel>();

    ArrayList<String> catArrayList = new ArrayList<String>();
    ArrayList<GenreModel> genreArrayList = new ArrayList<GenreModel>();
    ArrayList<String> genreList = new ArrayList<>();
    ArrayList<UsersModel> userApiResponseArrayList = new ArrayList<UsersModel>();
    ArrayList<ImagesItemModel> mImagesVideoArrayList = new ArrayList<ImagesItemModel>();
    ArrayList<ItemsModel> purchasedItemModal = new ArrayList<>();

    public ArrayList<ItemsModel> getPurchasedItemModal() {return purchasedItemModal;}
    public ArrayList<String> getGenreList() {
        return genreList;
    }
    public ArrayList<GenreModel> getGenreArrayList() {
        return genreArrayList;
    }
    public ArrayList<String> getCatArrayList() {
        return catArrayList;
    }

    public ArrayList<ImagesItemModel> getmImagesVideoArrayList() {
        return mImagesVideoArrayList;
    }

    /**
     * Create private constructor
     */

    private AppSingleton() {

    }
    /**
     * Create a static method to get instance.
     */
    public static AppSingleton getInstance() {
        if (myObj == null) {
            myObj = new AppSingleton();
        }
        return myObj;
    }

    public ArrayList<UsersModel> getUserApiResponseArrayList() {
        return userApiResponseArrayList;
    }

    public ArrayList<FbFriendsModel> getFbFriendsArrayList() {
        return fbFriendsArrayList;
    }

    public ArrayList<ItemsModel> getItemsArrayList() {
        return itemsArrayList;
    }

    public ArrayList<CategoriesModel> getCategoriesList() {
        return categoriesArrayList;
    }

    public void setItemsArrayList(ArrayList<ItemsModel> itemsArrayList) {
        this.itemsArrayList = itemsArrayList;
    }

}
