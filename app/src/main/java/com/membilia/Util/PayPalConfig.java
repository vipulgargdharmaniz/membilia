package com.membilia.Util;

import com.paypal.android.sdk.payments.PayPalConfiguration;

/**
 * Created by dharmaniapps on 27/12/17.
 */

public class PayPalConfig {
    public static final String PAYPAL_CLIENT_ID = "YOUR PAYPAL CLIENT ID";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
}
