package com.membilia.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.membilia.dbModal.ChatForItemModal;
import com.membilia.dbModal.DbProfile;
import com.membilia.dbModal.LastChat;
import com.membilia.dbModal.LoginUser;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;
import com.membilia.volley_models.MessageHistoryModal;
import com.membilia.volley_models.OrderHistroy_Sql;

import java.util.ArrayList;

/*
 * Created by dharmaniapps on 26/12/17.
*/

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHandler";
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 5;
    // Database Name
    private static final String DATABASE_NAME = "MembiliaNew";
    // Contacts table name
    private static final String TABLE_CONTACTS = "orderdetails";
    private static final String TABLE_MSG = "TABLE_MSG";
    private static final String TABLE_CHAT = "TABLE_CHAT";
    private static final String TABLE_PROFILE = "TABLE_PROFILE";
    private static final String TABLE_ITEM = "TABLE_ITEM";

    private static  final String Table_User = "Table_User";

    private static  final String Table_Name = "Table_Name";
    Context context;

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String ITEMID = "Item_ID";
    private static final String OrderName = "Name";
    private static final String OrderStory = "OrderStory";
    private static final String OrderActualPrice = "OrderActualPrice";
    private static final String OrderTotalPrice = "OrderTotalPrice";
    private static final String OrderNo = "Order_no";
    private static final String OrderDate = "OrderDate";
    private static final String SenderName = "SenderName";
    private static final String Recipient = "Recipient";
    private static final String DeliveryMethod = "DeliveryMethod";
    private static final String Address = "Address";
    private static final String SellerID = "SellerID";
    private static final String ReciverID = "ReciverID";
    private static final String ShippingPrice = "ShippingPrice";
    private static final String Itemprice = "ItemPrice";
    private static final String ItemImage = "ItemImage";
    private static final String isVideo = "isVideo";
    private static final String MessageId = "MessageId";
    private static final String Direction = "Direction";
    private static final String ItemId = "Item_id";
    private static final String ItemName = "ItemName";
    private static final String SenderIdMSG ="SenderId";
    private static final String SenderNameMSG ="SenderNameMSG";
    private static final String ReciverIdMSG ="ReciverIdMSG";
    private static final String ReciverNameMSG ="ReciverNameMSG";
    private static final String TimeStamp ="TimeStamp";
    private static final String TextMsg = "TextMsg";
    private static final String SenderImage = "SendImage";
    private static final String RecieverImage = "RecieverImage";

    private static final String OrderStatus = "OrderStatus";

    private static final String UserId= "UserId";
    private static final String UserName= "UserName";
    private static final String UserDescp= "UserDescp";
    private static final String UserPic= "UserPic";
    private static final String UserCountry= "UserCountry";
    private static final String UserRanking= "UserRanking";
    private static final String UserFollowers= "UserFollowers";
    private static final String UserFollowing= "UserFollowing";
    private static final String UserVerified= "UserVerified";
    private static final String MainItemId = "MainItemId";
    private static final String isFollowed = "isFollowed";


    private static final String ITEM_Id= "ITEM_Id";
    private static final String ITEM_User_id= "user_id";
    private static final String Item_name= "item_name";
    private static final String ItemStory= "Item_story";
    private static final String User_name= "user_name";
    private static final String Verified= "verified";
    private static final String IsFavorite= "isFavorite";
    private static final String Price= "price";
    private static final String Charity= "charity";
    private static final String IsSold ="isSold";
    private static final String ItemImageNew = "ItemImageNew";
    private static final String Useroriginal="useroriginal";
    private static final String Videoimage ="original_videoimage";
    private static final String Rating ="rating";
    private static final String ListType ="ListType";

    private static final String isRecived = "isRecived";


    private static final String strIsFavorite = "IsFavorite";
    SQLiteDatabase sqldb;


    private static  final String UserTokenDB = "token";
    private static final String UserIdDB = "userid";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + OrderName + " TEXT,"
                + OrderStory + " TEXT," + OrderActualPrice + " TEXT,"
                + OrderNo + " TEXT," + OrderDate + " TEXT,"
                + SenderName + " TEXT," + Recipient + " TEXT," + DeliveryMethod + " TEXT,"
                + Address + " TEXT," + SellerID + " TEXT," + ReciverID + " TEXT,"
                + ITEMID + " TEXT," +ShippingPrice+ " TEXT,"+Itemprice+ " TEXT,"+ ItemImage+ " TEXT,"+
                isVideo+ " TEXT , "+isRecived+" TEXT , "+OrderStatus+" TEXT, "+strIsFavorite+" TEXT "+")";
        db.execSQL(CREATE_CONTACTS_TABLE);

        String CREATE_MSG_TABLE = "CREATE TABLE " + TABLE_MSG + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +  Direction + " TEXT,"
                + ItemId + " TEXT," + MessageId + " TEXT,"
                + ItemName + " TEXT,"
                + SenderIdMSG + " TEXT," + SenderNameMSG + " TEXT,"
                + ReciverIdMSG + " TEXT," + ReciverNameMSG + " TEXT," + TimeStamp + " TEXT,"+TextMsg+" TEXT,"
                +SenderImage+" TEXT,"+RecieverImage+" TEXT"
                +")";
        db.execSQL(CREATE_MSG_TABLE);

        String CREATE_CHAT_TABLE = "CREATE TABLE " + TABLE_CHAT + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +  MainItemId + " TEXT ,"+  Direction + " TEXT ,"
                + ItemId + " TEXT , " + MessageId + " TEXT ,"
                + ItemName + " TEXT , "
                + SenderIdMSG + " TEXT , " + SenderNameMSG + " TEXT , "
                + ReciverIdMSG + " TEXT , " + ReciverNameMSG + " TEXT , " + TimeStamp + " TEXT , "+TextMsg+" TEXT,"
                +SenderImage+" TEXT , "+RecieverImage+" TEXT "
                +")";
        db.execSQL(CREATE_CHAT_TABLE);

        String CREATE_Profile_TABLE = "CREATE TABLE " + TABLE_PROFILE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +  UserId + " TEXT ,"+  UserName + " TEXT ,"
                + UserDescp + " TEXT , " + UserPic + " TEXT ,"
                + UserCountry + " TEXT , " + UserRanking + " TEXT , "
                + UserFollowers + " TEXT , " + UserFollowing + " TEXT , "+ UserVerified  +" TEXT , "+isFollowed+" TEXT "
                +")";
        db.execSQL(CREATE_Profile_TABLE);


        String CREATE_Item_TABLE = "CREATE TABLE " + TABLE_ITEM + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +  ITEM_Id + " TEXT ,"+  ITEM_User_id + " TEXT ,"
                + Item_name + " TEXT , " + ItemStory + " TEXT ,"
                + User_name + " TEXT , " + Verified + " BOOLEAN , "
                + IsFavorite + " BOOLEAN , " + Price + " TEXT , " + Charity  +" BOOLEAN ,"
                + IsSold + " BOOLEAN , " + ItemImageNew + " TEXT , "
                + Useroriginal + " TEXT , " + Videoimage + " TEXT , "  +Rating+" TEXT , "+SellerID+" TEXT , "+ListType+" TEXT "+")";
        db.execSQL(CREATE_Item_TABLE);


        String CREATE_Table_User = "CREATE TABLE " + Table_User + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +  UserTokenDB + " TEXT ,"
                + UserIdDB + " TEXT  " +")";
        db.execSQL(CREATE_Table_User);

        String CreateUserName = "CREATE TABLE " + Table_Name + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +  UserName + " TEXT " +")";
        db.execSQL(CreateUserName);
        this.sqldb = db;
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MSG );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEM );
        db.execSQL("DROP TABLE IF EXISTS " + Table_User );
        db.execSQL("DROP TABLE IF EXISTS " + Table_Name );
        // Create tables again
        onCreate(db);
    }

    public void onDelete() {
        SQLiteDatabase db = this.getWritableDatabase(); //
        db.execSQL("delete from " + Table_User);
        db.execSQL("delete from " + TABLE_CONTACTS);
        db.execSQL("delete from " + TABLE_MSG);
        db.execSQL("delete from " + TABLE_CHAT);
        db.execSQL("delete from " + TABLE_PROFILE);
        db.execSQL("delete from " + TABLE_ITEM);
        db.execSQL("delete from " + Table_Name);
    }

    public void removeName() {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(Table_Name, null, null);
    }
    public void removeAllList(String verified) {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_ITEM, "ListType" + " = \'" + verified+"\'", null);
    }
    public void removeAll() {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_CONTACTS, null, null);
    }
    public void removeAllLastChat() {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_MSG, null, null);
    }
    public void removeAllChat(String itemId) {
            SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_CHAT, "Item_id" + " = \'" + itemId+"\'", null);
    }
    public void UpdateProfile(String itemId) {
        SQLiteDatabase db = this.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TABLE_PROFILE, "UserId" + " = \'" + itemId+"\'", null);
    }
    public boolean CheckIsDataAlreadyInDBorNot(String TableName,
                                                      String dbfield, String fieldValue) {
        String Query = "Select * from " + TableName + " WHERE " + dbfield + " = " + fieldValue;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = sqldb.rawQuery(Query,null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public void addName(String loginUsersList) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        Log.e(TAG, "getRating: "+orderHistory.get(orderHistory.size()-1).getRating() );
        values.put(UserName, loginUsersList); //  token; //  userId
        db.insert(Table_Name, null, values);
        db.close(); // Closing database connection
    }

    public void addUser(ArrayList<LoginUser> loginUsersList) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        Log.e(TAG, "getRating: "+orderHistory.get(orderHistory.size()-1).getRating() );
        values.put(UserTokenDB, loginUsersList.get(0).getUserToken()); //  token
        values.put(UserIdDB, loginUsersList.get(0).getUserId()); //  userId
        // Inserting Row
        db.insert(Table_User, null, values);
        db.close(); // Closing database connection
    }
    public void addListItem(ArrayList<ItemsModel> orderHistory,String listtype) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        Log.e(TAG, "getRating: "+orderHistory.get(orderHistory.size()-1).getRating() );
        values.put(ITEM_Id, orderHistory.get(orderHistory.size()-1).getItemId()); //  Name 1
        values.put(ITEM_User_id, orderHistory.get(orderHistory.size()-1).getUserId()); //  story2
        values.put(Item_name, orderHistory.get(orderHistory.size()-1).getItemName()); //  no3
        values.put(ItemStory, orderHistory.get(orderHistory.size()-1).getItemStory()); //  date4
        values.put(User_name, orderHistory.get(orderHistory.size()-1).getUserName()); //  sender name5
        values.put(Verified,orderHistory.get(orderHistory.size()-1).getVerifieduser());
        values.put(IsFavorite, orderHistory.get(orderHistory.size()-1).getIsFavorite()); // recipent name7
        values.put(Price, orderHistory.get(orderHistory.size()-1).getPrice());//8
        values.put(Charity, orderHistory.get(orderHistory.size()-1).getCategory()); // sellerid9
        values.put(IsSold, orderHistory.get(orderHistory.size()-1).getIsSold()); // recivier id10
        values.put(ItemImageNew, orderHistory.get(orderHistory.size()-1).getItemImages().getOriginal1()); // address 11
        values.put(Useroriginal, orderHistory.get(orderHistory.size()-1).getUserImages().getOriginal()); //12
        values.put(Videoimage,orderHistory.get(orderHistory.size()-1).getItemVideo().getOriginal_videoimage());// price cost13
        values.put(Rating,orderHistory.get(orderHistory.size()-1).getRating());//14
        values.put(SellerID,orderHistory.get(orderHistory.size()-1).getSellerId());
        values.put(ListType,listtype);//15
        // Inserting Row
        db.insert(TABLE_ITEM, null, values);
        db.close(); // Closing database connection
    }
    public ArrayList<ItemsModel> getListItem(String verified){
        ArrayList<ItemsModel> hashSet= new ArrayList();
        //String selectQuery = "SELECT  * FROM " + TABLE_CHAT+" WHERE ItemId ="+itemId+" ;";
        String selectQuery = "SELECT * FROM TABLE_ITEM where ListType = '"+verified+"\'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(cursor.getString(1));
                itemsModel.setUserId(cursor.getString(2));
                itemsModel.setItemName(cursor.getString(3));
                itemsModel.setItemStory(cursor.getString(4));
                itemsModel.setUserName(cursor.getString(5));
                itemsModel.setVerifieduser(Integer.valueOf(cursor.getString(6)));
                itemsModel.setIsFavorite(Boolean.valueOf(cursor.getString(7)));
                itemsModel.setPrice(Integer.valueOf(cursor.getString(8)));
                itemsModel.setCharity(Boolean.valueOf(cursor.getString(9)));
                itemsModel.setIsSold(Boolean.valueOf(cursor.getString(10)));
                ItemImagesModel itemImages = new ItemImagesModel();
                itemImages.setOriginal1(cursor.getString(11));
                itemsModel.setItemImages(itemImages);
                ItemUserImagesModel userImagesModel = new ItemUserImagesModel();
                userImagesModel.setOriginal(cursor.getString(12));
                itemsModel.setUserImages(userImagesModel);
                ItemVideoModel itemVideoModel = new ItemVideoModel();
                itemVideoModel.setOriginal_videoimage(cursor.getString(13));
                itemsModel.setItemVideo(itemVideoModel);
                itemsModel.setRating(cursor.getString(14));
                itemsModel.setSellerId(cursor.getString(15));
                hashSet.add(itemsModel);
            } while (cursor.moveToNext());
        }
        return hashSet;
    }

    public ArrayList<LoginUser> getUser(){
        ArrayList<LoginUser> hashSet= new ArrayList();
        String selectQuery = "SELECT * FROM Table_User  ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                LoginUser itemsModel = new LoginUser();
                itemsModel.setUserToken(cursor.getString(1));
                itemsModel.setUserId(cursor.getString(2));
                hashSet.add(itemsModel);
            } while (cursor.moveToNext());
        }
        return hashSet;
    }

    public String getName(){

        String selectQuery = "SELECT * FROM Table_Name  ";
        String strName = "";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                strName = cursor.getString(1);
            } while (cursor.moveToNext());
        }
        return strName;
    }

    public ArrayList<ItemsModel> getListItem(){
        ArrayList<ItemsModel> hashSet= new ArrayList();
        //String selectQuery = "SELECT  * FROM " + TABLE_CHAT+" WHERE ItemId ="+itemId+" ;";
        String selectQuery = "SELECT * FROM TABLE_ITEM  ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(cursor.getString(1));
                itemsModel.setUserId(cursor.getString(2));
                itemsModel.setItemName(cursor.getString(3));
                itemsModel.setItemStory(cursor.getString(4));
                itemsModel.setUserName(cursor.getString(5));
                itemsModel.setVerifieduser(Integer.valueOf(cursor.getString(6)));
                itemsModel.setIsFavorite(Boolean.valueOf(cursor.getString(7)));
                itemsModel.setPrice(Integer.valueOf(cursor.getString(8)));
                itemsModel.setCharity(Boolean.valueOf(cursor.getString(9)));
                itemsModel.setIsSold(Boolean.valueOf(cursor.getString(10)));
                ItemImagesModel itemImages = new ItemImagesModel();
                itemImages.setOriginal1(cursor.getString(11));
                itemsModel.setItemImages(itemImages);
                ItemUserImagesModel userImagesModel = new ItemUserImagesModel();
                userImagesModel.setOriginal(cursor.getString(12));
                itemsModel.setUserImages(userImagesModel);
                ItemVideoModel itemVideoModel = new ItemVideoModel();
                itemVideoModel.setOriginal_videoimage(cursor.getString(13));
                itemsModel.setItemVideo(itemVideoModel);
                itemsModel.setRating(cursor.getString(14));
                hashSet.add(itemsModel);
            } while (cursor.moveToNext());
        }
        return hashSet;
    }

    // Adding new contact
    public void addContact(ArrayList<String> orderHistory) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(OrderName, orderHistory.get(0)); //  Name
        values.put(OrderStory, orderHistory.get(1)); //  story
        values.put(OrderNo, orderHistory.get(2)); //  no
        values.put(OrderDate, orderHistory.get(3)); //  date
        values.put(SenderName, orderHistory.get(4)); //  sender name
        values.put(Recipient, orderHistory.get(5)); // recipent name
        values.put(SellerID, orderHistory.get(6)); // sellerid
        values.put(ReciverID, orderHistory.get(7)); // recivier id
        values.put(DeliveryMethod, orderHistory.get(8));
        values.put(Address, orderHistory.get(9)); // address
        values.put(OrderActualPrice, orderHistory.get(10)); // price cost
        values.put(ITEMID, orderHistory.get(11));
        values.put(ShippingPrice,orderHistory.get(12));
        values.put(Itemprice,orderHistory.get(13));
        values.put(ItemImage,orderHistory.get(14));
        values.put(isVideo,orderHistory.get(15));
        values.put(isRecived,orderHistory.get(1));
        values.put(OrderStatus,orderHistory.get(16));
        values.put(strIsFavorite,orderHistory.get(18));
        Log.e(TAG, "orderHistory.get(19): "+orderHistory.get(18));
        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }


    public void addLastChat(ArrayList<String> orderHistory) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Direction, orderHistory.get(0)); //  Name
        values.put(ItemId, orderHistory.get(1)); //  story
        values.put(MessageId, orderHistory.get(2)); //  no
        values.put(ItemName, orderHistory.get(3)); //  date
        values.put(SenderIdMSG, orderHistory.get(4)); // recipent name
        values.put(SenderNameMSG, orderHistory.get(5)); // sellerid
        values.put(ReciverIdMSG, orderHistory.get(6)); // recivier id
        values.put(ReciverNameMSG, orderHistory.get(7));
        values.put(TimeStamp, orderHistory.get(8)); // address
        values.put(TextMsg, orderHistory.get(9)); // recivier id
        values.put(SenderImage, orderHistory.get(10));
        values.put(RecieverImage, orderHistory.get(11)); // address
        // Inserting Row
        db.insert(TABLE_MSG, null, values);
        db.close(); // Closing database connection
    }

    public void addChat(ArrayList<ChatForItemModal> orderHistory) {
//        Log.e("test", "getData getMessage: "+orderHistory.get(orderHistory.size()-1).getChatModal().getMessage() );
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.e("db", "db: "+ orderHistory.get(orderHistory.size()-1).getChatModal().getStrDirection());
        values.put(Direction, orderHistory.get(orderHistory.size()-1).getChatModal().getStrDirection()); //  Name
        values.put(MainItemId, orderHistory.get(orderHistory.size()-1).getMainItemId());
        values.put(ItemId, orderHistory.get(orderHistory.size()-1).getChatModal().getItemId()); //  story
        values.put(MessageId, orderHistory.get(orderHistory.size()-1).getChatModal().getMessageId()); //  no
        values.put(ItemName, orderHistory.get(orderHistory.size()-1).getChatModal().getItemName()); //  date
        values.put(SenderIdMSG, orderHistory.get(orderHistory.size()-1).getChatModal().getSenderId()); // recipent name
        values.put(SenderNameMSG,orderHistory.get(orderHistory.size()-1).getChatModal().getSenderName()); // sellerid
        values.put(ReciverIdMSG, orderHistory.get(orderHistory.size()-1).getChatModal().getReciverId()); // recivier id
        values.put(ReciverNameMSG, orderHistory.get(orderHistory.size()-1).getChatModal().getRecivierName());
        values.put(TimeStamp, orderHistory.get(orderHistory.size()-1).getChatModal().getTimeStamp()); // address
        values.put(TextMsg, orderHistory.get(orderHistory.size()-1).getChatModal().getMessage()); // recivier id
        values.put(SenderImage, orderHistory.get(orderHistory.size()-1).getChatModal().getSenderImage());
        values.put(RecieverImage, orderHistory.get(orderHistory.size()-1).getChatModal().getRecivierImage()); // address
        // Inserting Row
        db.insert(TABLE_CHAT, null, values);
        db.close(); // Closing database connection
    }

    public void addProfile(ArrayList<DbProfile> orderHistory) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserId, orderHistory.get(orderHistory.size()-1).getUserId()); //  Name
        values.put(UserName, orderHistory.get(orderHistory.size()-1).getUsername());
        values.put(UserDescp, orderHistory.get(orderHistory.size()-1).getUserDecp()); //  story
        values.put(UserPic, orderHistory.get(orderHistory.size()-1).getUserPic()); //  no
        values.put(UserCountry, orderHistory.get(orderHistory.size()-1).getUserCountry()); //  date
        values.put(UserRanking, orderHistory.get(orderHistory.size()-1).getUserRanking()); // recipent name
        values.put(UserFollowers,orderHistory.get(orderHistory.size()-1).getUserFollower()); // sellerid
        values.put(UserFollowing, orderHistory.get(orderHistory.size()-1).getUserFollowing()); // recivier id
        values.put(UserVerified, orderHistory.get(orderHistory.size()-1).getUserVerified());
        values.put(isFollowed, orderHistory.get(orderHistory.size()-1).getUserFollowed());
        // Inserting Row
        db.insert(TABLE_PROFILE, null, values);
        db.close(); // Closing database connection
    }
    public ArrayList<DbProfile> getProfile(String UserId){
        ArrayList<DbProfile> hashSet= new ArrayList();
        //String selectQuery = "SELECT  * FROM " + TABLE_CHAT+" WHERE ItemId ="+itemId+" ;";
        String selectQuery = "SELECT * FROM TABLE_PROFILE WHERE UserId = \'"+UserId+"\'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DbProfile lastChat = new DbProfile();
                lastChat.setUserId(cursor.getString(1));
                lastChat.setUsername(cursor.getString(2));
                lastChat.setUserDecp(cursor.getString(3));
                lastChat.setUserPic(cursor.getString(4));
                lastChat.setUserCountry(cursor.getString(5));
                lastChat.setUserRanking(cursor.getString(6));
                lastChat.setUserFollower(cursor.getString(7));
                lastChat.setUserFollowing(cursor.getString(8));
                lastChat.setUserVerified(cursor.getString(9));
                lastChat.setUserFollowed(cursor.getString(10));
                hashSet.add(lastChat);
            } while (cursor.moveToNext());
        }
        return hashSet;
    }
    public ArrayList<ChatForItemModal> getAllChat(String itemId){
        ArrayList<ChatForItemModal> hashSet= new ArrayList();
        //String selectQuery = "SELECT  * FROM " + TABLE_CHAT+" WHERE ItemId ="+itemId+" ;";
        String selectQuery = "SELECT * FROM TABLE_CHAT WHERE Item_id = \'"+itemId+"\'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                ChatForItemModal lastChat = new ChatForItemModal();
                MessageHistoryModal messageHistoryModal = new MessageHistoryModal();
                messageHistoryModal.setStrDirection(cursor.getString(2));
                messageHistoryModal.setMessageId(cursor.getString(3));
                messageHistoryModal.setItemId(cursor.getString(4));
                messageHistoryModal.setItemName(cursor.getString(5));
                messageHistoryModal.setSenderId(cursor.getString(6));
                messageHistoryModal.setSenderName(cursor.getString(7));
                messageHistoryModal.setReciverId(cursor.getString(8));
                messageHistoryModal.setRecivierName(cursor.getString(9));
                messageHistoryModal.setTimeStamp(cursor.getString(10));
                messageHistoryModal.setMessage(cursor.getString(11));
                messageHistoryModal.setSenderImage(cursor.getString(12));
                messageHistoryModal.setRecivierImage(cursor.getString(13));

                lastChat.setChatModal(messageHistoryModal);
                lastChat.setMainItemId(messageHistoryModal.getItemId());
                hashSet.add(lastChat);
            } while (cursor.moveToNext());
        }
        return hashSet;
    }
    public ArrayList<ChatForItemModal> getAllChat(){
        ArrayList<ChatForItemModal> hashSet= new ArrayList();
        String selectQuery = "SELECT  * FROM " + TABLE_CHAT;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                ChatForItemModal lastChat = new ChatForItemModal();
                MessageHistoryModal messageHistoryModal = new MessageHistoryModal();
                messageHistoryModal.setStrDirection(cursor.getString(2));
                messageHistoryModal.setMessageId(cursor.getString(3));
                messageHistoryModal.setItemId(cursor.getString(4));
                messageHistoryModal.setItemName(cursor.getString(5));
                messageHistoryModal.setSenderId(cursor.getString(6));
                messageHistoryModal.setSenderName(cursor.getString(7));
                messageHistoryModal.setReciverId(cursor.getString(8));
                messageHistoryModal.setRecivierName(cursor.getString(9));
                messageHistoryModal.setTimeStamp(cursor.getString(10));
                messageHistoryModal.setMessage(cursor.getString(11));
                messageHistoryModal.setSenderImage(cursor.getString(12));
                messageHistoryModal.setRecivierImage(cursor.getString(13));

                lastChat.setChatModal(messageHistoryModal);
                lastChat.setMainItemId(messageHistoryModal.getItemId());
                hashSet.add(lastChat);
            } while (cursor.moveToNext());
        }
        return hashSet;
    }
    public ArrayList<LastChat> getAllMessagesnew(){
        ArrayList<LastChat> hashSet= new ArrayList();
        String selectQuery = "SELECT  * FROM " + TABLE_MSG;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                LastChat lastChat = new LastChat();
                lastChat.setKey(cursor.getString(0));
                lastChat.setDirectionid(cursor.getString(1));
                lastChat.setItemId(cursor.getString(2));
                lastChat.setMessageid(cursor.getString(3));
                lastChat.setItemName(cursor.getString(4));
                lastChat.setSenderID(cursor.getString(5));
                lastChat.setSenderName(cursor.getString(6));
                lastChat.setRecieverID(cursor.getString(7));
                lastChat.setRecieverName(cursor.getString(8));
                lastChat.setTimeStamp(cursor.getString(9));
                lastChat.setMessage(cursor.getString(10));
                lastChat.setSenderImage(cursor.getString(11));
                lastChat.setRecieverImage(cursor.getString(12));
                hashSet.add(lastChat);
                // Adding contact to list
            } while (cursor.moveToNext());
        }
        return hashSet;
    }

    public ArrayList<DbProfile> getAllProfile(){
        ArrayList<DbProfile> hashSet= new ArrayList();
        //String selectQuery = "SELECT  * FROM " + TABLE_CHAT+" WHERE ItemId ="+itemId+" ;";
        String selectQuery = "SELECT * FROM TABLE_PROFILE ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DbProfile lastChat = new DbProfile();
                lastChat.setUserId(cursor.getString(1));
                lastChat.setUsername(cursor.getString(2));
                lastChat.setUserDecp(cursor.getString(3));
                lastChat.setUserPic(cursor.getString(4));
                lastChat.setUserCountry(cursor.getString(5));
                lastChat.setUserRanking(cursor.getString(6));
                lastChat.setUserFollower(cursor.getString(7));
                lastChat.setUserFollowing(cursor.getString(8));
                lastChat.setUserVerified(cursor.getString(9));
                lastChat.setUserFollowed(cursor.getString(10));
try {

}catch (Exception e){
    e.printStackTrace();
}

                hashSet.add(lastChat);
            } while (cursor.moveToNext());
        }
        return hashSet;
    }

    public ArrayList<MessageHistoryModal> getLastMessage(int id) {
        Cursor cursor = null;
        ArrayList<MessageHistoryModal> listOrderHistroy = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            cursor =  db.rawQuery( "select * from TABLE_MSG where id='"+id+"'", null );
            if(cursor.moveToFirst())
            {
                MessageHistoryModal orderHistroySql = new MessageHistoryModal();
                orderHistroySql.setStrDirection(cursor.getString(1));
                orderHistroySql.setItemId(cursor.getString(2));
                orderHistroySql.setMessageId(cursor.getString(3));
                orderHistroySql.setItemName(cursor.getString(4));
                orderHistroySql.setSenderId(cursor.getString(5));
                orderHistroySql.setSenderName(cursor.getString(6));
                orderHistroySql.setReciverId(cursor.getString(7));
                orderHistroySql.setRecivierName(cursor.getString(8));
                orderHistroySql.setTimeStamp(cursor.getString(9));
                orderHistroySql.setMessage(cursor.getString(10));
                orderHistroySql.setSenderImage(cursor.getString(11));
                orderHistroySql.setRecivierImage(cursor.getString(12));
                // Adding contact to list
                listOrderHistroy.add(orderHistroySql);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return listOrderHistroy;
    }


    public Cursor getData(String id) {
        Cursor res = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
             res =  db.rawQuery( "select * from orderdetails where Item_ID='"+id+"'", null );
            if(res.moveToFirst())
            {
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
       
        return res;
    }
    public Cursor getData() {
        Cursor res = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            res =  db.rawQuery( "select * from orderdetails" , null );

            while (res.moveToNext())
            {
                Log.e("test", "getData: "+res.getString(0) );
                Log.e("test", "getData: "+res.getString(1) );
                Log.e("test", "getData: "+res.getString(2) );
                Log.e("test", "getData: "+res.getString(3) );
                Log.e("test", "getData: "+res.getString(4) );
                Log.e("test", "getData: "+res.getString(5) );
                Log.e("test", "getData: "+res.getString(6) );
                Log.e("test", "getData: "+res.getString(7) );
                Log.e("test", "getData: "+res.getString(8) );
                Log.e("test", "getData: "+res.getString(9) );
                Log.e("test", "getData: "+res.getString(10) );
                Log.e("test", "getData: "+res.getString(11) );
                Log.e("test", "getData: "+res.getString(12) );
                Log.e("test", "getData: "+res.getString(13) );

                Log.e("test", "..............:: " );
                Log.e("test", "..............:: " );
                Log.e("test", "..............:: " );
                // Displaying record if found
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return res;
    }

    // Getting All Contacts
    public ArrayList<OrderHistroy_Sql> getAllContacts() {
        ArrayList<OrderHistroy_Sql> listOrderHistroy = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                OrderHistroy_Sql orderHistroySql = new OrderHistroy_Sql();
                orderHistroySql.setItemName(cursor.getString(0));
                orderHistroySql.setItemStory(cursor.getString(1));
                orderHistroySql.setOrderNo(cursor.getString(2));
                orderHistroySql.setOrderDate(cursor.getString(3));
                orderHistroySql.setSenderName(cursor.getString(4));
                orderHistroySql.setRecipientName(cursor.getString(5));
                orderHistroySql.setSellerID(cursor.getString(6));
                orderHistroySql.setReciverID(cursor.getString(7));
                orderHistroySql.setDeliveryMethod(cursor.getString(8));
                orderHistroySql.setAddress(cursor.getString(9));
                orderHistroySql.setItemPrice(cursor.getString(10));
                orderHistroySql.setItemID(cursor.getString(11));
                orderHistroySql.setShippingPrize(cursor.getString(12));
                orderHistroySql.setIsFav(cursor.getString(19));
                 Log.e("Atal", ": "+cursor.getString(19) );
                // Adding contact to list
                listOrderHistroy.add(orderHistroySql);
            } while (cursor.moveToNext());
        }

        // return contact list
        return listOrderHistroy;
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        // return count
        return cursor.getCount();
    }


}