package com.membilia.Util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PlayerPawnPreference {
    public static final String PREF_NAME = "App_PREF_New";
    public static final int MODE = Context.MODE_PRIVATE;

    public static final String LoginType = "loginType";
    public static final String DeviceToken = "deviceToken";


    public static final String FACEBOOK_TOKEN_ACCESS = "is_facebook_token";
    public static final String IS_FIRST_TIME = "is_first_time";
    public static final String VALUE_EXPIRATION = "expiration";
    public static final String VALUE_SIGN_UP = "signup";
    public static final String VALUE_TOKEN = "token";
    public static final String VALUE_USER_ID = "user_id";
    public static final String VALUE_COUNT = "Count";
    public static final String CREATE_HOME_SCREEN = "create_home";
    public static final String USER_PROFILE = "user_profile";
    public static final String USERNAME = "username";
    public static final String USER_DESCP ="userdescp";
    public static final String USER_COUNTRY = "usercountry";
    public static String latitude = "latitude";
    public static String longitude = "longitude";

    public static final String IsVerified ="IsVerified";

    public static   String ADDRESS = "address";

    public static   String CITY = "city";
    public static   String STATE = "state";
    public static   String COUNTRY = "country";
    public static   String POSTALCODE = "postalCode";



    public static final String Final_img = "Final_img";
    public static final String Final_username = "Final_username";
    public static final String Final_desc ="Final_desc";
    public static final String Final_country = "Final_country";


    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(Context context, String key,
                                      boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }
    public static String readString(Activity context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).commit();
    }

    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }


    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public static Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
