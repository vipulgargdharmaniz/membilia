package com.membilia.Util;



import org.json.JSONObject;

/*
 * Created by AM00356255 on 12/5/2016.
 */
public class Constants {

    public static  String BranchIO = "";
    public static  String BranchUserID = "";
    public static  String BranchItemID = "";
    public static  String BranchType = "";
    public static  String BranchOther = "";

    public static String FriendId = "";
    public static String TimeStamp = "";

    public static  String currentUserId= "";

    /*  Check on opening of Additem fragment after/berfore splashAddItem */
    public static  boolean SplashAddItemOn = false;
    public static  boolean FirstTimeNonFan = true;

    public static String FragmentRefresh ="";
    public static String NewMessage ="";

    /* payment method ***/
    public static String isPayPall ="";
    public static String isBankAccount ="";
    public static String isCheck ="";

public static String refreshOrderDetails = "";

    /* for setting screen*/
    public static String UserName="";
    public static String UserDesc = "";
    public static String UserCountry = "";
    public static String UserImage = "";

    public static  String OpenAddItemFrom = "home";
    public static  String OpenEditAddItemFrom = "fragment_details";

    public static String additemFragment = "0";
    public static boolean isVerifiedUser = false;

    public static String FragmentName = "";

    public static String ExploreFragmentCurrent = "";
    public static String VerifiedSeller = "VerifiedSeller";
    public static String NonVerifiedSeller = "NonVerifiedSeller";

    public static JSONObject filterJson = new JSONObject() ;

    public static String CallImageAdpaterFrom = "";
    public static String SelectedCountry = "";
    public static String SelectedCountryCode = "";
    /*Fragments TAGs*/
    public static final String ACTIVITY_FRAGMENT = "activity_fragment";
    public static final String CATEGORIES_FRAGMENT = "categoreis_fragment";
    public static final String CATEGORIES_ITEMS_FRAGMENT = "categories_item_fragment";
    public static final String EXPLORE_FRAGMENT = "explore_fragment";
    public static final String ITEMS_FRAGMENT = "items_fragment";
    public static final String PROFILE_FRAGMENT = "person_fragment";
    public static final String USER_FRAGMENT = "user_fragment";
    public static final String ADD_ITEM_PLAYERPAWN_FRAGMENT = "add_item_playerpawn_fragment";
    public static final String Fragment_Setting= "fragment_settting";
    public static final String Fragment_filter= "fragment_filter";
    public static final String Fragment_Add_Bank_Account= "fragment_add_bank_account";
    public static final String Fragment_PaymentMethod ="fragment_payment_method";
    public static final String Fragment_ProfileFragment="fragment_profile";
    public static final String Fragment_showbankaccount ="fragment_show_bank_account";
    public static final String Fragment_editpaypalaccount ="fragment_edit_paypal_account";
    public static final String Fragment_editpaypalsubmit ="fragment_edit_paypal_submit";
    public static final String Fragment_Country_List ="fragment_country_list";
    public static final String Fragment_Show_Shipping_Address ="fragment_show_shipping_address";
    public static final String Fragment_Shipping_Address ="fragment_shipping_address";
    public static final String Fragment_Contactus ="fragment_contactus0";
    public static final String Fragment_Aboutus ="fragment_aboutus";
    public static final String Fragment_Transaction_History ="fragment_trtansaction_history";
    public static final String Fragment_Message_History ="fragment_message_history";
    public static final String Fragemnt_Privacy_Policy ="fragment_privacy_ppolicy";
    public static final String Fragment_Order_History ="fragment_order_history";
    public static final String Fragment_Change_Password ="fragment_change_password";
    public static final String Fragment_Details ="fragment_details";
    public static final String Fragments_Tags ="fragments_tags";
    public static final String Fragment_Image ="fragment_image";
    public static final String Fragment_Play_Video ="fragment_play_video";

    public static final String ItemDetails ="ItemDetails";
    public static final String Edit_AddItem = "Edit_AddItem";
    public static final String Buy_Fragmnet = "buy_fragment";
    public static final String GiveReview_Fragment = "GiveReview_Fragment";
    public static final String CustomerCare = "CustomerCare";
    public static final String SendMessage = "SendMessage";
    public static final String Fragment_Follower = "fragment_followers";

    /********@Variables*********/
    public static  String LOGIN_TYPE  = "login_type";
    public static  String IS_ITEM_USER  = "is_item_user";
    public static  String IS_USERS_FILTER  = "is_users_filter";
    public static  boolean isITEM_DETAILS  = false;
    public static  boolean IS_HOMEPOST_REFRESH  = false;

    public static  boolean IS_FIRSTIME = false;

    public static String All_USER = "SeeAll";
    public static String GET_CATEGORIES = "GET_CATEGORIES";
    public static String GET_GENRE_VALUE = "GET_GENRE_VALUE";




    public static String CountryJson =  "[\n" +
            " {\"name\":\"United States\",\"nameImage\":\"United States\",\"dial_code\":\"+1\",\"code\":\"US\",\"Language\":\"\"},\n" +
            " {\"name\":\"United Kingdom\",\"nameImage\":\"United Kingdom\",\"dial_code\":\"+44\",\"code\":\"GB\",\"Language\":\"\"},\n" +
            " {\"name\":\"Afghanistan (افغانستان\u200E)\",\"nameImage\":\"Afghanistan\",\"dial_code\":\"+93\",\"code\":\"AF\",\"Language\":\"\"},\n" +
            " {\"name\":\"Albania (Shqipëri)\",\"nameImage\":\"Albania\",\"dial_code\":\"+355\",\"code\":\"AL\",\"Language\":\"\"},\n" +
            " {\"name\":\"Algeria (الجزائر\u200E)\",\"nameImage\":\"Algeria\",\"dial_code\":\"+213\",\"code\":\"DZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"American Samoa\",\"nameImage\":\"American Samoa\",\"dial_code\":\"+1684\",\"code\":\"AS\",\"Language\":\"\"},\n" +
            " {\"name\":\"Andorra\",\"nameImage\":\"Andorra\",\"dial_code\":\"+376\",\"code\":\"AD\",\"Language\":\"\"},\n" +
            " {\"name\":\"Angola\",\"nameImage\":\"Angola\",\"dial_code\":\"+244\",\"code\":\"AO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Anguilla\",\"nameImage\":\"Anguilla\",\"dial_code\":\"+1264\",\"code\":\"AI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Antigua and Barbuda\",\"nameImage\":\"Antigua and Barbuda\",\"dial_code\":\"+1268\",\"code\":\"AG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Argentina\",\"nameImage\":\"Argentina\",\"dial_code\":\"+54\",\"code\":\"AR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Armenia (Հայաստան)\",\"nameImage\":\"Armenia\",\"dial_code\":\"+374\",\"code\":\"AM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Aruba\",\"nameImage\":\"Aruba\",\"dial_code\":\"+297\",\"code\":\"AW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Australia\",\"nameImage\":\"Australia\",\"dial_code\":\"+61\",\"code\":\"AU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Austria (Österreich)\",\"nameImage\":\"Austria\",\"dial_code\":\"+43\",\"code\":\"AT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Azerbaijan (Azərbaycan)\",\"nameImage\":\"Azerbaijan\",\"dial_code\":\"+994\",\"code\":\"AZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bahamas\",\"nameImage\":\"Bahamas\",\"dial_code\":\"+1242\",\"code\":\"BS\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bahrain (البحرين\u200E)\",\"nameImage\":\"Bahrain\",\"dial_code\":\"+973\",\"code\":\"BH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bangladesh (বাংলাদেশ)\",\"nameImage\":\"Bangladesh\",\"dial_code\":\"+880\",\"code\":\"BD\",\"Language\":\"\"},\n" +
            " {\"name\":\"Barbados\",\"nameImage\":\"Barbados\",\"dial_code\":\"+1246\",\"code\":\"BB\",\"Language\":\"\"},\n" +
            " {\"name\":\"Belarus (Беларусь)\",\"nameImage\":\"Belarus\",\"dial_code\":\"+375\",\"code\":\"BY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Belgium (België)\",\"nameImage\":\"Belgium\",\"dial_code\":\"+32\",\"code\":\"BE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Belize\",\"nameImage\":\"Belize\",\"dial_code\":\"+501\",\"code\":\"BZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Benin (Bénin)\",\"nameImage\":\"Benin\",\"dial_code\":\"+229\",\"code\":\"BJ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bermuda\",\"nameImage\":\"Bermuda\",\"dial_code\":\"+1441\",\"code\":\"BM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bhutan (འབྲུག)\",\"nameImage\":\"Bhutan\",\"dial_code\":\"+975\",\"code\":\"BT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bolivia\",\"nameImage\":\"Bolivia\",\"dial_code\":\"+591\",\"code\":\"BO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bosnia and Herzegovina (Босна и Херцеговина)\",\"nameImage\":\"Bosnia and Herzegovina\",\"dial_code\":\"+387\",\"code\":\"BA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Botswana\",\"nameImage\":\"Botswana\",\"dial_code\":\"+267\",\"code\":\"BW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Brazil (Brasil)\",\"nameImage\":\"Brazil\",\"dial_code\":\"+55\",\"code\":\"BR\",\"Language\":\"\"},\n" +
            " {\"name\":\"British Indian Ocean Territory\",\"nameImage\":\"British Indian Ocean Territory\",\"dial_code\":\"+246\",\"code\":\"IO\",\"Language\":\"\"},\n" +
            " {\"name\":\"British Virgin Islands\",\"nameImage\":\"British Virgin Islands\",\"dial_code\":\"+1284\",\"code\":\"VG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Brunei\",\"nameImage\":\"Brunei\",\"dial_code\":\"+673\",\"code\":\"BN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Bulgaria (България)\",\"nameImage\":\"Bulgaria\",\"dial_code\":\"+359\",\"code\":\"BG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Burkina Faso\",\"nameImage\":\"Burkina Faso\",\"dial_code\":\"+226\",\"code\":\"BF\",\"Language\":\"\"},\n" +
            " {\"name\":\"Burundi (Uburundi)\",\"nameImage\":\"Burundi\",\"dial_code\":\"+257\",\"code\":\"BI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Cambodia (កម្ពុជា)\",\"nameImage\":\"Cambodia\",\"dial_code\":\"+855\",\"code\":\"KH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Cameroon (Cameroun)\",\"nameImage\":\"Cameroon\",\"dial_code\":\"+237\",\"code\":\"CM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Canada\",\"nameImage\":\"Canada\",\"dial_code\":\"+1\",\"code\":\"CA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Cape Verde (Kabu Verdi)\",\"nameImage\":\"Cape Verde\",\"dial_code\":\"+238\",\"code\":\"CV\",\"Language\":\"\"},\n" +
            " {\"name\":\"Caribbean Netherlands\",\"nameImage\":\"Caribbean Netherlands\",\"dial_code\":\"+599\",\"code\":\"BQ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Cayman Islands\",\"nameImage\":\"Cayman Islands\",\"dial_code\":\"+1345\",\"code\":\"KY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Central African Republic (République centrafricaine)\",\"nameImage\":\"Central African Republic\",\"dial_code\":\"+236\",\"code\":\"CF\",\"Language\":\"\"},\n" +
            " {\"name\":\"Chad (Tchad)\",\"nameImage\":\"Chad\",\"dial_code\":\"+235\",\"code\":\"TD\",\"Language\":\"\"},\n" +
            " {\"name\":\"Chile\",\"nameImage\":\"Chile\",\"dial_code\":\"+56\",\"code\":\"CL\",\"Language\":\"\"},\n" +
            " {\"name\":\"China (中国)\",\"nameImage\":\"China\",\"dial_code\":\"+86\",\"code\":\"CN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Colombia\",\"nameImage\":\"Colombia\",\"dial_code\":\"+57\",\"code\":\"CO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Comoros (جزر القمر\u200E)\",\"nameImage\":\"Comoros\",\"dial_code\":\"+269\",\"code\":\"KM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)\",\"nameImage\":\"Congo (DRC)\",\"dial_code\":\"+243\",\"code\":\"CD\",\"Language\":\"\"},\n" +
            " {\"name\":\"Congo (Republic) (Congo-Brazzaville)\",\"nameImage\":\"Congo (Republic)\",\"dial_code\":\"+242\",\"code\":\"CG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Cook Islands\",\"nameImage\":\"Cook Islands\",\"dial_code\":\"+682\",\"code\":\"CK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Costa Rica\",\"nameImage\":\"Costa Rica\",\"dial_code\":\"+506\",\"code\":\"CR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Côte d’Ivoire\",\"nameImage\":\"Côte d’Ivoire\",\"dial_code\":\"+225\",\"code\":\"CI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Croatia (Hrvatska)\",\"nameImage\":\"Croatia\",\"dial_code\":\"+385\",\"code\":\"HR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Cuba\",\"nameImage\":\"Cuba\",\"dial_code\":\"+53\",\"code\":\"CU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Curaçao\",\"nameImage\":\"Curaçao\",\"dial_code\":\"+599\",\"code\":\"CW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Cyprus (Κύπρος)\",\"nameImage\":\"Cyprus\",\"dial_code\":\"+357\",\"code\":\"CY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Czech Republic (Česká republika)\",\"nameImage\":\"Czech Republic\",\"dial_code\":\"+420\",\"code\":\"CZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Denmark (Danmark)\",\"nameImage\":\"Denmark\",\"dial_code\":\"+45\",\"code\":\"DK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Djibouti\",\"nameImage\":\"Djibouti\",\"dial_code\":\"+253\",\"code\":\"DJ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Dominica\",\"nameImage\":\"Dominica\",\"dial_code\":\"+1767\",\"code\":\"DM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Dominican Republic (República Dominicana)\",\"nameImage\":\"Dominican Republic\",\"dial_code\":\"+1\",\"code\":\"DO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Ecuador\",\"nameImage\":\"Ecuador\",\"dial_code\":\"+593\",\"code\":\"EC\",\"Language\":\"\"},\n" +
            " {\"name\":\"Egypt (مصر\u200E)\",\"nameImage\":\"Egypt\",\"dial_code\":\"+20\",\"code\":\"EG\",\"Language\":\"\"},\n" +
            " {\"name\":\"El Salvador\",\"nameImage\":\"El Salvador\",\"dial_code\":\"+503\",\"code\":\"SV\",\"Language\":\"\"},\n" +
            " {\"name\":\"Equatorial Guinea (Guinea Ecuatorial)\",\"nameImage\":\"Equatorial Guinea\",\"dial_code\":\"+240\",\"code\":\"GQ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Eritrea\",\"nameImage\":\"Eritrea\",\"dial_code\":\"+291\",\"code\":\"ER\",\"Language\":\"\"},\n" +
            " {\"name\":\"Estonia (Eesti)\",\"nameImage\":\"Estonia\",\"dial_code\":\"+372\",\"code\":\"EE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Ethiopia\",\"nameImage\":\"Ethiopia\",\"dial_code\":\"+251\",\"code\":\"ET\",\"Language\":\"\"},\n" +
            " {\"name\":\"Falkland Islands (Islas Malvinas)\",\"nameImage\":\"Falkland Islands\",\"dial_code\":\"+500\",\"code\":\"FK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Faroe Islands (Føroyar)\",\"nameImage\":\"Faroe Islands\",\"dial_code\":\"+298\",\"code\":\"FO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Fiji\",\"nameImage\":\"Fiji\",\"dial_code\":\"+679\",\"code\":\"FJ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Finland (Suomi)\",\"nameImage\":\"Finland\",\"dial_code\":\"+358\",\"code\":\"FI\",\"Language\":\"\"},\n" +
            " {\"name\":\"France\",\"nameImage\":\"France\",\"dial_code\":\"+33\",\"code\":\"FR\",\"Language\":\"\"},\n" +
            " {\"name\":\"French Guiana (Guyane française)\",\"nameImage\":\"French Guiana\",\"dial_code\":\"+594\",\"code\":\"GF\",\"Language\":\"\"},\n" +
            " {\"name\":\"French Polynesia (Polynésie française)\",\"nameImage\":\"French Polynesia\",\"dial_code\":\"+689\",\"code\":\"PF\",\"Language\":\"\"},\n" +
            " {\"name\":\"Gabon\",\"nameImage\":\"Gabon\",\"dial_code\":\"+241\",\"code\":\"GA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Gambia\",\"nameImage\":\"Gambia\",\"dial_code\":\"+220\",\"code\":\"GM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Georgia (საქართველო)\",\"nameImage\":\"Georgia\",\"dial_code\":\"+995\",\"code\":\"GE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Germany (Deutschland)\",\"nameImage\":\"Germany\",\"dial_code\":\"+49\",\"code\":\"DE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Ghana (Gaana)\",\"nameImage\":\"Ghana\",\"dial_code\":\"+233\",\"code\":\"GH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Gibraltar\",\"nameImage\":\"Gibraltar\",\"dial_code\":\"+350\",\"code\":\"GI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Greece (Ελλάδα)\",\"nameImage\":\"Greece\",\"dial_code\":\"+30\",\"code\":\"GR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Greenland (Kalaallit Nunaat)\",\"nameImage\":\"Greenland\",\"dial_code\":\"+299\",\"code\":\"GL\",\"Language\":\"\"},\n" +
            " {\"name\":\"Grenada\",\"nameImage\":\"Grenada\",\"dial_code\":\"+1473\",\"code\":\"GD\",\"Language\":\"\"},\n" +
            " {\"name\":\"Guadeloupe\",\"nameImage\":\"Guadeloupe\",\"dial_code\":\"+590\",\"code\":\"GP\",\"Language\":\"\"},\n" +
            " {\"name\":\"Guam\",\"nameImage\":\"Guam\",\"dial_code\":\"+1671\",\"code\":\"GU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Guatemala\",\"nameImage\":\"Guatemala\",\"dial_code\":\"+502\",\"code\":\"GT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Guinea (Guinée)\",\"nameImage\":\"Guinea\",\"dial_code\":\"+224\",\"code\":\"GN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Guinea-Bissau (Guiné Bissau)\",\"nameImage\":\"Guinea-Bissau\",\"dial_code\":\"+245\",\"code\":\"GW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Guyana\",\"nameImage\":\"Guyana\",\"dial_code\":\"+592\",\"code\":\"GY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Haiti\",\"nameImage\":\"Haiti\",\"dial_code\":\"+509\",\"code\":\"HT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Honduras\",\"nameImage\":\"Honduras\",\"dial_code\":\"+504\",\"code\":\"HN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Hong Kong (香港)\",\"nameImage\":\"Hong Kong\",\"dial_code\":\"+852\",\"code\":\"HK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Hungary (Magyarország)\",\"nameImage\":\"Hungary\",\"dial_code\":\"+36\",\"code\":\"HU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Iceland (Ísland)\",\"nameImage\":\"Iceland\",\"dial_code\":\"+354\",\"code\":\"IS\",\"Language\":\"\"},\n" +
            " {\"name\":\"India (भारत)\",\"nameImage\":\"India\",\"dial_code\":\"+91\",\"code\":\"IN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Indonesia\",\"nameImage\":\"Indonesia\",\"dial_code\":\"+62\",\"code\":\"ID\",\"Language\":\"\"},\n" +
            " {\"name\":\"Iran (ایران\u200E)\",\"nameImage\":\"Iran\",\"dial_code\":\"+98\",\"code\":\"IR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Iraq (العراق\u200E)\",\"nameImage\":\"Iraq\",\"dial_code\":\"+964\",\"code\":\"IQ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Ireland\",\"nameImage\":\"Ireland\",\"dial_code\":\"+353\",\"code\":\"IE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Israel (ישראל\u200E)\",\"nameImage\":\"Israel\",\"dial_code\":\"+972\",\"code\":\"IL\",\"Language\":\"\"},\n" +
            " {\"name\":\"Italy (Italia)\",\"nameImage\":\"Italy\",\"dial_code\":\"+39\",\"code\":\"IT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Jamaica\",\"nameImage\":\"Jamaica\",\"dial_code\":\"+1876\",\"code\":\"JM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Japan (日本)\",\"nameImage\":\"Japan\",\"dial_code\":\"+81\",\"code\":\"JP\",\"Language\":\"\"},\n" +
            " {\"name\":\"Jordan (الأردن\u200E)\",\"nameImage\":\"Jordan\",\"dial_code\":\"+962\",\"code\":\"JO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Kazakhstan (Казахстан)\",\"nameImage\":\"Kazakhstan\",\"dial_code\":\"+7\",\"code\":\"KZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Kenya\",\"nameImage\":\"Kenya\",\"dial_code\":\"+254\",\"code\":\"KE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Kiribati\",\"nameImage\":\"Kiribati\",\"dial_code\":\"686\",\"code\":\"KI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Kuwait (الكويت\u200E)\",\"nameImage\":\"Kuwait\",\"dial_code\":\"+965\",\"code\":\"KW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Kyrgyzstan (Кыргызстан)\",\"nameImage\":\"Kyrgyzstan\",\"dial_code\":\"+996\",\"code\":\"KG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Laos (ລາວ)\",\"nameImage\":\"Laos\",\"dial_code\":\"+856\",\"code\":\"LA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Latvia (Latvija)\",\"nameImage\":\"Latvia\",\"dial_code\":\"+371\",\"code\":\"LV\",\"Language\":\"\"},\n" +
            " {\"name\":\"Lebanon (لبنان\u200E)\",\"nameImage\":\"Lebanon\",\"dial_code\":\"+961\",\"code\":\"LB\",\"Language\":\"\"},\n" +
            " {\"name\":\"Lesotho\",\"nameImage\":\"Lesotho\",\"dial_code\":\"+266\",\"code\":\"LS\",\"Language\":\"\"},\n" +
            " {\"name\":\"Liberia\",\"nameImage\":\"Liberia\",\"dial_code\":\"+231\",\"code\":\"LR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Libya (ليبيا\u200E)\",\"nameImage\":\"Libya\",\"dial_code\":\"+218\",\"code\":\"Ly\",\"Language\":\"\"},\n" +
            " {\"name\":\"Liechtenstein\",\"nameImage\":\"Liechtenstein\",\"dial_code\":\"+423\",\"code\":\"LI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Lithuania (Lietuva)\",\"nameImage\":\"Lithuania\",\"dial_code\":\"+370\",\"code\":\"LT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Luxembourg\",\"nameImage\":\"Luxembourg\",\"dial_code\":\"+352\",\"code\":\"LU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Macau (澳門)\",\"nameImage\":\"Macau\",\"dial_code\":\"+853\",\"code\":\"MO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Macedonia (FYROM) (Македонија)\",\"nameImage\":\"Macedonia\",\"dial_code\":\"+389\",\"code\":\"MK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Madagascar (Madagasikara)\",\"nameImage\":\"Madagascar\",\"dial_code\":\"+261\",\"code\":\"MG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Malawi\",\"nameImage\":\"Malawi\",\"dial_code\":\"+265\",\"code\":\"MW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Malaysia\",\"nameImage\":\"Malaysia\",\"dial_code\":\"+60\",\"code\":\"MY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Maldives\",\"nameImage\":\"Maldives\",\"dial_code\":\"+96\",\"code\":\"MV\",\"Language\":\"\"},\n" +
            " {\"name\":\"Mali\",\"nameImage\":\"Mali\",\"dial_code\":\"+223\",\"code\":\"ML\",\"Language\":\"\"},\n" +
            " {\"name\":\"Malta\",\"nameImage\":\"Malta\",\"dial_code\":\"+356\",\"code\":\"MT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Marshall Islands\",\"nameImage\":\"Marshall Islands\",\"dial_code\":\"+692\",\"code\":\"MH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Martinique\",\"nameImage\":\"Martinique\",\"dial_code\":\"+596\",\"code\":\"MQ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Mauritania (موريتانيا\u200E)\",\"nameImage\":\"Mauritania\",\"dial_code\":\"+222\",\"code\":\"MR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Mauritius (Moris)\",\"nameImage\":\"Mauritius\",\"dial_code\":\"+230\",\"code\":\"MU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Mexico (México)\",\"nameImage\":\"Mexico\",\"dial_code\":\"+52\",\"code\":\"MX\",\"Language\":\"\"},\n" +
            " {\"name\":\"Micronesia\",\"nameImage\":\"Micronesia\",\"dial_code\":\"+691\",\"code\":\"MF\",\"Language\":\"\"},\n" +
            " {\"name\":\"Moldova (Republica Moldova)\",\"nameImage\":\"Moldova\",\"dial_code\":\"+373\",\"code\":\"MD\",\"Language\":\"\"},\n" +
            " {\"name\":\"Monaco\",\"nameImage\":\"Monaco\",\"dial_code\":\"+377\",\"code\":\"MC\",\"Language\":\"\"},\n" +
            " {\"name\":\"Mongolia (Монгол)\",\"nameImage\":\"Mongolia\",\"dial_code\":\"+976\",\"code\":\"MN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Montenegro (Crna Gora)\",\"nameImage\":\"Montenegro\",\"dial_code\":\"+382\",\"code\":\"ME\",\"Language\":\"\"},\n" +
            " {\"name\":\"Montserrat\",\"nameImage\":\"Montserrat\",\"dial_code\":\"+1664\",\"code\":\"MS\",\"Language\":\"\"},\n" +
            " {\"name\":\"Morocco (المغرب\u200E)\",\"nameImage\":\"Morocco\",\"dial_code\":\"+212\",\"code\":\"MA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Mozambique (Moçambique)\",\"nameImage\":\"Mozambique\",\"dial_code\":\"+258\",\"code\":\"MZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Myanmar (Burma) (မြန်မာ)\",\"nameImage\":\"Myanmar\",\"dial_code\":\"+95\",\"code\":\"MM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Namibia (Namibië)\",\"nameImage\":\"Namibia\",\"dial_code\":\"+264\",\"code\":\"NA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Nauru\",\"nameImage\":\"Nauru\",\"dial_code\":\"+674\",\"code\":\"NR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Nepal (नेपाल)\",\"nameImage\":\"Nepal\",\"dial_code\":\"+977\",\"code\":\"NP\",\"Language\":\"\"},\n" +
            " {\"name\":\"Netherlands (Nederland)\",\"nameImage\":\"Netherlands\",\"dial_code\":\"+31\",\"code\":\"NL\",\"Language\":\"\"},\n" +
            " {\"name\":\"New Caledonia (Nouvelle-Calédonie)\",\"nameImage\":\"New Caledonia\",\"dial_code\":\"+687\",\"code\":\"NC\",\"Language\":\"\"},\n" +
            " {\"name\":\"New Zealand\",\"nameImage\":\"New Zealand\",\"dial_code\":\"+64\",\"code\":\"NZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Nicaragua\",\"nameImage\":\"Nicaragua\",\"dial_code\":\"+505\",\"code\":\"NI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Niger (Nijar)\",\"nameImage\":\"Niger\",\"dial_code\":\"+227\",\"code\":\"NE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Nigeria\",\"nameImage\":\"Nigeria\",\"dial_code\":\"+234\",\"code\":\"NG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Niue\",\"nameImage\":\"Niue\",\"dial_code\":\"+683\",\"code\":\"NU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Norfolk Island\",\"nameImage\":\"Norfolk Island\",\"dial_code\":\"+672\",\"code\":\"NF\",\"Language\":\"\"},\n" +
            " {\"name\":\"North Korea (조선 민주주의 인민 공화국)\",\"nameImage\":\"North Korea\",\"dial_code\":\"+850\",\"code\":\"KP\",\"Language\":\"\"},\n" +
            " {\"name\":\"Northern Mariana Islands\",\"nameImage\":\"Northern Mariana Islands\",\"dial_code\":\"+1670\",\"code\":\"MP\",\"Language\":\"\"},\n" +
            " {\"name\":\"Norway (Norge)\",\"nameImage\":\"Norway\",\"dial_code\":\"+47\",\"code\":\"NO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Oman (عُمان\u200E)\",\"nameImage\":\"Oman\",\"dial_code\":\"+968\",\"code\":\"OM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Pakistan (پاکستان\u200E)\",\"nameImage\":\"Pakistan\",\"dial_code\":\"+92\",\"code\":\"PK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Palau\",\"nameImage\":\"Palau\",\"dial_code\":\"+680\",\"code\":\"PW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Palestine\",\"nameImage\":\"Palestine\",\"dial_code\":\"+970\",\"code\":\"PS\",\"Language\":\"\"},\n" +
            " {\"name\":\"Panama (Panamá)\",\"nameImage\":\"Panama\",\"dial_code\":\"+507\",\"code\":\"PA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Papua New Guinea\",\"nameImage\":\"Papua New Guinea\",\"dial_code\":\"+675\",\"code\":\"PG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Paraguay\",\"nameImage\":\"Paraguay\",\"dial_code\":\"+595\",\"code\":\"PY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Peru (Perú)\",\"nameImage\":\"Peru\",\"dial_code\":\"+51\",\"code\":\"PE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Philippines\",\"nameImage\":\"Philippines\",\"dial_code\":\"+63\",\"code\":\"PH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Poland (Polska)\",\"nameImage\":\"Poland\",\"dial_code\":\"+48\",\"code\":\"PL\",\"Language\":\"\"},\n" +
            " {\"name\":\"Portugal\",\"nameImage\":\"Portugal\",\"dial_code\":\"+351\",\"code\":\"PT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Puerto Rico\",\"nameImage\":\"Puerto Rico\",\"dial_code\":\"+1\",\"code\":\"PR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Qatar (قطر\u200E)\",\"nameImage\":\"Qatar\",\"dial_code\":\"+974\",\"code\":\"QA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Réunion (La Réunion)\",\"nameImage\":\"Réunion\",\"dial_code\":\"+262\",\"code\":\"RE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Romania (România)\",\"nameImage\":\"Romania\",\"dial_code\":\"+40\",\"code\":\"RO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Russia (Россия)\",\"nameImage\":\"Russia\",\"dial_code\":\"+7\",\"code\":\"RU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Rwanda\",\"nameImage\":\"Rwanda\",\"dial_code\":\"+250\",\"code\":\"RW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saint Barthélemy (Saint-Barthélemy)\",\"nameImage\":\"Saint Barthélemy\",\"dial_code\":\"+590\",\"code\":\"BL\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saint Helena\",\"nameImage\":\"Saint Helena\",\"dial_code\":\"+290\",\"code\":\"SH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saint Kitts and Nevis\",\"nameImage\":\"Saint Kitts and Nevis\",\"dial_code\":\"+1869\",\"code\":\"KN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saint Lucia\",\"nameImage\":\"Saint Lucia\",\"dial_code\":\"+1758\",\"code\":\"LC\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saint Martin (Saint-Martin (partie française))\",\"nameImage\":\"Saint Martin\",\"dial_code\":\"+590\",\"code\":\"MF\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)\",\"nameImage\":\"Saint Pierre and Miquelon\",\"dial_code\":\"+508\",\"code\":\"PM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saint Vincent and the Grenadines\",\"nameImage\":\"Saint Vincent and the Grenadines\",\"dial_code\":\"+1784\",\"code\":\"VC\",\"Language\":\"\"},\n" +
            " {\"name\":\"Samoa\",\"nameImage\":\"Samoa\",\"dial_code\":\"+685\",\"code\":\"WS\",\"Language\":\"\"},\n" +
            " {\"name\":\"San Marino\",\"nameImage\":\"San Marino\",\"dial_code\":\"+378\",\"code\":\"SM\",\"Language\":\"\"},\n" +
            " {\"name\":\"São Tomé and Príncipe (São Tomé e Príncipe)\",\"nameImage\":\"São Tomé and Príncipe\",\"dial_code\":\"+239\",\"code\":\"ST\",\"Language\":\"\"},\n" +
            " {\"name\":\"Saudi Arabia (المملكة العربية السعودية\u200E)\",\"nameImage\":\"Saudi Arabia\",\"dial_code\":\"+966\",\"code\":\"SA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Senegal (Sénégal)\",\"nameImage\":\"Senegal\",\"dial_code\":\"+221\",\"code\":\"SN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Serbia (Србија)\",\"nameImage\":\"Serbia\",\"dial_code\":\"+381\",\"code\":\"RS\",\"Language\":\"\"},\n" +
            " {\"name\":\"Seychelles\",\"nameImage\":\"Seychelles\",\"dial_code\":\"+248\",\"code\":\"SC\",\"Language\":\"\"},\n" +
            " {\"name\":\"Sierra Leone\",\"nameImage\":\"Sierra Leone\",\"dial_code\":\"+232\",\"code\":\"SL\",\"Language\":\"\"},\n" +
            " {\"name\":\"Singapore\",\"nameImage\":\"Singapore\",\"dial_code\":\"+65\",\"code\":\"SG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Sint Maarten\",\"nameImage\":\"Sint Maarten\",\"dial_code\":\"+1721\",\"code\":\"SX\",\"Language\":\"\"},\n" +
            " {\"name\":\"Slovakia (Slovensko)\",\"nameImage\":\"Slovakia\",\"dial_code\":\"+421\",\"code\":\"SK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Slovenia (Slovenija)\",\"nameImage\":\"Slovenia\",\"dial_code\":\"+386\",\"code\":\"SI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Solomon Islands\",\"nameImage\":\"Solomon Islands\",\"dial_code\":\"+252\",\"code\":\"SB\",\"Language\":\"\"},\n" +
            " {\"name\":\"Somalia (Soomaaliya)\",\"nameImage\":\"Somalia\",\"dial_code\":\"+27\",\"code\":\"SO\",\"Language\":\"\"},\n" +
            " {\"name\":\"South Africa\",\"nameImage\":\"South Africa\",\"dial_code\":\"+82\",\"code\":\"ZA\",\"Language\":\"\"},\n" +
            " {\"name\":\"South Korea (대한민국)\",\"nameImage\":\"South Korea\",\"dial_code\":\"+211\",\"code\":\"KR\",\"Language\":\"\"},\n" +
            " {\"name\":\"South Sudan (جنوب السودان\u200E)\",\"nameImage\":\"South Sudan\",\"dial_code\":\"+34\",\"code\":\"SS\",\"Language\":\"\"},\n" +
            " {\"name\":\"Spain (España)\",\"nameImage\":\"Spain\",\"dial_code\":\"+94\",\"code\":\"ES\",\"Language\":\"\"},\n" +
            " {\"name\":\"Sri Lanka (ශ්\u200Dරී ලංකාව)\",\"nameImage\":\"Sri Lanka\",\"dial_code\":\"+249\",\"code\":\"LK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Sudan (السودان\u200E)\",\"nameImage\":\"Sudan\",\"dial_code\":\"\",\"code\":\"SD\",\"Language\":\"\"},\n" +
            " {\"name\":\"Suriname\",\"nameImage\":\"Suriname\",\"dial_code\":\"+597\",\"code\":\"SR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Swaziland\",\"nameImage\":\"Swaziland\",\"dial_code\":\"+268\",\"code\":\"SZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Sweden (Sverige)\",\"nameImage\":\"Sweden\",\"dial_code\":\"+46\",\"code\":\"SE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Switzerland (Schweiz)\",\"nameImage\":\"Switzerland\",\"dial_code\":\"+41\",\"code\":\"CH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Syria (سوريا\u200E)\",\"nameImage\":\"Syria\",\"dial_code\":\"+963\",\"code\":\"SY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Taiwan (台灣)\",\"nameImage\":\"Taiwan\",\"dial_code\":\"+886\",\"code\":\"TW\",\"Language\":\"\"},\n" +
            " {\"name\":\"Tajikistan\",\"nameImage\":\"Tajikistan\",\"dial_code\":\"+992\",\"code\":\"TJ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Tanzania\",\"nameImage\":\"Tanzania\",\"dial_code\":\"+255\",\"code\":\"TZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Thailand (ไทย)\",\"nameImage\":\"Thailand\",\"dial_code\":\"+66\",\"code\":\"TH\",\"Language\":\"\"},\n" +
            " {\"name\":\"Timor-Leste\",\"nameImage\":\"Timor-Leste\",\"dial_code\":\"+670\",\"code\":\"TL\",\"Language\":\"\"},\n" +
            " {\"name\":\"Togo\",\"nameImage\":\"Togo\",\"dial_code\":\"+228\",\"code\":\"TG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Tokelau\",\"nameImage\":\"Tokelau\",\"dial_code\":\"+690\",\"code\":\"TK\",\"Language\":\"\"},\n" +
            " {\"name\":\"Tonga\",\"nameImage\":\"Tonga\",\"dial_code\":\"+676\",\"code\":\"TO\",\"Language\":\"\"},\n" +
            " {\"name\":\"Trinidad and Tobago\",\"nameImage\":\"Trinidad and Tobago\",\"dial_code\":\"+1868\",\"code\":\"TT\",\"Language\":\"\"},\n" +
            " {\"name\":\"Tunisia (تونس\u200E)\",\"nameImage\":\"Tunisia\",\"dial_code\":\"+216\",\"code\":\"TN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Turkey (Türkiye)\",\"nameImage\":\"Turkey\",\"dial_code\":\"+90\",\"code\":\"TR\",\"Language\":\"\"},\n" +
            " {\"name\":\"Turkmenistan\",\"nameImage\":\"Turkmenistan\",\"dial_code\":\"+993\",\"code\":\"TM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Turks and Caicos Islands\",\"nameImage\":\"Turks and Caicos Islands\",\"dial_code\":\"+1649\",\"code\":\"TC\",\"Language\":\"\"},\n" +
            " {\"name\":\"Tuvalu\",\"nameImage\":\"Tuvalu\",\"dial_code\":\"+688\",\"code\":\"TV\",\"Language\":\"\"},\n" +
            " {\"name\":\"U.S. Virgin Islands\",\"nameImage\":\"U S Virgin Islands\",\"dial_code\":\"+1340\",\"code\":\"VI\",\"Language\":\"\"},\n" +
            " {\"name\":\"Uganda\",\"nameImage\":\"Uganda\",\"dial_code\":\"+256\",\"code\":\"UG\",\"Language\":\"\"},\n" +
            " {\"name\":\"Ukraine (Україна)\",\"nameImage\":\"Ukraine\",\"dial_code\":\"+380\",\"code\":\"UA\",\"Language\":\"\"},\n" +
            " {\"name\":\"United Arab Emirates (الإمارات العربية المتحدة\u200E)\",\"nameImage\":\"United Arab Emirates\",\"dial_code\":\"+971\",\"code\":\"AE\",\"Language\":\"\"},\n" +
            " {\"name\":\"United Kingdom\",\"nameImage\":\"United Kingdom\",\"dial_code\":\"+44\",\"code\":\"GB\",\"Language\":\"\"},\n" +
            " {\"name\":\"United States\",\"nameImage\":\"United States\",\"dial_code\":\"+1\",\"code\":\"US\",\"Language\":\"\"},\n" +
            " {\"name\":\"Uruguay\",\"nameImage\":\"Uruguay\",\"dial_code\":\"+598\",\"code\":\"UY\",\"Language\":\"\"},\n" +
            " {\"name\":\"Uzbekistan (Oʻzbekiston)\",\"nameImage\":\"Uzbekistan\",\"dial_code\":\"+998\",\"code\":\"UZ\",\"Language\":\"\"},\n" +
            " {\"name\":\"Vanuatu\",\"nameImage\":\"Vanuatu\",\"dial_code\":\"+678\",\"code\":\"VU\",\"Language\":\"\"},\n" +
            " {\"name\":\"Vatican City (Città del Vaticano)\",\"nameImage\":\"Vatican City\",\"dial_code\":\"+39\",\"code\":\"VA\",\"Language\":\"\"},\n" +
            " {\"name\":\"Venezuela\",\"nameImage\":\"Venezuela\",\"dial_code\":\"+58\",\"code\":\"VE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Vietnam (Việt Nam)\",\"nameImage\":\"Vietnam\",\"dial_code\":\"+84\",\"code\":\"VN\",\"Language\":\"\"},\n" +
            " {\"name\":\"Wallis and Futuna\",\"nameImage\":\"Wallis and Futuna\",\"dial_code\":\"+681\",\"code\":\"WF\",\"Language\":\"\"},\n" +
            " {\"name\":\"Yemen (اليمن\u200E)\",\"nameImage\":\"Yemen\",\"dial_code\":\"+967\",\"code\":\"YE\",\"Language\":\"\"},\n" +
            " {\"name\":\"Zambia\",\"nameImage\":\"Zambia\",\"dial_code\":\"+260\",\"code\":\"ZM\",\"Language\":\"\"},\n" +
            " {\"name\":\"Zimbabwe\",\"nameImage\":\"Zimbabwe\",\"dial_code\":\"+263\",\"code\":\"ZW\",\"Language\":\"\"}\n" +
            " \n" +
            " ]";


    public static String domainkey = "+domain";
    public static String creationsoursekey = "~creation_source";
    public static String clicktimekey = "+click_timestamp";
    public static String useridkey = "userid";
    public static String referrerkey = "+referrer";
    public static String titlekey = "$og_title";
    public static String imgurlkey = "$og_image_url";
    public static String campaignkey = "~campaign";
    public static String clicked_branch_linkkey = "+clicked_branch_link";
    public static String match_guaranteed = "+match_guaranteed";
    public static String urlkey = "+url";
    public static String namekey = "name";
    public static String featurekey = "invite";
    public static String ios_deepviewkey = "ios_deepview";
    public static String desktop_urlkey = "$desktop_url";
    public static String isFirstsessionkey = "+is_first_session";
    public static String idkey = "~id";
    public static String onetimekey = "$one_time_use";


}
