package com.membilia.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.membilia.R;
import com.membilia.volley_models.CountryModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Dharmani Apps on 4/22/2017.
 */

public class Utilities {

    static Dialog pdialog = null;

    /***********
     * Hide Keyboard
     * ***********/
    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /***********
     * Show Keyboard
     * ***********/
    public static void showKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    /**
     * validate your email address format. Ex-akhi@mani.com
     */
    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public static void showProgressDialog(Activity mActivity) {
        pdialog = new Dialog(mActivity);
        pdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pdialog.setContentView(R.layout.dialog_progress);
        pdialog.setCanceledOnTouchOutside(false);
        ProgressBar circlePB = (ProgressBar) pdialog.findViewById(R.id.circlePB);
        circlePB.getIndeterminateDrawable().setColorFilter(mActivity.getResources().getColor(R.color.green),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        pdialog.setCancelable(false);
        pdialog.show();
    }

//    public static  boolean isProgressDialogVisible(Activity mActivity){
//        if (pdialog.isShowing()){
//            return true;
//        }
//        else {
//            return false;
//        }
//    }

    public static void hideProgressDialog() {
        if (pdialog.isShowing()) {
            pdialog.dismiss();
        }
    }


    public static String getDate(String OurDate) {
        String strActualFormat = "";
        try {
            DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd");
            // parse the date string into Date object
            Date date = srcDf.parse(OurDate);
            DateFormat destDf = new SimpleDateFormat("MMM dd,yyyy");
            // format the date into another format
            strActualFormat = destDf.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return strActualFormat;
    }

    public static String gettingTimeInHours(long millis){
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;

        String strHours = ""+ hour;

        return  strHours;
    }
    public static String gettingTimeInDays(long milliseconds) {
        int days = (int) ((milliseconds / (1000 * 60 * 60 * 24)) % 7);
        int weeks = (int) (milliseconds / (1000 * 60 * 60 * 24 * 7));
        String strDays = ""+ days;
        return  strDays;
    }
    public static ArrayList<CountryModel> getCountriesPojo() {
        ArrayList<CountryModel> countriesArrayList = new ArrayList<CountryModel>();
        try {

            JSONArray jsonArray = new JSONArray(Countries.COUNTRIES_INFO);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CountryModel mModel = new CountryModel();
                mModel.setName(jsonObject.getString("name"));
                mModel.setNameImage(jsonObject.getString("nameImage"));
                mModel.setDailCode(jsonObject.getString("dial_code"));
                mModel.setCode(jsonObject.getString("code"));
                if (!jsonObject.isNull("Language")) {
                    mModel.setLanguage(jsonObject.getString("Language"));
                }
                countriesArrayList.add(mModel);
            }
            return countriesArrayList;
        } catch (Exception e) {
            e.printStackTrace();
            return countriesArrayList;
        }
    }

}
