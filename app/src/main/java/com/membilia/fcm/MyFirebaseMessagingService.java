package com.membilia.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.ActModel;
import com.membilia.volley_models.Act_ActivityDetailsModel;
import com.membilia.volley_models.Act_UserImagesModel;
import com.membilia.volley_models.Act_UserLocationModel;
import com.membilia.volley_models.Act_UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniapps on 17/1/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = "FirebaseMessagingService";
    String notification_code, notificatioMessage;
    String reciviedActivityId;
    ArrayList<ActModel> actModelArrayList ;
    String openFragment;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a notification payload.
//        if (remoteMessage.getData().size() > 0) {
//            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//            scheduleJob();
//        }


//        if (remoteMessage.getNotification() != null) {
//            notification_code = remoteMessage.getData().get("notification_code");
//            notificatioMessage = remoteMessage.getNotification().getBody();
//            sendNotification(remoteMessage.getData().get("body"));
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }
    /*
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    private void sendNotification(String body) {
        try {
            JSONObject jsonObject = new JSONObject(body);
//            if (notification_code.equalsIgnoreCase("1")) {
//                JSONObject userJson = jsonObject.getJSONObject("user");
//                String userId = userJson.getString("user_id");
//                openNextScreen(userId);
//            }
            reciviedActivityId = jsonObject.getString("activity_id");
            gettingAllItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gettingAllItems() {
        String strUrl = WebServicesConstants.ACTIVITY_FRAGMENTS + "user_id=" + PlayerPawnPreference.readString(this, PlayerPawnPreference.VALUE_USER_ID, "") + "&perPage=" + 10 + "&page_no=" + 1;
        Log.i(TAG, "strUrl" + strUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onResponse" + response.toString());
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(MyFirebaseMessagingService.this, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ActivityFragment");
    }

    private void parseResponse(JSONArray response) {
        actModelArrayList = new ArrayList<>();
        try {
            for (int i = 0; i < 10; i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ActModel actModel = new ActModel();
                if (!jsonObject.isNull("type")) {
                    actModel.setType(jsonObject.getString("type"));
                }
                if (!jsonObject.isNull("text")) {
                    actModel.setText(jsonObject.getString("text"));
                }
                if (!jsonObject.isNull("activity_id")) {
                    actModel.setActivity_id(jsonObject.getString("activity_id"));
                }
                if (!jsonObject.isNull("created")) {
                    actModel.setCreated(jsonObject.getString("created"));
                }
                if (!jsonObject.isNull("user")) {
                    JSONObject jsonActUser = jsonObject.getJSONObject("user");
                    Act_UserModel act_userModel = new Act_UserModel();
                    act_userModel.setUser_id(jsonActUser.getString("user_id"));
                    act_userModel.setName(jsonActUser.getString("name"));
                    act_userModel.setDescription(jsonActUser.getString("description"));
                    act_userModel.setEmail(jsonActUser.getString("email"));
                    act_userModel.setFollowerCount(jsonActUser.getString("followerCount"));
                    act_userModel.setFollowingCount(jsonActUser.getString("followingCount"));
                    act_userModel.setRating(jsonActUser.getString("rating"));
                    act_userModel.setRanking(jsonActUser.getString("ranking"));
                    act_userModel.setIsFollowed(jsonActUser.getBoolean("isFollowed"));
                    act_userModel.setFlag(jsonActUser.getString("flag"));
                    act_userModel.setIsVerified(jsonActUser.getString("isVerified"));

                    if (!jsonActUser.isNull("location")) {
                        JSONObject jsonLocation = jsonActUser.getJSONObject("location");
                        Act_UserLocationModel act_userLocationModel = new Act_UserLocationModel();
                        act_userLocationModel.setAdministrative_area_level_1(jsonLocation.getString("administrative_area_level_1"));
                        act_userLocationModel.setCountry(jsonLocation.getString("country"));
                        act_userLocationModel.setCountry_code(jsonLocation.getString("country_code"));
                        act_userLocationModel.setLat(jsonLocation.getString("lat"));
                        act_userLocationModel.setLng(jsonLocation.getString("lng"));
                        act_userLocationModel.setLocality(jsonLocation.getString("locality"));
                        act_userLocationModel.setPostal_code(jsonLocation.getString("postal_code"));
                        act_userModel.setAct_UserLocationModel(act_userLocationModel);
                    }
                    if (!jsonActUser.isNull("user_images")) {
                        JSONObject jsonUserImages = jsonActUser.getJSONObject("user_images");
                        Act_UserImagesModel act_userImagesModel = new Act_UserImagesModel();
                        act_userImagesModel.setOriginal(jsonUserImages.getString("original"));
                        act_userModel.setAct_UserImagesModel(act_userImagesModel);
                    }
                    actModel.setAct_UserModel(act_userModel);
                }
                if (!jsonObject.isNull("ActivityDetail")) {
                    JSONObject jsonItemObject = jsonObject.getJSONObject("ActivityDetail");
                    Act_ActivityDetailsModel detailsModel = new Act_ActivityDetailsModel();
                    detailsModel.setItem_id(jsonItemObject.getString("item_id"));
                    actModel.setAct_ActivityDetailsModel(detailsModel);
                }
                if (reciviedActivityId.equalsIgnoreCase(jsonObject.getString("activity_id"))) {
                    actModelArrayList.add(actModel);
                }
            }
            if (actModelArrayList.size()>0){
                openfragment(actModelArrayList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openfragment(ArrayList<ActModel> actModelArrayList) {
        String type = actModelArrayList.get(0).getType();
        openFragment = String.valueOf(getFragmentType(Integer.parseInt(type)));
        Intent intent = null;
        if (openFragment.equalsIgnoreCase("Profile")) {
            Constants.BranchIO = "True";
            Constants.BranchUserID = actModelArrayList.get(0).getAct_UserModel().getUser_id();
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            intent = new Intent(this, HomeActivity.class);

        }else if (openFragment.equalsIgnoreCase("ProfileReview")){
            Constants.BranchIO = "True";
            Constants.BranchOther = "ProfileReview";
            Constants.BranchUserID = actModelArrayList.get(0).getAct_UserModel().getUser_id();
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            intent = new Intent(this, HomeActivity.class);
        }
        else if (openFragment.equalsIgnoreCase("Explore")){
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
             intent = new Intent(this, HomeActivity.class);
        }
        else if (openFragment.equalsIgnoreCase("Details")){
            String stritemid = actModelArrayList.get(0).getAct_ActivityDetailsModel().getItem_id();
            Constants.BranchIO = "True";
            Constants.BranchUserID = "";
            Constants.BranchItemID = stritemid;
            startActivity(new Intent(MyFirebaseMessagingService.this, HomeActivity.class));
        }
        else {
            intent = new Intent(this, HomeActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.applogo2)
                .setContentTitle("Membilia")
                .setContentText(notificatioMessage)
                .setAutoCancel(false)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


    private String getFragmentType(int type) {
        if (type==2||type==6||type==7){
            openFragment = "Profile";
            return openFragment;
        } else if (type==15){
            return "ProfileReview";
        }else if(type==4||type==12){
            return "Explore";
        }else if (type==1||type==8||type==10||type==13||type==14||type==16){
            return "Details";
        }
        return "";
    }

    private void openNextScreen(String userID) {
        Constants.BranchIO = "True";
        Constants.BranchUserID = userID;
        PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.applogo2)
                .setContentTitle("Membilia")
                .setContentText(notificatioMessage)
                .setAutoCancel(false)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
