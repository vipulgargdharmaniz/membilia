package com.membilia.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by dharmaniapps on 17/1/18.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    String TAG = "FirebaseInstanceIdService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "*********Refreshed token***********: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String refreshedToken) {
        Log.e(TAG, "*******Refreshed token**********: " + refreshedToken);
    }


}
