package com.membilia.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.nex3z.notificationbadge.NotificationBadge;
import com.membilia.BuildConfig;
import com.membilia.Fragments.ActivityFragment;
import com.membilia.Fragments.AddItemFragment;
import com.membilia.Fragments.AddItemSlidingFragment;
import com.membilia.Fragments.CategoryFragment;
import com.membilia.Fragments.ExploreFragment;
import com.membilia.Fragments.Fragment_Details;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.Fragments.NonVerifyUserFragment;
import com.membilia.Fragments.SendMessage_Fragment;
import com.membilia.PlayPawnPref_Once;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.DatabaseHandler;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.dbModal.LoginUser;
import com.membilia.services.GetChatService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.membilia.Util.Constants.IS_HOMEPOST_REFRESH;

/*
 * Created by dharmaniz on 25/5/17.
 */
public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";
    public RelativeLayout searchRL, filterRL;

    public static ImageView imgHomeExploreIV, imgCategoriesIV, imgActivityIV, imgProfileIV, imgAddPlayerPawn;
    public NotificationBadge badgeIV;
    Intent mIntent;
    String strLoginType;
    public static LinearLayout bottombar;
    public static HomeActivity mActivity;
    public static String forProfileID = "";
    public static String UserItself = "";
    public static String forSearchItem = "";
    public static String itemSearch = "";
    public static String userSerchFor = "";
    public static int Pageno_alluser = 1;
    public static int Pageno_Item = 1;
    /* used for handing item list in parsing*/
    public static String itemType = "all";
    public static String UserType = "new";
    public static String isfilterResponse = "";
    public static String onPause = "no";
    public static String createhome = "no";
    public String strCreatehome = "";
    public static String CountryName = "";
    public static String FromSetting = "";
    private LinearLayout llHomeExplore, llCategories, llProfile;
    private FrameLayout llActivity;
    double latitude = 0;
    double longitude = 0;
    public static DatabaseHandler db;
    static String userToken;

    public static boolean isAddItemOpen = true;
    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    /**
     * Provides access to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;
    /**
     * Provides access to the Location Settings API.
     */
    private SettingsClient mSettingsClient;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Callback for Location events.
     */
    private LocationCallback mLocationCallback;
    /**
     * Represents a geographical location.
     */
    private Location mCurrentLocation;
    private Boolean mRequestingLocationUpdates = false;


//////////////////////////////////////////

    public static void switchFragment(FragmentActivity mActivity, final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
        Constants.FragmentName = Tag;
        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.framelayoutContainer, fragment, Tag);
            if (addToStack)
                fragmentTransaction.addToBackStack(Tag);
            if (bundle != null)
                fragment.setArguments(bundle);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        }
    }

    public static void SpecificFragment(FragmentActivity mActivity, String backFragment, Fragment fragment) {
        String backStateName = backFragment;
        FragmentManager manager = mActivity.getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.framelayoutContainer, fragment);
            ft.commit();
            manager.executePendingTransactions();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        UserItself = PlayerPawnPreference.readString(HomeActivity.this, PlayerPawnPreference.VALUE_USER_ID, "");

      /* call from profile , buy screen , fragmentdetails*/
//        getAllOrderHistory();
        mActivity = new HomeActivity();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            // Create channel to show notifications.
//            String channelId = getString(R.string.default_notification_channel_id);
//            String channelName = "Notification";
//            NotificationManager notificationManager =
//                    getSystemService(NotificationManager.class);
//            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
//                    channelName, NotificationManager.IMPORTANCE_LOW));
//        }
        Log.e(TAG, "Token " + PlayerPawnPreference.readString(HomeActivity.this, PlayerPawnPreference.VALUE_TOKEN, ""));
        Log.w(TAG, "UserId: " + UserItself);
        Constants.IS_FIRSTIME = true;

        setUpViews();
        setUpClickListeners();
        mIntent = mActivity.getIntent();
        if (mIntent != null) {
            strLoginType = mIntent.getStringExtra("login_type");
        }

//        setStatusBarGradiant(HomeActivity.this);
        strCreatehome = PlayerPawnPreference.readString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "");
        if (strCreatehome.equalsIgnoreCase("yes")) {
            createhome = "yes";
        }
        db = new DatabaseHandler(this);
        userToken = PlayerPawnPreference.readString(HomeActivity.this, PlayerPawnPreference.VALUE_TOKEN, "");
        getBadge(userToken);
//        badgeIV.setNumber(45);
        updateValuesFromBundle(savedInstanceState);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();

        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        } else if (!checkPermissions()) {
            requestPermissions();
        }
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            startLocationUpdates();
        }


        Intent intent = new Intent(HomeActivity.this, GetChatService.class);
        intent.putExtra("Token", userToken);
        startService(intent);
    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        KEY_REQUESTING_LOCATION_UPDATES);
            }
            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(KEY_LAST_UPDATED_TIME_STRING)) {

            }
            updateUI();
        }
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Creates a callback for receiving location events.
     */
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                updateLocationUI();
            }
        };
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }


    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    private void startLocationUpdates() {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");
                        //noinspection MissingPermission
                        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        updateUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(HomeActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(HomeActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }
                        updateUI();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Updates all UI fields.
     */
    private void updateUI() {
        updateLocationUI();
    }

    /**
     * Sets the value of the UI fields for the location latitude, longitude and last update time.
     */
    private void updateLocationUI() {
        currentLocation();
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
    }


    @Override
    protected void onResume() {
        super.onResume();

        bottombar.setVisibility(View.VISIBLE);
        imgAddPlayerPawn.setVisibility(View.VISIBLE);


        if (Constants.BranchIO.equalsIgnoreCase("true")) {
            switchFragment(HomeActivity.this, new ExploreFragment(), "ExploreFragment", true, null);

            if (Constants.BranchOther.equalsIgnoreCase("Message")) {
                HomeActivity.forProfileID = Constants.BranchUserID;
                Constants_AddItem.ITEM_ID = Constants.BranchItemID;
                Bundle bundle = new Bundle();
                bundle.putString("friend_id", Constants.FriendId);
                bundle.putString("timeStamp", Constants.TimeStamp);
                bundle.putString("item_id", Constants_AddItem.ITEM_ID);
                Log.e(TAG, "onResumeITEM_ID: " + Constants.FriendId);
                Log.e(TAG, "onResumeTimeStamp: " + Constants.TimeStamp);
                Log.e(TAG, "onResumeITEM_ID: " + Constants_AddItem.ITEM_ID);
                switchFragment(HomeActivity.this, new SendMessage_Fragment(), "SendMessage_Fragment", true, bundle);
            } else if (!Constants.BranchItemID.equalsIgnoreCase("")) {
                String stritemid = Constants.BranchItemID;
                Fragment_Details fragment = new Fragment_Details();
                Bundle bundle = new Bundle();
                bundle.putString("Object", stritemid);
                fragment.setArguments(bundle);
                switchFragment(HomeActivity.this, new Fragment_Details(), Constants.Fragment_Details, false, bundle);
            } else if (Constants.BranchOther.equalsIgnoreCase("ProfileReview")) {
                HomeActivity.forProfileID = Constants.BranchUserID;
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser", "false");
                bundle.putString("subFragment", "review");
                switchFragment(HomeActivity.this, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            } else if (!Constants.BranchUserID.equalsIgnoreCase("")) {
                HomeActivity.forProfileID = Constants.BranchUserID;
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser", "false");
                switchFragment(HomeActivity.this, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, false, bundle);
            }
        } else {
            /// this block happens when we minimize the app or wakeup lock is triggred
            if (!onPause.equalsIgnoreCase("yes")) {
                if (Constants.isITEM_DETAILS)
                    switchFragment(HomeActivity.this, new Fragment_Details(), Constants.Fragment_Details, false, null);
                else
                    switchFragment(HomeActivity.this, new ExploreFragment(), Constants.EXPLORE_FRAGMENT, false, null);
            }
            if (createhome.equalsIgnoreCase("yes")) {
                switchFragment(HomeActivity.this, new ExploreFragment(), Constants.EXPLORE_FRAGMENT, false, null);
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "no");
                createhome = "no";
            }
            if (createhome.equalsIgnoreCase("no") && Constants.SplashAddItemOn) {
                HomeActivity.switchFragment(HomeActivity.this, new AddItemFragment(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, true, null);
            }
        }
        Constants.BranchIO = "false";
        Constants.BranchItemID = "";
        Constants.BranchUserID = "";


        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        } else if (!checkPermissions()) {
            requestPermissions();
        }
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            startLocationUpdates();
        }
        updateUI();
    }


    @Override


    /**
     * Stores activity data in the Bundle.
     */
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, "");
        super.onSaveInstanceState(savedInstanceState);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(R.color.black));
            window.setBackgroundDrawable(background);
        }
    }

    protected void setUpViews() {
        imgHomeExploreIV = findViewById(R.id.imgHomeExploreIV);
        imgCategoriesIV = findViewById(R.id.imgCategoriesIV);
        imgActivityIV = findViewById(R.id.imgActivityIV);
        imgProfileIV = findViewById(R.id.imgProfileIV);
        imgAddPlayerPawn = findViewById(R.id.imgAddPlayerPawn);
        bottombar = findViewById(R.id.bottombar);
        llHomeExplore = findViewById(R.id.llHomeExplore);
        llCategories = findViewById(R.id.llCategories);
        llActivity = findViewById(R.id.llActivity);
        llProfile = findViewById(R.id.llProfile);
        filterRL = findViewById(R.id.filterRL);
        searchRL = findViewById(R.id.searchRL);
        /*By Default View*/
        imgHomeExploreIV.setImageResource(R.drawable.tab1_press);
        imgCategoriesIV.setImageResource(R.drawable.tab2);
        imgActivityIV.setImageResource(R.drawable.activitypeach);
        imgProfileIV.setImageResource(R.drawable.tab5);
        badgeIV = findViewById(R.id.badgeIV);


    }

    protected void setUpClickListeners() {
        userToken = PlayerPawnPreference.readString(HomeActivity.this, PlayerPawnPreference.VALUE_TOKEN, "");
        llHomeExplore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBadge(userToken);
                homeClick();
                Constants.additemFragment = "0";
                IS_HOMEPOST_REFRESH = true;
                HomeActivity.itemType = "relativeClick";
                switchFragment(HomeActivity.this, new ExploreFragment(), Constants.EXPLORE_FRAGMENT, false, null);
            }
        });
        llCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAddItemOpen) {
                    getBadge(userToken);
                    categoryClick();
                    IS_HOMEPOST_REFRESH = true;
                    Constants.additemFragment = "0";
                    switchFragment(HomeActivity.this, new CategoryFragment(), Constants.CATEGORIES_FRAGMENT, false, null);

                }
            }
        });
        llActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAddItemOpen) {
                    getBadge(userToken);
                    activityClick();
                    IS_HOMEPOST_REFRESH = true;
                    switchFragment(HomeActivity.this, new ActivityFragment(), Constants.ACTIVITY_FRAGMENT, false, null);

                }
            }
        });
        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAddItemOpen) {
                    isAddItemOpen = true;
                    getBadge(userToken);
                    clickProfile();
                    IS_HOMEPOST_REFRESH = true;
                    Constants.additemFragment = "0";
                    ArrayList<LoginUser> loginUserArrayList = HomeActivity.db.getUser();
                    Log.e(TAG, "onClick: " + loginUserArrayList.size());
//                Log.e(TAG, "onClick:HomeActivity.db.getUser()  " + loginUserArrayList.get(0).getUserId());
                    if (loginUserArrayList.size() > 0)
                        forProfileID = loginUserArrayList.get(0).getUserId();
                    Bundle bundle = new Bundle();
                    bundle.putString("isSameUser", "true");
//              switchFragment(HomeActivity.this, new ProfileFragment(), Constants.PROFILE_FRAGMENT, false, bundle);
                    switchFragment(HomeActivity.this, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, false, bundle);
                }
            }
        });
        imgAddPlayerPawn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAddItemOpen) {
                    AddItemClick();
                    isAddItemOpen = false;
                    Constants.additemFragment = "0";
                    boolean isVerified = PlayerPawnPreference.readBoolean(HomeActivity.this, PlayerPawnPreference.IsVerified, false);
                    if (isVerified) {
                        if (PlayPawnPref_Once.readString(HomeActivity.this, PlayPawnPref_Once.FirstTimeItemSplash, "").equalsIgnoreCase("yes")) {
                            Constants.OpenAddItemFrom = "home";
                            switchFragment(HomeActivity.this, new AddItemFragment(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, true, null);
                        } else {
                            PlayPawnPref_Once.writeString(HomeActivity.this, PlayPawnPref_Once.FirstTimeItemSplash, "yes");
                            switchFragment(HomeActivity.this, new AddItemSlidingFragment(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, true, null);
                        }
                    } else {
                        if (PlayPawnPref_Once.readString(HomeActivity.this, PlayPawnPref_Once.ApplyasFan, "").equalsIgnoreCase("true")) {
                            Constants.OpenAddItemFrom = "home";
                            switchFragment(HomeActivity.this, new AddItemFragment(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, false, null);
                        } else {
                            switchFragment(HomeActivity.this, new NonVerifyUserFragment(), "NonVerify", true, null);
                        }
                    }
                }

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    public boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionState3 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS);
        int permissionState4 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_CONTACTS);
        return permissionState == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED
                && permissionState3 == PackageManager.PERMISSION_GRANTED && permissionState4 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(HomeActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(HomeActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mRequestingLocationUpdates) {
                    Log.i(TAG, "Permission granted, updates requested, starting location updates");
                    startLocationUpdates();
                }
            } else {
                // Permission denied.
                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.
                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    @Override
    public void onBackPressed() {
        Constants.BranchIO = "";

        bottombar.setVisibility(View.VISIBLE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
        FragmentManager fm = getSupportFragmentManager();
        Log.i("MainActivity", Constants.FragmentName);

        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            ActivityCompat.finishAffinity(this);
        }
    }

    public static void AddItemClick() {
        imgHomeExploreIV.setImageResource(R.drawable.tab1);
        imgCategoriesIV.setImageResource(R.drawable.tab2);
//        imgActivityIV.setBackground(mActivity.getResources().getDrawable(R.drawable.activitypeach));
        imgActivityIV.setImageResource(R.drawable.activitypeach);
        imgProfileIV.setImageResource(R.drawable.tab5);
    }

    public static void homeClick() {
        imgHomeExploreIV.setImageResource(R.drawable.tab1_press);
        imgCategoriesIV.setImageResource(R.drawable.tab2);
//        imgActivityIV.setBackground(mActivity.getResources().getDrawable(R.drawable.activitypeach));

        imgActivityIV.setImageResource(R.drawable.activitypeach);
        imgProfileIV.setImageResource(R.drawable.tab5);
    }

    public static void categoryClick() {
        imgHomeExploreIV.setImageResource(R.drawable.tab1);
        imgCategoriesIV.setImageResource(R.drawable.tab2_press);
        imgActivityIV.setImageResource(R.drawable.activitypeach);
        imgProfileIV.setImageResource(R.drawable.tab5);
    }

    public static void activityClick() {
        imgHomeExploreIV.setImageResource(R.drawable.tab1);
        imgCategoriesIV.setImageResource(R.drawable.tab2);
        imgActivityIV.setImageResource(R.drawable.activitygreen);
        imgProfileIV.setImageResource(R.drawable.tab5);
    }

    public static void clickProfile() {
        imgHomeExploreIV.setImageResource(R.drawable.tab1);
        imgCategoriesIV.setImageResource(R.drawable.tab2);
        imgActivityIV.setImageResource(R.drawable.activitypeach);
        imgProfileIV.setImageResource(R.drawable.tab5_press);
    }

    public void currentLocation() {
        if (mCurrentLocation != null) {
//            Log.e(TAG, "true: ");
//            Log.e(TAG, "currentLocation: " + mCurrentLocation.getLatitude());
//            Log.e(TAG, "currentLocation: " + mCurrentLocation.getLongitude());
            latitude = mCurrentLocation.getLatitude();
            longitude = mCurrentLocation.getLongitude();
            GetCurrentLocation();
//            Log.e(TAG, "currentLocation: " + GetCurrentLocation());
        }
    }

    public String GetCurrentLocation() {
        if (latitude != 0) {
            String CompleteAddress = "";
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(HomeActivity.this, Locale.getDefault());
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.latitude, String.valueOf(latitude));
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.longitude, String.valueOf(longitude));
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                //String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                CompleteAddress = address + " .... " + city + " ....  " + state + " .... " + country + " .... " + postalCode;
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.CITY, city);
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.STATE, state);
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.COUNTRY, country);
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.POSTALCODE, postalCode);
                PlayerPawnPreference.writeString(HomeActivity.this, PlayerPawnPreference.ADDRESS, address);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return CompleteAddress;
        } else {
            return null;
        }
    }

    public void getAllOrderHistory() {
        String strURL = WebServicesConstants.OrderHistroy + "user_id=" + UserItself + "&perPage=100&page_no=1&type=0";
        Log.e(TAG, "strURL: " + strURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse:type0 " + response);
                if (db.getAllContacts().size() > 0) {
                    db.removeAll();
                }
                Log.e(TAG, "new size : " + db.getAllContacts().size());
                parseDATA(response, 0);
                getAllPurchaseOrderHistory();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                Log.e(TAG, "userToken: " + userToken);
                headers.put("Authorization", userToken);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

    public void getAllPurchaseOrderHistory() {
        String strURL = WebServicesConstants.OrderHistroy + "user_id=" + UserItself + "&perPage=100&page_no=1&type=1";
        Log.e(TAG, "strURL: " + strURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse:type1 " + response);
                parseDATA(response, 1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", userToken);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void parseDATA(String response, int type) {
        try {
            String strOrderName = "", strOrderStory = "", strSellerId = "", strRecipientId = "";
            String strSellerName = "", strRecipientName = "", strOrderId = "", strItemId, strOrderDate = "";
            String strPrice, strAddress, strShippingPrize;
            String deliveryType;
            boolean isFavorite;
            String strIsFavorite;
            String Orderstatus;
            String strimage, stritemName, strPrize, isVideo;
            boolean isDeliverytype;
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONObject itemObjectJson = jsonObject.getJSONObject("item_object");
                JSONObject senderObjectJson = jsonObject.getJSONObject("sender");
                JSONObject reciverObjectJson = jsonObject.getJSONObject("reciver");
                JSONObject locationObjectJson = jsonObject.getJSONObject("address");
                JSONObject videoObject = itemObjectJson.getJSONObject("item_video");
                JSONObject imageObject = itemObjectJson.getJSONObject("item_images");
                isFavorite = itemObjectJson.getBoolean("isFavorite");
                if (isFavorite) {
                    strIsFavorite = "true";
                } else {
                    strIsFavorite = "false";
                }
                strShippingPrize = jsonObject.getString("transfer_price");
                strOrderId = (jsonObject.getString("order_id"));
                Orderstatus = (jsonObject.getString("status1"));
                boolean isViewed = jsonObject.getBoolean("is_reviewed");
                Log.e(TAG, "isViewed: " + isViewed);
                strOrderDate = itemObjectJson.getString("pawned_date");

                deliveryType = String.valueOf(itemObjectJson.getBoolean("delivery_type"));

                strAddress = locationObjectJson.getString("address_line_one") + ",\n" +
                        locationObjectJson.getString("address_line_two") + ",\n" +
                        locationObjectJson.getString("city") + ",\n" +
                        locationObjectJson.getString("state") + " ," +
                        locationObjectJson.getString("country");
                Log.e(TAG, "parseDATA: " + itemObjectJson.getString("item_name"));
//                if (type==0){
                strRecipientName = reciverObjectJson.getString("name");
                strRecipientId = reciverObjectJson.getString("user_id");
                strSellerId = senderObjectJson.getString("user_id");
                strSellerName = senderObjectJson.getString("name");
//                }else {
//                    strRecipientId =      HomeActivity.db.getUser().get(0).getUserId();
//                    strRecipientName = HomeActivity.db.getName();
//                    strSellerId = senderObjectJson.getString("user_id");
//                    strSellerName = senderObjectJson.getString("name");
//                }

                if (itemObjectJson.has("item_name"))
                    strOrderName = (itemObjectJson.getString("item_name"));
                strOrderStory = (itemObjectJson.getString("item_story"));
                strItemId = (itemObjectJson.getString("item_id"));
                strPrice = (itemObjectJson.getString("price"));

                if (videoObject.has("original_videoimage")) {
                    strimage = videoObject.getString("original_videoimage");
                    isVideo = "true";
                } else {
                    strimage = imageObject.getString("original_1");
                    isVideo = "false";
                }

//                Log.e(TAG, "parseDATA: " + strOrderId + ".." + strOrderDate + ".." + strSellerName + ".." + strOrderName + ".." + strOrderStory);
//                Log.e(TAG, "parseDATA: " + strSellerId + ".." + deliveryType + ".." + strSellerName + ".." + strPrice);
                ArrayList<String> list = new ArrayList<>();
                list.add(strOrderName);
                list.add(strOrderStory);
                list.add(strOrderId);
                list.add(strOrderDate);
                list.add(strSellerName);
                list.add(strRecipientName);
                list.add(strSellerId);
                list.add(strRecipientId);
                list.add(deliveryType);
                list.add(strAddress);
                list.add(strPrice);
                list.add(strItemId);
                list.add(strShippingPrize);
                list.add(strPrice);
                list.add(strimage);
                list.add(isVideo);
                list.add(Orderstatus);

                if (isViewed)
                    list.add("true");
                else {
                    list.add("false");
                }
                Log.e("strIsFavorite", "strIsFavorite: " + strIsFavorite);
                list.add(strIsFavorite);
                db.addContact(list);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        db.getAllContacts();
        Log.e(TAG, "db.getAllContacts(): " + db.getAllContacts().size());
    }

    public void getBadge(final String usertoken) {
        String strUrl = WebServicesConstants.BadgeCount;
        Log.e(TAG, "getBadge: " + strUrl);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", UserItself);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject response1 = new JSONObject(response);
                    String message = response1.getString("badge");
                    Log.e(TAG, "onResponse:message " + message);
                    Constants.NewMessage = message;
                    String activicity_count = response1.getString("activicity_count");
                    Log.e(TAG, "activicity_count:message " + activicity_count);
                    if (!activicity_count.equalsIgnoreCase("0")) {
                        badgeIV.setVisibility(View.VISIBLE);
                        Log.e(TAG, "activity_count:VISIBLE ");
                        int badgeCount = 1;
                        badgeIV.setNumber(Integer.parseInt(activicity_count));
//                        BadgeFactory.createCircle(HomeActivity.this).setBadgeCount(activicity_count).setHeight(14).setWidth(14).setTextSize(10).bind(badgeIV);
                    } else {
                        Log.e(TAG, "activity_count:GONE ");
                        badgeIV.setVisibility(View.GONE);
                        badgeIV.setNumber(0);
//                        BadgeFactory.createCircle(HomeActivity.this).setBadgeCount(0).setHeight(0).setWidth(0).setTextSize(0).unbind();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", usertoken);
                return hashMap;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);

    }

    public void refreshPage() {
        homeClick();
        Constants.additemFragment = "0";
        IS_HOMEPOST_REFRESH = true;
        HomeActivity.itemType = "relativeClick";
        switchFragment(HomeActivity.this, new ExploreFragment(), Constants.EXPLORE_FRAGMENT, false, null);
    }
}