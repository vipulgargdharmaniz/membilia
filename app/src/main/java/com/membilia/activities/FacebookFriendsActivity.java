package com.membilia.activities;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.membilia.BaseActivity;
import com.membilia.R;
import com.membilia.Util.AppSingleton;
import com.membilia.adapters.FbFriendsAdapter;
import com.membilia.volley_models.FbFriendsModel;

import java.util.ArrayList;

public class FacebookFriendsActivity extends BaseActivity {
    String TAG = "FacebookFriendsActivity";
    Activity mActivity = FacebookFriendsActivity.this;
    Resources mResources;
    RelativeLayout backRL;
    RecyclerView recycler_view;
    FbFriendsAdapter mAdapter;
    ArrayList<FbFriendsModel> fbFriendsModelArrayList = new ArrayList<FbFriendsModel>();
    AppSingleton mAppSingleton = AppSingleton.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_friends);
        Log.i(TAG, "onCreate: ");
        mResources = mActivity.getResources();
        fbFriendsModelArrayList = mAppSingleton.getFbFriendsArrayList();
    }



    @Override
    protected void setUpViews() {
        backRL = (RelativeLayout) findViewById(R.id.backRL);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);

        setAdatper();
    }


    @Override
    protected void setUpClickListeners() {
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setAdatper() {
        mAdapter = new FbFriendsAdapter(mActivity, fbFriendsModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(mAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


}
