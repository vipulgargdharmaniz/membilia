package com.membilia.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.BaseActivity;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.volley.UtilsVolley;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class AddItemForNotVerifyUser extends BaseActivity {
    private final String TAG = "AddItemForNotVerifyUser";
    Activity mActivity = AddItemForNotVerifyUser.this;
    Resources mResources;
    EditText editNameET, editReasonOfEnqiryET, editPhoneNoET, editEmailAddressET;
    TextView txtSubmitTV, postTV;
    LinearLayout layoutCancel;
    String strName, strReasonEnquiry, strPhoneNumber, strEmailAddress;
    String ScreenType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item_for_not_verify_user);
        Log.i(TAG, "onCreate: ");
        mResources = mActivity.getResources();
        Intent getdata = getIntent();
        ScreenType = getdata.getStringExtra("screen");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void setUpViews() {
        editNameET = findViewById(R.id.editNameET);
        editReasonOfEnqiryET = findViewById(R.id.editReasonOfEnqiryET);
        editPhoneNoET = findViewById(R.id.editPhoneNoET);
        editEmailAddressET = findViewById(R.id.editEmailAddressET);
        txtSubmitTV = findViewById(R.id.txtSubmitTV);
        layoutCancel = findViewById(R.id.layoutCancel);
        postTV = findViewById(R.id.postTV);
        postTV.setVisibility(View.GONE);
    }

    @Override
    protected void setUpClickListeners() {
        layoutCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SplashAddItemOn = false;
                onBackPressed();
            }
        });

        txtSubmitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strName = editNameET.getText().toString();
                strReasonEnquiry = editReasonOfEnqiryET.getText().toString();
                strPhoneNumber = editPhoneNoET.getText().toString();
                strEmailAddress = editEmailAddressET.getText().toString();
                /*if (strName.equals("") && strReasonEnquiry.equals("") && strPhoneNumber.equals("") && strEmailAddress.equals("")){
                    AlertDialogManager.showAlertDialog(mActivity,mResources.getString(R.string.app_name),mResources.getString(R.string.allfields));
                }else*/
                if (strName.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), "Name is required.");
                } else if (strReasonEnquiry.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), "Reason of inquiry is required.");
                } else if (strPhoneNumber.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), "Phone number is required.");
                } else if (strEmailAddress.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), "Email is required.");
                } else if (!Utilities.emailValidator(strEmailAddress)) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.entervalidemailaddress));
                } else {
                    /*Execute API*/
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("name", strName);
                        jsonObject.put("email", strEmailAddress);
                        jsonObject.put("phone", strPhoneNumber);
                        jsonObject.put("reason", strReasonEnquiry);
                        hitService(jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void hitService(final JSONObject jsonObject) {
        String strUrl = WebServicesConstants.APPLY_AS_SELLER;
        Log.e(TAG, "hitService: " + strUrl);
        Utilities.showProgressDialog(AddItemForNotVerifyUser.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                Utilities.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("messgae")) {
                        String message = jsonObject.getString("messgae");
                        AlertDialogManager.showAlertDialog(AddItemForNotVerifyUser.this, getString(R.string.app_name), message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                String json;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(AddItemForNotVerifyUser.this, getString(R.string.app_name), "You have already requested for seller access. We are reviewing your account and will get back to your soon.");
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(AddItemForNotVerifyUser.this, PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
