package com.membilia.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.membilia.BaseActivity;
import com.membilia.R;
import com.membilia.contacts.Contact;
import com.membilia.contacts.ContactFetcher;
import com.membilia.contacts.ContactsAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ContactsActivity extends BaseActivity {
    public static String TAG = "ContactsActivity";

    Activity mActivity = ContactsActivity.this;
    TextView cancelTV;
    EditText editSearchET;
    ListView lvContacts;
    ArrayList<Contact> listContactsArrayList;
    ContactsAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        Log.i(TAG, "onCreate: ");

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void setUpViews() {
        cancelTV =  findViewById(R.id.cancelTV);
        editSearchET =  findViewById(R.id.editSearchET);
        lvContacts = findViewById(R.id.lvContacts);

        /*
         *Set Adapter
         *****/
        setAdapter();
    }

    @Override
    protected void setUpClickListeners() {
        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.filter(s.toString());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        lvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                Contact model = listContactsArrayList.get(i);
                String toNumber = model.numbers.get(0).number; // contains spaces.
                toNumber = toNumber.replace("+", "").replace(" ", "");
                Intent smsIntent ;
//                smsIntent.setType("vnd.android-dir/mms-sms");
//                smsIntent.putExtra("address", "" + toNumber);
//                smsIntent.putExtra("sms_body", "http://www.membilia.com/");
//                startActivity(smsIntent);


                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(ContactsActivity.this); //Need to change the build to API 19
                    smsIntent = new Intent(Intent.ACTION_SEND);
                    smsIntent.setType("text/plain");
                    smsIntent.putExtra(Intent.EXTRA_TEXT,"http://www.membilia.com/");
                    //if no default app is configured, then choose any app that support this intent.
                    if (defaultSmsPackageName != null) {
                        smsIntent.setPackage(defaultSmsPackageName);
                    }
                } else {
                    smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address","" + toNumber);
                    smsIntent.putExtra("sms_body","http://www.membilia.com/");
                }
                startActivity(smsIntent);

                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void setAdapter() {
        listContactsArrayList = new ContactFetcher(this).fetchAll();
        Collections.sort(listContactsArrayList, new Comparator<Contact>() {
            @Override
            public int compare(Contact s1, Contact s2) {
                return s1.name.compareToIgnoreCase(s2.name);
            }
        });
        mAdapter = new ContactsAdapter(mActivity, listContactsArrayList, mActivity.getResources());

        lvContacts.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

}


//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        checkPermissions();
////            requestPermission();
//                        }
//                        if (ContextCompat.checkSelfPermission(InviteActivity.this,
//                        Manifest.permission.READ_CONTACTS) + ContextCompat
//                        .checkSelfPermission(InviteActivity.this,
//                        Manifest.permission.WRITE_CONTACTS)
//                        != PackageManager.PERMISSION_GRANTED)
//
//
//                        startActivity(new Intent(mActivity, ContactsActivity.class));


