package com.membilia.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import com.membilia.BaseActivity;
import com.membilia.PlayPawnPref_Once;
import com.membilia.R;
import com.membilia.adapters.ItemSplashAdapter;


/*
 * Created by dharmaniapps on 4/12/17.
 */

public class AddItemSlidingActivity extends BaseActivity {

    TabLayout tabLayout;
    private ViewPager mViewPager;
    private ItemSplashAdapter mPagerAdapter;
    int layoutArray[] = {R.layout.item_splash1, R.layout.item_splash2, R.layout.item_splash3};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_slides);

        PlayPawnPref_Once.writeString(AddItemSlidingActivity.this,PlayPawnPref_Once.FirstTimeItemSplash,"yes");
    }

    @Override
    protected void setUpViews() {
        super.setUpViews();
        mViewPager = findViewById(R.id.viewpager);
        tabLayout =  findViewById(R.id.tabDots);
        setViewPagerAdapter();

    }

    private void setViewPagerAdapter() {
        mPagerAdapter = new ItemSplashAdapter(this, layoutArray, tabLayout);
        mViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
