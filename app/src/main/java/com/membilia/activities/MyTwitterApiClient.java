package com.membilia.activities;

import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import retrofit2.Call;

import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by dharmaniapps on 18/1/18.
 */

public class MyTwitterApiClient extends TwitterApiClient {

    public MyTwitterApiClient(TwitterSession session) {
        super(session);
    }

    public GetUsersShowAPICustomService getCustomService() {
        return getService(GetUsersShowAPICustomService.class);
    }

    public GetFollowersIdsnew getFollowersIds1() {
        return getService(GetFollowersIdsnew.class);
    }

    public Getprpofile getprpofile() {
        return getService(Getprpofile.class);
    }

    public GetFollowersIds getFollowersIds() {
        return getService(GetFollowersIds.class);
    }

    public GetFriends getCustomService1() {
        return getService(GetFriends.class);
    }
}

interface GetUsersShowAPICustomService {
    @GET("/1.1/followers/list.json?cursor=-1&skip_status=true&include_user_entities=false")
    Call<JSONObject> show(@Query("user_id") long userId, @Query("screen_name") String username);
}

interface GetFollowersIds {
    @GET("/1.1/followers/list.json")
    Call<JSONObject> show(@Query("user_id") Long userId, @Query("screen_name") String
            var, @Query("skip_status") Boolean var1, @Query("include_user_entities") Boolean var2, @Query("count") Integer var3);
}

interface GetFollowersIdsnew {
    @GET("/1.1/followers/list.json")
    Call<JSONObject> show(@Query("user_id") Long userId);
}

interface Getprpofile {
    @GET("/1.1/users/show.json")
    Call<User> show(@Query("user_id") long userId);
}


interface GetFriends {
    @GET("/1.1/followers/ids.json")
    Call<User> show(@Query("user_id") long userId);
}