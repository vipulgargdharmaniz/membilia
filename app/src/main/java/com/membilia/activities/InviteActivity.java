package com.membilia.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.membilia.BaseActivity;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;


public class InviteActivity extends BaseActivity {
    public static String TAG = "InviteActivity";
    Activity mActivity = InviteActivity.this;
    LinearLayout findContactLL, facebookFriendsLL, twitterFriendsLL;
    TextView facebookTitleTV, facebookFriendsTV, twitterTitleTV, twitterFriendsTV;
    CircleImageView imgfacebookCIV, imgTwitterCIV;

    ImageView imgCheckIV;
    Intent mIntent;
    String strLoginType = "";

    private String readContactsStr = Manifest.permission.READ_CONTACTS;
    private String cameraStr = Manifest.permission.CAMERA;
    private String writeStorageStr = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String readStorageStr = Manifest.permission.READ_EXTERNAL_STORAGE;

    private static final int PERMISSION_REQUEST_CODE = 200;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_invite);
        Log.i(TAG, "onCreate: ");
        mIntent = mActivity.getIntent();
        if (mIntent != null) {
            strLoginType = mIntent.getStringExtra(Constants.LOGIN_TYPE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission()) {
                requestPermission();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void setUpViews() {
        findContactLL = findViewById(R.id.findContactLL);
        facebookFriendsLL = findViewById(R.id.facebookFriendsLL);
        twitterFriendsLL = findViewById(R.id.twitterFriendsLL);
        imgCheckIV = findViewById(R.id.imgCheckIV);
        facebookTitleTV = findViewById(R.id.facebookTitleTV);
        facebookFriendsTV = findViewById(R.id.facebookFriendsTV);
        twitterTitleTV = findViewById(R.id.twitterTitleTV);
        twitterFriendsTV = findViewById(R.id.twitterFriendsTV);
        imgfacebookCIV = findViewById(R.id.imgfacebookCIV);
        imgTwitterCIV = findViewById(R.id.imgTwitterCIV);

        if (strLoginType.equals("email")) {
            facebookFriendsLL.setVisibility(View.GONE);
            twitterFriendsLL.setVisibility(View.GONE);
        } else if (strLoginType.equals("facebook")) {
            facebookFriendsLL.setVisibility(View.VISIBLE);
            twitterFriendsLL.setVisibility(View.GONE);
        } else if (strLoginType.equals("twitter")) {
            facebookFriendsLL.setVisibility(View.GONE);
            twitterFriendsLL.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setUpClickListeners() {
        findContactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    startActivity(new Intent(mActivity, ContactsActivity.class));
                } else {
                    requestPermission();
                }
            }

        });

        facebookFriendsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String appLinkUrl, previewImageUrl;
                appLinkUrl = "https://fb.me/282706508871565";
                previewImageUrl = "http://membiliaapp.com/staging/admin/assets/images/banner_img.png";
//                if (AppInviteDialog.canShow()) {
//                    AppInviteContent content = new AppInviteContent.Builder()
//                            .setApplinkUrl(appLinkUrl)
//                            .setPreviewImageUrl(previewImageUrl)
//                            .build();
//                    AppInviteDialog.show(InviteActivity.this, content);
//                }


//                Uri targetUrl =
//                        AppLinks.getTargetUrlFromInboundIntent(InviteActivity.this, getIntent());
//                if (targetUrl != null) {
//                    Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
//                }else {
//                    AppLinkData.fetchDeferredAppLinkData(
//                            InviteActivity.this,
//                            new AppLinkData.CompletionHandler() {
//                                @Override
//                                public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
//                                    //process applink data
//                                    Log.e(TAG, "onDeferredAppLinkDataFetched: "+appLinkData);
//                                }
//                            });
//                }

//                appLinkUrl = "https://www.mydomain.com/myapplink";
                previewImageUrl = "https://www.mydomain.com/my_invite_image.jpg";

                if (AppInviteDialog.canShow()) {
                    Log.e(TAG, "onClick:AppInviteDialog.canShow() " );
                    AppInviteContent content = new AppInviteContent.Builder()
                            .setApplinkUrl(appLinkUrl)
                            .setPreviewImageUrl(previewImageUrl)
                            .build();
                    AppInviteDialog.show(InviteActivity.this, content);
                }
                else {
                    Log.e(TAG, "onClick: no AppInvite" );
                }

//                appLinkUrl = "https://fb.me/493857950766025";
//                previewImageUrl = "http://2.bp.blogspot.com/-99shOruuadw/VQsG2T233sI/AAAAAAAAEi0/noFTxUBh_rg/s1600/appscripts.png";
//
//                AppInviteContent content = new AppInviteContent.Builder()
//                        .setApplinkUrl(appLinkUrl)
//                        .setPreviewImageUrl(previewImageUrl)
//                        .build();
//                AppInviteDialog.show(InviteActivity.this, content);
            }
        });

        twitterFriendsLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();

                getFriendsList(session);
            }
        });


        imgCheckIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerPawnPreference.writeString(InviteActivity.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");

                Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }
        });
    }

    private void getFriendsList(final TwitterSession session) {

        MyTwitterApiClient apiclients = new MyTwitterApiClient(session);
        Call<JSONObject> call = apiclients.getFollowersIds().show(session.getUserId(), null, true, true, 100);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void success(Result<JSONObject> result) {
                Log.e(TAG, "onResponse: " + result.data.toString());
                Log.e(TAG, "onResponse: " + result.response);
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
            }
        });

        Call<com.twitter.sdk.android.core.models.User> userCall = TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(true, false, true);
        userCall.enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
            @Override
            public void success(Result<User> result) {
                MyTwitterApiClient myTwitterApiClient = new MyTwitterApiClient(session);
                myTwitterApiClient.getFollowersIds1().show(session.getUserId()).enqueue(new Callback<JSONObject>() {

                    @Override
                    public void success(Result<JSONObject> result) {
                        Log.e(TAG, "success: " + result.data);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        exception.printStackTrace();
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
            }
        });
    }

    private boolean checkPermission() {
        int camera = ContextCompat.checkSelfPermission(getApplicationContext(), cameraStr);
        int readContacts = ContextCompat.checkSelfPermission(getApplicationContext(), readContactsStr);
        int writeContacts = ContextCompat.checkSelfPermission(getApplicationContext(), writeStorageStr);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), readStorageStr);

        return camera == PackageManager.PERMISSION_GRANTED && readContacts == PackageManager.PERMISSION_GRANTED &&
                writeContacts == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{cameraStr, readContactsStr, writeStorageStr, readStorageStr}, PERMISSION_REQUEST_CODE);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:

                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult == grantResults.length - 1) {
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                startActivity(new Intent(mActivity, ContactsActivity.class));
                            } else {
                                requestPermission();
                            }
                        } else {
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            } else {
                                Log.e(TAG, "onRequestPermissionsResult: failed");
                                requestPermission();
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(this);
    }
}
