package com.membilia.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.membilia.R;

/**
 * Created by dharmaniapps on 27/12/17.
 */

public class Payment_Activity extends AppCompatActivity {

    private static final String TAG = "Payment_Activity";
    RelativeLayout leftRL , loginRL ,paywithcardRL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        setView();
        setOnclick();
    }
    private void setView() {
        leftRL = (RelativeLayout) findViewById(R.id.leftRL);
        loginRL = (RelativeLayout) findViewById(R.id.loginRL);
        paywithcardRL = (RelativeLayout) findViewById(R.id.paywithcardRL);
    }
    private void setOnclick() {

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        loginRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Payment_Activity.this, PayPalLogin_Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_from_right, R.anim.animback);
            }
        });

        paywithcardRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


}
