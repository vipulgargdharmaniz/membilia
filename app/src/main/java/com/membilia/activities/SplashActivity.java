package com.membilia.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.membilia.BaseActivity;
import com.membilia.PlayPawnPref_Once;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.splashslides.SplashSlidesActivity;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ActModel;
import com.membilia.volley_models.Act_ActivityDetailsModel;
import com.membilia.volley_models.Act_UserImagesModel;
import com.membilia.volley_models.Act_UserLocationModel;
import com.membilia.volley_models.Act_UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends BaseActivity {
    public static final int SPLASH_TIME_OUT = 2500;
    Activity mActivity = SplashActivity.this;
    private static final String TAG = "SplashActivity";
    boolean isFromNotification = false;
    String notificationCode = "", notificationBody;
    ArrayList<ActModel> actModelArrayList;
    String reciviedActivityId;
    String openFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (getIntent().getExtras() != null) {

            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);

            }
            Bundle bundle = getIntent().getExtras();
            notificationCode = bundle.getString("notification_code");
            if (notificationCode != null) {
                isFromNotification = true;
            }
            notificationBody = bundle.getString("body");
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isFromNotification)
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "token in splash: "+PlayerPawnPreference.readString(SplashActivity.this, PlayerPawnPreference.VALUE_TOKEN, "") );
                    Log.e(TAG, "run:FirstTimeSplash  "+ PlayPawnPref_Once.readString(SplashActivity.this, PlayPawnPref_Once.FirstTimeSplash, "") );
                    if (!PlayerPawnPreference.readString(SplashActivity.this, PlayerPawnPreference.VALUE_TOKEN, "").equalsIgnoreCase("")) {
                        PlayerPawnPreference.writeString(SplashActivity.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
                        startActivity(new Intent(mActivity, HomeActivity.class));
                        finish();
                        overridePendingTransition(R.anim.in_from_right, R.anim.animback);
                    }
                    else if (PlayPawnPref_Once.readString(SplashActivity.this, PlayPawnPref_Once.FirstTimeSplash, "").equalsIgnoreCase("")) {
                        PlayPawnPref_Once.writeString(SplashActivity.this, PlayPawnPref_Once.FirstTimeSplash, "yes");
                        startActivity(new Intent(mActivity, SplashSlidesActivity.class));
                        finish();
                    }
                    else if (PlayerPawnPreference.readString(SplashActivity.this, PlayerPawnPreference.VALUE_TOKEN, "").equals("")) {
                        startActivity(new Intent(mActivity, SelectionActivity.class));
                        finish();
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    }
                    else {
                        startActivity(new Intent(mActivity, SplashSlidesActivity.class));
                        finish();
                    }
                }
            }, SPLASH_TIME_OUT);
        else {
            try {
                JSONObject jsonObject = new JSONObject(notificationBody);
                reciviedActivityId = jsonObject.getString("activity_id");
                gettingAllItems();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void gettingAllItems() {
        Utilities.showProgressDialog(this);
        String strUrl = WebServicesConstants.ACTIVITY_FRAGMENTS + "user_id=" + PlayerPawnPreference.readString(this, PlayerPawnPreference.VALUE_USER_ID, "") + "&perPage=" + 10 + "&page_no=" + 1;
        Log.i(TAG, "strUrl" + strUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onResponse" + response.toString());
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onErrorResponse" + error.toString());
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(SplashActivity.this, getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(SplashActivity.this, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ActivityFragment");
    }


    private void parseResponse(JSONArray response) {
        actModelArrayList = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ActModel actModel = new ActModel();
                if (!jsonObject.isNull("type")) {
                    actModel.setType(jsonObject.getString("type"));
                }
                if (!jsonObject.isNull("text")) {
                    actModel.setText(jsonObject.getString("text"));
                }
                if (!jsonObject.isNull("activity_id")) {
                    actModel.setActivity_id(jsonObject.getString("activity_id"));
                }
                if (!jsonObject.isNull("created")) {
                    actModel.setCreated(jsonObject.getString("created"));
                }
                if (!jsonObject.isNull("user")) {
                    JSONObject jsonActUser = jsonObject.getJSONObject("user");
                    Act_UserModel act_userModel = new Act_UserModel();
                    act_userModel.setUser_id(jsonActUser.getString("user_id"));
                    act_userModel.setName(jsonActUser.getString("name"));
                    act_userModel.setDescription(jsonActUser.getString("description"));
                    act_userModel.setEmail(jsonActUser.getString("email"));
                    act_userModel.setFollowerCount(jsonActUser.getString("followerCount"));
                    act_userModel.setFollowingCount(jsonActUser.getString("followingCount"));
                    act_userModel.setRating(jsonActUser.getString("rating"));
                    act_userModel.setRanking(jsonActUser.getString("ranking"));
                    act_userModel.setIsFollowed(jsonActUser.getBoolean("isFollowed"));
                    act_userModel.setFlag(jsonActUser.getString("flag"));
                    act_userModel.setIsVerified(jsonActUser.getString("isVerified"));

                    if (!jsonActUser.isNull("location")) {
                        JSONObject jsonLocation = jsonActUser.getJSONObject("location");
                        Act_UserLocationModel act_userLocationModel = new Act_UserLocationModel();
                        act_userLocationModel.setAdministrative_area_level_1(jsonLocation.getString("administrative_area_level_1"));
                        act_userLocationModel.setCountry(jsonLocation.getString("country"));
                        act_userLocationModel.setCountry_code(jsonLocation.getString("country_code"));
                        act_userLocationModel.setLat(jsonLocation.getString("lat"));
                        act_userLocationModel.setLng(jsonLocation.getString("lng"));
                        act_userLocationModel.setLocality(jsonLocation.getString("locality"));
                        act_userLocationModel.setPostal_code(jsonLocation.getString("postal_code"));
                        act_userModel.setAct_UserLocationModel(act_userLocationModel);
                    }
                    if (!jsonActUser.isNull("user_images")) {
                        JSONObject jsonUserImages = jsonActUser.getJSONObject("user_images");
                        Act_UserImagesModel act_userImagesModel = new Act_UserImagesModel();
                        act_userImagesModel.setOriginal(jsonUserImages.getString("original"));
                        act_userModel.setAct_UserImagesModel(act_userImagesModel);
                    }
                    actModel.setAct_UserModel(act_userModel);
                }
                if (!jsonObject.isNull("ActivityDetail")) {
                    JSONObject jsonItemObject = jsonObject.getJSONObject("ActivityDetail");
                    Act_ActivityDetailsModel detailsModel = new Act_ActivityDetailsModel();
                    detailsModel.setItem_id(jsonItemObject.getString("item_id"));
                    detailsModel.setSeller_id(jsonItemObject.getString("seller_id"));
                    if (jsonItemObject.has("item_name")){
                        detailsModel.setItem_name(jsonItemObject.getString("item_name"));
                    }

                    actModel.setAct_ActivityDetailsModel(detailsModel);
                }
                if (reciviedActivityId.equalsIgnoreCase(jsonObject.getString("activity_id"))) {
                    actModelArrayList.add(actModel);
                }
            }
            if (actModelArrayList.size() > 0) {
                openfragment(actModelArrayList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openfragment(ArrayList<ActModel> actModelArrayList) {
        String type = actModelArrayList.get(0).getType();
        openFragment = String.valueOf(getFragmentType(Integer.parseInt(type)));
        Log.e(TAG, "openFragment: " + openFragment);
        if (openFragment.equalsIgnoreCase("Profile")) {
            Constants.BranchIO = "True";
            Constants.BranchUserID = actModelArrayList.get(0).getAct_UserModel().getUser_id();
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else if (openFragment.equalsIgnoreCase("ProfileReview")) {
            Constants.BranchIO = "True";
            Constants.BranchOther = "ProfileReview";
            Constants.BranchUserID = actModelArrayList.get(0).getAct_UserModel().getUser_id();
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else if (openFragment.equalsIgnoreCase("Explore")) {
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else if (openFragment.equalsIgnoreCase("Details")) {
            String stritemid = actModelArrayList.get(0).getAct_ActivityDetailsModel().getItem_id();
            Constants.BranchIO = "True";
            Constants.BranchUserID = "";
            Constants.BranchItemID = stritemid;
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            finish();
        }
        else if (openFragment.equalsIgnoreCase("Message")) {
            Constants.BranchOther = "Message";
            String stritemid = actModelArrayList.get(0).getAct_ActivityDetailsModel().getItem_id();
            Constants.BranchIO = "True";
            Constants.BranchUserID = "";
            Constants.BranchItemID = stritemid;
            Constants_AddItem.ITEM_NAME = actModelArrayList.get(0).getAct_ActivityDetailsModel().getItem_name();
            Log.e(TAG, "FriendId: "+actModelArrayList.get(0).getAct_ActivityDetailsModel().getSeller_id() );
            Constants.FriendId = actModelArrayList.get(0).getAct_ActivityDetailsModel().getSeller_id() + "playpawn";
            Constants.TimeStamp = actModelArrayList.get(0).getCreated();

            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            finish();
        }
        else {
            PlayerPawnPreference.writeString(this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }


    private String getFragmentType(int type) {
        if ( type == 6 || type == 7) {
            openFragment = "Profile";
            return openFragment;
        } else if (type == 15) {
            return "ProfileReview";
        } else if (type == 4 || type == 12) {
            return "Explore";
        } else if (type == 1 || type == 8 || type == 10 || type == 13 || type == 14 || type == 16|| type == 3||type == 2 ||type==17) {
            return "Details";
        } else if (type == 9) {
            return "Message";
        }
        return "";
    }
}
