package com.membilia.activities;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.membilia.BaseActivity;
import com.membilia.R;
import com.membilia.adapters.TwitterFriendsAdapter;
import com.membilia.volley_models.TwitterFriendsModel;

import java.util.ArrayList;

public class TwitterFollowersActivity extends BaseActivity {
    String TAG = "TwitterFollowersActivity";
    Activity mActivity = TwitterFollowersActivity.this;
    Resources mResources;
    RecyclerView recycler_view;
    RelativeLayout backRL;
    TwitterFriendsAdapter mAdapter;
    ArrayList<TwitterFriendsModel> twitterArrayList = new ArrayList<TwitterFriendsModel>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter_followers);
        Log.i(TAG, "onCreate: ");
        mResources = mActivity.getResources();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void setUpViews() {
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        backRL = (RelativeLayout) findViewById(R.id.backRL);

        setAdapter();
    }

    private void setAdapter() {
        mAdapter = new TwitterFriendsAdapter(mActivity, twitterArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(mAdapter);
    }

    @Override
    protected void setUpClickListeners() {
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
