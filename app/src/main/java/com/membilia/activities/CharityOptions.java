package com.membilia.activities;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.adapters.CharityAdapter;
import com.membilia.volley_models.CharitySelection_Modal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by dharmaniapps on 6/12/17.
 */

public class CharityOptions extends Fragment {

    LinearLayout doneLL;
    EditText searchET;
    RecyclerView recyclerRV;

    Resources mResources;
    CharityAdapter mCharityAdapter;

    List<String> charityArraylist = new ArrayList<>();
    List<CharitySelection_Modal> newcharityArraylist = new ArrayList<>();

    View view ;
    public static String  strCharity = "";
    int currentlistSize = 0;
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.charityoption);
//
//        setView();
//        setClick();
//
//        setCharityData();
//
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.charityoption,null);
        setView(view);
        setClick();

        setCharityData();

        return view;
    }

    private void setCharityData() {
        mResources = this.getResources();
        mResources.getStringArray(R.array.array_sports);
        charityArraylist =  Arrays.asList(mResources.getStringArray(R.array.charityOptions));


        for (int i =0 ; i<charityArraylist.size() ; i++){
            CharitySelection_Modal charitySelection_modal = new CharitySelection_Modal();
            charitySelection_modal.setCharityName(charityArraylist.get(i));
            charitySelection_modal.setSelected(false);
            newcharityArraylist.add(charitySelection_modal);
        }

        setAdapter(newcharityArraylist);



        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
                //after the change calling the method and passing the search input
            }
        });
    }

    public void setOnEditText(String getCurrentValue){
        searchET.setText(getCurrentValue);

    }

    private void setAdapter(List<CharitySelection_Modal> newcharityArraylist) {
        currentlistSize = newcharityArraylist.size();
        mCharityAdapter = new CharityAdapter(getActivity() , newcharityArraylist , CharityOptions.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(mCharityAdapter);
        mCharityAdapter.notifyDataSetChanged();
    }

    private void setView(View view) {
        recyclerRV = (RecyclerView) view.findViewById(R.id.recyclerRV);
        doneLL = (LinearLayout) view.findViewById(R.id.doneLL);
        searchET= (EditText) view.findViewById(R.id.searchET);


    }

    private void setClick() {
       doneLL.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               if (currentlistSize ==0){
                   Constants_AddItem.ITEM_CHARITYNAME =searchET.getText().toString();
                   Log.e(Constants_AddItem.ITEM_CHARITYNAME, "onClick: "+Constants_AddItem.ITEM_CHARITYNAME );
                   FragmentManager fm = getActivity().getSupportFragmentManager();
                   fm.popBackStack();
                   Constants.OpenAddItemFrom = "Charity";
                   Constants.OpenEditAddItemFrom = "Charity";
//                   if (!Constants.FragmentName.equalsIgnoreCase(Constants.Edit_AddItem)){
//                       HomeActivity.SpecificFragment((FragmentActivity) getActivity(),Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT,new AddItemFragment());
//                   }
//                   else{
//                       HomeActivity.SpecificFragment((FragmentActivity) getActivity(),Constants.Edit_AddItem,new Edit_AddItem());
//                   }/
               }
               else {
                   Constants_AddItem.ITEM_CHARITYNAME =strCharity;
                   Log.e(Constants_AddItem.ITEM_CHARITYNAME, "onClick: "+Constants_AddItem.ITEM_CHARITYNAME );
                   FragmentManager fm = getActivity().getSupportFragmentManager();
                   fm.popBackStack();
                   Constants.OpenAddItemFrom = "Charity";
                   Constants.OpenEditAddItemFrom = "Charity";
//                   if (!Constants.FragmentName.equalsIgnoreCase(Constants.Edit_AddItem)){
//                       HomeActivity.SpecificFragment((FragmentActivity) getActivity(),Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT,new AddItemFragment());
//                   }
//                   else{
//                       HomeActivity.SpecificFragment((FragmentActivity) getActivity(),Constants.Edit_AddItem,new Edit_AddItem());
//                   }
               }

           }
       });
    }
    private void filter(String text) {
        //new array list that will hold the filtered data
        List<CharitySelection_Modal> filterdNames = new ArrayList<CharitySelection_Modal>();
        //looping through existing elements
        for (CharitySelection_Modal s : newcharityArraylist) {
            //if the existing elements contains the search input
            if (s.getCharityName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }
        Log.e("filter", "filter: "+text);
        Log.e("filter", "filterdNames: "+filterdNames.size());
        //calling a method of the adapter class and passing the filtered list
        setAdapter(filterdNames);
    }
}
