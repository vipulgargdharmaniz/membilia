package com.membilia.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.membilia.BaseActivity;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.Constants;
import com.membilia.Util.Countries;
import com.membilia.Util.DatabaseHandler;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.dbModal.LoginUser;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.CountryModel;
import com.membilia.volley_models.FbFriendsModel;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import retrofit2.Call;
/*
 * Created by dharmaniz on 24/5/17.
 */

public class SelectionActivity extends BaseActivity {
    /*Twitter*/
    public String TAG = "SelectionActivity";
    private String fbUserID, fbToken, tUsername, tToken, tSecret, tUser_id;
    private static String strFullName;
    Activity mActivity = SelectionActivity.this;
    Resources mResources;
    private TextView SignupTV, SignInTV, logintwitterBT, loginFacebookBT;
    ArrayList<FbFriendsModel> fbFriendsArrayList = new ArrayList<>();
    private String twitterImage, twitterDescription;
    private String strUserProfilePic = "", twitterUserName = "", twitterDesc = "";
    //Twitter
    TwitterLoginButton twitter_login_button;
    private String fbUserName = "";
    private boolean login = false;
    boolean isTwitterVerified = false;

    String facebookDescription = "";

    /*Facebook*/
    private CallbackManager callbackManager;

    /* branch io variables*/
    private String branchUserID = "";
    private String verifyString = WebServicesConstants.BASE_URL + "verify.php?";
    private String strCountriesInfo = "";
    private ArrayList<CountryModel> countriesArrayList = new ArrayList<>();
    String TwitterLocatiom = "";
    String deviceToken;
    public static DatabaseHandler db;


    @Override
    protected void onStart() {
        super.onStart();
        /*Facebook Integration*/
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("lifecycle", "onNewIntent: " + intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SelectionActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                deviceToken = instanceIdResult.getToken();
                Log.e(TAG, "onSuccess: "+deviceToken );
            }
        });

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_selection);

        setUpViews();
        setUpClickListeners();




        db = new DatabaseHandler(this);
        fbFriendsArrayList = AppSingleton.getInstance().getFbFriendsArrayList();
        mResources = mActivity.getResources();

        LoginManager.getInstance().logOut();

        SharedPreferences preferences = PlayerPawnPreference.getPreferences(SelectionActivity.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.DeviceToken, deviceToken);
        Branch branch = Branch.getInstance(getApplicationContext());
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    try {
                        Log.e(TAG, "onInitFinished: " + referringParams);
                        if (referringParams.length() > 2) {
                            branchUserID = referringParams.getString("userId");
                            if (branchUserID != null) {
                                Log.e(TAG, "********verifyBranch******" + verifyString);
                                verifyString = verifyString + "user_id=" + branchUserID + "&device_type=1&device_token=" + deviceToken;
                                StringRequest request = new StringRequest(Request.Method.POST, verifyString, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e(TAG, "***********RESPONSE*********: " + response);
                                        parseResponse(response);
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e(TAG, "onErrorResponse: " + error.toString());
                                    }
                                }) {
                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        return super.getHeaders();
                                    }
                                };
                                PlayerPawnApplication.getInstance().addToRequestQueue(request);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.i("MyApp", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }


    protected void setUpViews() {
        SignInTV = findViewById(R.id.signintv);
        SignupTV = findViewById(R.id.signupTV);
        logintwitterBT = findViewById(R.id.logintwitterBT);
        loginFacebookBT = findViewById(R.id.loginFacebookBT);
        twitter_login_button = findViewById(R.id.twitter_login_button);
    }

    private boolean checkPermission() {
        int location1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int location2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int readContact = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStorage = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return location1 == PackageManager.PERMISSION_GRANTED && location2 == PackageManager.PERMISSION_GRANTED
                && readContact == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED &&
                writeStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                break;
        }
    }


    protected void setUpClickListeners() {
        SignInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, SigninActivity.class));
                overridePendingTransition(R.anim.in_from_left, R.anim.animback);

            }
        });

        SignupTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, SignupActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.animback);
            }
        });


        logintwitterBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.LoginType, "social");
                if (checkPermission()) {
                    loginTwitter();
                    twitter_login_button.performClick();
                } else {
                    requestPermission();
                }
            }
        });

        loginFacebookBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.LoginType, "social");
                if (checkPermission()) {
                    login = false;
                    loginWithFacebook();
                } else {
                    requestPermission();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        strCountriesInfo = Countries.COUNTRIES_INFO;
        getCountriesModel();
    }



    private void getCountriesModel() {
        try {
            JSONArray jsonArray = new JSONArray(strCountriesInfo);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CountryModel mModel = new CountryModel();
                mModel.setName(jsonObject.getString("name"));
                mModel.setNameImage(jsonObject.getString("nameImage"));
                mModel.setDailCode(jsonObject.getString("dial_code"));
                mModel.setCode(jsonObject.getString("code"));
                if (!jsonObject.isNull("Language")) {
                    mModel.setLanguage(jsonObject.getString("Language"));
                }
                countriesArrayList.add(mModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loginTwitter() {

        /*SetUp Twitter Login*/
        twitter_login_button.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                tUsername = session.getUserName();
                tToken = authToken.token;
                tSecret = authToken.secret;
                tUser_id = String.valueOf(session.getUserId());
                getUserData(result);
                login = true;
            }

            @Override
            public void failure(TwitterException exception) {
                exception.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                LoginManager.getInstance().logOut();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object,
                                                    GraphResponse response) {
                                Log.i("loginResult", response.toString());
                                try {
                                    fbUserName = object.getString("name");
                                    fbUserName = fbUserName.replace(" ", "%20");
                                    fbUserName = fbUserName.substring(0, 1).toUpperCase() + fbUserName.substring(1, fbUserName.length());
                                    Log.e(TAG, "fbUserName: '" + fbUserName);
                                    strFullName = object.getString("name");
                                    executeLoginWithFacebook();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields",
                        "id,name,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
                ///////////////
                fbUserID = loginResult.getAccessToken().getUserId();
                fbToken = loginResult.getAccessToken().getToken();

                strUserProfilePic = "https://graph.facebook.com/" + fbUserID + "/picture?type=large";
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "Login attempt cancelled.");
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
            }
        });
    }

    private void getFbProfile(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
//                         initiateLoginWithFB(object);
                        Log.e(TAG, "onCompleted: " + object);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

//        twitter_login_button.onActivityResult(requestCode, resultCode, data);


        final TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
        if (twitterAuthClient.getRequestCode() == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void executeLoginWithFacebook() {
        Log.e(TAG, "executeLoginWithFacebook: "+fbToken );
        Log.e(TAG, "executeLoginWithFacebook: "+deviceToken );
        if (deviceToken.equalsIgnoreCase("")) {
            Toast.makeText(mActivity, "no device token", Toast.LENGTH_SHORT).show();
        }
        Utilities.showProgressDialog(mActivity);
        PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.LoginType, "social");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServicesConstants.FACEBOOK_LOGIN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("*****EXCUTE_FB*****", response);
                        Utilities.hideProgressDialog();
                        parseResponse(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            // EAADk72AuZA2wBADHKySuLtFZCQLOXG7ZATROZC95ZBcLBfYtGqYJ6UUZC3zolwXYNnBtXelSZBD0t5Bm5qW9pCZATFzEzDsvTtadeXiS4QTknQZAXZCqZBWPxt6Yc0VkQz0IPiJiO5o5ZBKKdVptxhRslUbyXvIaJHzDKNNwXW0Pxa2j16LHQOIy9xEmTuIHhqWkU8WWwhcBXTFPPYZAUSU6RmuIo
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Log.i(TAG, "*****PARAMS*****: " + fbUserID + "...." + fbToken + "...."
                        + fbUserName + "............" + strUserProfilePic);
                params.put("token", fbToken);
                params.put("user_id", fbUserID);
                params.put("device_token", deviceToken);
                params.put("device_type", "0");
                params.put("name", fbUserName);
                params.put("image_url", strUserProfilePic);
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "LoginWithFacebookRequest");
    }

    private void parseResponse(String res) {
        try {
            JSONObject jsonObject = new JSONObject(res);
            Log.e(TAG, "parseResponse: " + jsonObject.getInt("expiration"));
            PlayerPawnPreference.writeInteger(SelectionActivity.this, PlayerPawnPreference.VALUE_EXPIRATION, jsonObject.getInt("expiration"));
            PlayerPawnPreference.writeBoolean(SelectionActivity.this, PlayerPawnPreference.VALUE_SIGN_UP, jsonObject.getBoolean("signup"));
            PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.VALUE_TOKEN, jsonObject.getString("token"));
            PlayerPawnPreference.writeInteger(SelectionActivity.this, PlayerPawnPreference.VALUE_COUNT, jsonObject.getInt("Count"));
            PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.VALUE_USER_ID, jsonObject.getString("user_id"));
            PlayerPawnPreference.writeBoolean(SelectionActivity.this, PlayerPawnPreference.IsVerified, jsonObject.getBoolean("isVerified"));
            SelectionActivity.db.removeName();
            SelectionActivity.db.addName(strFullName);
            PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.USERNAME, strFullName);
            if (jsonObject.has("description")){
                facebookDescription = jsonObject.getString("description");
            }
            Log.e(TAG, "parseResponse: "+ facebookDescription);
            executefacbookAPIDetails(strFullName);
            ArrayList<LoginUser> loginUsersList = new ArrayList<>();
            LoginUser loginModal = new LoginUser();
            loginModal.setUserToken(jsonObject.getString("token"));
            loginModal.setUserId(jsonObject.getString("user_id"));
            loginUsersList.add(loginModal);
            db.addUser(loginUsersList);

            if (jsonObject.getString("user_id").length() > 0) {
                if (jsonObject.getInt("Count") <= 5) {
                    Intent intent = new Intent(mActivity, InviteActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.LOGIN_TYPE, "facebook");
                    startActivity(intent);
                    finish();

                } else {
                    PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.LOGIN_TYPE, "facebook");
                    startActivity(intent);
                    finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executefacbookAPIDetails(String strName) {
        String url = WebServicesConstants.UPDATE_USER;
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonLocation = new JSONObject();
        try {
            Log.e(TAG, "executefacbookAPIDetails: strName" + strName);
            jsonObject.put("name", strName);
                jsonObject.put("description", facebookDescription);
            jsonLocation.put("administrative_area_level_1", "");
            jsonLocation.put("country", TwitterLocatiom);
            jsonLocation.put("lat", "");
            jsonLocation.put("lng", "");
            jsonLocation.put("locality", "");
            jsonLocation.put("postal_code", "");
            jsonObject.put("location", jsonLocation);
            jsonObject.put("verified", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "executefacbookAPIDetails: " + jsonObject);
        JsonObjectRequest jsonsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(SelectionActivity.this, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };

        // Adding request to request queue
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonsonObjReq, "UploadUserDetailsRequest");
    }

    public void getUserData(final Result<TwitterSession> result) {
        final TwitterSession session = result.data;
        Call<User> userResult = TwitterCore.getInstance().getApiClient(session).getAccountService().verifyCredentials(true, false, false);
        userResult.enqueue(new Callback<User>() {
            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
            }
            @Override
            public void success(Result<User> userResult) {
                Log.e(TAG, "userResult: " + userResult);
                User user = userResult.data;
                twitterImage = user.profileImageUrl.replace("_normal", "");
                twitterDescription = user.description;
                try {
                    isTwitterVerified = user.verified;
                    tUsername = user.name;
                    int usernameLength = tUsername.length();
                    try {
                        tUsername = tUsername.substring(0, 1).toUpperCase() + tUsername.substring(1, usernameLength);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    twitterUserName = user.name;
                    twitterDesc = user.description;
                    for (int i = 0; i < countriesArrayList.size(); i++) {
                        if (user.location.contains(countriesArrayList.get(i).getNameImage())) {
                            TwitterLocatiom = countriesArrayList.get(i).getNameImage();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                executeLoginWithTwitter();
            }
        });
    }

    private void executeLoginWithTwitter() {
        if (deviceToken.equalsIgnoreCase("")) {
            Toast.makeText(mActivity, "no device token", Toast.LENGTH_SHORT).show();
        }
        Utilities.showProgressDialog(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServicesConstants.TWITTER_LOGIN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "Twitter_Response: " + response);
                        parseResponseForTwitter(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("token", tToken);
                params.put("user_id", tUser_id);
                params.put("secret", tSecret);
                params.put("device_token", deviceToken);
                params.put("device_type", "0");
                params.put("name", twitterUserName);
                params.put("image_url", twitterImage);
                params.put("description", twitterDesc);
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "loginwithTwitter");
    }

    private void parseResponseForTwitter(String res) {
        try {
            JSONObject jsonObject = new JSONObject(res);
            PlayerPawnPreference.writeInteger(SelectionActivity.this, PlayerPawnPreference.VALUE_EXPIRATION, jsonObject.getInt("expiration"));
            PlayerPawnPreference.writeBoolean(SelectionActivity.this, PlayerPawnPreference.VALUE_SIGN_UP, jsonObject.getBoolean("signup"));
            PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.VALUE_TOKEN, jsonObject.getString("token"));
            PlayerPawnPreference.writeInteger(SelectionActivity.this, PlayerPawnPreference.VALUE_COUNT, jsonObject.getInt("Count"));
            PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.VALUE_USER_ID, jsonObject.getString("user_id"));

            ArrayList<LoginUser> loginUsersList = new ArrayList<>();
            LoginUser loginModal = new LoginUser();
            loginModal.setUserToken(jsonObject.getString("token"));
            loginModal.setUserId(jsonObject.getString("user_id"));
            loginUsersList.add(loginModal);
            db.addUser(loginUsersList);

            executeTwitterUpdates(tUsername, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void executeTwitterUpdates(String strName, final JSONObject resultObject) {
        int passVerified = 1;
        if (isTwitterVerified) {
            passVerified = 0;
        }
        SelectionActivity.db.removeName();
        SelectionActivity.db.addName(strName);
        PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.USERNAME, strName);

        String url = WebServicesConstants.UPDATE_USER;
        final JSONObject jsonObject = new JSONObject();
        JSONObject jsonLocation = new JSONObject();
        try {
            jsonObject.put("name", strName);
            if (login) {
                jsonObject.put("description", twitterDescription);
            } else {
                jsonObject.put("description", "");
            }
            jsonLocation.put("administrative_area_level_1", "");
            jsonLocation.put("country", TwitterLocatiom);
            jsonLocation.put("lat", "");
            jsonLocation.put("lng", "");
            jsonLocation.put("locality", "");
            jsonLocation.put("postal_code", "");
            jsonObject.put("location", jsonLocation);
            jsonObject.put("verified", passVerified);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "JsonForUpdateUser: " + jsonObject);
        JsonObjectRequest jsonsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {


                            if (response.has("isVerified")) {
                                Log.e(TAG, "isVerified: " + resultObject.getBoolean("isVerified"));
                                boolean verify = resultObject.getBoolean("isVerified");
                                PlayerPawnPreference.writeBoolean(SelectionActivity.this, PlayerPawnPreference.IsVerified, verify);
                            }
                            if (resultObject.getString("user_id").length() > 0) {
                                try {
                                    if (resultObject.getInt("Count") <= 5) {
                                        Intent intent = new Intent(mActivity, InviteActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.putExtra(Constants.LOGIN_TYPE, "twitter");
                                        startActivity(intent);
                                        finish();
                                        Utilities.hideProgressDialog();
                                    } else {
                                        PlayerPawnPreference.writeString(SelectionActivity.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
                                        Intent intent = new Intent(mActivity, HomeActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.putExtra(Constants.LOGIN_TYPE, "twitter");
                                        startActivity(intent);
                                        finish();
                                        Utilities.hideProgressDialog();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(SelectionActivity.this, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        // Adding request to request queue
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonsonObjReq, "UpdateUser");
    }

    /*public void TwitterLogout() {
       *//* TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (twitterSession != null) {
            ClearCookies(getApplicationContext());
//            Twitter.getSessionManager().clearActiveSession();
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
            Twitter.logOut();
        }*//*
        CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
        TwitterCore.getInstance().getSessionManager().clearActiveSession();
    }*/
}
