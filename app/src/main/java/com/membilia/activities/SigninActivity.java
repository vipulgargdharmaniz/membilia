package com.membilia.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.membilia.BaseActivity;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.DatabaseHandler;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.dbModal.LoginUser;
import com.membilia.volley.UtilsVolley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SigninActivity extends BaseActivity {
    public String TAG = "SigninActivity";
    Activity mActivity = SigninActivity.this;
    Resources mResources;
    EditText emailET, passwordET;
    RelativeLayout backRL, mainRv;
    TextView forgotpasswordtv, signinTV;
    String strEmail, strPassword;
    String deviceToken;
    String authToken;
    public static DatabaseHandler db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mResources = mActivity.getResources();
        db = new DatabaseHandler(this);
        Log.i(TAG, "onCreate: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
//        setStatusBarGradiant(SigninActivity.this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SigninActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                deviceToken = instanceIdResult.getToken();

            }
        });
        Log.e(TAG, "onResume: "+deviceToken );
    }

    @Override
    protected void setUpViews() {
        forgotpasswordtv = findViewById(R.id.forgotpasswordTV);
        backRL = findViewById(R.id.backRL);
        mainRv = findViewById(R.id.mainRv);
        signinTV = findViewById(R.id.signinTV);
        emailET = findViewById(R.id.emailET);
        passwordET = findViewById(R.id.passwordET);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(R.color.black));
            window.setBackgroundDrawable(background);
        }
    }

    public void grantpermission() {
        if (checkPermission2()) {
            startActivity(new Intent(SigninActivity.this, SelectionActivity.class));
            finish();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission2() {
        int location1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int location2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int readContact = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStorage = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return location1 == PackageManager.PERMISSION_GRANTED && location2 == PackageManager.PERMISSION_GRANTED
                && readContact == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED &&
                writeStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0) {
                    boolean fineES = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean coarseES = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean contactES = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    boolean readES = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean writeES = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    if (fineES && coarseES && contactES && readES && writeES ) {
                        // write your logic here
                        executeSignInAPI();
                    } else {
                        Toast.makeText(mActivity, getString(R.string.goto_settings), Toast.LENGTH_SHORT).show();
                    }
                }

                break;
        }
    }

    @Override
    protected void setUpClickListeners() {

        forgotpasswordtv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, RestorePasswordActivity.class));
            }
        });

        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        signinTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strEmail = emailET.getText().toString();
                strPassword = passwordET.getText().toString();
                if (strEmail.equals("") && strPassword.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.allfields));
                } else if (strEmail.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.pleaseenteremail));
                } else if (!Utilities.emailValidator(strEmail)) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.isvalidemail));
                } else if (strPassword.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.pleaseenterpwed));
                } else {
                    /*Execute API*/
                    if (checkPermission2()) {
                        executeSignInAPI();
                    } else {
                        requestPermission();
                    }

                }
            }
        });
        mainRv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(mActivity, v);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private void executeSignInAPI() {
        if (deviceToken.equalsIgnoreCase("")) {
            Toast.makeText(mActivity, "no device token", Toast.LENGTH_SHORT).show();
        }
        Utilities.showProgressDialog(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServicesConstants.SIGN_IN,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "onResponse: " + response);
                        String userId = "";
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            userId = jsonObject.getString("user_id");
                            authToken = jsonObject.getString("token");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        executeProfileData(response, userId, authToken);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                String json;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null) {
                                if (json.contains("wrong Username")) {
                                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), "Wrong username or password");

                                } else {
                                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);

                                }
                            }
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", strEmail);
                params.put("password", strPassword);
                params.put("device_token", deviceToken);
//                params.put("device_token", PlayerPawnPreference.readString(SigninActivity.this, PlayerPawnPreference.VALUE_TOKEN, ""));
                params.put("device_type", "0");
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "SignInRequest");
    }


    private void executeProfileData(final String signupResponse, String userid, final String authToken) {
        String strUrl = WebServicesConstants.USER_PROFILE + userid;
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject profileJson = new JSONObject(response);
                    PlayerPawnPreference.writeString(SigninActivity.this, PlayerPawnPreference.USERNAME, profileJson.getString("name"));
                    SigninActivity.db.removeName();
                    SigninActivity.db.addName(profileJson.getString("name"));
                    if (profileJson.has("isVerified")) {
                        Log.e(TAG, "isVerified: " + profileJson.getBoolean("isVerified"));
                        boolean verify = profileJson.getBoolean("isVerified");
                        PlayerPawnPreference.writeBoolean(SigninActivity.this, PlayerPawnPreference.IsVerified, verify);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                parseResponse(signupResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(SigninActivity.this, getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", authToken);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "ProfileRequest");
    }

    private void parseResponse(String res) {
        JSONObject jsonObject;
        boolean gmailVerified = false;
        int Count = 0;
        PlayerPawnPreference.writeString(SigninActivity.this, PlayerPawnPreference.LoginType, "manual");
        try {
            jsonObject = new JSONObject(res);
            PlayerPawnPreference.writeInteger(mActivity, PlayerPawnPreference.VALUE_EXPIRATION, jsonObject.getInt("expiration"));
            PlayerPawnPreference.writeBoolean(mActivity, PlayerPawnPreference.VALUE_SIGN_UP, jsonObject.getBoolean("signup"));
            PlayerPawnPreference.writeString(mActivity, PlayerPawnPreference.VALUE_TOKEN, jsonObject.getString("token"));
            PlayerPawnPreference.writeString(mActivity, PlayerPawnPreference.VALUE_USER_ID, jsonObject.getString("user_id"));
            ArrayList<LoginUser> loginUsersList = new ArrayList<>();
            LoginUser loginModal = new LoginUser();
            loginModal.setUserToken(jsonObject.getString("token"));
            loginModal.setUserId(jsonObject.getString("user_id"));
            loginUsersList.add(loginModal);
            db.addUser(loginUsersList);

            if (jsonObject.has("verify_account")) {
                if (jsonObject.getString("verify_account").equalsIgnoreCase("1"))
                    gmailVerified = true;
            }
            if (jsonObject.has("Count")) {
                Count = Integer.parseInt(jsonObject.getString("Count"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (!gmailVerified) {
            AlertDialogManager.showAlertDialogOK(SigninActivity.this, getString(R.string.app_name), "Please verify your account from email!");
        } else {
            if (Count < 5) {
                Intent intent = new Intent(mActivity, InviteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.LOGIN_TYPE, "email");
                startActivity(intent);
                finish();
                Utilities.hideProgressDialog();
            } else {
                PlayerPawnPreference.writeString(SigninActivity.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
                Intent intent = new Intent(mActivity, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.LOGIN_TYPE, "email");
                startActivity(intent);
                finish();
                Utilities.hideProgressDialog();
            }

        }
    }

    public  void showAlertDialog(String strMessage, final int count) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_showalert);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle =  alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage =  alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss =  alertDialog.findViewById(R.id.txtDismiss);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

}
