package com.membilia.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.membilia.BaseActivity;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.Countries;
import com.membilia.Util.DatabaseHandler;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.dbModal.LoginUser;
import com.membilia.fonts.TextViews.TextViewRaleway_Light;
import com.membilia.receiver.ConnectivityReceiver;
import com.membilia.volley.AppHelper;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley.VolleyMultipartRequest;
import com.membilia.volley_models.CountryModel;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;

/*
 * Created by dharmaniz on 25/5/17.
 */

public class SignupActivity extends BaseActivity {
    private static final int PERMISSIONS_MULTIPLE_REQUEST = 11;
    private static final int CAMERA_REQUEST = 111;
    private static final int GALLERY_REQUEST = 222;
    public String TAG = "SignupActivity";
    public Bitmap mBitmapImage;
    Activity mActivity = SignupActivity.this;
    Resources mResources;
    RelativeLayout backRL;
    CircleImageView profileIV;
    EditText emailET, firstnameET, lastnameET, passwordET, confirmpasswordET;
    TextView signupTV, termsandcondition;
    String strEmail, strFirstName, strLastName, strPassword, strConfirmPwd, strDeviceToken, strFullName, strDescription = "";
    ConnectivityReceiver mReceiver;
    File finalFile = null;
    String strEncodedImage64 = "";
    byte imageByteArray[];
    Bitmap imageData = null;
    private boolean login = false;

    private TextViewRaleway_Light logintwitterBT, loginFacebookBT;
    private TwitterLoginButton twitter_login_button;

    private String fbUserID, fbToken, tUsername, tToken, tSecret, tUser_id;
    private String twitterImage, twitterDescription;
    private CallbackManager callbackManager;
    private com.facebook.login.widget.LoginButton login_button_faceook;

    private String whichPermission = "";
    private String strUserProfilePic = "";
    private String strCountriesInfo = "";
    private ArrayList<CountryModel> countriesArrayList = new ArrayList<>();
    String TwitterLocatiom = "";

    String Token;
    public static DatabaseHandler db;

    String deviceToken = "";

    String strDesc = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsignup);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        mResources = mActivity.getResources();
        mReceiver = new ConnectivityReceiver();

        db = new DatabaseHandler(this);
        Twitter.initialize(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
//        setStatusBarGradiant(SignupActivity.this);


        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(SignupActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String mToken = instanceIdResult.getToken();
                deviceToken = mToken;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        strCountriesInfo = Countries.COUNTRIES_INFO;
        getCountriesModel();
    }

    private void getCountriesModel() {
        try {
            JSONArray jsonArray = new JSONArray(strCountriesInfo);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CountryModel mModel = new CountryModel();
                mModel.setName(jsonObject.getString("name"));
                mModel.setNameImage(jsonObject.getString("nameImage"));
                mModel.setDailCode(jsonObject.getString("dial_code"));
                mModel.setCode(jsonObject.getString("code"));
                if (!jsonObject.isNull("Language")) {
                    mModel.setLanguage(jsonObject.getString("Language"));
                }
                countriesArrayList.add(mModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.top_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(R.color.black));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    protected void setUpViews() {
        backRL = findViewById(R.id.backRL);

        profileIV = findViewById(R.id.profileIV);
        emailET = findViewById(R.id.emailET);
        firstnameET = findViewById(R.id.firstnameET);
        lastnameET = findViewById(R.id.lastnameET);
        passwordET = findViewById(R.id.passwordET);
        confirmpasswordET = findViewById(R.id.confirmpasswordET);
        signupTV = findViewById(R.id.signupTV);
        termsandcondition = findViewById(R.id.termsandcondition);

        logintwitterBT = findViewById(R.id.logintwitterBT);
        loginFacebookBT = findViewById(R.id.loginFacebookBT);

        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "Refreshed token: " + refreshedToken);
            strDeviceToken = refreshedToken;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void setUpClickListeners() {
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        profileIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAndroidPermissions();
            }
        });

        signupTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strEmail = emailET.getText().toString();
                firstnameET.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                strFirstName = firstnameET.getText().toString();
                strLastName = lastnameET.getText().toString();
                strPassword = passwordET.getText().toString();
                strConfirmPwd = confirmpasswordET.getText().toString();

                strFullName = strFirstName + " " + strLastName;
                if (strEmail.equals("") && strFirstName.equals("") && strLastName.equals("") && strPassword.equals("") && strConfirmPwd.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.allfields));
                } else if (strEmail.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.pleaseenteremail));
                } else if (!Utilities.emailValidator(strEmail)) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.isvalidemail));
                } else if (strFirstName.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.pleaseenterfirstname));
                } else if (strLastName.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.pleaseenterlastname));
                } else if (strPassword.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.pleaseenterpwed));
                } else if (strPassword.length() < 8) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.passwordlengthsmall));
                } else if (strConfirmPwd.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.confirmpleaseenter));
                } else if (strConfirmPwd.length() < 8) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.passwordlengthsmall));
                } else if (!strConfirmPwd.equals(strPassword)) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.confirmpwddosenotmatch));
                } else if (finalFile == null) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.allfields));
                } else {
                    if (!ConnectivityReceiver.isConnected()) {
                        AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.allfields));
                    } else {
                       /*Execute API*/
                        executeSignUpAPI();
                    }
                }
            }
        });


        logintwitterBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginTwitter();
                twitter_login_button.performClick();
            }
        });

        loginFacebookBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
                loginWithFacebook();
            }
        });

    }

    private void loginWithFacebook() {
        login_button_faceook = findViewById(R.id.login_button_faceook);
        login_button_faceook.setReadPermissions("user_friends");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                LoginManager.getInstance().logOut();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object,
                                                    GraphResponse response) {
                                Log.i("LoginActivity", response.toString());
                                try {
                                    Log.e(TAG, "facebook respose: " + object);
                                    strFullName = object.getString("name");
                                    strEmail = object.getString("email");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields",
                        "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();

                Log.e(TAG, "User ID:  " + loginResult.getAccessToken().getUserId() + "\n" + "Auth Token: " + loginResult.getAccessToken().getToken());
//                getFbProfile(loginResult.getAccessToken());
//                getFbfriends((loginResult.getAccessToken()));
                fbUserID = loginResult.getAccessToken().getUserId();
                fbToken = loginResult.getAccessToken().getToken();
                strUserProfilePic = "https://graph.facebook.com/" + fbUserID + "/picture?type=large";
//                Log.e(TAG, "onSuccess:strUserProfilePic " + strUserProfilePic);
                /*Execute Login With Facebook API*/
                executeLoginWithFacebook();
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "Login attempt cancelled.");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e(TAG, "Login attempt failed.");
            }
        });
    }

    private void executeLoginWithFacebook() {
        if (deviceToken.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.app_name), "no device token");
        }
        Utilities.showProgressDialog(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServicesConstants.FACEBOOK,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "onResponse: " + response);
                        parseResponse(response, "facebook");
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Utilities.hideProgressDialog();
                Log.e(TAG, "****onErrorResponse****" + error.toString());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", fbUserID);
                params.put("token", fbToken);
                params.put("device_token", deviceToken);
                params.put("device_type", "0");
                params.put("image_url", strUserProfilePic);
                return params;
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "LoginWithFacebookRequest");
    }


    private void getFbProfile(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
//                         initiateLoginWithFB(object);
                        PlayerPawnPreference.writeString(SignupActivity.this, PlayerPawnPreference.FACEBOOK_TOKEN_ACCESS, accessToken.toString());
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link, email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getFbfriends(final AccessToken accessToken) {
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + fbUserID + "/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                        Log.e(TAG, "**********REsPONCE*********" + response.toString());
                    }
                }
        ).executeAsync();
    }

    private void loginTwitter() {
        twitter_login_button = findViewById(R.id.twitter_login_button);
        /*SetUp Twitter Login*/
        twitter_login_button.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                tUsername = session.getUserName();
                tToken = authToken.token;
                tSecret = authToken.secret;
                tUser_id = String.valueOf(session.getUserId());
                getUserData(result);
                login = true;
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });
    }


    public void getUserData(Result<TwitterSession> result) {
        TwitterSession session = result.data;
        final String username = session.getUserName();
        Call<User> userResult = TwitterCore.getInstance().getApiClient(session).getAccountService().verifyCredentials(true, false, false);
        userResult.enqueue(new Callback<User>() {
            @Override
            public void failure(TwitterException e) {
                Log.e(TAG, "******ERROR******" + e.toString());
            }

            @Override
            public void success(Result<User> userResult) {
                User user = userResult.data;
                twitterImage = user.profileImageUrl.replace("_normal", "");
                twitterDescription = user.description;
                try {
                    for (int i = 0; i < countriesArrayList.size(); i++) {
                        if (user.location.contains(countriesArrayList.get(i).getNameImage())) {
                            Log.e(TAG, "TwitterLocatiom: " + TwitterLocatiom);
                            TwitterLocatiom = countriesArrayList.get(i).getNameImage();
                        }
                    }
                    if (user.email != null) {
                        Log.d("email", user.email);
                    }
                    if (user.description != null) {
                        Log.d("des", user.description);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                executeLoginWithTwitter();
            }
        });
    }

    private void executeLoginWithTwitter() {
        if (deviceToken.equalsIgnoreCase("")) {
            Toast.makeText(mActivity, "no device token", Toast.LENGTH_SHORT).show();
        }
        Utilities.showProgressDialog(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServicesConstants.TWITTER,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utilities.hideProgressDialog();
                        Log.e(TAG, "onResponse: " + response);
                        parseResponseForTwitter(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error);
                Utilities.hideProgressDialog();
                Log.e(TAG, "****onErrorResponse****" + error.toString());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", tUser_id);
                params.put("token", tToken);
                params.put("device_token", deviceToken);
                params.put("device_type", "0");
                params.put("secret", tSecret);
                params.put("image_url", twitterImage);
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "LoginWithFacebookRequest");
    }

    private void parseResponseForTwitter(String res) {
        try {
            JSONObject jsonObject = new JSONObject(res);
            PlayerPawnPreference.writeInteger(SignupActivity.this, PlayerPawnPreference.VALUE_EXPIRATION, jsonObject.getInt("expiration"));
            PlayerPawnPreference.writeBoolean(SignupActivity.this, PlayerPawnPreference.VALUE_SIGN_UP, jsonObject.getBoolean("signup"));
            PlayerPawnPreference.writeString(SignupActivity.this, PlayerPawnPreference.VALUE_TOKEN, jsonObject.getString("token"));
            PlayerPawnPreference.writeInteger(SignupActivity.this, PlayerPawnPreference.VALUE_COUNT, jsonObject.getInt("Count"));
            PlayerPawnPreference.writeString(SignupActivity.this, PlayerPawnPreference.VALUE_USER_ID, jsonObject.getString("user_id"));
            executeTwitterAPIDetails(tUsername);

            ArrayList<LoginUser> loginUsersList = new ArrayList<>();
            LoginUser loginModal = new LoginUser();
            loginModal.setUserToken(jsonObject.getString("token"));
            loginModal.setUserId(jsonObject.getString("user_id"));
            loginUsersList.add(loginModal);
            db.addUser(loginUsersList);

            if (jsonObject.getString("user_id").length() > 0) {
                if (jsonObject.getInt("Count") <= 5) {
                    Intent intent = new Intent(mActivity, InviteActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.LOGIN_TYPE, "twitter");
                    startActivity(intent);
                    finish();
                } else {
                    PlayerPawnPreference.writeString(SignupActivity.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.LOGIN_TYPE, "twitter");
                    startActivity(intent);
                    finish();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void executeTwitterAPIDetails(String strName) {
        String url = WebServicesConstants.UPDATE_USER;
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonLocation = new JSONObject();
        SignupActivity.db.removeName();
        SignupActivity.db.addName(strName);
        PlayerPawnPreference.writeString(SignupActivity.this, PlayerPawnPreference.USERNAME, strName);
        try {
            jsonObject.put("name", strName);
            if (login) {
                jsonObject.put("description", twitterDescription);
            } else {
                jsonObject.put("description", "");
            }
            jsonLocation.put("administrative_area_level_1", "");
            jsonLocation.put("country", TwitterLocatiom);
            jsonLocation.put("lat", "");
            jsonLocation.put("locality", "");
            jsonLocation.put("postal_code", "");
            jsonObject.put("location", jsonLocation);
            jsonObject.put("verified", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
//                        executeUploadImageAPI();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(SignupActivity.this, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        // Adding request to request queue
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonsonObjReq, "UploadUserDetailsRequest");
    }


    public void executeUploadImageAPI(final boolean isVerified, final String signupType) {
        Log.e(TAG, "executeUploadImageAPI: " + mBitmapImage);
        if (signupType.equalsIgnoreCase("facebook")) {
            try {
                URL url = new URL(strUserProfilePic);
                mBitmapImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        Log.e(TAG, "executeUploadImageAPI: " + mBitmapImage);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, WebServicesConstants.UPLOAD_IMAGE_ON_SERVER, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Utilities.hideProgressDialog();
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    String message = result.getString("message");
                    Log.e(TAG, "*******IMAGE UPLOAD SUCCESSFULLY******");

                    if (isVerified) {
                        Intent intent = new Intent(mActivity, InviteActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.LOGIN_TYPE, "email");
                        startActivity(intent);
                        finish();
                    } else if (!isVerified && signupType.equalsIgnoreCase("signup")) {
                        showAlertDialogOK(SignupActivity.this, "Membilia", "We have sent a verification email to your provided email address. Please verify to complete sign in");
//            Toast.makeText(SignupActivity.this, "Please check email", Toast.LENGTH_SHORT).show();
                    } else if (!isVerified && signupType.equalsIgnoreCase("facebook")) {
                        Intent intent = new Intent(mActivity, InviteActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(Constants.LOGIN_TYPE, "email");
                        startActivity(intent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Utilities.hideProgressDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);
                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", error.toString());
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("image", new DataPart("file_avatar.jpg", AppHelper.getFileDataFromBitmap(SignupActivity.this, mBitmapImage), "image/jpeg"));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(multipartRequest, "UploadImageRequest");
    }

    private void executeSignUpAPIDetails(final boolean isVerified, final String signupType) {
        String url = WebServicesConstants.UPDATE_USER;
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonLocation = new JSONObject();
        SignupActivity.db.removeName();
        SignupActivity.db.addName(strFullName);
        PlayerPawnPreference.writeString(SignupActivity.this, PlayerPawnPreference.USERNAME, strFullName);

        try {
            jsonObject.put("name", strFullName);
            jsonObject.put("description", strDesc);
            jsonLocation.put("administrative_area_level_1", "");
            jsonLocation.put("country", "");
            jsonLocation.put("lat", "");
            jsonLocation.put("locality", "");
            jsonLocation.put("postal_code", "");
            jsonObject.put("location", jsonLocation);
            jsonObject.put("verified", "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse:UPDATE_USER " + response);
                        executeUploadImageAPI(isVerified, signupType);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Utilities.hideProgressDialog();
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", Token);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonsonObjReq, "UploadUserDetailsRequest");
    }


    private void executeSignUpAPI() {
        Utilities.showProgressDialog(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServicesConstants.SIGN_UP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "onResponse:executeSignUpAPI " + response);
                        parseResponse(response, "signup");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Utilities.hideProgressDialog();
                Log.e(TAG, "****onErrorResponse****" + error.toString());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", strEmail);
                params.put("password", strPassword);
                params.put("device_token", strDeviceToken);
                params.put("device_type", "0");
                params.put("name", strFullName);
                params.put("verified", "0");
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "SignUpRequest");
    }


    private void parseResponse(String res, String signupType) {
        boolean isVerified = false;
        try {
            JSONObject jsonObject = new JSONObject(res);
            if (signupType.equalsIgnoreCase("facebook")) {
                PlayerPawnPreference.writeInteger(mActivity, PlayerPawnPreference.VALUE_EXPIRATION, jsonObject.getInt("expiration"));
                PlayerPawnPreference.writeBoolean(mActivity, PlayerPawnPreference.VALUE_SIGN_UP, jsonObject.getBoolean("signup"));
                PlayerPawnPreference.writeString(mActivity, PlayerPawnPreference.VALUE_TOKEN, jsonObject.getString("token"));
                PlayerPawnPreference.writeString(mActivity, PlayerPawnPreference.VALUE_USER_ID, jsonObject.getString("user_id"));
                Token = jsonObject.getString("token");


                ArrayList<LoginUser> loginUsersList = new ArrayList<>();
                LoginUser loginModal = new LoginUser();
                loginModal.setUserToken(jsonObject.getString("token"));
                loginModal.setUserId(jsonObject.getString("user_id"));
                loginUsersList.add(loginModal);
                db.addUser(loginUsersList);

                if (jsonObject.has("description")) {
                    strDesc = jsonObject.getString("description");
                }

            } else {
                if (!jsonObject.getString("verify_account").equalsIgnoreCase("0")) {
                    isVerified = true;
                }
            }
            Token = jsonObject.getString("token");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        executeSignUpAPIDetails(isVerified, signupType);
    }

    @Override
    public void onBackPressed() {
        finish();

    }


    private void showCameraGalleryDialog() {
        final Dialog cameraGalleryDialog = new Dialog(mActivity);
        cameraGalleryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraGalleryDialog.setContentView(R.layout.dialog_camra_gallery);
        cameraGalleryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCamera = cameraGalleryDialog.findViewById(R.id.txtCamera);
        TextView txtGallery = cameraGalleryDialog.findViewById(R.id.txtGallery);
        TextView txtCancel = cameraGalleryDialog.findViewById(R.id.txtCancel);

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
            }
        });
        txtCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
                openCamera();
            }
        });
        txtGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
                openGallery();
            }
        });

        cameraGalleryDialog.show();
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) == null) {
            return;
        }
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        InputStream stream = null;
        callbackManager.onActivityResult(requestCode, resultCode, data);

        final TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
        if (twitterAuthClient.getRequestCode() == requestCode) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
             /*Getting Image in onActivityResult
            * that is select from Gallery*/
            try {
                Bitmap bitmap = null;
                stream = getContentResolver().openInputStream(data.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                try {
                    bitmap = getCorrectlyOrientedImage(SignupActivity.this, data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mBitmapImage = bitmap;
                profileIV.setImageBitmap(bitmap);
                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(mBitmapImage);
                Log.e(TAG, "galleryuri: " + tempUri);
                // CALL THIS METHOD TO GET THE ACTUAL PATH
                finalFile = new File(getRealPathFromURI(tempUri));
                strEncodedImage64 = encodeImage(mBitmapImage);
                imageByteArray = getByteArray(mBitmapImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            imageData = (Bitmap) data.getExtras().get("data");
            Uri tempUria = getImageUri(imageData);
            Log.e(TAG, "cameraURi: " + tempUria);
            try {
                imageData = getCorrectlyOrientedImage(SignupActivity.this, tempUria);
            } catch (IOException e) {
                e.printStackTrace();
            }
            profileIV.setImageBitmap(imageData);
            mBitmapImage = imageData;
            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(mBitmapImage);
            // CALL THIS METHOD TO GET THE ACTUAL PATH
            finalFile = new File(getRealPathFromURI(tempUri));
            strEncodedImage64 = encodeImage(mBitmapImage);
            imageByteArray = getByteArray(mBitmapImage);

        }
    }

    public byte[] getByteArray(Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(mActivity.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = mActivity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void checkAndroidPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            showCameraGalleryDialog();
        }
    }

    @SuppressLint("NewApi")
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (mActivity, Manifest.permission.CAMERA)) {

                Snackbar.make(mActivity.findViewById(android.R.id.content),
                        mResources.getString(R.string.pleasegrantpermissions),
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();

            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        PERMISSIONS_MULTIPLE_REQUEST);
            }
        } else {
            // write your logic code if permission already granted
            showCameraGalleryDialog();
        }
    }


    public static Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();
        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);
        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }
        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 400 || rotatedHeight > 400) {
            float widthRatio = ((float) rotatedWidth) / ((float) 400);
            float heightRatio = ((float) rotatedHeight) / ((float) 400);
            float maxRatio = Math.max(widthRatio, heightRatio);
            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
        }
        return srcBitmap;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri, new String[]{
                MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public void showAlertDialogOK(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_showalert);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setText("Ok");
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }
}
