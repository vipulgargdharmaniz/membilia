package com.membilia.activities;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.BaseActivity;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.volley.UtilsVolley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dharmaniz on 25/5/17.
 */

public class RestorePasswordActivity extends BaseActivity {
    public String TAG = "RestorePasswordActivity";
    Activity mActivity = RestorePasswordActivity.this;
    Resources mResources;
    EditText emailET;
    TextView txtSendTV;
    RelativeLayout backRL;
    String strEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restorepassword);
        mResources = mActivity.getResources();
        Log.i(TAG, "onCreate: ");
    }

    @Override
    protected void setUpViews() {
        backRL = (RelativeLayout) findViewById(R.id.backRL);
        emailET = (EditText) findViewById(R.id.emailET);
        txtSendTV = (TextView) findViewById(R.id.txtSendTV);
    }

    @Override
    protected void setUpClickListeners() {
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        txtSendTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strEmail = emailET.getText().toString();
                if (strEmail.equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.pleaseenteremail));
                } else if (!Utilities.emailValidator(strEmail)) {
                    AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mResources.getString(R.string.isvalidemail));
                } else {
                    /*Execute API*/
                    executeForgotPwdAPI();
                }
            }
        });

    }

    public void executeForgotPwdAPI() {
        /*Utilities.showProgressDialog(mActivity);
        Retrofit mRetrofit = ApiClient.getClient(WebServicesConstants.BASE_URL);
        ApiService mApiService = mRetrofit.create(ApiService.class);
        Call<ForgotPwdResponse> mCall = mApiService.apiForgotPassword(strEmail);
        mCall.enqueue(new Callback<ForgotPwdResponse>() {
            @Override
            public void onResponse(Call<ForgotPwdResponse> call, Response<ForgotPwdResponse> response) {
                if (response.code() == 200) {
                    ForgotPwdResponse mForgotPwdResponse = response.body();
                    if (mForgotPwdResponse.getMessage() == true) {
                        Toast.makeText(mActivity, "Please Check your registered email to getting new password.", Toast.LENGTH_SHORT).show();
                        mActivity.finish();
                    }
                } else if (response.code() == 403) {
                    try {
                        JSONObject mJsonObject = new JSONObject(response.errorBody().string());
                        Log.e(TAG, "****errorBody*****" + mJsonObject.getString("message"));
                        AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), mJsonObject.getString("message"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mActivity, "Something Wrong", Toast.LENGTH_SHORT).show();
                }
                Utilities.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<ForgotPwdResponse> call, Throwable t) {
                Log.e(TAG, "*****onFailure******" + t.toString());
                Utilities.hideProgressDialog();
            }
        });*/

        Utilities.showProgressDialog(mActivity);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebServicesConstants.FORGOT_PASSWORD,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utilities.hideProgressDialog();
                        Log.i("VOLLEY", response);
                        parseResponse(response);

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Utilities.hideProgressDialog();
                Log.e(TAG, "****onErrorResponse****" + error.toString());
                String json = null;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mResources.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", strEmail);
                return params;
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "SignUpRequest");
    }

    private void parseResponse(String res) {
        try {
            JSONObject jsonObject = new JSONObject(res);
            if (jsonObject.getBoolean("message") == true) {
                Toast.makeText(mActivity, "Please Check your registered email to getting new password.", Toast.LENGTH_SHORT).show();
                mActivity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
