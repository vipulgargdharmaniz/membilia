package com.membilia.activities;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.membilia.BaseActivity;
import com.membilia.PlayPawnPref_Once;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.DatabaseHandler;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.dbModal.LoginUser;
import com.membilia.splashslides.SplashSlidesActivity;
import org.json.JSONObject;

import java.util.ArrayList;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/*
 * Created by dharmaniapps on 15/1/18.
 */
public class SplashForBranch extends BaseActivity {

    private static final String TAG = "SplashForBranch";
    String deviceId, branchUserID = "", userID = "", itemID = "", branchType;
    private String verifyString = WebServicesConstants.BASE_URL + "verify.php?";
    private boolean isforProfile = false;

    DatabaseHandler db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        deviceId = refreshedToken;
        db = new DatabaseHandler(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Branch branch = Branch.getInstance(getApplicationContext());
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    Log.e(TAG, "onInitFinished: " + referringParams);
                    if (referringParams.length() > 2) {
                        try {
                            itemID = referringParams.getString("item_id");
                            Log.e(TAG, "onInitFinished:userID  " + itemID);
                            branchType = referringParams.getString("$og_title");
                            isforProfile = true;
                            openNextScreen(branchType, itemID, userID);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            userID = referringParams.getString("user_id");
                            Log.e(TAG, "onInitFinished:userID  " + userID);
                            branchType = referringParams.getString("$og_title");
                            isforProfile = true;
                            openNextScreen(branchType, itemID, userID);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            userID = referringParams.getString("userId");
                            Log.e(TAG, "onInitFinished:userID  " + userID);
                            branchType = referringParams.getString("$og_title");
                            isforProfile = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!isforProfile) {
                            verifyString = verifyString + "user_id=" + userID + "&device_type=1&device_token=" + deviceId;
                            Log.e(TAG, "strUrl: " + verifyString);
                            StringRequest request = new StringRequest(Request.Method.POST, verifyString, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.e(TAG, "***********RESPONSE*********: " + response);
                                    parseResponse(response);
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e(TAG, "onErrorResponse: " + error.toString());
                                }
                            }) {
                            };
                            PlayerPawnApplication.getInstance().addToRequestQueue(request);
                        } else {
                            Log.e(TAG, "isforProfile: " + isforProfile);
                        }
                    }
                } else {
                    Log.i("MyApp", error.getMessage());
                }
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void openNextScreen(String branchType, String itemID, String userID) {
        Constants.BranchIO = "True";
        Constants.BranchUserID = userID;
        Constants.BranchItemID = itemID;
        Constants.BranchType = branchType;

        if (!PlayerPawnPreference.readString(SplashForBranch.this, PlayerPawnPreference.VALUE_TOKEN, "").equals("")) {
            PlayerPawnPreference.writeString(SplashForBranch.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
            startActivity(new Intent(SplashForBranch.this, HomeActivity.class));
            finish();
            overridePendingTransition(R.anim.in_from_right, R.anim.animback);
        } else if (PlayPawnPref_Once.readString(SplashForBranch.this, PlayPawnPref_Once.FirstTimeSplash, "").equalsIgnoreCase("")) {
            PlayPawnPref_Once.writeString(SplashForBranch.this, PlayPawnPref_Once.FirstTimeSplash, "yes");
            startActivity(new Intent(SplashForBranch.this, SplashSlidesActivity.class));
            finish();
        } else if (PlayerPawnPreference.readString(SplashForBranch.this, PlayerPawnPreference.VALUE_TOKEN, "").equals("")) {
            startActivity(new Intent(SplashForBranch.this, SelectionActivity.class));
            finish();
            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        } else {
            startActivity(new Intent(SplashForBranch.this, SplashSlidesActivity.class));
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void parseResponse(String res) {
        try {
            JSONObject jsonObject = new JSONObject(res);
            Log.e(TAG, "parseResponse: " + jsonObject.getInt("expiration"));
            PlayerPawnPreference.writeInteger(SplashForBranch.this, PlayerPawnPreference.VALUE_EXPIRATION, jsonObject.getInt("expiration"));
            PlayerPawnPreference.writeBoolean(SplashForBranch.this, PlayerPawnPreference.VALUE_SIGN_UP, jsonObject.getBoolean("signup"));
            PlayerPawnPreference.writeString(SplashForBranch.this, PlayerPawnPreference.VALUE_TOKEN, jsonObject.getString("token"));
            PlayerPawnPreference.writeInteger(SplashForBranch.this, PlayerPawnPreference.VALUE_COUNT, jsonObject.getInt("Count"));
            PlayerPawnPreference.writeString(SplashForBranch.this, PlayerPawnPreference.VALUE_USER_ID, jsonObject.getString("user_id"));

            ArrayList<LoginUser> loginUsersList = new ArrayList<>();
            LoginUser loginModal = new LoginUser();
            loginModal.setUserToken(jsonObject.getString("token"));
            loginModal.setUserId(jsonObject.getString("user_id"));
            loginUsersList.add(loginModal);
            db.addUser(loginUsersList);

            if (jsonObject.getString("user_id").length() > 0) {
                if (jsonObject.getInt("Count") <= 5) {
                    Intent intent = new Intent(SplashForBranch.this, InviteActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.LOGIN_TYPE, "email");
                    startActivity(intent);
                    finish();
                } else {
                    PlayerPawnPreference.writeString(SplashForBranch.this, PlayerPawnPreference.CREATE_HOME_SCREEN, "yes");
                    Intent intent = new Intent(SplashForBranch.this, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.LOGIN_TYPE, "facebook");
                    startActivity(intent);
                    finish();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
