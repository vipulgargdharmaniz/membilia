package com.membilia.activities;


import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.BaseActivity;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.volley_models.Charity_Model;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class CharityActivity extends BaseActivity {
    Activity mActivity = CharityActivity.this;
    public String TAG = "CharityActivity";
    TextView remainingTV,soldTV,totalTV;
    LinearLayout leftll;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charity);
        Log.i(TAG, "onCreate: ");
        getItems();

        setStatusBarGradiant(CharityActivity.this);
    }

    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.charity_back);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(R.color.black));
            window.setBackgroundDrawable(background);
        }
    }


    @Override
    protected void setUpClickListeners() {
        leftll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(0,R.anim.out_to_right);
            }
        });
    }

    @Override
    protected void setUpViews() {
        leftll =  findViewById(R.id.leftll);
        remainingTV= findViewById(R.id.remainingTV);
        soldTV= findViewById(R.id.soldTV);
        totalTV= findViewById(R.id.totalTV);
    }

    private void getItems() {
        Utilities.showProgressDialog(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, WebServicesConstants.CHARITY,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utilities.hideProgressDialog();
                        Log.i("VOLLEY", response);
                        parseResponse(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Utilities.hideProgressDialog();
                Log.e(TAG, "****onErrorResponse****" + error.toString());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(mActivity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);

    }

    private void parseResponse(String res){
        try {
            JSONObject jsonObject = (new JSONObject(res));
            Charity_Model charity_model= new Charity_Model();
            charity_model.setSold(jsonObject.getString("sold"));
            charity_model.setTotal(jsonObject.getString("total"));
            charity_model.setRemain(jsonObject.getString("remain"));
            setDataOnWidgets(charity_model);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }


    }
    private void setDataOnWidgets(Charity_Model charity_model){
        totalTV.setText("$" +charity_model.getTotal());
        remainingTV.setText("$" +charity_model.getRemain());
        soldTV.setText("$" +charity_model.getSold());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,R.anim.out_to_right);
    }
}