package com.membilia.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.membilia.R;

/**
 * Created by dharmaniapps on 27/12/17.
 */

public class PayPalLogin_Activity extends AppCompatActivity {

    private static final String TAG = "PayPalLogin_Activity";
    EditText emailET , passwordET;
    LinearLayout passCross,emailCross , infoLL , newUserLL;
    RelativeLayout leftRL;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypal);
        setView();
        setOnClick();
    }

    private void setView() {
        emailET = (EditText) findViewById(R.id.emailET);
        passwordET = (EditText) findViewById(R.id.passwordET);

        passCross = (LinearLayout) findViewById(R.id.passCross);
        emailCross = (LinearLayout) findViewById(R.id.emailCross);
        infoLL = (LinearLayout) findViewById(R.id.infoLL);
        newUserLL = (LinearLayout) findViewById(R.id.newUserLL);
        leftRL = (RelativeLayout) findViewById(R.id.leftRL);
    }
    private void setOnClick() {

        infoLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        newUserLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

}
