package com.membilia.dbModal;

import java.io.Serializable;

/*
 * Created by dharmaniapps on 27/2/18.
 */

public class DbProfile implements Serializable {
    private String userId= "";
    private String username= "";
    private String userDecp= "";
    private String userCountry= "";
    private String userPic= "";
    private String userRanking= "";
    private String userFollower= "";
    private String userFollowing= "";
    private String userVerified= "";
    private String userFollowed = "";

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserDecp() {
        return userDecp;
    }

    public void setUserDecp(String userDecp) {
        this.userDecp = userDecp;
    }

    public String getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getUserRanking() {
        return userRanking;
    }

    public void setUserRanking(String userRanking) {
        this.userRanking = userRanking;
    }

    public String getUserFollower() {
        return userFollower;
    }

    public void setUserFollower(String userFollower) {
        this.userFollower = userFollower;
    }

    public String getUserFollowing() {
        return userFollowing;
    }

    public void setUserFollowing(String userFollowing) {
        this.userFollowing = userFollowing;
    }

    public String getUserVerified() {
        return userVerified;
    }

    public void setUserVerified(String userVerified) {
        this.userVerified = userVerified;
    }

    public String getUserFollowed() {
        return userFollowed;
    }

    public void setUserFollowed(String userFollowed) {
        this.userFollowed = userFollowed;
    }
}
