package com.membilia.dbModal;

import java.io.Serializable;

/*
 * Created by dharmaniapps on 5/3/18.
 */

public class LoginUser implements Serializable {


    private String userToken;
    private String userId;


    public String getUserToken() {
        return userToken;
    }
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
}
