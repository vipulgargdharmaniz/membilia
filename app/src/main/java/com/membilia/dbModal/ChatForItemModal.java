package com.membilia.dbModal;

import com.membilia.volley_models.MessageHistoryModal;

import java.io.Serializable;

/*
 * Created by dharmaniapps on 23/2/18.
 */

public class ChatForItemModal implements Serializable {

    String MainItemId;
    MessageHistoryModal ChatModal;

    public String getMainItemId() {
        return MainItemId;
    }

    public void setMainItemId(String mainItemId) {
        MainItemId = mainItemId;
    }

    public MessageHistoryModal getChatModal() {
        return ChatModal;
    }

    public void setChatModal(MessageHistoryModal chatModal) {
        ChatModal = chatModal;
    }


}
