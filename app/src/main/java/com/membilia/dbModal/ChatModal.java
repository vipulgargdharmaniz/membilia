package com.membilia.dbModal;

/*
 * Created by dharmaniapps on 23/2/18.
 */
import java.io.Serializable;

public class ChatModal implements Serializable {

    String messageid;
    String directionid;
    String message;
    String timeStamp;
    String itemId;
    String itemName;
    String senderID;
    String senderName;
    String senderImage;
    String recieverID;
    String recieverName;
    String recieverImage;

    public String getMessageid() {
        return messageid;
    }

    public void setMessageid(String messageid) {
        this.messageid = messageid;
    }

    public String getDirectionid() {
        return directionid;
    }

    public void setDirectionid(String directionid) {
        this.directionid = directionid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderImage() {
        return senderImage;
    }

    public void setSenderImage(String senderImage) {
        this.senderImage = senderImage;
    }

    public String getRecieverID() {
        return recieverID;
    }

    public void setRecieverID(String recieverID) {
        this.recieverID = recieverID;
    }

    public String getRecieverName() {
        return recieverName;
    }

    public void setRecieverName(String recieverName) {
        this.recieverName = recieverName;
    }

    public String getRecieverImage() {
        return recieverImage;
    }

    public void setRecieverImage(String recieverImage) {
        this.recieverImage = recieverImage;
    }

}

