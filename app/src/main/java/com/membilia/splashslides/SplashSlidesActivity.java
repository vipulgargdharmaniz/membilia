package com.membilia.splashslides;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.membilia.R;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.activities.SelectionActivity;
import com.membilia.adapters.SplashPagerAdapter;

public class SplashSlidesActivity extends AppCompatActivity {
    public String TAG = "SplashSlidesActivity";
    int layoutArray[] = {R.layout.slide2, R.layout.slide1, R.layout.slide3};
    TabLayout tabLayout;
    private ViewPager mViewPager;
    SplashPagerAdapter mPagerAdapter;

    String Locationfine = Manifest.permission.ACCESS_FINE_LOCATION;
    String LocationCoarse = Manifest.permission.ACCESS_COARSE_LOCATION;
    String READ_CONTACTS = Manifest.permission.READ_CONTACTS;
    String READ_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    String WRITE_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_slides);
        Log.i(TAG, "onCreate: ");
        PlayerPawnPreference.writeBoolean(SplashSlidesActivity.this, PlayerPawnPreference.IS_FIRST_TIME, true);
        setUpViews();
    }

    private void viewpagerFxn() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    tabLayout.setVisibility(View.INVISIBLE);
                } else {
                    tabLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    protected void setUpViews() {
        mViewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabDots);
        setViewPagerAdapter();
        viewpagerFxn();
    }


    private void setViewPagerAdapter() {
        mPagerAdapter = new SplashPagerAdapter(this, layoutArray, tabLayout);
        mViewPager.setAdapter(mPagerAdapter);
//        tabLayout.setupWithViewPager(mViewPager, true);
    }

    public void grantpermission() {
        if (checkPermission2()) {
            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
            finish();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission2() {
        int location1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int location2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int readContact = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStorage = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return location1 == PackageManager.PERMISSION_GRANTED && location2 == PackageManager.PERMISSION_GRANTED
                && readContact == PackageManager.PERMISSION_GRANTED && readStorage == PackageManager.PERMISSION_GRANTED &&
                writeStorage == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Locationfine, LocationCoarse, READ_CONTACTS, READ_STORAGE, WRITE_STORAGE}, 100);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 100:
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            startActivity(new Intent(SplashSlidesActivity.this, SelectionActivity.class));
                            finish();
                        }
                    }
                }
                break;
        }
    }
}