package com.membilia.volley_models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dharmaniz on 19/7/17.
 */

public  class CountryModel extends ArrayList<CharSequence> implements Serializable
{
    private String name;
    private String nameImage;
    private String dial_code;
    private String code;
    private String Language;

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setNameImage(String nameImage) {
        this.nameImage = nameImage;
    }
    public String getNameImage() {
        return nameImage;
    }

    public void setDailCode(String dial_code) {
        this.dial_code = dial_code;
    }
    public String getDailCode() {
        return dial_code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getCode() {
        return code;
    }


    public void setLanguage(String Language) {
        this.Language = Language;
    }
    public String getLanguage() {
        return Language;
    }



}
