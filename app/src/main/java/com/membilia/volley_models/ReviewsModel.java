package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 18/7/17.
 */

public class ReviewsModel implements Serializable {
    private String itemName ="";
    private String review;
    private String rating;
    private String isReviewer;
    private ReviewSenderModel sender;
    private ReviewReceiverModel receiver;
    private String date ="";


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;

    }

    public ReviewSenderModel getSender() {
        return sender;
    }

    public void setSender(ReviewSenderModel sender) {
        this.sender = sender;
    }

    public ReviewReceiverModel getReceiver() {
        return receiver;
    }

    public void setReceiver(ReviewReceiverModel receiver) {
        this.receiver = receiver;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getIsReviewer() {
        return isReviewer;
    }

    public void setIsReviewer(String isReviewer) {
        this.isReviewer = isReviewer;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}

