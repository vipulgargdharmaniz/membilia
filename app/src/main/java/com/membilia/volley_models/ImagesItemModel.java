package com.membilia.volley_models;

import android.graphics.Bitmap;

/**
 * Created by dharmaniapps on 5/12/17.
 */

public class ImagesItemModel {


    String id = "";
    String UriType = "";
    String strImageVideoPath = "";
    String strVideoImage = "";
    byte[] strUrlByteArray ;
    Bitmap strVideoThumbnail  = null;
    String strCameFrom = "";
    String editImageString = "";

    public String getEditImageString() {return editImageString;}

    public void setEditImageString(String editImageString) {this.editImageString = editImageString;}

    public byte[] getStrVideobyteArray() {
        return strVideobyteArray;
    }

    public void setStrVideobyteArray(byte[] strVideobyteArray) {
        this.strVideobyteArray = strVideobyteArray;
    }

    byte[] strVideobyteArray = null;



    public String getStrCameFrom() {
        return strCameFrom;}
    public void setStrCameFrom(String strCameFrom) {
        this.strCameFrom = strCameFrom;}
    public String getStrVideoImage() {
        return strVideoImage;
    }
    public void setStrVideoImage(String strVideoImage) {
        this.strVideoImage = strVideoImage;
    }
    public Bitmap getStrVideoThumbnail() {
        return strVideoThumbnail;
    }
    public void setStrVideoThumbnail(Bitmap strVideoThumbnail) {
        this.strVideoThumbnail = strVideoThumbnail;}
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getStrImageVideoPath() {
        return strImageVideoPath;}
    public void setStrImageVideoPath(String strImageVideoPath) {
        this.strImageVideoPath = strImageVideoPath;}

    public byte[] getStrUrlByteArray() {
        return strUrlByteArray;
    }

    public void setStrUrlByteArray(byte[] strUrlByteArray) {
        this.strUrlByteArray = strUrlByteArray;
    }

    public String getUriType() {
        return UriType;
    }

    public void setUriType(String uriType) {
        UriType = uriType;
    }
}
