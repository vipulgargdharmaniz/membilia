package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 18/7/17.
 */

public class UserImagesModel implements Serializable {


    private String original;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
