package com.membilia.volley_models;

import java.io.Serializable;

/*
 * Created by dharmaniz on 29/8/17.
 */

public class ItemDetailMoldel implements Serializable {
    private  String item_id;
    private  String user_id;
    private  String last_used;
    private  String price;
    private  Boolean isAutographed;
    private  String category;
    private  Boolean charity;
    private  String charity_name;
    private  String currency;
    private  Boolean delivery_type;
    private  String item_name;
    private  String item_story;
    private  String sport;
    private  String weight;
    private  String created;
    private  String status;
    private  String width;
    private  String height;
    private  String length;
    private  String person_transfertext;
    private  String newtimestamp;
    private  String onHold;
    private  String status2;
    private  String user_name;
    private  String seller_id;
    private  Boolean isFavorite;
    private  String rating;
    private  Boolean isSold;
    private  String pawned_date;
    private IDUserImageModel IDUserImageModel;
    private IDItemImageModel IDItemImageModel;
    private  ItemDetailLocationModel itemDetailLocationModel;
    private IDItemVideoModel IDItemVideoModel;
    private IDTagModel IDTagModel;
    private String genre ="";
    private String genre_value="";

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    private String verified = "";


    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getGenre_value() {
        return genre_value;
    }

    public void setGenre_value(String genre_value) {
        this.genre_value = genre_value;
    }

    public com.membilia.volley_models.IDTagModel getIDTagModel() {
        return IDTagModel;
    }

    public void setIDTagModel(com.membilia.volley_models.IDTagModel IDTagModel) {
        this.IDTagModel = IDTagModel;
    }

    public IDUserImageModel getIDUserImageModel() {
        return IDUserImageModel;
    }

    public void setIDUserImageModel(IDUserImageModel IDUserImageModel) {
        this.IDUserImageModel = IDUserImageModel;
    }

 public IDItemVideoModel getIDItemVideoModel() {
        return IDItemVideoModel;
    }

    public void setIDItemVideoModel(IDItemVideoModel IDItemVideoModel) {
        this.IDItemVideoModel = IDItemVideoModel;
    }

 public IDItemImageModel getIDItemImageModel() {
        return IDItemImageModel;
    }

    public void setIDItemImageModel(IDItemImageModel IDItemImageModel) {
        this.IDItemImageModel = IDItemImageModel;
    }


    public ItemDetailLocationModel getItemDetailLocationModel() {
        return itemDetailLocationModel;
    }

    public void setItemDetailLocationModel(ItemDetailLocationModel itemDetailLocationModel) {
        this.itemDetailLocationModel = itemDetailLocationModel;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
    public String getLast_used() {
        return last_used;
    }

    public void setLast_used(String last_used) {
        this.last_used = last_used;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
 public String getPawned_date() {
        return pawned_date;
    }

    public void setPawned_date(String pawned_date) {
        this.pawned_date = pawned_date;
    }

 public Boolean getIsAutographed() {
        return isAutographed;
    }

    public void setIsAutographed(Boolean isAutographed) {
        this.isAutographed = isAutographed;
    }

 public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
     public Boolean getCharity() {
        return charity;
    }

    public void setCharity(Boolean charity) {
        this.charity = charity;
    }
     public String getCharity_name() {
        return charity_name;
    }

    public void setCharity_name(String charity_name) {
        this.charity_name = charity_name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

  public Boolean getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(Boolean delivery_type) {
        this.delivery_type = delivery_type;
    }

  public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

  public String getItem_story() {
        return item_story;
    }

    public void setItem_story(String item_story) {
        this.item_story = item_story;
    }

public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

public String getWeight() {
        return weight;
    }

    public void setweight(String weight) {
        this.weight = weight;
    }

public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

public String getStatus ()
{
    return status;
}
public void setStatus (String status){
    this.status=status;
}

public String getWidth ()
{
    return width;
}
public void setWidth (String width){
    this.width=width;
}

public String getHeight()
{
    return height;
}
public void setHeight(String height){
    this.height=height;
}

public String getLength()
{
    return length;
}
public void setLength(String length){
    this.length=length;
}

   public String getPerson_transfertext()
{
    return person_transfertext;
}
public void setPerson_transfertext(String person_transfertext){
    this.person_transfertext=person_transfertext;
}
   public String getNewtimestamp()
{
    return newtimestamp;
}
public void setNewtimestamp(String newtimestamp){
    this.newtimestamp=newtimestamp;
}
    public String getOnHold()
{
    return onHold;
}
public void setOnHold(String onHold){
    this.onHold=onHold;
}
    public String getStatus2()
{
    return status2;
}
public void setStatus2(String status2){
    this.status2=status2;
}
    public String getUser_name()
{
    return user_name;
}
public void setUser_name(String user_name){
    this.user_name=user_name;
}
    public String getSeller_id()
{
    return seller_id;
}
public void setSeller_id(String seller_id){
    this.seller_id=seller_id;
}
     public Boolean getIsFavorite()
{
    return isFavorite;
}
public void setIsFavorite(Boolean isFavorite){
    this.isFavorite=isFavorite;
}
     public String getRating()
{
    return rating;
}
public void setRating(String rating){
    this.rating=rating;
}
     public Boolean getIsSold()
{
    return isSold;
}
public void setIsSold(Boolean isSold){
    this.isSold=isSold;
}

}
















