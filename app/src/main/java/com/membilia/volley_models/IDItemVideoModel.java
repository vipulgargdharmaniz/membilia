package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 1/9/17.
 */

public class IDItemVideoModel implements Serializable {
    private String original_videoimage = "";
    private String original_1 = "";

    public String getOriginal_videoimage() {
        return original_videoimage;
    }

    public void setOriginal_videoimage(String original_videoimage) {
        this.original_videoimage = original_videoimage;
    }

    public String getOriginal_1() {
        return original_1;
    }

    public void setOriginal_1(String original_1) {
        this.original_1 = original_1;
    }
}
