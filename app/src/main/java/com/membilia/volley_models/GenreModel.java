package com.membilia.volley_models;

import java.util.List;

/**
 * Created by dharmaniapps on 5/12/17.
 */

public class GenreModel {

    String genreName = "";

    public List<GenreValue> getGenreValueList() {
        return genreValueList;
    }

    List<GenreValue> genreValueList ;

    public void setGenreValueList(List<GenreValue> genreValueList) {
        this.genreValueList = genreValueList;
    }



    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }



}
