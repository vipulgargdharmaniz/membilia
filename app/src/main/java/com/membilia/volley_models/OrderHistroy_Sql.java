package com.membilia.volley_models;

/*
 * Created by dharmaniapps on 26/12/17.
 */

public class OrderHistroy_Sql {

    public String orderNo="";
    public String orderDate = "";
    public String senderName = "";
    public String deliveryMethod ;
    public String recipientName = "";
    private String address = "";
    private String itemPrice  = "";
    public String itemName ="";
    public String itemStory = "";

    public String sellerID = "";
    public String reciverID = "";
    public String itemID = "";
    String ShippingPrize = "";

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    String isFav = "";
    public String getShippingPrize() {
        return ShippingPrize;
    }

    public void setShippingPrize(String shippingPrize) {
        ShippingPrize = shippingPrize;
    }



    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }



    public String getSellerID() {
        return sellerID;
    }

    public void setSellerID(String sellerID) {
        this.sellerID = sellerID;
    }

    public String getReciverID() {
        return reciverID;
    }

    public void setReciverID(String reciverID) {
        this.reciverID = reciverID;
    }



    public OrderHistroy_Sql() {

    }


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String isDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemStory() {
        return itemStory;
    }

    public void setItemStory(String itemStory) {
        this.itemStory = itemStory;
    }



    public OrderHistroy_Sql(String name , String story,String orderNo,String date,String senderName , String RecipientName
            , String sellerId,String recivierid,String delivermethod , String address, String price ){
    }


}
