package com.membilia.volley_models;

/**
 * Created by Dharmani Apps on 6/30/2017.
 */

public class ItemVideoModel {

    private String original1;
    private String original_videoimage;

    public String getOriginal1() {
        return original1;
    }

    public void setOriginal1(String original1) {
        this.original1 = original1;
    }

    public String getOriginal_videoimage() {
        return original_videoimage;
    }

    public void setOriginal_videoimage(String original_videoimage) {
        this.original_videoimage = original_videoimage;
    }
}
