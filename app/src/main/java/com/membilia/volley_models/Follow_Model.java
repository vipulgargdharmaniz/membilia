package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniapps on 31/10/17.
 */

public class Follow_Model implements Serializable{

    public String Userid = "";
    public String UserName = "";
    public String Desc ="";
    public String Email ="";
    public int FollowersCount =0;
    public int FollowingCount =0;
    public int Rating =0;
    public int Ranking =0;
    public boolean IsFollowed = false;
    public boolean IsVerified = false;
    public String Flag ="";
    public ItemLocationModel Location;
    private ItemUserImagesModel UserImages;

    public String getUserid() {
        return Userid;
    }

    public void setUserid(String userid) {
        Userid = userid;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getFollowersCount() {
        return FollowersCount;
    }

    public void setFollowersCount(int followersCount) {
        FollowersCount = followersCount;
    }

    public int getFollowingCount() {
        return FollowingCount;
    }

    public void setFollowingCount(int followingCount) {
        FollowingCount = followingCount;
    }

    public int getRating() {
        return Rating;
    }

    public void setRating(int rating) {
        Rating = rating;
    }

    public int getRanking() {
        return Ranking;
    }

    public void setRanking(int ranking) {
        Ranking = ranking;
    }

    public boolean isFollowed() {
        return IsFollowed;
    }

    public void setFollowed(boolean followed) {
        IsFollowed = followed;
    }

    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public ItemLocationModel getLocation() {
        return Location;
    }

    public void setLocation(ItemLocationModel location) {
        Location = location;
    }

    public ItemUserImagesModel getUserImages() {
        return UserImages;
    }

    public void setUserImages(ItemUserImagesModel userImages) {
        UserImages = userImages;
    }

}
