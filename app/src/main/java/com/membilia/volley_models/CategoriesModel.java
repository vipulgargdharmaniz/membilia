package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 8/6/17.
 */

public class CategoriesModel implements Serializable {
    String categoryID = "";
    String name = "";
    int image;

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}