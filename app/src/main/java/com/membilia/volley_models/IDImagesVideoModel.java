package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 4/9/17.
 */

public class IDImagesVideoModel implements Serializable {
    String strImageUrl = "";
    String strVideoUrl = "";
    int intPosition;
    String imageType = "";
    String VideoLink = "";

    public String getVideoLink() {
        return VideoLink;
    }

    public void setVideoLink(String videoLink) {
        VideoLink = videoLink;
    }



    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }



    public int getIntPosition() {
        return intPosition;
    }

    public void setIntPosition(int intPosition) {
        this.intPosition = intPosition;
    }

    public String getStrImageUrl() {
        return strImageUrl;
    }

    public void setStrImageUrl(String strImageUrl) {
        this.strImageUrl = strImageUrl;
    }

    public String getStrVideoUrl() {
        return strVideoUrl;
    }

    public void setStrVideoUrl(String strVideoUrl) {
        this.strVideoUrl = strVideoUrl;
    }
}
