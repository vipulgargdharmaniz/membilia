package com.membilia.volley_models;

/**
 * Created by dharmaniz on 23/8/17.
 */

public class ActModel {
   private Act_ActivityDetailsModel Act_ActivityDetailsModel;
    private Act_UserModel Act_UserModel;
    private String text = "";
    private String type = "";
    private String activity_id = "";
    private String created = "";

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Act_ActivityDetailsModel getAct_ActivityDetailsModel() {
        return Act_ActivityDetailsModel;
    }

    public void setAct_ActivityDetailsModel(Act_ActivityDetailsModel Act_ActivityDetailsModel) {
        this.Act_ActivityDetailsModel = Act_ActivityDetailsModel;
    }
    public Act_UserModel getAct_UserModel() {
        return Act_UserModel;
    }

    public void setAct_UserModel(Act_UserModel Act_UserModel) {
        this.Act_UserModel = Act_UserModel;
    }
}
