package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniapps on 2/1/18.
 */

public class AccountDetailsModal implements Serializable {

    public String accountId = "";
    public String accountName = "";
    public String bankName = "";
    public String routingNo = "";
    public String accountNo = "";
    public String created = "";
    public String accountType = "";
    public boolean isDefault  = false;
    public boolean isSavingAccount = false;


    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public boolean isSavingAccount() {
        return isSavingAccount;
    }

    public void setSavingAccount(boolean savingAccount) {
        isSavingAccount = savingAccount;
    }




    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getRoutingNo() {
        return routingNo;
    }

    public void setRoutingNo(String routingNo) {
        this.routingNo = routingNo;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getAccountType() {return accountType;}

    public void setAccountType(String accountType) {this.accountType = accountType;}
}
