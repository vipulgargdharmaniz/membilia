package com.membilia.volley_models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dharmani Apps on 7/3/2017.
 */

public class ItemsModel implements Serializable{
    private String itemId;
    private String userId;
    private String lastUsed;
    private Integer price;
    private Boolean isAutographed;
    private String category;
    private Boolean charity;
    private String charityName;
    private String currency;
    private Boolean deliveryType;
    private String itemName;
    private String itemStory;
    private String sport;
    private String weight;
    private String created;
    private String status;
    private String width;
    private String height;
    private String length;
    private String personTransfertext;
    private String newtimestamp;
    private String onHold;
    private String status2;
    private String userName;
    private String sellerId;
    private Boolean isFavorite;
    private String rating;
    private ItemUserImagesModel userImages;
    private String pawnedDate;
    private List<String> tags = null;
    private ItemLocationModel location;
    private Boolean isSold;
    private ItemImagesModel itemImages;
    private ItemVideoModel itemVideo;
    private Integer itemIndx;
    private Integer verifieduser;


    public ItemLocationModel getLocation() {
        return location;
    }

    public void setLocation(ItemLocationModel location) {
        this.location = location;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(String lastUsed) {
        this.lastUsed = lastUsed;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getIsAutographed() {
        return isAutographed;
    }

    public void setIsAutographed(Boolean isAutographed) {
        this.isAutographed = isAutographed;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getCharity() {
        return charity;
    }

    public void setCharity(Boolean charity) {
        this.charity = charity;
    }

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Boolean getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(Boolean deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemStory() {
        return itemStory;
    }

    public void setItemStory(String itemStory) {
        this.itemStory = itemStory;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getPersonTransfertext() {
        return personTransfertext;
    }

    public void setPersonTransfertext(String personTransfertext) {
        this.personTransfertext = personTransfertext;
    }

    public String getNewtimestamp() {
        return newtimestamp;
    }

    public void setNewtimestamp(String newtimestamp) {
        this.newtimestamp = newtimestamp;
    }

    public String getOnHold() {
        return onHold;
    }

    public void setOnHold(String onHold) {
        this.onHold = onHold;
    }

    public String getStatus2() {
        return status2;
    }

    public void setStatus2(String status2) {
        this.status2 = status2;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public ItemUserImagesModel getUserImages() {
        return userImages;
    }

    public void setUserImages(ItemUserImagesModel userImages) {
        this.userImages = userImages;
    }

    public String getPawnedDate() {
        return pawnedDate;
    }

    public void setPawnedDate(String pawnedDate) {
        this.pawnedDate = pawnedDate;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }



    public Boolean getIsSold() {
        return isSold;
    }

    public void setIsSold(Boolean isSold) {
        this.isSold = isSold;
    }

    public ItemImagesModel getItemImages() {
        return itemImages;
    }

    public void setItemImages(ItemImagesModel itemImages) {
        this.itemImages = itemImages;
    }

    public ItemVideoModel getItemVideo() {
        return itemVideo;
    }

    public void setItemVideo(ItemVideoModel itemVideo) {
        this.itemVideo = itemVideo;
    }

    public Integer getItemIndx() {
        return itemIndx;
    }

    public void setItemIndx(Integer itemIndx) {
        this.itemIndx = itemIndx;
    }

    public Integer getVerifieduser() {
        return verifieduser;
    }
    public void setVerifieduser(Integer verifiedused) {
        this.verifieduser = verifiedused;
    }

}
