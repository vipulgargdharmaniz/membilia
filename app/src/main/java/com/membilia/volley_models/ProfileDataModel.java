package com.membilia.volley_models;


import java.io.Serializable;

/*
 * Created by dharmaniz on 18/7/17.
 */

public class ProfileDataModel implements Serializable {

    private String userId= "";
    private String name="";
    private String description= "";
    private String email= "";
    private Integer followerCount=0;
    private Integer followingCount=0;
    private Integer rating=0;
    private Integer ranking=0;
    private Boolean isFollowed=false;
    private ProfileLocationModel locationModel;
    private String flag= "";
    private Boolean isVerified=false;
    private ProfileImageModel profileImageModel;

    public ProfileLocationModel getLocationModel() {
        return locationModel;
    }

    public void setLocationModel(ProfileLocationModel locationModel) {
        this.locationModel = locationModel;
    }

    public ProfileImageModel getProfileImageModel() {
        return profileImageModel;
    }

    public void setProfileImageModel(ProfileImageModel profileImageModel) {
        this.profileImageModel = profileImageModel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        this.followerCount = followerCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Boolean getIsFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(Boolean isFollowed) {
        this.isFollowed = isFollowed;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }



}