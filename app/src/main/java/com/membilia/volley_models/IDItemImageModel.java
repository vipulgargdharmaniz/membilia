package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 29/8/17.
 */

public class IDItemImageModel implements Serializable {
    public String original_1;
    public String original_2;
    public String original_3;
    public String original_4;
    public String original_5;
    public String original_6;

    public String getOriginal_1()
    {
        return original_1;
    }
    public void setOriginal_1(String original_1){
        this.original_1=original_1;
    }

    public String getOriginal_2()
    {
        return original_2;
    }
    public void setOriginal_2(String original_2){
        this.original_2=original_2;
    }

    public String getOriginal_3()
    {
        return original_3;
    }
    public void setOriginal_3(String original_3){
        this.original_3=original_3;
    }

    public String getOriginal_4()
    {
        return original_4;
    }
    public void setOriginal_4(String original_4){
        this.original_4=original_4;
    }

    public String getOriginal_5()
    {
        return original_5;
    }
    public void setOriginal_5(String original_5){
        this.original_5=original_5;
    }

    public String getOriginal_6()
    {
        return original_6;
    }
    public void setOriginal_6(String original_6){
        this.original_6=original_6;
    }

}
