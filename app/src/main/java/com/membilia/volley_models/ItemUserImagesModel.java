package com.membilia.volley_models;


/*
 * Created by Dharmani Apps on 6/28/2017.
 */

public class ItemUserImagesModel {
    private String original;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }
}
