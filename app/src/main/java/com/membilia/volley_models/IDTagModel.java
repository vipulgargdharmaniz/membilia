package com.membilia.volley_models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dharmaniz on 29/8/17.
 */

public class IDTagModel implements Serializable {
    ArrayList<String> tagsArrayList;

    public ArrayList<String> getTagsArrayList() {
        return tagsArrayList;
    }

    public void setTagsArrayList(ArrayList<String> tagsArrayList) {
        this.tagsArrayList = tagsArrayList;
    }
}
