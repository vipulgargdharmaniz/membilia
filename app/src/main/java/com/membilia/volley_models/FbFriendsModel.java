package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 6/28/2017.
 */

public class FbFriendsModel implements Serializable{
    String ID = "";
    String Name = "";
    String Image = "";

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
