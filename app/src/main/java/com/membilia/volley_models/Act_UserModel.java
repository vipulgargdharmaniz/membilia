package com.membilia.volley_models;

/**
 * Created by dharmaniz on 23/8/17.
 */

public class Act_UserModel {
    private Act_UserImagesModel Act_UserImagesModel;
    private Act_UserLocationModel Act_UserLocationModel;
    private String user_id;
    private String name;
    private String description;
    private String email;
    private String followerCount;
    private String followingCount;
    private String rating;
    private String isVerified;
    private String flag;
    private String ranking;
    private Boolean isFollowed;


    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

 public void setFollowerCount(String followerCount) {
        this.followerCount = followerCount;
    }

    public String getFollowerCount() {
        return followerCount;
    }

 public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getRanking() {
        return ranking;
    }
    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getIsVerified() {
        return isVerified;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

public void setFollowingCount(String followingCount) {
        this.followingCount = followingCount;
    }

    public String getFollowingCount() {
        return followingCount;
    }

public void setIsFollowed(Boolean isFollowed) {
        this.isFollowed = isFollowed;
    }

    public Boolean getIsFollowed() {
        return isFollowed;
    }

    public Act_UserImagesModel getAct_UserImagesModel() {
        return Act_UserImagesModel;
    }

    public void setAct_UserImagesModel(Act_UserImagesModel Act_UserImagesModel) {
        this.Act_UserImagesModel = Act_UserImagesModel;
    }
public Act_UserLocationModel getAct_UserLocationModel() {
        return Act_UserLocationModel;
    }

    public void setAct_UserLocationModel(Act_UserLocationModel Act_UserLocationModel) {
        this.Act_UserLocationModel = Act_UserLocationModel;
    }

}
