package com.membilia.volley_models;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Comparator;

/*
 * Created by dharmaniapps on 3/1/18.
 */

public class MessageHistoryModal implements Serializable ,Comparable<MessageHistoryModal>{


    boolean direction=false;
    String messageId="";
    String message="";
    String itemId="";
    String itemName="";
    String unreadCount="";
    String senderId="";



    String senderName="";
    String senderImage="";
    String timeStamp="";

    String reciverId="";
    String recivierImage="";
    String recivierName="";
    String strDirection="";

    public String getStrDirection() {
        return strDirection;
    }

    public void setStrDirection(String strDirection) {
        this.strDirection = strDirection;
    }

    public int getIntDirection() {
        return intDirection;
    }

    public void setIntDirection(int intDirection) {
        this.intDirection = intDirection;
    }
    int intDirection = 0;
    public static final Comparator<MessageHistoryModal> DESCENDING_COMPARATOR = new Comparator<MessageHistoryModal>() {
        // Overriding the compare method to sort the age
        public int compare(MessageHistoryModal d, MessageHistoryModal d1) {
            return Integer.parseInt(d.timeStamp) - Integer.parseInt(d1.timeStamp);
        }
    };

    public String getReciverId() {
        return reciverId;
    }

    public void setReciverId(String reciverId) {
        this.reciverId = reciverId;
    }

    public String getRecivierImage() {
        return recivierImage;
    }

    public void setRecivierImage(String recivierImage) {
        this.recivierImage = recivierImage;
    }

    public String getRecivierName() {
        return recivierName;
    }

    public void setRecivierName(String recivierName) {
        this.recivierName = recivierName;
    }


    public boolean isDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderImage() {
        return senderImage;
    }

    public void setSenderImage(String senderImage) {
        this.senderImage = senderImage;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }


    @Override
    public int compareTo(@NonNull MessageHistoryModal d) {
        return (this.timeStamp).compareTo(d.timeStamp);
    }
}
