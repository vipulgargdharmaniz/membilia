package com.membilia.volley_models;

/**
 * Created by dharmaniapps on 5/12/17.
 */

public class GenreValue {


    String categoryID = "";
    String description = "";
    String valueName = "";

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }


}
