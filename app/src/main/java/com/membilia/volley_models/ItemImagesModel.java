package com.membilia.volley_models;

/**
 * Created by Dharmani Apps on 6/30/2017.
 */

public class ItemImagesModel {

    private String original1;
    private String original2;
    private String original3;

    public String getOriginal1() {
        return original1;
    }

    public void setOriginal1(String original1) {
        this.original1 = original1;
    }

    public String getOriginal2() {
        return original2;
    }

    public void setOriginal2(String original2) {
        this.original2 = original2;
    }

    public String getOriginal3() {
        return original3;
    }

    public void setOriginal3(String original3) {
        this.original3 = original3;
    }

}
