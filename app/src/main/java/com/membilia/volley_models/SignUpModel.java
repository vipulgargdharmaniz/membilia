package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by Dharmani Apps on 7/3/2017.
 */

public class SignUpModel implements Serializable {

    Integer expiration;
    Boolean signup;
    String token;
    String userId;

    public Integer getExpiration() {
        return expiration;
    }

    public void setExpiration(Integer expiration) {
        this.expiration = expiration;
    }

    public Boolean getSignup() {
        return signup;
    }

    public void setSignup(Boolean signup) {
        this.signup = signup;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}


