package com.membilia.volley_models;

/**
 * Created by dharmaniz on 23/8/17.
 */

public class Act_ActivityDetailsModel {
    private String item_id;
    private String user_id;
    private String last_used;
    private String price;
    private Boolean isAutographed;
    private String category;
    private Boolean charity;
    private String charity_name;
    private String currency;
    private Boolean delivery_type;
    private String item_name;
    private String item_story;
    private String sport;
    private String weight;
    private String created;

    private String status;
    private String width;
    private String height;
    private String length;
    private String person_transfertext;
    private String newtimestamp;
    private String onHold;
    private String status2;
    private String user_name;
    private String seller_id;
    private String isFavorite;

    private String rating;
    private Act_UserLocationModel Act_UserLocationModel;

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setLast_used(String last_used) {
        this.last_used = last_used;
    }

    public String getLast_used() {
        return last_used;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setIsAutographed(Boolean isAutographed) {
        this.isAutographed = isAutographed;
    }

    public Boolean getIsAutographed() {
        return isAutographed;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCharity(Boolean charity) {
        this.charity = charity;
    }

    public Boolean getCharity() {
        return charity;
    }

    public void setCharity_name(String charity_name) {
        this.charity_name = charity_name;
    }

    public String getCharity_name() {
        return charity_name;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getSport() {
        return sport;
    }

    public void setDelivery_type(Boolean delivery_type) {
        this.delivery_type = delivery_type;
    }

    public Boolean getDelivery_type() {
        return delivery_type;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_story(String item_story) {
        this.item_story = item_story;
    }

    public String getItem_story() {
        return item_story;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }


    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getweight() {
        return weight;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreated() {
        return created;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getWidth() {
        return width;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHeight() {
        return height;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getLength() {
        return length;
    }
 public void setPerson_transfertext(String person_transfertext) {
        this.person_transfertext = person_transfertext;
    }

    public String getPerson_transfertext() {
        return person_transfertext;
    }
 public void setNewtimestamp(String newtimestamp) {
        this.newtimestamp = newtimestamp;
    }

    public String getNewtimestamp() {
        return newtimestamp;
    }
 public void setOnHold(String onHold) {
        this.onHold = onHold;
    }

    public String getOnHold() {
        return onHold;
    }

public void setStatus2(String status2) {
        this.status2 = status2;
    }

    public String getStatus2() {
        return status2;
    }

public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_name() {
        return user_name;
    }

public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getSeller_id() {
        return seller_id;
    }

public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getIsFavorite() {
        return isFavorite;
    }
public void setRating(String Rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public Act_UserLocationModel getAct_UserLocationModel() {
        return Act_UserLocationModel;
    }

    public void setAct_UserLocationModel(Act_UserLocationModel Act_UserLocationModel) {
        this.Act_UserLocationModel = Act_UserLocationModel;
    }
}
