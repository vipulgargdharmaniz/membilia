package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 18/7/17.
 */

public class ReviewReceiverModel implements Serializable {

    private String userId;
    private String name;
    private String description;
    private String email;
    private Integer followerCount;
    private Integer followingCount;
    private Integer rating;
    private Integer ranking;
    private Boolean isFollowed;
    private ReviewRecevierLocationModel location;
    private String flag;
    private Boolean isVerified;
    private ReviewRecieverImages userImages;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        this.followerCount = followerCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Boolean getIsFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(Boolean isFollowed) {
        this.isFollowed = isFollowed;
    }



    public String getFlag() {
        return flag;
    }

    public ReviewRecevierLocationModel getLocation() {
        return location;
    }

    public void setLocation(ReviewRecevierLocationModel location) {
        this.location = location;
    }

    public ReviewRecieverImages getUserImages() {
        return userImages;
    }

    public void setUserImages(ReviewRecieverImages userImages) {
        this.userImages = userImages;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }



}