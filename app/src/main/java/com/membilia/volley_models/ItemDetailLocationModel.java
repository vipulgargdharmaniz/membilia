package com.membilia.volley_models;

import java.io.Serializable;

/**
 * Created by dharmaniz on 29/8/17.
 */

public class ItemDetailLocationModel implements Serializable {
    private String administrative_area_level_1;
    private String country;
    private String country_code;
    private String lat;
    private String lng;
    private String locality;
    private String postal_code;

    public String getAdministrative_area_level_1()
    {
        return administrative_area_level_1;
    }
    public void setAdministrative_area_level_1(String administrative_area_level_1){
        this.administrative_area_level_1=administrative_area_level_1;
    }
    public String getCountry()
    {
        return country;
    }
    public void setCountry(String country){
        this.country=country;
    }

public String getCountry_code()
    {
        return country_code;
    }
    public void setCountry_code(String country_code){
        this.country_code=country_code;
    }

    public String getLat()
    {
        return lat;
    }
    public void setLat(String lat){
        this.lat=lat;
    }

 public String getLng()
    {
        return lng;
    }
    public void setLng(String lng){
        this.lng=lng;
    }

 public String getLocality()
    {
        return locality;
    }
    public void setLocality(String locality){
        this.locality=locality;
    }

 public String getPostal_code()
    {
        return postal_code;
    }
    public void setPostal_code(String postal_code){
        this.postal_code=postal_code;
    }


}
