package com.membilia.volley_models;

/**
 * Created by dharmaniapps on 7/12/17.
 */

public class CharitySelection_Modal {

    public String getCharityName() {
        return charityName;
    }

    public void setCharityName(String charityName) {
        this.charityName = charityName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    String charityName = "";
    boolean isSelected = false;
}
