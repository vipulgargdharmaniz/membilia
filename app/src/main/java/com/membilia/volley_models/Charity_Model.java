package com.membilia.volley_models;

/**
 * Created by dharmaniz on 7/9/17.
 */

public class Charity_Model {
    private String total;
    private String sold;
    private String remain;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSold() {
        return sold;
    }

    public void setSold(String sold) {
        this.sold = sold;
    }

    public String getRemain() {
        return remain;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }
}
