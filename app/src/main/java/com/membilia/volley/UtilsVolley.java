package com.membilia.volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dharmani Apps on 7/5/2017.
 */

public class UtilsVolley {

    public static String trimMessage(String json, String key) {
        String trimmedString = null;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }
}
