package com.membilia;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.iid.FirebaseInstanceId;
import com.membilia.receiver.ConnectivityReceiver;
import com.membilia.volley.LruBitmapCache;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;

import java.util.UUID;

import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;

/**
 * Created by dharmaniz on 7/6/17.
 */

public class PlayerPawnApplication extends Application {
    private static PlayerPawnApplication mInstance;
    public static final String TAG = PlayerPawnApplication.class
            .getSimpleName();
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    public static final int CONNECTION_TIMEOUT = 120 * 1000;//120 Seconds
    private static Context mCtx;


    static String strCountriesInfo = "";

    public static synchronized PlayerPawnApplication getInstance() {
        return mInstance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        /**/
        Twitter.initialize(this);
        Fresco.initialize(this);
        /*Crashltics*/
        Fabric.with(this, new Crashlytics());
        /*Internetconnection*/
        mInstance = this;
        /*Facebook Login*/
        FacebookSdk.sdkInitialize(getApplicationContext());

//        strCountriesInfo = Countries.COUNTRIES_INFO;
//        setUpCountriesAdapter();

        Branch branch = Branch.getAutoInstance(this);
        Branch.enableLogging();
        Branch.setPlayStoreReferrerCheckTimeout(3000);
        branch.setIdentity(UUID.randomUUID().toString());

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.e(TAG,"********TOKEN*******"+refreshedToken);

        final TwitterSession activeSession = TwitterCore.getInstance()
                .getSessionManager().getActiveSession();

        // example of custom OkHttpClient with logging HttpLoggingInterceptor
//        final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
//        final OkHttpClient customClient = new OkHttpClient.Builder()
//                .addInterceptor(loggingInterceptor).build();
//
//        // pass custom OkHttpClient into TwitterApiClient and add to TwitterCore
//        final TwitterApiClient customApiClient;
//        if (activeSession != null) {
//            customApiClient = new TwitterApiClient(activeSession, customClient);
//            TwitterCore.getInstance().addApiClient(activeSession, customApiClient);
//        } else {
//            customApiClient = new TwitterApiClient(customClient);
//            TwitterCore.getInstance().addGuestApiClient(customApiClient);
//        }
    }


    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }



    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache(getApplicationContext()));
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                CONNECTION_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
}
