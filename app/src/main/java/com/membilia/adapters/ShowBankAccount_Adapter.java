package com.membilia.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.membilia.Fragments.Fragmentaddbankaccount;
import com.membilia.Fragments.Fragmentshowbankaccount;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Utilities;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.AccountDetailsModal;

import java.util.ArrayList;

/*
 * Created by dharmaniapps on 30/12/17.
 */

public class ShowBankAccount_Adapter extends RecyclerView.Adapter<ShowBankAccount_Adapter.MyViewHolder> {

    private static final String TAG = "ShowBankAccount_Adapter";
    Activity mActivity;
    private ArrayList<AccountDetailsModal> accountArraylist;
    private Fragmentshowbankaccount mFragment;

    public ShowBankAccount_Adapter(FragmentActivity activity, ArrayList<AccountDetailsModal> modelArrayList, Fragmentshowbankaccount fragmentshowbankaccount) {
        this.mActivity = activity;
        this.accountArraylist = modelArrayList;
        this.mFragment = fragmentshowbankaccount;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.swipelayout_, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final AccountDetailsModal tempModel = accountArraylist.get(position);
        holder.accountNameTV.setText(accountArraylist.get(position).getAccountName());
        holder.bankNameTV.setText(accountArraylist.get(position).getBankName());
        holder.rountingNoTV.setText(accountArraylist.get(position).getRoutingNo());
        holder.accountNoTV.setText(accountArraylist.get(position).getAccountNo());

        if (accountArraylist.get(position).isSavingAccount()) {
            holder.accountTypeTV.setText("Savings");
        } else {
            holder.accountTypeTV.setText("Checking");
        }

        if (accountArraylist.get(position).isDefault()) {
            Fragmentshowbankaccount.defaultPostion = position;
            holder.defaultIV.setImageResource(R.drawable.check_green);
            holder.defaultTV.setTextColor(mActivity.getResources().getColor(R.color.peach));
        } else {
            holder.defaultIV.setImageResource(R.drawable.unchecknew);
            holder.defaultTV.setTextColor(mActivity.getResources().getColor(R.color.gray_dark));
        }

        holder.defaultLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.showProgressDialog(mActivity);
                mFragment.MakeDefault(accountArraylist.get(position).getAccountId());
            }
        });

        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("accountName", accountArraylist.get(position).getAccountName());
                bundle.putString("bankName", accountArraylist.get(position).getBankName());
                bundle.putString("rountingNo", accountArraylist.get(position).getRoutingNo());
                bundle.putString("accountNo", accountArraylist.get(position).getAccountNo());
                bundle.putString("accountId", accountArraylist.get(position).getAccountId());
                bundle.putBoolean("Type", accountArraylist.get(position).isSavingAccount());
                HomeActivity.switchFragment((FragmentActivity) mActivity, new Fragmentaddbankaccount(), Constants.Fragment_Add_Bank_Account, true, bundle);
            }
        });
    }


    @Override
    public int getItemCount() {
        return accountArraylist.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView accountNameTV, bankNameTV, rountingNoTV, accountNoTV, accountTypeTV, defaultTV;
        ImageView defaultIV;
        LinearLayout addressItemLL, defaultLL, itemLL;

        MyViewHolder(View view) {
            super(view);
            accountNameTV = itemView.findViewById(R.id.streeTV);
            bankNameTV = itemView.findViewById(R.id.aprtTV);
            rountingNoTV = itemView.findViewById(R.id.cityTV);
            accountNoTV = itemView.findViewById(R.id.stateTV);
            accountTypeTV = itemView.findViewById(R.id.countryTV);
            defaultIV = itemView.findViewById(R.id.defaultIV);
            defaultLL = itemView.findViewById(R.id.defaultLL);
            defaultTV = itemView.findViewById(R.id.defaultTV);
            addressItemLL = itemView.findViewById(R.id.addressItemLL);
            itemLL = itemView.findViewById(R.id.itemLL);
        }
    }
}
