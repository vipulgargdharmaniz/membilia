package com.membilia.adapters;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.UsersModel;
import java.util.ArrayList;

/*
 * Created by Dharmani Apps on 6/28/2017.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    Activity mActivity;
    ArrayList<UsersModel> usersArrayList;
    private static final String TAG = "UserAdapter";
    private String strImageUrl= "";
    private Uri imgUri;

    public UserAdapter(Activity mActivity, ArrayList<UsersModel> usersArrayList) {
        this.mActivity = mActivity;
        this.usersArrayList = usersArrayList;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_userlist, parent, false);

        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, int position) {
        final UsersModel tempModel = usersArrayList.get(position);
        holder.tempProfilePic.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);

        String strUserVerified = String.valueOf(tempModel.getIsVerified());
//        if (strUserVerified.equalsIgnoreCase("true")){
            holder.tempUserName.setText(tempModel.getName());
            strImageUrl = tempModel.getUserImages().getOriginal();
            if (!strImageUrl.equals("")){
               imgUri = Uri.parse(strImageUrl);
                holder.tempProfilePic.setImageURI(imgUri);
            }
            holder.img_verified.setVisibility(View.VISIBLE);
//            holder.ll_item.setVisibility(View.VISIBLE);
//        }
        if (strUserVerified.equalsIgnoreCase("true")){
            holder.img_verified.setVisibility(View.VISIBLE);
//            holder.img_verified.setBackground(mActivity.getDrawable(R.drawable.check_green));
        }

//        else {
////            removeItem(position);
//            holder.img_verified.setVisibility(View.GONE);
//            holder.ll_item.setVisibility(View.GONE);
//        }

        holder.ll_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.forProfileID = tempModel.getUserId();
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser","false");
                HomeActivity.switchFragment((FragmentActivity) mActivity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });

    }

    @Override
    public int getItemCount() {
        return usersArrayList.size();
    }


     class UserViewHolder extends RecyclerView.ViewHolder {
         TextView tempUserName, tempAccountName;
         SimpleDraweeView tempProfilePic ,borderRing;
         LinearLayout ll_item;
        ImageView img_verified;

         UserViewHolder(View view) {
            super(view);
            tempUserName =  view.findViewById(R.id.tempUserName);
//            tempAccountName = (TextView) view.findViewById(R.id.tempAccountName);
            tempProfilePic =  view.findViewById(R.id.tempProfilePic);
            borderRing =  view.findViewById(R.id.borderRing);
            ll_item =  view.findViewById(R.id.ll_item);
            img_verified = view.findViewById(R.id.img_verified);
        }
    }
    public void removeItem(int position) {
        usersArrayList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }
}