package com.membilia.adapters;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.R;
import com.membilia.Util.Utilities;
import com.membilia.views.RatingBarView;
import com.membilia.volley_models.ReviewsModel;

import java.util.ArrayList;

/**
 * Created by Dharmani Apps on 6/28/2017.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsViewHolder> {
    Activity mActivity;
    ArrayList<ReviewsModel> usersArrayList;
    private String strUri = "";
    private Uri uri;

    public ReviewsAdapter(Activity mActivity, ArrayList<ReviewsModel> usersArrayList) {
        this.mActivity = mActivity;
        this.usersArrayList = usersArrayList;
    }

    @Override
    public ReviewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_reviews, parent, false);

        return new ReviewsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReviewsViewHolder holder, int position) {
        ReviewsModel tempModel = usersArrayList.get(position);
        holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);
        holder.profileRIV.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        if (tempModel.getSender().getUserImages().getOriginal()!=null) {
            strUri = tempModel.getSender().getUserImages().getOriginal().toString();
            if (!strUri.equalsIgnoreCase("")){

                uri = Uri.parse(strUri);
                holder.profileRIV.setImageURI(uri);
            }
//            Picasso.with(mActivity).load(tempModel.getSender().getUserImages().getOriginal()).into(holder.profileRIV);
        }

        if (tempModel.getSender().getIsVerified()==true){
            holder.imgVerify.setVisibility(View.VISIBLE);
        }
        else {
            holder.imgVerify.setVisibility(View.GONE);
        }

        if (tempModel.getItemName()!=null){
            if (tempModel.getItemName().equalsIgnoreCase("null")){
                Log.e("Review Adapter", "if: "+tempModel.getItemName() );

                holder.txtitemName.setText("");
            }
            else {
                Log.e("Review Adapter", "else: "+tempModel.getItemName() );
                holder.txtitemName.setText(tempModel.getItemName().toString());
            }
        }
        else {
            Log.e("Review Adapter", "tempModel. "+tempModel.getItemName());
            holder.txtitemName.setText("");
        }



        holder.ratingBarRB.setStar(Integer.parseInt(tempModel.getRating()), true);
        holder.txtReviews.setText(tempModel.getReview());
        String strTemp = tempModel.getDate();
        String strArray[] = strTemp.split("T");
        String strDate = strArray[0];
        String strActualFormat = Utilities.getDate(strDate);
        holder.txtDate.setText(strActualFormat);
        holder.txtUsernameTV.setText(tempModel.getSender().getName());



    }

    @Override
    public int getItemCount() {
        return usersArrayList.size();
    }


    public class ReviewsViewHolder extends RecyclerView.ViewHolder {
        public TextView txtUsernameTV, txtReviews, txtDate,  txtitemName;
        public SimpleDraweeView profileRIV , borderRing;
        public ImageView imgVerify;
        public RatingBarView ratingBarRB;

        public ReviewsViewHolder(View view) {
            super(view);
            txtUsernameTV = (TextView) view.findViewById(R.id.usernameTV);
            txtReviews = (TextView) view.findViewById(R.id.txtReviews);
            txtDate = (TextView) view.findViewById(R.id.txtDate);
            txtitemName = (TextView) view.findViewById(R.id.txtitemName);
            profileRIV = (SimpleDraweeView) view.findViewById(R.id.profileIV);
            borderRing = (SimpleDraweeView) view.findViewById(R.id.borderRing);
            imgVerify = (ImageView) view.findViewById(R.id.imgVerify);
            ratingBarRB = (RatingBarView) view.findViewById(R.id.ratingBarRB);

        }
    }
}