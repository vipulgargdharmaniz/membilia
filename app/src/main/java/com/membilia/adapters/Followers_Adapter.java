package com.membilia.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.Follow_Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dharmaniapps on 2/11/17.
 */

public class Followers_Adapter extends RecyclerView.Adapter<Followers_Adapter.ItemView>{

    private static final String TAG = "Following_Adapter";
    private Activity activity;
    private ArrayList<Follow_Model> modelArrayList;

    private String imgURl ="";
    private boolean isFollow = false;
    private Fragment fragment;
    private Uri imgUri;



    public String strUrlFOLLOW = WebServicesConstants.BASE_URL+"user/"+ HomeActivity.forProfileID+"/follow";
    public String strUrlUNFOLLOW = WebServicesConstants.BASE_URL+"user/"+HomeActivity.forProfileID+"/unfollow";


    public Followers_Adapter(FragmentActivity activity, ArrayList<Follow_Model> modelArrayList, Fragment fragment) {
        this.activity = activity;
        this.modelArrayList = modelArrayList;
        this.fragment = fragment;

    }

    @Override
    public Followers_Adapter.ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_follow, parent, false);
        return new ItemView(itemView);
    }

    @Override
    public void onBindViewHolder(final ItemView holder, final int position) {
        final Follow_Model follow_model = modelArrayList.get(position);

        holder.imageIV.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.border_ring);

        holder.txtName.setText("@"+follow_model.getUserName());

        imgURl = follow_model.getUserImages().getOriginal();
        if (!imgURl.equals("")){
            imgUri = Uri.parse(imgURl);
            holder.imageIV.setImageURI(imgUri);
//            Picasso.with(activity).load(follow_model.getUserImages().getOriginal()).placeholder(R.drawable.fb_defaulticon).into(holder.imageIV);
        }

        isFollow = follow_model.isFollowed();
        if (isFollow){
            holder.txt_follow.setText(R.string.Following);
            holder.rl_follow.setBackground(activity.getResources().getDrawable(R.drawable.greybutton));
            holder.txt_follow.setTextColor(activity.getResources().getColor(R.color.green));
        }
        else {
            holder.txt_follow.setText(R.string.Follow);
            holder.rl_follow.setBackground(activity.getResources().getDrawable(R.drawable.peachbg));
            holder.txt_follow.setTextColor(activity.getResources().getColor(R.color.white));
        }

        holder.rl_follow.setOnClickListener(new View.OnClickListener() {
            @SuppressLint({"NewApi", "ResourceAsColor"})
            @Override
            public void onClick(View view) {
                Log.e(TAG, "userid: "+follow_model.getUserid());
                HomeActivity.forProfileID = follow_model.getUserid();
                if (!isFollow){
                    strUrlFOLLOW=      WebServicesConstants.BASE_URL+"user/"+ follow_model.getUserid()+"/follow";
                    boolean result =   followServices();
                    holder.txt_follow.setText(R.string.Following);
                    holder.rl_follow.setBackground(activity.getResources().getDrawable(R.drawable.greybutton));
                    follow_model.setFollowed(result);
                    holder.txt_follow.setTextColor(R.color.green);
                    refresh(position,true);
                }
                if(isFollow) {
                    strUrlUNFOLLOW=      WebServicesConstants.BASE_URL+"user/"+ follow_model.getUserid()+"/unfollow";
                    boolean result1 =   unFollowServices();
                    holder.rl_follow.setBackground(activity.getResources().getDrawable(R.drawable.peachbg));
                    follow_model.setFollowed(result1);
                    holder.txt_follow.setText(R.string.Follow);
                    holder.txt_follow.setTextColor(activity.getResources().getColor(R.color.white));
                    refresh(position,false);
                }
            }
        });
        holder.rl_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.forProfileID = follow_model.getUserid();
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser","false");
                HomeActivity.switchFragment((FragmentActivity) activity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });
    }
    private boolean followServices() {
        Log.e(TAG, "unFollowServices: "+strUrlFOLLOW );
        StringRequest stringRequest = new StringRequest(Request.Method.POST,strUrlFOLLOW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*********onResponse*********" + response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    isFollow = jsonObject.getBoolean("isFollowed");

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json = null;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(activity, activity.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(activity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "FollowRequest");
        return isFollow;
    }

    private boolean unFollowServices() {
        Log.e(TAG, "unFollowServices: "+strUrlUNFOLLOW );
        StringRequest stringRequest = new StringRequest(Request.Method.GET,strUrlUNFOLLOW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*********onResponse*********" + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    isFollow = jsonObject.getBoolean("isFollowed");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json = null;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(activity, activity.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(activity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "FollowRequest");

        return isFollow;
    }


    public void refresh(int position,boolean isFollow) {
        modelArrayList.get(position).setFollowed(isFollow);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ItemView extends RecyclerView.ViewHolder{

        TextView txtName;
        TextView txt_follow;
        RelativeLayout rl_follow;
        SimpleDraweeView imageIV , borderRing;
        RelativeLayout rl_item;

        public ItemView(View itemView) {
            super(itemView);
            txt_follow = (TextView) itemView.findViewById(R.id.txt_follow);
            txtName = (TextView) itemView.findViewById(R.id.nameTV);
            rl_follow = (RelativeLayout) itemView.findViewById(R.id.rl_follow);
            imageIV = (SimpleDraweeView) itemView.findViewById(R.id.imageIV);
            borderRing = (SimpleDraweeView) itemView.findViewById(R.id.borderRing);
            rl_item = (RelativeLayout) itemView.findViewById(R.id.rl_item);
        }
    }
}