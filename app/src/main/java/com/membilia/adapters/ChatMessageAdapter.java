package com.membilia.adapters;

import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.R;
import com.membilia.volley_models.MessageHistoryModal;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/*
 * Created by dharmaniapps on 3/1/18.
 */

public class ChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    FragmentActivity activity;
    ArrayList<MessageHistoryModal> arrayList;

    public ChatMessageAdapter(FragmentActivity activity, ArrayList<MessageHistoryModal> arrayList, String userId) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatmessageitem, parent, false);
            return new LeftViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.right_chatmessageitem, parent, false);
            return new RightViewHolder(view);
        }
    }
    // right_chatmessageitem

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder.getItemViewType() == 0) {
            LeftViewHolder leftViewHolder = (LeftViewHolder) holder;
            leftViewHolder.msgTV.setText(arrayList.get(position).getMessage());

            int a = getDatesDifferenceInDays(Long.valueOf(arrayList.get(position).getTimeStamp()));

            if (a <= 0) {
                long number = Long.valueOf(arrayList.get(position).getTimeStamp());
                Date date = new Date(number * 1000);
                DateFormat srcDf = new java.text.SimpleDateFormat("hh:mm a");
                String formattedDate = srcDf.format(date);
                leftViewHolder.dateTV.setText(formattedDate);
            } else {
                long number = Long.valueOf(arrayList.get(position).getTimeStamp());
                Date date = new Date(number * 1000);
                DateFormat srcDf = new java.text.SimpleDateFormat("dd MMM yy hh:mm a");
                String formattedDate = srcDf.format(date);
                leftViewHolder.dateTV.setText(formattedDate);
            }

            if (!arrayList.get(position).getSenderImage().equalsIgnoreCase("")) {
                leftViewHolder.profileIV.setImageURI(Uri.parse(arrayList.get(position).getSenderImage()));
            }
        } else {
            RightViewHolder rightViewHolder = (RightViewHolder) holder;
            rightViewHolder.msgTV.setText(arrayList.get(position).getMessage());
            int a = getDatesDifferenceInDays(Long.valueOf(arrayList.get(position).getTimeStamp()));
            if (a <= 0) {
                long number = Long.valueOf(arrayList.get(position).getTimeStamp());
                Date date = new Date(number * 1000);
                DateFormat srcDf = new java.text.SimpleDateFormat("hh:mm a");
                String formattedDate = srcDf.format(date);
                rightViewHolder.dateTV.setText(formattedDate);
            } else {
                long number = Long.valueOf(arrayList.get(position).getTimeStamp());
                Date date = new Date(number * 1000);
                DateFormat srcDf = new java.text.SimpleDateFormat("dd MMM yy hh:mm a");
                String formattedDate = srcDf.format(date);
                rightViewHolder.dateTV.setText(formattedDate);
            }

        }
    }

    static int getDatesDifferenceInDays(Long startDate) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Date endData1 = new Date(timestamp.getTime() / 1000);
        Date startDate1 = new Date(startDate);
        long different = endData1.getTime() - startDate1.getTime();
        return (int) TimeUnit.SECONDS.toDays(different);
    }

    @Override
    public int getItemViewType(int position) {
        if (arrayList.get(position).getStrDirection().equalsIgnoreCase("false")) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class LeftViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView profileIV;
        TextView msgTV, dateTV;

        LeftViewHolder(View itemView) {
            super(itemView);
            profileIV = itemView.findViewById(R.id.profileIV);
            msgTV = itemView.findViewById(R.id.msgTV);
            dateTV = itemView.findViewById(R.id.dateTV);
        }
    }


    class RightViewHolder extends RecyclerView.ViewHolder {
        TextView msgTV, dateTV;

        public RightViewHolder(View itemView) {
            super(itemView);
            msgTV = itemView.findViewById(R.id.msgTV);
            dateTV = itemView.findViewById(R.id.dateTV);
        }
    }
}
