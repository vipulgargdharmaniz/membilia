package com.membilia.adapters;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.UsersModel;
import java.util.ArrayList;
/*
 * Created by dharmaniapps on 13/10/17.
 */

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.UserViewHolder> {

    private static final String TAG = "HorizontalAdapter";
    Activity mActivity;
    ArrayList<UsersModel> usersArrayList;

//    RoundingParams roundingParams =RoundingParams.asCircle();
//    RoundingParams roundingParamspeach =RoundingParams.asCircle();

    public HorizontalAdapter(Activity mActivity, ArrayList<UsersModel> usersArrayList) {
        this.mActivity = mActivity;
        this.usersArrayList = usersArrayList;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_horizontal_item, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, int position) {
        final UsersModel tempModel = usersArrayList.get(position);
        holder.tempUserName.setText(tempModel.getName());

        holder.simpleDraweeView.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);
        holder.borderRing1.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);
        if (tempModel.getIsVerified()) {
            holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringgreen);
            holder.borderRing1.getHierarchy().setPlaceholderImage(R.drawable.ringgreen);
        } else {
            holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringpeach);
            holder.borderRing1.getHierarchy().setPlaceholderImage(R.drawable.ringpeach);
        }

        if (!tempModel.getIsVerified()) {
            holder.img_verified.setVisibility(View.INVISIBLE);
        } else {
            holder.img_verified.setVisibility(View.VISIBLE);
        }
        if (tempModel.getUserImages().getOriginal()!=null)
        if (!tempModel.getUserImages().getOriginal().equals("")) {
            String IMAGE_URL = tempModel.getUserImages().getOriginal();
            Uri imguri = Uri.parse(IMAGE_URL);
            holder.simpleDraweeView.setImageURI(imguri);
        }
        holder.ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.forProfileID = tempModel.getUserId();
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser", "false");
                HomeActivity.switchFragment((FragmentActivity) mActivity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersArrayList.size();
    }

     class UserViewHolder extends RecyclerView.ViewHolder {
         TextView tempUserName;
         ImageView img_verified;
         LinearLayout ll_profile;

        SimpleDraweeView simpleDraweeView, borderRing, borderRing1;

         UserViewHolder(View view) {
            super(view);
            tempUserName =  view.findViewById(R.id.tempUserName);
            img_verified =  view.findViewById(R.id.img_verified);
            ll_profile =  view.findViewById(R.id.ll_profile);
            simpleDraweeView =  view.findViewById(R.id.tempProfilePic);
            borderRing1 =  view.findViewById(R.id.borderRing1);
            borderRing =  view.findViewById(R.id.borderRing);
        }
    }
}
