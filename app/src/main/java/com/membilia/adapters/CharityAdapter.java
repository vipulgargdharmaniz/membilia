package com.membilia.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.membilia.R;
import com.membilia.activities.CharityOptions;
import com.membilia.volley_models.CharitySelection_Modal;

import java.util.List;

/**
 * Created by dharmaniapps on 6/12/17.
 */

public  class CharityAdapter extends RecyclerView.Adapter<CharityAdapter.MyViewAdapter> {

    Context context;
    List<CharitySelection_Modal> charityArrayList;
    private int selectedPosition = -1;// no selection by default
    int currentPostion =-1 ;
    String getCurrentValue = "";
    CharityOptions fragment;

    public CharityAdapter(Activity charityOptions, List<CharitySelection_Modal> charityArraylist , CharityOptions fragment) {
        this.context = charityOptions;
        this.charityArrayList = charityArraylist;
        this.fragment = fragment;
    }

    @Override
    public MyViewAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.charityitem_layout, parent, false);

        return new CharityAdapter.MyViewAdapter(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewAdapter holder, final int position) {
        final CharitySelection_Modal charitySelection_modal = charityArrayList.get(position);

        currentPostion = position;


//        if (charitySelection_modal.isSelected()){
//            holder.checkbox.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.check_peach));
//        }
//        else {
//            holder.checkbox.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.unchecknew));
//
//        }


        holder.charityNameTV.setText(charitySelection_modal.getCharityName());
        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = position;
                holder.checkbox.setImageDrawable(context.getResources().getDrawable(R.drawable.check_peach));
                getCurrentValue = charitySelection_modal.getCharityName();
                Log.e("Charity", "onClick: "+getCurrentValue );
                CharityOptions.strCharity =getCurrentValue;
                notifyDataSetChanged();

//                ( (CharityOptions)fragment).setOnEditText(getCurrentValue);

            }
        });

        if(selectedPosition==position){
            charitySelection_modal.setSelected(true);
            holder.checkbox.setImageDrawable(context.getResources().getDrawable(R.drawable.check_peach));
        }
        else
        {
            charitySelection_modal.setSelected(false);
            holder.checkbox.setImageDrawable(context.getResources().getDrawable(R.drawable.unchecknew));

        }

    }

    private void refresh(CharitySelection_Modal charitySelection_modal) {
        charitySelection_modal.setSelected(true);
        selectedPosition = currentPostion;
        notifyDataSetChanged();
    }




    @Override
    public int getItemCount() {
        return charityArrayList.size();
    }

    public class MyViewAdapter extends RecyclerView.ViewHolder {

        TextView charityNameTV;
        ImageView checkbox;
        public MyViewAdapter(View view) {
            super(view);
            checkbox = (ImageView) view.findViewById(R.id.checkbox);
            charityNameTV = (TextView) view.findViewById(R.id.charityNameTV);
        }
    }
}
