package com.membilia.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.CategoriesItemsFragment;
import com.membilia.Fragments.Fragment_Details;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.CharityActivity;
import com.membilia.activities.HomeActivity;
import com.membilia.activities.New_PlayVideo;
import com.membilia.interfaces.LoadMoreItem;
import com.membilia.views.RatingBarView;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ItemsModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniapps on 10/11/17.
 */

public class CategoriesItem_Rest extends RecyclerView.Adapter<CategoriesItem_Rest.CatItemViewHolder> {

    String TAG = "CategoriesItem_Rest";
    Activity mActivity;
    ArrayList<ItemsModel> itemsArrayList;
    LoadMoreItem loadMoreItem;
    private String strUrl = WebServicesConstants.BASE_URL;
    private String isFavorite = "false";
    private String strImageUrl ="";
    private Uri imgUri;
    String sellerid ="";

    public CategoriesItem_Rest(Activity mActivity, ArrayList<ItemsModel> itemsArrayList, LoadMoreItem loadMoreItem,CategoriesItemsFragment fragment) {
        this.mActivity = mActivity;
        this.itemsArrayList = itemsArrayList;
        this.loadMoreItem = loadMoreItem;
    }

    @Override
    public CategoriesItem_Rest.CatItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_itemlist, parent, false);

        return new CategoriesItem_Rest.CatItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CategoriesItem_Rest.CatItemViewHolder holder, int position) {
        final ItemsModel tempModel = itemsArrayList.get(position);

        holder.usernameTV.setText(tempModel.getUserName());
        holder.imgPostImageIV.setImageUrl(tempModel.getItemImages().getOriginal1(), PlayerPawnApplication.getInstance().getImageLoader());


         sellerid = tempModel.getSellerId();
        if (tempModel.getSellerId().contains("playpawn")){
            sellerid = tempModel.getSellerId();
        }
        else {
            sellerid = tempModel.getSellerId()+"playpawn";
        }

        if (tempModel.getIsSold()){
            holder.imgSoldOutIV.setVisibility(View.VISIBLE);
            if (HomeActivity.UserItself.equalsIgnoreCase(sellerid)){
                holder.imgSoldOutIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.purchase));
            }
            else {
                holder.imgSoldOutIV.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.sold));

            }

        } else {
            holder.imgSoldOutIV.setVisibility(View.GONE);
        }


        if (tempModel.getItemVideo().getOriginal_videoimage() != null) {
            holder.imgPawnCostIV.setVisibility(View.VISIBLE);
            holder.imgPostImageIV.setBackgroundResource(R.drawable.red_placeholder);
            holder.imgPostImageIV.setImageUrl(tempModel.getItemVideo().getOriginal_videoimage(), PlayerPawnApplication.getInstance().getImageLoader());
        } else {
            holder.imgPawnCostIV.setVisibility(View.GONE);
            holder.imgPostImageIV.setBackgroundResource(R.drawable.itemplacehoder);
            holder.imgPostImageIV.setImageUrl(tempModel.getItemImages().getOriginal1(), PlayerPawnApplication.getInstance().getImageLoader());
        }



        strImageUrl = tempModel.getUserImages().getOriginal();
        if (!strImageUrl.equals("")){
            imgUri = Uri.parse(strImageUrl);
            holder.profileIV.getHierarchy().setPlaceholderImage(R.drawable.default_img);
            holder.profileIV.setImageURI(imgUri);
        }
        else {
            holder.profileIV.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        }
        holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwithgray);

        if (tempModel.getIsFavorite()) {
            holder.imgFavriteUnFavrite.setImageResource(R.drawable.star_yellow);
        }


        holder.usernameTV.setText(tempModel.getUserName());
        String number = String.valueOf(tempModel.getPrice());
        double amount = Double.parseDouble(number);
        DecimalFormat formatter = new DecimalFormat("#,###");
        String formatted = formatter.format(amount);
        holder.txtItemCostTV.setText("$" + formatted);
        holder.txtItemDescription.setText(tempModel.getItemStory());
        holder.txtItemTitle.setText(tempModel.getItemName());
        holder.ratingBarRB.setStar(Math.round(Float.parseFloat(tempModel.getRating())), true);
        holder.ratingBarRB.setClickable(false);

        if(tempModel.getCharity()){
            holder.llCharity.setVisibility(View.VISIBLE);
            holder.llCharity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, CharityActivity.class);
                    mActivity.startActivity(intent);
                    mActivity.overridePendingTransition(R.anim.in_from_right, R.anim.animback);

                }
            });

        }else
        {
            holder.llCharity.setVisibility(View.GONE);
        }

        if (tempModel.getVerifieduser()==0)
            holder.imgVerityUnverity.setVisibility(View.GONE);
        else {
            holder.imgVerityUnverity.setVisibility(View.VISIBLE);
        }

        holder.imgFavriteUnFavrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (tempModel.getIsFavorite()) {
                    String result = setServices("itemunfav/" + tempModel.getItemId());
                    tempModel.setIsFavorite(false);
                    holder.imgFavriteUnFavrite.setImageResource(R.drawable.star_white);
                } else {
                    String result1 = setServices("itemfav/" + tempModel.getItemId());
                    tempModel.setIsFavorite(true);
                    holder.imgFavriteUnFavrite.setImageResource(R.drawable.star_yellow);
                }
            }
        });


        holder.imgPostImageIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempModel.getItemVideo().getOriginal_videoimage() != null)
                {
                    Intent intent = new Intent(mActivity, New_PlayVideo.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("VIDEO", tempModel.getItemVideo().getOriginal1().trim());
                    intent.putExtras(bundle);
                    mActivity.startActivity(intent);
                    mActivity.overridePendingTransition(R.anim.in_from_right, R.anim.animback);
                } else
                    if (tempModel.getItemId().length() > 0) {
                        String stritemid = tempModel.getItemId();
                        Fragment_Details fragment = new Fragment_Details();
                        Bundle bundle = new Bundle();
                        bundle.putString("Object", stritemid);
                        fragment.setArguments(bundle);
                        HomeActivity.switchFragment((FragmentActivity) mActivity, new Fragment_Details(), Constants.Fragment_Details, true, bundle);
                    }
            }
        });

        holder.bottomviewRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tempModel.getItemId().length() > 0) {
                    String stritemid = tempModel.getItemId();
                    Fragment_Details fragment = new Fragment_Details();
                    Bundle bundle = new Bundle();
                    bundle.putString("Object", stritemid);
                    fragment.setArguments(bundle);
                    HomeActivity.switchFragment((FragmentActivity) mActivity, new Fragment_Details(), Constants.Fragment_Details, true, bundle);
                }
            }
        });

        holder.topviewRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.forProfileID = tempModel.getUserId();
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser","false");
                HomeActivity.switchFragment((FragmentActivity) mActivity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });
    }

    private String setServices(String itemType) {

        String strUrl1 = strUrl + itemType;

        StringRequest postRequest = new StringRequest(Request.Method.GET, strUrl1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            isFavorite = String.valueOf(jsonObject.getBoolean("isFavorite"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        isFavorite = "error";
                        String json ;
                        NetworkResponse response = error.networkResponse;
                        if (response != null && response.data != null) {
                            switch (response.statusCode) {
                                case 403:
                                    json = new String(response.data);
                                    json = UtilsVolley.trimMessage(json, "message");
                                    if (json != null)
                                         AlertDialogManager.showAlertDialog(mActivity, mActivity.getResources().getString(R.string.app_name), json);
                                    break;
                            }
                        }
                    }
                }
        ) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(mActivity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().
                addToRequestQueue(postRequest);

        return isFavorite;
    }

    @Override
    public int getItemCount() {
        return itemsArrayList.size();
    }

     class CatItemViewHolder extends RecyclerView.ViewHolder {
         SimpleDraweeView profileIV , borderRing;
         TextView usernameTV;
         ImageView imgVerityUnverity;
         RatingBarView ratingBarRB;
         NetworkImageView imgPostImageIV;
         TextView txtItemTitle, txtItemCostTV, txtItemDescription;
         ImageView imgFavriteUnFavrite, charityicon;
         LinearLayout llCharity, ll1;
         ImageView imgPawnCostIV, imgSoldOutIV;
         RelativeLayout nameRV, bottomviewRV;
         RelativeLayout imgRV;
         RelativeLayout topviewRV;


         CatItemViewHolder(View view) {
            super(view);
            profileIV =  view.findViewById(R.id.profileIV);
            borderRing =  view.findViewById(R.id.borderRing);
            usernameTV =  view.findViewById(R.id.usernameTV);
            imgVerityUnverity =  view.findViewById(R.id.imgVerityUnverity);
            ratingBarRB =  view.findViewById(R.id.ratingBarRB);
            imgPostImageIV =  view.findViewById(R.id.imgPostImageIV);
            txtItemTitle =  view.findViewById(R.id.txtItemTitle);
            txtItemCostTV =  view.findViewById(R.id.txtItemCostTV);
            txtItemDescription =  view.findViewById(R.id.txtItemDescription);
            imgFavriteUnFavrite =  view.findViewById(R.id.imgFavriteUnFavrite);
            charityicon =  view.findViewById(R.id.charityicon);
            imgPawnCostIV =  view.findViewById(R.id.imgPawnCostIV);
            imgSoldOutIV =  view.findViewById(R.id.imgSoldOutIV);
            llCharity = view.findViewById(R.id.llCharity);
            ll1 =  view.findViewById(R.id.ll1);
            nameRV =  view.findViewById(R.id.nameRV);
            imgRV =  view.findViewById(R.id.imgRV);
            bottomviewRV =  view.findViewById(R.id.bottomviewRV);
            topviewRV =  view.findViewById(R.id.topviewRV);
        }
    }
}
