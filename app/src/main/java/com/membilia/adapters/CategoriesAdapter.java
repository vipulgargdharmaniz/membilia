package com.membilia.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.membilia.R;
import com.membilia.volley_models.CategoriesModel;

import java.util.ArrayList;

/**
 * Created by dharmaniz on 8/6/17.
 */

public class CategoriesAdapter extends BaseAdapter {
    Activity mActivity;
    ArrayList<CategoriesModel> mArrayList;

    public CategoriesAdapter(Activity mActivity, ArrayList<CategoriesModel> mArrayList) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.item_category, parent, false);
            holder = new ViewHolder();
            holder.itemImage = (ImageView) convertView.findViewById(R.id.itemImage);
            holder.itemText = (TextView) convertView.findViewById(R.id.itemText);
            holder.ll_item = (LinearLayout) convertView.findViewById(R.id.ll_item);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final CategoriesModel tempValue = mArrayList.get(position);
        holder.itemImage.setImageResource(tempValue.getImage());
        holder.itemText.setText(tempValue.getName());

//        holder.ll_item.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Toast.makeText(mActivity,""+tempValue.getName(),Toast.LENGTH_LONG).show();
//            }
//        });

        return convertView;
    }

    public class ViewHolder {
        ImageView itemImage;
        TextView itemText;
        LinearLayout ll_item;
    }
}
