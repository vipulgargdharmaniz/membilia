package com.membilia.adapters;

/*
 * Created by dharmaniapps on 25/1/18.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.Fragment_Details;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.CharityActivity;
import com.membilia.activities.HomeActivity;
import com.membilia.activities.New_PlayVideo;
import com.membilia.views.RatingBarView;
import com.membilia.volley_models.ItemsModel;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ItemListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ItemListAdapter";
    private ArrayList<ItemsModel> itemList;
    FragmentActivity activity;
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    private String isFavorite = "false";

    public ItemListAdapter(FragmentActivity activity) {
        this.activity = activity;
        this.itemList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        View itemView2;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ITEM:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.itemlistadapter, parent, false);
                viewHolder = new MyViewHolder(itemView);
                break;
            case LOADING:
                if (itemList.size() == 0) {
                    Log.e(TAG, "onCreateViewHolder111: " + itemList.size());
                    itemView = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.itemlistadapter, parent, false);
                    viewHolder = new MyViewHolder(itemView);
                } else {
                    Log.e(TAG, "onCreateViewHolder 222: " + itemList.size());
                    itemView2 = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_progress, parent, false);
                    viewHolder = new MyLoaderHolder(itemView2);
                }
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                final MyViewHolder holder = (MyViewHolder) holder1;
                final ItemsModel tempModel = itemList.get(position);

                holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwithgray);
                holder.usernameTV.setText(tempModel.getUserName());
                holder.txtItemTitle.setText(tempModel.getItemName());
                Log.e(TAG, "getUserImages: " + tempModel.getUserImages().getOriginal());
                if (!tempModel.getUserImages().getOriginal().equalsIgnoreCase(""))
                    holder.profileIV.setImageURI(Uri.parse(tempModel.getUserImages().getOriginal()));
                if (tempModel.getItemVideo().getOriginal_videoimage() != null) {
                    holder.itemIV.getHierarchy().setPlaceholderImage(R.drawable.red_placeholder);
                    holder.itemIV.setImageURI(Uri.parse(tempModel.getItemVideo().getOriginal_videoimage()));
                } else if (tempModel.getItemImages().getOriginal1() != null) {
                    if (Uri.parse(tempModel.getItemImages().getOriginal1()) != null){
                        holder.itemIV.getHierarchy().setPlaceholderImage(R.drawable.itemplacehoder);
                        holder.itemIV.setImageURI(Uri.parse(tempModel.getItemImages().getOriginal1()));
                    }

                }
                holder.itemStoryTV.setText(tempModel.getItemStory());

                Log.e(TAG, "onBindViewHolder:price " + String.valueOf(tempModel.getPrice()));
                String number = String.valueOf(tempModel.getPrice());
                double amount = Double.parseDouble(number);
                DecimalFormat formatter = new DecimalFormat("#,###");
                String formatted = formatter.format(amount);
                holder.itemCostTV.setText("$" + formatted);

                if (tempModel.getVerifieduser() == 1)
                    holder.verifyIV.setVisibility(View.VISIBLE);
                else
                    holder.verifyIV.setVisibility(View.GONE);
                holder.ratingBarRB.setStar(Math.round(Float.parseFloat(tempModel.getRating())), true);


                holder.llCharity.setVisibility(View.GONE);
                if (tempModel.getCharity()) {
                    holder.llCharity.setVisibility(View.VISIBLE);
                    holder.llCharity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (AlertDialogManager.haveNetworkConnection(activity)) {
                                Intent intent = new Intent(activity, CharityActivity.class);
                                activity.startActivity(intent);
                                activity.overridePendingTransition(R.anim.in_from_right, R.anim.animback);

                            }else {
                                AlertDialogManager.showAlertDialogOK(activity, "Membilia", "Please connect with internet!");
                            }
                        }
                    });

                } else {
                    holder.llCharity.setVisibility(View.GONE);
                }

                if (tempModel.getIsFavorite()) {
                    holder.imgFavriteUnFavrite.setImageResource(R.drawable.star_yellow);
                }

                holder.imgFavriteUnFavrite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (AlertDialogManager.haveNetworkConnection(activity)) {
                            methodForFav(tempModel.getIsFavorite(), tempModel, holder);
                        }else {
                            AlertDialogManager.showAlertDialogOK(activity, "Membilia", "Please connect with internet!");
                        }
                    }
                });

                holder.topviewRL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (AlertDialogManager.haveNetworkConnection(activity)) {
                            HomeActivity.forProfileID = tempModel.getUserId();
                            Bundle bundle = new Bundle();
                            bundle.putString("isSameUser", "false");
                            HomeActivity.switchFragment(activity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);

                        } else {
                            AlertDialogManager.showAlertDialogOK(activity, "Membilia", "Please connect with internet!");
                        }
                    }
                });
                if (tempModel.getItemVideo().getOriginal_videoimage() != null) {
                    holder.imgPawnCostIV.setVisibility(View.VISIBLE);
                } else {
                    holder.imgPawnCostIV.setVisibility(View.GONE);
                }

                holder.bottomviewRV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AlertDialogManager.haveNetworkConnection(activity)) {
                            String stritemid = tempModel.getItemId();
                            Fragment_Details fragment = new Fragment_Details();
                            Bundle bundle = new Bundle();
                            bundle.putString("Object", stritemid);
                            fragment.setArguments(bundle);
                            HomeActivity.switchFragment(activity, new Fragment_Details(), Constants.Fragment_Details, true, bundle);
                        }else {
                            AlertDialogManager.showAlertDialogOK(activity, "Membilia", "Please connect with internet!");
                        }
                    }
                });

                String sellerid;
                if (tempModel.getSellerId().contains("playpawn")) {
                    sellerid = tempModel.getSellerId();
                } else {
                    sellerid = tempModel.getSellerId() + "playpawn";
                }

                if (tempModel.getIsSold()) {
                    holder.imgSoldOutIV.setVisibility(View.VISIBLE);
                    if (HomeActivity.UserItself.equalsIgnoreCase(sellerid)) {
                        holder.imgSoldOutIV.setImageDrawable(activity.getResources().getDrawable(R.drawable.purchase));
                    } else {
                        holder.imgSoldOutIV.setImageDrawable(activity.getResources().getDrawable(R.drawable.sold));

                    }
                } else {
                    holder.imgSoldOutIV.setVisibility(View.GONE);
                }

                holder.itemIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (AlertDialogManager.haveNetworkConnection(activity)) {
                            if (tempModel.getItemVideo().getOriginal_videoimage() != null) {
                                Intent intent = new Intent(activity, New_PlayVideo.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("VIDEO", tempModel.getItemVideo().getOriginal1().toString().trim());
                                intent.putExtras(bundle);
                                activity.startActivity(intent);
                                activity.overridePendingTransition(R.anim.in_from_right, R.anim.animback);

                            } else if (tempModel.getItemId().length() > 0) {

                                String stritemid = tempModel.getItemId();
                                Fragment_Details fragment = new Fragment_Details();
                                Bundle bundle = new Bundle();
                                bundle.putString("Object", stritemid);
                                fragment.setArguments(bundle);
                                HomeActivity.switchFragment(activity, new Fragment_Details(), Constants.Fragment_Details, true, bundle);
                            }
                        }else {
                            AlertDialogManager.showAlertDialogOK(activity, "Membilia", "Please connect with internet!");
                        }

                    }
                });
                break;
            case LOADING:
                break;
        }
    }

    private void methodForFav(Boolean isFavorite, ItemsModel tempModel, MyViewHolder holder) {
        if (isFavorite) {
            if (!PlayerPawnPreference.readString(activity, PlayerPawnPreference.VALUE_TOKEN, "").isEmpty()) {
                setServices("itemunfav/" + tempModel.getItemId());
                tempModel.setIsFavorite(false);
                holder.imgFavriteUnFavrite.setImageResource(R.drawable.star_white);
            }
        } else {
            if (!PlayerPawnPreference.readString(activity, PlayerPawnPreference.VALUE_TOKEN, "").isEmpty()) {
                setServices("itemfav/" + tempModel.getItemId());
                tempModel.setIsFavorite(true);
                holder.imgFavriteUnFavrite.setImageResource(R.drawable.star_yellow);
            }
        }
    }


    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == itemList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(ArrayList<ItemsModel> actModelArrayList) {
        itemList.addAll(actModelArrayList);
        notifyItemInserted(itemList.size() - 1);
    }


    public void addAll(ArrayList<ItemsModel> actModelArrayList) {
        add(actModelArrayList);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        int position = itemList.size();
        ItemsModel result = getItem(position);
        if (result != null) {
            itemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private ItemsModel getItem(int position) {
        return itemList.get(position);
    }

    public void clear() {
        if (itemList.size() > 0) {
            int size = this.itemList.size();
            this.itemList.clear();
            notifyItemRangeRemoved(0, size);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        SimpleDraweeView profileIV, borderRing, itemIV;
        TextView usernameTV, itemStoryTV;
        ImageView verifyIV;
        RatingBarView ratingBarRB;
        TextView txtItemTitle, itemCostTV;
        ImageView imgFavriteUnFavrite, charityicon;
        LinearLayout llCharity, ll1;
        ImageView imgPawnCostIV, imgSoldOutIV;
        RelativeLayout nameRV, bottomviewRV;
        RelativeLayout imgRV;
        RelativeLayout topviewRV, topviewRL;

        MyViewHolder(View view) {
            super(view);
            profileIV = view.findViewById(R.id.profileIV);
            borderRing = view.findViewById(R.id.borderRing);
            usernameTV = view.findViewById(R.id.usernameTV);
            verifyIV = view.findViewById(R.id.verifyIV);
            itemIV = view.findViewById(R.id.itemIV);
            ratingBarRB = view.findViewById(R.id.ratingBarRB);
            txtItemTitle = view.findViewById(R.id.txtItemTitle);
            itemCostTV = view.findViewById(R.id.itemCostTV);
            itemStoryTV = view.findViewById(R.id.itemStoryTV);
            imgFavriteUnFavrite = view.findViewById(R.id.imgFavriteUnFavrite);
            charityicon = view.findViewById(R.id.charityicon);
            imgPawnCostIV = view.findViewById(R.id.imgPawnCostIV);
            imgSoldOutIV = view.findViewById(R.id.imgSoldOutIV);
            llCharity = view.findViewById(R.id.llCharity);
            ll1 = view.findViewById(R.id.ll1);
            nameRV = view.findViewById(R.id.nameRV);
            imgRV = view.findViewById(R.id.imgRV);
            bottomviewRV = view.findViewById(R.id.bottomviewRV);
            topviewRV = view.findViewById(R.id.topviewRV);
            topviewRL = view.findViewById(R.id.topviewRL);
        }
    }

    public class MyLoaderHolder extends RecyclerView.ViewHolder {
        MyLoaderHolder(View itemView) {
            super(itemView);
        }
    }


    private String setServices(String itemType) {
        String strUrl1 = WebServicesConstants.BASE_URL + itemType;
        StringRequest postRequest = new StringRequest(Request.Method.GET, strUrl1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            isFavorite = String.valueOf(jsonObject.getBoolean("isFavorite"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(activity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(postRequest);
        return isFavorite;
    }
}
