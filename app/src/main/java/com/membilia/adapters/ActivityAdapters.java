package com.membilia.adapters;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.ExploreFragment;
import com.membilia.Fragments.Fragment_Details;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.Fragments.SendMessage_Fragment;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.ActModel;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.membilia.Util.Constants.IS_HOMEPOST_REFRESH;
/*
 * Created by dharmaniz on 23/8/17.
 */

public class ActivityAdapters extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ActivityAdapters";
    Activity mActivity;
    private ArrayList<ActModel> usersArrayList;
    private String openFragment = "";

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;

    public ActivityAdapters(Activity mActivity) {
        this.mActivity = mActivity;
        usersArrayList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;
        View itemView2;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ITEM:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_activitylist, parent, false);
                viewHolder = new ActivityViewHolder(itemView);
                break;
            case LOADING:
                itemView2 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(itemView2);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case ITEM:
                final ActivityViewHolder actHolder = (ActivityViewHolder) holder;
                final ActModel tempModel = usersArrayList.get(position);
                String Name = tempModel.getAct_UserModel().getName();
                String decrrption = tempModel.getText();
                String name = getColoredSpanned(Name, "#00796B");
                String surName = getColoredSpanned(decrrption, "#b3b3b3");

                actHolder.imageIV.getHierarchy().reset();
                actHolder.imageIV.getHierarchy().setPlaceholderImage(R.drawable.footerlogo);


                Log.e(TAG, "onBindViewHolder: " + tempModel.getType() + "    " + tempModel.getText());
                if (tempModel.getType().equals("1")) {
                    actHolder.iconImg.setImageResource(R.drawable.add_new);
                } else if (tempModel.getType().equals("2")) {
                    actHolder.iconImg.setImageResource(R.drawable.now_on);
                } else if (tempModel.getType().equals("3")) {
                    actHolder.iconImg.setImageResource(R.drawable.shipped);
                } else if (tempModel.getType().equals("4")) {
                    actHolder.iconImg.setImageResource(R.drawable.sale_act);
                } else if (tempModel.getType().equals("5")) {
                    actHolder.iconImg.setImageResource(R.drawable.new_register);
                } else if (tempModel.getType().equals("6")) {
                    actHolder.iconImg.setImageResource(R.drawable.follow_you);
                } else if (tempModel.getType().equals("7")) {
                    actHolder.iconImg.setImageResource(R.drawable.celebrity);
                } else if (tempModel.getType().equals("8")) {
                    actHolder.iconImg.setImageResource(R.drawable.live);
                } else if (tempModel.getType().equals("9")) {
                    actHolder.iconImg.setImageResource(R.drawable.message_act);
                } else if (tempModel.getType().equals("10")) {
                    actHolder.iconImg.setImageResource(R.drawable.sold_activity);
                } else if (tempModel.getType().equals("11")) {
                    actHolder.iconImg.setImageResource(R.drawable.money);
                } else if (tempModel.getType().equals("12")) {
                    actHolder.iconImg.setImageResource(R.drawable.new_register);
                } else if (tempModel.getType().equals("13")) {
                    actHolder.iconImg.setImageResource(R.drawable.refund_new);
                } else if (tempModel.getType().equals("14")) {
                    actHolder.iconImg.setImageResource(R.drawable.purchased);
                } else if (tempModel.getType().equals("15")) {
                    actHolder.iconImg.setImageResource(R.drawable.review);
                } else if (tempModel.getType().equals("16")) {
                    actHolder.iconImg.setImageResource(R.drawable.refund_new);
                } else if (tempModel.getType().equals("17")) {
                    actHolder.iconImg.setImageResource(R.drawable.charity_new);
                }
                 /*Setting Up Time*/
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());

                //return number of milliseconds since January 1, 1970, 00:00:00 GMT
                System.out.println(timestamp.getTime());
                int a = getDatesDifferenceInDays(Long.valueOf(tempModel.getCreated()), timestamp.getTime() / 1000);

                long different = timestamp.getTime() / 1000 - Long.valueOf(tempModel.getCreated());

                int hours = (int) TimeUnit.SECONDS.toHours(different);
                int mints = (int) TimeUnit.SECONDS.toMinutes(different);


                if (a > 30) {
                    int months = a / 30;
                    if (months == 1) {
                        actHolder.timeTV.setText(months + " month ago");
                    } else {
                        actHolder.timeTV.setText(months + " months ago");
                    }

                } else if (a < 30 && a > 7) {
                    int weeks = a / 7;
                    if (weeks == 1) {
                        actHolder.timeTV.setText(weeks + " week ago");
                    } else {
                        actHolder.timeTV.setText(weeks + " weeks ago");
                    }
                } else if (a > 0) {
                    if (a > 1) {
                        actHolder.timeTV.setText(a + " days ago");
                    } else {
                        actHolder.timeTV.setText(a + " day ago");
                    }
                } else {

                    if (hours < 1) {
                        actHolder.timeTV.setText(mints + " minutes ago");
                    } else
                        actHolder.timeTV.setText(hours + " hours ago");

                }

                if (tempModel.getType().equals("4")) {
                    try {
                        Uri uri = Uri.parse("android.resource://com.playerpaw/drawable/footerlogo");
                        actHolder.imageIV.setImageURI(uri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    actHolder.nameTV.setText(Html.fromHtml(surName));
                } else {
                    if (!tempModel.getAct_UserModel().getAct_UserImagesModel().getOriginal().equals("")) {
                        actHolder.imageIV.setImageURI(Uri.parse(tempModel.getAct_UserModel().getAct_UserImagesModel().getOriginal()));
                    }
                    actHolder.nameTV.setText(Html.fromHtml(name + " " + surName));
                }

                actHolder.itemLL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openFragment = String.valueOf(getFragmentType(Integer.parseInt(tempModel.getType())));
                        if (openFragment.equalsIgnoreCase("Profile")) {
                            HomeActivity.forProfileID = tempModel.getAct_UserModel().getUser_id();
                            Bundle bundle = new Bundle();
                            bundle.putString("isSameUser", "false");
                            bundle.putString("subFragment", "fav");
                            HomeActivity.switchFragment((FragmentActivity) mActivity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
                        } else if (openFragment.equalsIgnoreCase("ProfileReview")) {
                            HomeActivity.forProfileID = tempModel.getAct_UserModel().getUser_id();
                            Bundle bundle = new Bundle();
                            bundle.putString("isSameUser", "false");
                            bundle.putString("subFragment", "review");
                            HomeActivity.switchFragment((FragmentActivity) mActivity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
                        } else if (openFragment.equalsIgnoreCase("Expore")) {
                            Constants.additemFragment = "0";
                            IS_HOMEPOST_REFRESH = true;
                            HomeActivity.itemType = "relativeClick";
                            HomeActivity.switchFragment((FragmentActivity) mActivity, new ExploreFragment(), Constants.EXPLORE_FRAGMENT, false, null);
                        } else if (openFragment.equalsIgnoreCase("Expore Admin")) {
                            Constants.additemFragment = "0";
                            IS_HOMEPOST_REFRESH = true;
                            HomeActivity.itemType = "relativeClick";
                            HomeActivity.switchFragment((FragmentActivity) mActivity, new ExploreFragment(), Constants.EXPLORE_FRAGMENT, false, null);
                        } else if (openFragment.equalsIgnoreCase("Details")) {
                            String stritemid = tempModel.getAct_ActivityDetailsModel().getItem_id();
                            Fragment_Details fragment = new Fragment_Details();
                            Bundle bundle = new Bundle();
                            bundle.putString("Object", stritemid);
                            fragment.setArguments(bundle);
                            HomeActivity.switchFragment((FragmentActivity) mActivity, new Fragment_Details(), Constants.Fragment_Details, true, bundle);
                        } else if (openFragment.equalsIgnoreCase("Message")) {
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();
                            Constants_AddItem.ITEM_ID = tempModel.getAct_ActivityDetailsModel().getItem_id();
                            Log.e(TAG, "onClick:getSeller_id " + tempModel.getAct_ActivityDetailsModel().getSeller_id());
                            Log.e(TAG, "onClick:ITEM_ID " + Constants_AddItem.ITEM_ID);

                            if (!Constants_AddItem.ITEM_ID.equalsIgnoreCase("playpawn")) {
                                Bundle bundle = new Bundle();
                                bundle.putString("friend_id", tempModel.getAct_ActivityDetailsModel().getSeller_id() + "playpawn");
                                bundle.putString("timeStamp", ts);
                                bundle.putString("item_id", tempModel.getAct_ActivityDetailsModel().getItem_id());

                                HomeActivity.switchFragment((FragmentActivity) mActivity, new SendMessage_Fragment(), "SendMessage_Fragment", true, bundle);
                            } else {
                                AlertDialogManager.showAlertDialog(mActivity, "Membilia", "Sorry this message is related to particular item which has been deleted");
                            }

//
                        } else {
                            Toast.makeText(mActivity, "Under Progress", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case LOADING:
//                Do nothing
                break;
        }
    }

    private String getFragmentType(int type) {
        if (type == 5 || type == 6 || type == 7) {
            openFragment = "Profile";
            return openFragment;
        } else if (type == 15) {
            return "ProfileReview";
        } else if (type == 12) {
            return "Expore";
        } else if (type == 1 || type == 8 || type == 10 || type == 13 || type == 14 || type == 16 || type == 3 || type == 2 || type == 17) {
            return "Details";
        } else if (type == 9) {
            return "Message";
        } else if (type == 4) {
            return "Expore Admin";
        }
        return "";
    }

    static int getDatesDifferenceInDays(Long startDate, Long endDate) {
        Date endData1 = new Date(endDate);
        Date startDate1 = new Date(startDate);
        long different = endData1.getTime() - startDate1.getTime();
        return (int) TimeUnit.SECONDS.toDays(different);
    }

    /*********FOR COLOR CHNAGE IN SINGLE TEXT VIEW ***********/
    private String getColoredSpanned(String text, String color) {
        return "<font color=" + color + ">" + text + "</font>";
    }

    @Override
    public int getItemCount() {
        return usersArrayList == null ? 0 : usersArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == usersArrayList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(ArrayList<ActModel> actModelArrayList) {
        usersArrayList.addAll(actModelArrayList);
        notifyItemInserted(usersArrayList.size() - 1);
    }


    public void addAll(ArrayList<ActModel> actModelArrayList) {
        add(actModelArrayList);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        int position = usersArrayList.size() - 1;
        ActModel result = getItem(position);
        if (result != null) {
//            usersArrayList.remove(position);
//            notifyItemRemoved(position);
        }
    }

    public void clear() {
        usersArrayList.clear();
        notifyDataSetChanged();
    }

    private ActModel getItem(int position) {
        return usersArrayList.get(position);
    }

    public class ActivityViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV, timeTV;
        SimpleDraweeView imageIV;
        ImageView iconImg;
        LinearLayout itemLL;

        ActivityViewHolder(View view) {
            super(view);
            nameTV = view.findViewById(R.id.nameTV);
            timeTV = view.findViewById(R.id.timeTV);
            imageIV = view.findViewById(R.id.imageIV);
            iconImg = view.findViewById(R.id.iconImg);
            itemLL = view.findViewById(R.id.itemLL);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        LoadingVH(View itemView) {
            super(itemView);
        }
    }
}