package com.membilia.adapters;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.FragmentMessageHistory;
import com.membilia.Fragments.SendMessage_Fragment;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.MessageHistoryModal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
/*
 * Created by dharmaniapps on 3/1/18.
 */

public class MessageHistoryAdapter extends RecyclerView.Adapter<MessageHistoryAdapter.MyViewHolder> {


    FragmentActivity activity;
    ArrayList<MessageHistoryModal> arrayList;
    private String friendId = "";
    FragmentMessageHistory mFragment;

    public MessageHistoryAdapter(FragmentActivity activity, ArrayList<MessageHistoryModal> arrayList, String userId, FragmentMessageHistory mFragment) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.mFragment = mFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_messagelist, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    friendId = arrayList.get(position).getReciverId();
                    Bundle bundle = new Bundle();
                    bundle.putString("friend_id", friendId);
                    bundle.putString("timeStamp", arrayList.get(position).getTimeStamp());
                    bundle.putString("item_id", arrayList.get(position).getItemId());
                    Constants_AddItem.ITEM_NAME = arrayList.get(position).getItemName();
                    Constants_AddItem.ITEM_ID = arrayList.get(position).getItemId();
                    HomeActivity.switchFragment(activity, new SendMessage_Fragment(), Constants.SendMessage, true, bundle);
            }
        });
        String itemname = "\""+arrayList.get(position).getItemName()+"\"";
        holder.itemNameTV.setText(itemname);
        holder.msgTV.setText(arrayList.get(position).getMessage());
        holder.nameTV.setText("@" + arrayList.get(position).getRecivierName());
        if (!arrayList.get(position).getRecivierImage().equalsIgnoreCase("")){
            holder.profileIV.setImageURI(Uri.parse(arrayList.get(position).getRecivierImage()));
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String current = df.format(c.getTime());
        long number = Long.valueOf(arrayList.get(position).getTimeStamp());
        Date date = new Date(number * 1000);
        DateFormat srcDf = new java.text.SimpleDateFormat("dd MMM yyyy hh:mm a");
        String formattedDate = srcDf.format(date);
        if (formattedDate.contains(current)) {
            DateFormat srcDf1 = new java.text.SimpleDateFormat("hh:mm a");
            String formattedDate1 = srcDf1.format(date);
            holder.dateTV.setText(formattedDate1);
        } else
            holder.dateTV.setText(formattedDate);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTV, dateTV, msgTV,itemNameTV;
        SimpleDraweeView profileIV;
        LinearLayout itemLL;

        MyViewHolder(View view) {
            super(view);
            nameTV = view.findViewById(R.id.nameTV);
            profileIV = view.findViewById(R.id.profileIV);
            dateTV = view.findViewById(R.id.dateTV);
            msgTV = view.findViewById(R.id.msgTV);
            itemLL = view.findViewById(R.id.itemLL);
            itemNameTV = view.findViewById(R.id.itemNameTV);
        }
    }
}
