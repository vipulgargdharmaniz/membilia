package com.membilia.adapters;

import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.R;
import com.membilia.volley_models.DetailedTransactionModal;

import java.util.ArrayList;

/**
 * Created by dharmaniapps on 30/12/17.
 */

public class DetailedPayment_Adapter extends RecyclerView.Adapter<DetailedPayment_Adapter.MyViewHolder> {

    FragmentActivity activity;
    ArrayList<DetailedTransactionModal> arrayList;

    public DetailedPayment_Adapter(FragmentActivity activity, ArrayList<DetailedTransactionModal> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detailed_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.itemNameTV.setText(arrayList.get(position).getItem_name());
        holder.salePriceTV.setText(arrayList.get(position).getPrice());
        holder.dateTV.setText(arrayList.get(position).getOrder_date());
        holder.earningTV.setText(arrayList.get(position).getEarning());
        if (!arrayList.get(position).getItem_image().equalsIgnoreCase(""))
        holder.itemImgIV.setImageURI(Uri.parse(arrayList.get(position).getItem_image()));
//        holder.itemNameTV.setText(arrayList.get(position).getItem_name());
        if (!arrayList.get(position).getVideo_image().equalsIgnoreCase(""))
            holder.itemImgIV.setImageURI(Uri.parse(arrayList.get(position).getVideo_image()));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView itemNameTV, dateTV, salePriceTV, earningTV;
        public SimpleDraweeView itemImgIV;
        public ImageView iconImg;
        public LinearLayout detailedItemLLl;

        public MyViewHolder(View view) {
            super(view);
            itemNameTV = (TextView) view.findViewById(R.id.itemNameTV);
            dateTV = (TextView) view.findViewById(R.id.dateTV);
            itemImgIV = (SimpleDraweeView) view.findViewById(R.id.itemImgIV);
            iconImg = (ImageView) view.findViewById(R.id.iconImg);
            detailedItemLLl = (LinearLayout) view.findViewById(R.id.detailedItemLL);
            earningTV = (TextView) view.findViewById(R.id.earningTV);
            salePriceTV = (TextView) view.findViewById(R.id.salePriceTV);
        }
    }
}
