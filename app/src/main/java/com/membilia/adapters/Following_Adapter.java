package com.membilia.adapters;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.Follow_Model;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/*
 * Created by dharmaniapps on 31/10/17.
 */

public class Following_Adapter extends RecyclerView.Adapter<Following_Adapter.ItemView> {

    private static final String TAG = "Following_Adapter";
    private Activity activity;
    private ArrayList<Follow_Model> modelArrayList;
    private String imgURl = "";
    private boolean isFollow = false;
    private Uri uri;
     String strUrlFOLLOW = WebServicesConstants.BASE_URL + "user/" + HomeActivity.forProfileID + "/follow";
     String strUrlUNFOLLOW = WebServicesConstants.BASE_URL + "user/" + HomeActivity.forProfileID + "/unfollow";


    public Following_Adapter(FragmentActivity activity, ArrayList<Follow_Model> modelArrayList, Fragment fragment) {
        this.activity = activity;
        this.modelArrayList = modelArrayList;


    }

    @Override
    public ItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_follow, parent, false);

        return new Following_Adapter.ItemView(itemView);
    }

    @Override
    public void onBindViewHolder(ItemView holder, final int position) {

        final Follow_Model follow_model = modelArrayList.get(position);

        holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.border_ring);
        holder.imageIV.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        holder.txtName.setText("@" + follow_model.getUserName());

        imgURl = follow_model.getUserImages().getOriginal();
        if (!imgURl.equals("")) {
            uri = Uri.parse(imgURl);
            holder.imageIV.setImageURI(uri);
        }

        isFollow = follow_model.isFollowed();

        holder.ll_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "userid: " + follow_model.getUserid());
                HomeActivity.forProfileID = follow_model.getUserid();

                if (isFollow) {
                    strUrlUNFOLLOW = WebServicesConstants.BASE_URL + "user/" + follow_model.getUserid() + "/unfollow";

                    boolean result1 = unFollowServices();
                    follow_model.setFollowed(result1);
                    removeItem(position);
                }
            }
        });

        holder.rl_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.forProfileID = follow_model.getUserid();
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser", "false");
                HomeActivity.switchFragment((FragmentActivity) activity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });
    }

    private boolean followServices() {
        Log.e(TAG, "unFollowServices: " + strUrlFOLLOW);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrlFOLLOW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*********onResponse*********" + response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    isFollow = jsonObject.getBoolean("isFollowed");

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json ;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(activity, activity.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(activity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "FollowRequest");
        return isFollow;
    }

    private boolean unFollowServices() {
        Log.e(TAG, "unFollowServices: " + strUrlUNFOLLOW);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrlUNFOLLOW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*********onResponse*********" + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    isFollow = jsonObject.getBoolean("isFollowed");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json ;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(activity, activity.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(activity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "FollowRequest");

        return isFollow;
    }


    public void removeItem(int position) {
        modelArrayList.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

     class ItemView extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtFollow;
        LinearLayout ll_follow;
        SimpleDraweeView imageIV, borderRing;
        RelativeLayout rl_item;

         ItemView(View itemView) {
            super(itemView);
            txtFollow =  itemView.findViewById(R.id.txt_follow);
            txtName =  itemView.findViewById(R.id.nameTV);
            ll_follow =  itemView.findViewById(R.id.ll_follow);
            imageIV =  itemView.findViewById(R.id.imageIV);
            borderRing =  itemView.findViewById(R.id.borderRing);
            rl_item =  itemView.findViewById(R.id.rl_item);
        }
    }
}
