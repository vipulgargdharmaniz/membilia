package com.membilia.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.membilia.Fragments.Country_Fragment;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.CountryModel;
import java.util.ArrayList;

/*
 * Created by Dharmani Apps on 6/28/2017.
 */

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.CountriesViewHolder> {
    Activity mActivity;
    ArrayList<CountryModel> countriesArrayList;
    Country_Fragment fragmentsetting;

    public CountriesAdapter(Activity mActivity, ArrayList<CountryModel> countriesArrayList) {
        this.mActivity = mActivity;
        this.countriesArrayList = countriesArrayList;
    }
    public CountriesAdapter(Activity mActivity, ArrayList<CountryModel> countriesArrayList ,Country_Fragment fragmentsetting) {
        this.mActivity = mActivity;
        this.countriesArrayList = countriesArrayList;
        this.fragmentsetting = fragmentsetting;
    }
    @Override
    public CountriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_country, parent, false);

        return new CountriesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CountriesViewHolder holder, final int position) {
        final CountryModel tempModel = countriesArrayList.get(position);
        holder.countryTV.setText(tempModel.getNameImage());
        holder.countrycodeTV.setText(tempModel.getCode());

        holder.llLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Country_Fragment fragment = new Country_Fragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Object", tempModel);
                fragment.setArguments(bundle);
                /*
                 * here clear current fragment  and open new fragment
                 * */
                Constants.UserCountry = tempModel.getNameImage();
                HomeActivity.CountryName = tempModel.getNameImage();
                Constants.SelectedCountry = tempModel.getNameImage();
                Constants.SelectedCountryCode = tempModel.getCode();
                fragmentsetting.clearFragment();
            }
        });
    }

    @Override
    public int getItemCount() {
        return countriesArrayList.size();
    }


     class CountriesViewHolder extends RecyclerView.ViewHolder {
         TextView countryTV, countrycodeTV;
         RelativeLayout llLayout;

         CountriesViewHolder(View view) {
            super(view);
            countryTV =  view.findViewById(R.id.countryTV);
            countrycodeTV =  view.findViewById(R.id.countrycodeTV);
            llLayout =  view.findViewById(R.id.llLayout);
        }
    }
}