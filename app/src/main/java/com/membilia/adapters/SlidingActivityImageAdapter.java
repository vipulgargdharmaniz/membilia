package com.membilia.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.NetworkImageView;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;

import com.membilia.volley_models.IDImagesVideoModel;

import java.util.ArrayList;

/*
 * Created by dharmaniz on 2/9/17.
 */

public class SlidingActivityImageAdapter extends PagerAdapter {


    private ArrayList<IDImagesVideoModel> mArrayList;
    private LayoutInflater inflater;
    private Context context;


    public SlidingActivityImageAdapter(Context context, ArrayList<IDImagesVideoModel> mArrayList) {
        this.context = context;
        this.mArrayList=mArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.sliding_activity_image_details, view, false);

        IDImagesVideoModel mModel = mArrayList.get(position);
        final NetworkImageView image1 =  imageLayout.findViewById(R.id.image1);

        if(mModel.getStrImageUrl()!=null) {
            image1.setImageUrl(mModel.getStrImageUrl(), PlayerPawnApplication.getInstance().getImageLoader());
//           Picasso.with(context).load(mModel.getStrImageUrl()).into(image1);
           }
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}