package com.membilia.adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.toolbox.NetworkImageView;
import com.membilia.Fragments.Fragment_Image;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import com.membilia.activities.New_PlayVideo;
import com.membilia.volley_models.IDImagesVideoModel;

import java.util.ArrayList;

public class Sliding_I_D_ImageAdapter extends PagerAdapter {

    private ArrayList<IDImagesVideoModel> mArrayList;
    private LayoutInflater inflater;
    private Context context;


    public Sliding_I_D_ImageAdapter(Context context, ArrayList<IDImagesVideoModel> mArrayList) {
        this.context = context;
        this.mArrayList = mArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.sliding_image_details, view, false);

        final IDImagesVideoModel tempValue = mArrayList.get(position);
        final NetworkImageView itemImages = imageLayout.findViewById(R.id.itemImages);
        final ImageView videoImag = imageLayout.findViewById(R.id.videoimage);

        if (tempValue.getImageType().equalsIgnoreCase("video")) {
            itemImages.setBackgroundResource(R.drawable.red_placeholder);
            videoImag.setVisibility(View.VISIBLE);
        } else {
            itemImages.setBackgroundResource(R.drawable.red_placeholder);
            videoImag.setVisibility(View.GONE);
        }
        itemImages.setImageUrl(tempValue.getStrImageUrl(), PlayerPawnApplication.getInstance().getImageLoader());

        itemImages.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                Constants.isITEM_DETAILS = true;
                if (tempValue.getImageType().equalsIgnoreCase("video")) {
                    Intent intent = new Intent(context, New_PlayVideo.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("VIDEO", tempValue.getVideoLink().toString().trim());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                } else {
                    Fragment_Image fragment = new Fragment_Image();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("IMAGE", mArrayList);
                    bundle.putInt("POSITION", position);
                    fragment.setArguments(bundle);
                    HomeActivity.switchFragment(((FragmentActivity) context), fragment, Constants.Fragment_Image, true, bundle);
                }
            }
        });


        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}