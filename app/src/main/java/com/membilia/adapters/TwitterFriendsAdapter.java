package com.membilia.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.membilia.R;
import com.membilia.volley_models.TwitterFriendsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Dharmani Apps on 6/28/2017.
 */

public class TwitterFriendsAdapter extends RecyclerView.Adapter<TwitterFriendsAdapter.TwitterViewHolder> {
    Activity mActivity;
    ArrayList<TwitterFriendsModel> friendsArrayList;

    public TwitterFriendsAdapter(Activity mActivity, ArrayList<TwitterFriendsModel> friendsArrayList) {
        this.mActivity = mActivity;
        this.friendsArrayList = friendsArrayList;
    }

    @Override
    public void onBindViewHolder(TwitterViewHolder holder, int position) {
        TwitterFriendsModel tempModel = friendsArrayList.get(position);
        holder.itemFriendName.setText(tempModel.getName());
        Picasso.with(mActivity).load(tempModel.getImage()).into(holder.itemProfilePic);
    }

    @Override
    public int getItemCount() {
        return friendsArrayList.size();
    }

    @Override
    public TwitterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_friends_list, parent, false);

        return new TwitterViewHolder(itemView);
    }

    public class TwitterViewHolder extends RecyclerView.ViewHolder {
        public TextView itemFriendName;
        public CircleImageView itemProfilePic;
        public LinearLayout itemLL;

        public TwitterViewHolder(View view) {
            super(view);
            itemFriendName = (TextView) view.findViewById(R.id.itemFriendName);
            itemProfilePic = (CircleImageView) view.findViewById(R.id.itemProfilePic);
            itemLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}