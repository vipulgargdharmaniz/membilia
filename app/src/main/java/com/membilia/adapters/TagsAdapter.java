package com.membilia.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.membilia.Fragments.Fragments_Tags;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import java.util.ArrayList;

/*
 * Created by Dharmani Apps on 6/28/2017.
 */

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {
    Activity mActivity;
    ArrayList<String> tagsArrayList;

    public TagsAdapter(Activity mActivity, ArrayList<String> tagsArrayList) {
        this.mActivity = mActivity;
        this.tagsArrayList = tagsArrayList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tags, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (tagsArrayList.get(position).length() > 0)
            holder.itemTitelTV.setText(tagsArrayList.get(position));

        holder.itemTitelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragments_Tags fragment_tags= new Fragments_Tags();
                Bundle bundle = new Bundle();
                bundle.putString("name",tagsArrayList.get(position));
                fragment_tags.setArguments(bundle);
                HomeActivity.switchFragment((FragmentActivity) mActivity, new Fragments_Tags(), Constants.Fragments_Tags, true, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tagsArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitelTV;

        public ViewHolder(View view) {
            super(view);
            itemTitelTV =  itemView.findViewById(R.id.itemTitelTV);
        }
    }
}


