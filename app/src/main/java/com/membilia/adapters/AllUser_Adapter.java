package com.membilia.adapters;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.NewProfileFragment;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley_models.UsersModel;
import java.util.ArrayList;

/*
 * Created by dharmaniapps on 3/11/17.
 */

public class AllUser_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Activity mActivity;
    private ArrayList<UsersModel> usersArrayList;
    private static final String TAG = "UserAdapter";
    private String strImg = "";
    private Uri imgUri;

    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;


    public AllUser_Adapter(FragmentActivity activity) {
        this.mActivity = activity;
        usersArrayList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        View itemView2 = null;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ITEM:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.items_userlist, parent, false);
                viewHolder = new AllUser_Adapter.MyViewHolder(itemView);
                break;
            case LOADING:
                itemView2 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(itemView2);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                 MyViewHolder holder = (MyViewHolder) holder1;
                final UsersModel tempModel = usersArrayList.get(position);
                holder.tempUserName.setText(tempModel.getName());
                String strUserVerified = String.valueOf(tempModel.getIsVerified());
                if (!tempModel.getUserImages().getOriginal().equals("")) {
                    strImg = tempModel.getUserImages().getOriginal();
                    imgUri = Uri.parse(strImg);
                    holder.tempProfilePic.getHierarchy().setPlaceholderImage(R.drawable.default_img);
                    holder.tempProfilePic.setImageURI(imgUri);
                } else {
                    holder.tempProfilePic.getHierarchy().setPlaceholderImage(R.drawable.default_img);
                }
                holder.borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);
                holder.ll_item.setVisibility(View.VISIBLE);

                if (strUserVerified.equalsIgnoreCase("true")) {
                    holder.img_verified.setVisibility(View.VISIBLE);
                } else {
                    holder.img_verified.setVisibility(View.GONE);
                }
                holder.ll_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        HomeActivity.forProfileID = tempModel.getUserId();
                        Bundle bundle = new Bundle();
                        bundle.putString("isSameUser", "false");
                        HomeActivity.switchFragment((FragmentActivity) mActivity, new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
                    }
                });
                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == usersArrayList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    @Override
    public int getItemCount() {
        return usersArrayList == null ? 0 : usersArrayList.size();
    }

    public void add(ArrayList<UsersModel> actModelArrayList) {
        usersArrayList.addAll(actModelArrayList);
        notifyItemInserted(usersArrayList.size() - 1);
    }


    public void addAll(ArrayList<UsersModel> actModelArrayList) {
        add(actModelArrayList);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
    }
    public void removeLoadingFooter() {
        isLoadingAdded = false;
        if (usersArrayList.size()>0){
            int position = usersArrayList.size() - 1;
            UsersModel result = getItem(position);
            if (result != null) {
                usersArrayList.remove(position);
                notifyItemRemoved(position);
            }
        }else {
            Toast.makeText(mActivity,"No user found!",Toast.LENGTH_SHORT).show();
        }
    }
    private UsersModel getItem(int position) {
        return usersArrayList.get(position);
    }

    public void clear() {
        int size = this.usersArrayList.size();
        this.usersArrayList.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tempUserName; /*tempAccountName*/
        SimpleDraweeView tempProfilePic, borderRing;
        LinearLayout ll_item;
        ImageView img_verified;
         MyViewHolder(View view) {
            super(view);
            tempUserName =  view.findViewById(R.id.tempUserName);
            tempProfilePic =  view.findViewById(R.id.tempProfilePic);
            borderRing = view.findViewById(R.id.borderRing);
            ll_item =  view.findViewById(R.id.ll_item);
            img_verified =  view.findViewById(R.id.img_verified);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
         LoadingVH(View itemView) {
            super(itemView);
        }
    }
}
