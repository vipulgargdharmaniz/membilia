package com.membilia.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.membilia.R;
import com.membilia.volley_models.FbFriendsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Created by Dharmani Apps on 6/28/2017.
 */
public class FbFriendsAdapter extends RecyclerView.Adapter<FbFriendsAdapter.FBViewHolder> {
    Activity mActivity;
    ArrayList<FbFriendsModel> friendsArrayList;

    public FbFriendsAdapter(Activity mActivity, ArrayList<FbFriendsModel> friendsArrayList) {
        this.mActivity = mActivity;
        this.friendsArrayList = friendsArrayList;
    }

    @Override
    public void onBindViewHolder(FBViewHolder holder, int position) {
        FbFriendsModel tempModel = friendsArrayList.get(position);
        holder.itemFriendName.setText(tempModel.getName());
        Picasso.with(mActivity).load(tempModel.getImage()).into(holder.itemProfilePic);
    }

    @Override
    public int getItemCount() {
        return friendsArrayList.size();
    }

    public class FBViewHolder extends RecyclerView.ViewHolder {
        public TextView itemFriendName;
        public CircleImageView itemProfilePic;
        public LinearLayout itemLL;

        public FBViewHolder(View view) {
            super(view);
            itemFriendName =  view.findViewById(R.id.itemFriendName);
            itemProfilePic =  view.findViewById(R.id.itemProfilePic);
        }
    }

    @Override
    public FBViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_friends_list, parent, false);
        return new FBViewHolder(itemView);
    }
}