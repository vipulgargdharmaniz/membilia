package com.membilia.adapters;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.membilia.R;
import com.membilia.splashslides.SplashSlidesActivity;
import java.util.Arrays;

public class SplashPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int layoutID[] = {};
    TabLayout mtabLayout;

    public SplashPagerAdapter(Context context, int layoutID[], TabLayout tabLayout) {
        mContext = context;
        this.layoutID = layoutID;
        this.mtabLayout = tabLayout;

        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemPosition(Object object) {
        if (Arrays.asList(object).contains((View) object)) {
            return Arrays.asList(object).indexOf((View) object);
        } else {
            return POSITION_NONE;
        }
    }

    @Override
    public int getCount() {
        return layoutID.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(layoutID[position], container, false);
        container.addView(itemView);


        if (position == 2) {
            TextView continueBT = itemView.findViewById(R.id.continueBT);
            continueBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((SplashSlidesActivity)mContext).grantpermission();
                }
            });

        }
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
