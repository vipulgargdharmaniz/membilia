package com.membilia.adapters;

/**
 * Created by dharmaniapps on 5/12/17.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.Fragments.AddItemFragment;
import com.membilia.Fragments.Edit_AddItem;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.interfaces.GetItemCountAndRemoveItem;
import com.membilia.volley_models.ImagesItemModel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;


public class AddImageListAdapter extends RecyclerView.Adapter<AddImageListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<ImagesItemModel> mArrayList = new ArrayList<>();
    GetItemCountAndRemoveItem mGetItemCountAndRemoveItem;
    private static final String TAG = "AddImageListAdapter";
//    InputStream stream = null;
//    Bitmap bitmap = null;

    public AddImageListAdapter(Context context, ArrayList<ImagesItemModel> mArrayList, GetItemCountAndRemoveItem mGetItemCountAndRemoveItem) {
        this.mContext = context;
        this.mArrayList = mArrayList;
        this.mGetItemCountAndRemoveItem = mGetItemCountAndRemoveItem;

        Log.v(TAG, "AddImageListAdapter size: "+mArrayList.size() );
    }

    @Override
    public AddImageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_items, parent, false);
        ViewHolder vH = new ViewHolder(v);
        return vH;
    }

    @Override
    public void onBindViewHolder(AddImageListAdapter.ViewHolder holder, final int position) {
        ImagesItemModel itemModel = mArrayList.get(position);
//        Log.e(TAG, "/////////////////////// ");
//        Log.e(TAG, "/////////////////////// :" );
//        Log.e(TAG, "/////////////////////// : " );
//        Log.e(TAG, "onBindViewHolder: "+Constants.CallImageAdpaterFrom );
//        Log.e(TAG, "testadapter:getUriType "+itemModel.getUriType() );  // gives imagetype or videotype
//        Log.e(TAG, "testadapter:getStrVideoImage "+itemModel.getStrVideoImage() );
//        Log.e(TAG, "testadapter:getEditImageString "+itemModel.getEditImageString() );// image url or base64
//        Log.e(TAG, "testadapter:getStrVideoThumbnail "+itemModel.getStrVideoThumbnail() );
//        Log.e(TAG, "testadaptergetStrUrlByteArray: "+itemModel.getStrUrlByteArray() );// byte array for image and video
//        Log.e(TAG, "testadapter:getStrImageVideoPath "+itemModel.getStrImageVideoPath() );//image url or path
        RoundingParams roundingParams = new RoundingParams();
        roundingParams.setCornersRadii(20, 20, 20, 20);
        roundingParams.setOverlayColor(Color.WHITE);
        holder.crossIV.getHierarchy().setPlaceholderImage(R.drawable.camera_additem);
        holder.itemimgIV.getHierarchy().setRoundingParams(roundingParams);

        holder.crossIV.getHierarchy().setPlaceholderImage(R.drawable.addimage_cross);
        // this is ok.
        if (Constants.CallImageAdpaterFrom.equalsIgnoreCase("service")) {
            if (itemModel.getUriType().equalsIgnoreCase("image_type")) {
                holder.itemimgIV.setImageURI(Uri.parse(itemModel.getStrImageVideoPath()));
            } else if (itemModel.getUriType().equalsIgnoreCase("video_type")) {
                    holder.itemimgIV.setImageURI(Uri.parse(itemModel.getStrVideoImage()));
            }
        }
        else if (Constants.CallImageAdpaterFrom.equalsIgnoreCase("camera")) {
           if (itemModel.getUriType().equalsIgnoreCase("image_type")){
               holder.itemimgIV.setImageURI(Uri.parse(itemModel.getStrImageVideoPath()));
           }
           else {
               if (itemModel.getStrVideoImage()!=null){
                   if (itemModel.getStrVideoImage().contains("http")){
                       Log.e(TAG, "finalvideo: "+itemModel.getStrVideoImage() );
                       holder.itemimgIV.setImageURI(Uri.parse(itemModel.getStrVideoImage()));
                   }
                   else {
                       Log.e(TAG, "finalvideo: "+itemModel.getStrVideoThumbnail() );
                       holder.itemimgIV.setImageBitmap(itemModel.getStrVideoThumbnail());
                   }
               }else {
                   holder.itemimgIV.setImageBitmap(itemModel.getStrVideoThumbnail());
               }

           }

        }
        else if (Constants.CallImageAdpaterFrom.equalsIgnoreCase("AddItem")){
            if(!itemModel.getUriType().equalsIgnoreCase("video_type")){
                holder.itemimgIV.setImageURI(Uri.parse(itemModel.getStrImageVideoPath()));
            }

            else if (itemModel.getUriType().equalsIgnoreCase("video_type")) {
                holder.itemimgIV.setImageBitmap(itemModel.getStrVideoThumbnail());
            }
        }

        holder.crossIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mArrayList.get(position).getUriType().equalsIgnoreCase("video_type")) {
                    AddItemFragment.isVideoSelected = false;
                    Edit_AddItem.isVideoSelected = false;
                }


                if (position==0 && !Constants_AddItem.imagepath1.equalsIgnoreCase("")){
                    Constants_AddItem.imagepath1 ="";
                }
                if (position==1 && !Constants_AddItem.imagepath2.equalsIgnoreCase("")){
                    Constants_AddItem.imagepath2 ="";
                }
                if (position==2 && !Constants_AddItem.imagepath3.equalsIgnoreCase("")){
                    Constants_AddItem.imagepath3 ="";
                }
                if (position==3 && !Constants_AddItem.imagepath4.equalsIgnoreCase("")){
                    Constants_AddItem.imagepath4 ="";
                }
                if (position==4 && !Constants_AddItem.imagepath5.equalsIgnoreCase("")){
                    Constants_AddItem.imagepath5 ="";
                }
                if (position==5 && !Constants_AddItem.imagepath6.equalsIgnoreCase("")){
                    Constants_AddItem.imagepath6 ="";
                }
                mGetItemCountAndRemoveItem.getCountAndPositionForRemove(position);
            }
        });


    }


    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    private void removeItem(int position) {
        mArrayList.remove(position);
        notifyDataSetChanged();
    }

    private void loadImageFromStorage(String imgPath, ImageView imgImageView) {
        try {
            File f = new File(imgPath);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            imgImageView.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    private Bitmap loadImageFromBitmap(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Bitmap original = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        original.compress(Bitmap.CompressFormat.PNG, 50, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));


        return decoded;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public SimpleDraweeView itemimgIV, crossIV;


        public ViewHolder(View itemView) {
            super(itemView);
            itemimgIV = (SimpleDraweeView) itemView.findViewById(R.id.itemImgIV);
            crossIV = (SimpleDraweeView) itemView.findViewById(R.id.crossIV);
        }
    }
}
