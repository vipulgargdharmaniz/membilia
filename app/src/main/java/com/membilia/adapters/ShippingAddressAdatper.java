package com.membilia.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.Fragments.FragmentShowShippingAddress;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ShippingAddressModel;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/*
 * Created by Dharmani Apps on 6/28/2017.
 */

public class ShippingAddressAdatper extends RecyclerView.Adapter<ShippingAddressAdatper.AddressViewHolder> {
    Activity mActivity;
    ArrayList<ShippingAddressModel> addressesArrayList;
    private static final String TAG = "ShippingAddressAdatper";
    private FragmentShowShippingAddress fragmentShowShippingAddress;
    String strCameFrom;

    public ShippingAddressAdatper(Activity mActivity, ArrayList<ShippingAddressModel> addressesArrayList, FragmentShowShippingAddress fragmentShowShippingAddress, String strCameFrom) {
        this.mActivity = mActivity;
        this.addressesArrayList = addressesArrayList;
        this.fragmentShowShippingAddress = fragmentShowShippingAddress;
        this.strCameFrom = strCameFrom;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.swipelayout_, parent, false);

        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AddressViewHolder holder, final int position) {
        final ShippingAddressModel tempModel = addressesArrayList.get(position);
        holder.streetTV.setText(tempModel.getAddress_line_one());
        if (tempModel.getAddress_line_two().equalsIgnoreCase("")){
            holder.aprtTV.setVisibility(View.GONE);
        }else {
            holder.aprtTV.setVisibility(View.VISIBLE);
            holder.aprtTV.setText(tempModel.getAddress_line_two());
        }

        holder.cityTV.setText(tempModel.getCity());
        holder.stateTV.setText(tempModel.getState());
        holder.countryTV.setText(tempModel.getCountry() + "-" + tempModel.getZipcode());
        holder.defaultLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tempModel.getIsDefault().equalsIgnoreCase("false")) {
                    setAddressDefault(tempModel.getAddress_id());
                }
            }
        });
        if (position==0) {
            holder.defaultIV.setImageResource(R.drawable.check_green);
            holder.defaultTV.setTextColor(mActivity.getResources().getColor(R.color.peach));
        } else {
            holder.defaultIV.setImageResource(R.drawable.unchecknew);
        }
        holder.itemLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (strCameFrom.equalsIgnoreCase("buy")){
                    setAddressDefault(tempModel.getAddress_id());
                }else {
                    String street = tempModel.getAddress_line_one();
                    String aprtTV = tempModel.getAddress_line_two();
                    String cityTV = tempModel.getCity();
                    String stateTV = tempModel.getState();
                    String countryTV = tempModel.getCountry();
                    String zipCode = tempModel.getZipcode();
                    String addressId = tempModel.getAddress_id();
                    fragmentShowShippingAddress.editAddress(street, aprtTV, cityTV, stateTV, countryTV, zipCode, addressId);
                }
            }
        });
    }

    private void excuteDeleteAddress(String addressID, final int position, final AddressViewHolder holder) {
        String strUrl = WebServicesConstants.SAVE_USER_ADDRESS + "/" + addressID;
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, strUrl,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utilities.hideProgressDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            if (message.equalsIgnoreCase("true")) {
                                fragmentShowShippingAddress.gettingAllAddresses();
//                                addressesArrayList.remove(position);
//                                notifyDataSetChanged();
//                                holder.swipeLL.setRefreshing(true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                String json ;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(mActivity, mActivity.getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(mActivity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "delete");
    }

    private void setAddressDefault(String address_id) {
        String url = WebServicesConstants.DefaultAddress + address_id;
        Log.e(TAG, "url: " + url);
        Utilities.showProgressDialog(mActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(response);
                    boolean message = jsonObject.getBoolean("message");
                    if (message) {
                        fragmentShowShippingAddress.backstack();
//                        HomeActivity.switchFragment((FragmentActivity) mActivity,new FragmentShowShippingAddress(), Constants.Fragment_Show_Shipping_Address,true,null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                Utilities.hideProgressDialog();
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(mActivity, PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.DefaultAddress);
    }

    @Override
    public int getItemCount() {
        return addressesArrayList.size();
    }


     class AddressViewHolder extends RecyclerView.ViewHolder {
         TextView streetTV, aprtTV, cityTV, stateTV, countryTV, defaultTV;
         ImageView defaultIV;
         LinearLayout defaultLL, addressItemLL, itemLL;
//         SwipeRefreshLayout swipeLL;
        //        public RadioButton shippingRB;

         AddressViewHolder(View view) {
            super(view);
            streetTV = itemView.findViewById(R.id.streeTV);
            aprtTV =  itemView.findViewById(R.id.aprtTV);
            cityTV =  itemView.findViewById(R.id.cityTV);
            stateTV =  itemView.findViewById(R.id.stateTV);
            countryTV =  itemView.findViewById(R.id.countryTV);
            defaultIV = itemView.findViewById(R.id.defaultIV);
            defaultLL = itemView.findViewById(R.id.defaultLL);
            defaultTV = itemView.findViewById(R.id.defaultTV);
            addressItemLL =  itemView.findViewById(R.id.addressItemLL);
            itemLL =  itemView.findViewById(R.id.itemLL);
//            deleteLL =  itemView.findViewById(R.id.deleteLL);
//             swipeLL = itemLL.findViewById(R.id.swipeLL);
        }
    }

    private void dialogBox(final ShippingAddressModel tempModel, final int position, final AddressViewHolder holder) {
        // custom dialog
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));



        // set the custom dialog components - text, image and button
        LinearLayout yesll =  dialog.findViewById(R.id.yesll);
        LinearLayout noll =  dialog.findViewById(R.id.noll);
        TextView messageTV =  dialog.findViewById(R.id.messageTV);
        messageTV.setText("Are you sure, you want to delete Shipping address?");
        noll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        yesll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                excuteDeleteAddress(tempModel.getAddress_id(),position,holder);
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}