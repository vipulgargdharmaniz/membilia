package com.membilia.interfaces;

/**
 * Created by dharmaniapps on 6/12/17.
 */

public interface GetItemCountAndRemoveItem {
    public void getCountAndPositionForRemove(int position);
}
