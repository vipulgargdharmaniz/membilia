package com.membilia.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.MessageHistoryAdapter;
import com.membilia.volley_models.MessageHistoryModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentMessageHistory extends Fragment {
    View itemsview;
    private static final String TAG = "FragmentMessageHistory";
    LinearLayout topbar;
    RelativeLayout leftRL;
    TextView titleTV, nodataTV;
    RecyclerView recyclerRV;
    NestedScrollView mNestedScrollView;
    MessageHistoryAdapter messageHistoryAdapter;
    int perPage = 20;
    int page_no = 1;
    ArrayList<MessageHistoryModal> arrayList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (itemsview == null) {
            itemsview = inflater.inflate(R.layout.fragment_message_history, null);
            setUpViews(itemsview);
            setupclicks();
        }
        return itemsview;
    }

    private void CheckforDB() {
        Log.e(TAG, "CheckforDB: " + HomeActivity.db.getAllMessagesnew().size());
        if (HomeActivity.db.getAllMessagesnew().size() > 0) {
            arrayList = new ArrayList<>();
            int dbSize = HomeActivity.db.getAllMessagesnew().size();
            for (int i = 1; i <= dbSize; i++) {
                parseDbResponse(HomeActivity.db.getLastMessage(i));
            }
            setAdapter(arrayList);
        }
    }

    private void parseDbResponse(ArrayList<MessageHistoryModal> allMessagesnew) {
        MessageHistoryModal modal = new MessageHistoryModal();
        if (allMessagesnew.get(0).getStrDirection().equalsIgnoreCase("true")) {
            modal.setDirection(true);
        } else {
            modal.setDirection(false);
        }
        modal.setItemId(allMessagesnew.get(0).getItemId());
        modal.setMessageId(allMessagesnew.get(0).getMessageId());
        modal.setItemName(allMessagesnew.get(0).getItemName());
        modal.setSenderId(allMessagesnew.get(0).getSenderId());
        modal.setSenderName(allMessagesnew.get(0).getSenderName());
        modal.setReciverId(allMessagesnew.get(0).getReciverId());
        modal.setRecivierName(allMessagesnew.get(0).getRecivierName());
        modal.setTimeStamp(allMessagesnew.get(0).getTimeStamp());
        modal.setMessage(String.valueOf(allMessagesnew.get(0).getMessage()));
        modal.setSenderImage(allMessagesnew.get(0).getSenderImage());
        modal.setRecivierImage(allMessagesnew.get(0).getRecivierImage());
        arrayList.add(modal);
    }

    private void getAllMessages() {
        CheckforDB();
        String strUrl = WebServicesConstants.MessageHistory + "perPage=" + perPage + "&page_no=" + page_no;
        Log.e(TAG, "getAllMessages: " + strUrl);
        Utilities.showProgressDialog(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() > 0)
                        parseResponseData(response);
                    else
                        nodataTV.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return header;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.MessageHistory);
    }


    private void setUpViews(View v) {
        topbar = v.findViewById(R.id.topbar);
        leftRL = v.findViewById(R.id.leftRL);
        titleTV = v.findViewById(R.id.titleTV);
        recyclerRV = v.findViewById(R.id.recyclerRV);
        mNestedScrollView = v.findViewById(R.id.mNestedScrollView);
        nodataTV = v.findViewById(R.id.nodataTV);
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        titleTV.setText("Message History");
    }

    private void setupclicks() {
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.bottombar.setVisibility(View.VISIBLE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (haveNetworkConnection()) {
            Log.e(TAG, "onCreateView: connected ");
            arrayList.clear();
            getAllMessages();
        } else {
            Log.e(TAG, "onCreateView: not Connected ");
            CheckforDB();
        }
    }

    private void parseResponseData(String response) {
        try {
            arrayList = new ArrayList<>();
            Utilities.hideProgressDialog();
            JSONArray jsonArray = new JSONArray(response);
            JSONObject mainJsonObject;
            JSONObject senderJson;
            JSONObject reciverJson;
            JSONObject senderImageJson;
            JSONObject reciverImageJson;
            HomeActivity.db.removeAllLastChat();
            for (int i = 0; i < jsonArray.length(); i++) {


                mainJsonObject = jsonArray.getJSONObject(i);
                MessageHistoryModal modal = new MessageHistoryModal();
                modal.setDirection(mainJsonObject.getBoolean("direction"));
                if (!mainJsonObject.getString("message_id").equalsIgnoreCase("playpawn")) {
                    modal.setMessageId(mainJsonObject.getString("message_id"));
                    modal.setMessage(mainJsonObject.getString("text"));
                    modal.setItemId(mainJsonObject.getString("item_id"));
                    modal.setItemName(mainJsonObject.getString("item_name"));
                    Constants_AddItem.ITEM_NAME = mainJsonObject.getString("item_name");
                    modal.setUnreadCount(mainJsonObject.getString("unread_count"));

                    if (modal.isDirection()) {
                        senderJson = mainJsonObject.getJSONObject("sender");
                        modal.setSenderId(senderJson.getString("user_id"));

                        modal.setSenderName(senderJson.getString("name"));

                        senderImageJson = senderJson.getJSONObject("user_images");
                        modal.setSenderImage(senderImageJson.getString("original"));

                        reciverJson = mainJsonObject.getJSONObject("receiver");
                        modal.setReciverId(reciverJson.getString("user_id"));

                        modal.setRecivierName(reciverJson.getString("name"));

                        reciverImageJson = reciverJson.getJSONObject("user_images");
                        modal.setRecivierImage(reciverImageJson.getString("original"));

                    } else {
                        senderJson = mainJsonObject.getJSONObject("receiver");
                        modal.setSenderId(senderJson.getString("user_id"));
                        modal.setSenderName(senderJson.getString("name"));
                        senderImageJson = senderJson.getJSONObject("user_images");
                        modal.setSenderImage(senderImageJson.getString("original"));
                        reciverJson = mainJsonObject.getJSONObject("sender");
                        modal.setReciverId(reciverJson.getString("user_id"));
                        modal.setRecivierName(reciverJson.getString("name"));
                        reciverImageJson = reciverJson.getJSONObject("user_images");
                        modal.setRecivierImage(reciverImageJson.getString("original"));

                    }

                    modal.setTimeStamp(mainJsonObject.getString("timeStamp"));
                    arrayList.add(modal);
                    ArrayList list = new ArrayList<>();
                    list.add(String.valueOf(modal.isDirection()));
                    list.add(mainJsonObject.getString("item_id"));
                    list.add(mainJsonObject.getString("message_id"));
                    list.add(mainJsonObject.getString("item_name"));
                    /// iam taking sender as app user who is using application ///
                    //  and reciver is the second user in the app////
                    list.add(modal.getSenderId());
                    list.add(modal.getSenderName());
                    list.add(modal.getReciverId());
                    list.add(modal.getRecivierName());
                    list.add(modal.getTimeStamp());
                    list.add(modal.getMessage());
                    list.add(modal.getSenderImage());
                    list.add(modal.getRecivierImage());
                    HomeActivity.db.addLastChat(list);
                }

            }
            Log.e(TAG, "db.getAllMessagesnew().size() " + HomeActivity.db.getAllMessagesnew().size());
            setAdapter(arrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utilities.hideProgressDialog();
    }

    private void setAdapter(ArrayList<MessageHistoryModal> arrayList) {
        String userId = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
        messageHistoryAdapter = new MessageHistoryAdapter(getActivity(), arrayList, userId, FragmentMessageHistory.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerRV.setNestedScrollingEnabled(false);
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(messageHistoryAdapter);
        messageHistoryAdapter.notifyDataSetChanged();
    }


    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
