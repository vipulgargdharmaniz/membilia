package com.membilia.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.membilia.R;

/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentOrderHistory extends Fragment {

    RelativeLayout purchasedRL, pawndRL;
    TextView purchasedTV, pawndTV;
    View itemFooter, userFooter;
    ImageView imgLeftIV;
    private static final String TAG = "FragmentOrderHistory";
    View itemsview;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (itemsview==null){
            itemsview= inflater.inflate(R.layout.fragment_order_history, null);
            Log.i(TAG, "onCreateView: ");
            setUpViews(itemsview);
            setupclicks();
        }

        return itemsview;
    }
    private void setUpViews(View v) {
        purchasedRL =  v.findViewById(R.id.purchasedRL);
        pawndRL =  v.findViewById(R.id.pawndRL);
        itemFooter = v.findViewById(R.id.itemFooter);
        userFooter = v.findViewById(R.id.userFooter);
        purchasedTV =  v.findViewById(R.id.purchasedTV);
        pawndTV =  v.findViewById(R.id.pawndTV);
        imgLeftIV =  v.findViewById(R.id.imgLeftIV);

       setUpByDefaultFragment();
    }

    private void setupclicks() {

        purchasedRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFooter.setVisibility(View.VISIBLE);
                userFooter.setVisibility(View.GONE);
                purchasedTV.setTextColor(getActivity().getResources().getColor(R.color.peach));
                pawndTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                switchFragment(getActivity(), new Fragment_purchased(), false);
            }
        });

        pawndRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFooter.setVisibility(View.GONE);
                userFooter.setVisibility(View.VISIBLE);
                purchasedTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                pawndTV.setTextColor(getActivity().getResources().getColor(R.color.peach));
                switchFragment(getActivity(), new PawnFragment(), false);
            }
        });
        imgLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });


    }
    private void setUpByDefaultFragment() {
//        purchasedTV.setTextColor(mResources.getColor(R.color.colorPrimary));
//        pawndTV.setTextColor(mResources.getColor(R.color.gray_darkcoal));
//        itemFooter.setVisibility(View.VISIBLE);
//        userFooter.setVisibility(View.GONE);
        itemFooter.setVisibility(View.VISIBLE);
        userFooter.setVisibility(View.GONE);
        switchFragment(getActivity(), new Fragment_purchased(), false);


    }
    public static void switchFragment(FragmentActivity activity, Fragment fragment, boolean addToStack ) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            ft.replace(R.id.framelayoutinorder, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            ft.commit();
        }
    }


}