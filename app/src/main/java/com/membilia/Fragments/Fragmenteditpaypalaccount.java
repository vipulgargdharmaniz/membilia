package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;

/*
 * Created by dharmaniz on 14/7/17.
 */

public class Fragmenteditpaypalaccount extends Fragment {
    View itemView;
    Button editb;
    RelativeLayout searchRL;
    private static final String TAG = "Fragmenteditpaypalaccou";
    String strPayPalAccount = "";
    TextView paypalTV, titleTV;
    boolean isAddType = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String strPayPal = bundle.getString("strPaypal", "");
            if (!strPayPal.equalsIgnoreCase("")) {
                this.strPayPalAccount = strPayPal;
                isAddType = false;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemView = inflater.inflate(R.layout.fragment_edit_paypal_account, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemView);
        setUpclicks();
        return itemView;

    }

    private void setUpViews(View v) {
        editb = v.findViewById(R.id.editb);
        searchRL = v.findViewById(R.id.searchRL);
        paypalTV = v.findViewById(R.id.paypalTV);
        titleTV = v.findViewById(R.id.titleTV);
        paypalTV.setText(strPayPalAccount);

        if (!strPayPalAccount.equalsIgnoreCase("")) {
            editb.setText(getActivity().getResources().getString(R.string.edit));
            titleTV.setText(getActivity().getResources().getString(R.string.editpaypal));
            paypalTV.setTextColor(getActivity().getResources().getColor(R.color.green));

        } else {
            editb.setText(getActivity().getResources().getString(R.string.add));
            titleTV.setText(getActivity().getResources().getString(R.string.addpaypal));
        }
    }

    private void setUpclicks() {
        editb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("strPaypal", strPayPalAccount);
                HomeActivity.switchFragment(getActivity(), new Fragmenteditpaypalsubmit(), Constants.Fragment_editpaypalsubmit, true, bundle);
            }
        });
        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }
}
