package com.membilia.Fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
/*
 * Created by dharmaniapps on 27/12/17.
 */

public class Customercare_Fragment extends Fragment {

    private static final String TAG = "Customercare_Fragment";
    View view;
    LinearLayout topbar;
    RelativeLayout leftRL;
    TextView titleTV, submitTV, refundPolicyTV, nextTV, refundTV;
    EditText inquiryET;
    ScrollView scrollView;

    String strInquiryMsg = "";

    String senderID,recipientID,itemID,orderNO;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.customecare_fragment, null);

       Bundle getBundle = getArguments();
       if (getBundle!=null){
           senderID = getBundle.getString("sender_id");
           recipientID = getBundle.getString("recieverid");
           itemID = getBundle.getString("item_id");
           orderNO = getBundle.getString("order_id");
       }
        setView(view);
        setClick(view);
        setDefault();
        return view;
    }

    private void setView(View view) {

        titleTV = view.findViewById(R.id.titleTV);
        leftRL = view.findViewById(R.id.leftRL);
        topbar = view.findViewById(R.id.topbar);
        inquiryET = view.findViewById(R.id.inquiryET);
        submitTV = view.findViewById(R.id.submitTV);
        refundPolicyTV = view.findViewById(R.id.refundPolicyTV);
        nextTV = view.findViewById(R.id.nextTV);
        scrollView = view.findViewById(R.id.scrollView);
        refundTV = view.findViewById(R.id.refundTV);

        refundTV.setText(getActivity().getResources().getString(R.string.refundpolicy));
    }

    private void setClick(View view) {
        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (inquiryET.getText().toString().equalsIgnoreCase("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "please provide some reason.");
                } else {

                    strInquiryMsg = inquiryET.getText().toString();
                    String userId = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
                    inquiryMethod();
                }

            }
        });

        refundPolicyTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.setVisibility(View.GONE);
                topbar.setVisibility(View.GONE);
                HomeActivity.bottombar.setVisibility(View.GONE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
            }
        });
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        nextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.setVisibility(View.VISIBLE);
                topbar.setVisibility(View.VISIBLE);
                HomeActivity.bottombar.setVisibility(View.VISIBLE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setDefault() {
        titleTV.setText("Customer Care");
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
    }

    private void inquiryMethod() {

        String strUrl = WebServicesConstants.Refund;
        Log.e(TAG, "inquiryMethod: " + strUrl);

        final JSONObject requestObject = new JSONObject();
        try {
            requestObject.put("refund_message", strInquiryMsg);
            requestObject.put("sender_id", senderID);
            requestObject.put("receiver_id", recipientID);
            requestObject.put("item_id", itemID);
            requestObject.put("refund_key", "true");
            requestObject.put("order_id", orderNO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest requestString = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                alertDialog("You request has been submiitted to Membilia customer care. Please " +
                        "allow  for upto 48 hours for our agents to contact you. Thank you! share the legacy.");

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestObject.toString().getBytes();
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(requestString, "Inquiry");
    }


    public  void alertDialog( String strMessage) {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_showalert);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle =  alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage =  alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss =  alertDialog.findViewById(R.id.txtDismiss);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();

            }
        });
        alertDialog.show();
    }
}
