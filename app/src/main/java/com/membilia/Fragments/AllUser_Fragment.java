package com.membilia.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PaginationScrollListener;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.AllUser_Adapter;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.UserImagesModel;
import com.membilia.volley_models.UserLocationModel;
import com.membilia.volley_models.UsersModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static android.content.Context.INPUT_METHOD_SERVICE;
/*
 * Created by dharmaniapps on 2/11/17.
 */

public class AllUser_Fragment extends Fragment {

    private static final String TAG = "AllUser_Fragment";
    /* widgets*/
    private RecyclerView recyclerRV;
    private View usersview;
    private AllUser_Adapter mAdapter;
    private RelativeLayout rl_back, filterRL;
    private EditText ed_search;
    private LinearLayout ll_cancel;
    /* variables*/
    ArrayList<UsersModel> usersArrayList;
    private int perPage = 20;
    private int page_no = 1;
    String strURLSearchUSer = WebServicesConstants.USER_SEARCH + "perPage=" + perPage + "&page_no=" + page_no;
    String strURlAllUser = WebServicesConstants.USERS_LISTING + "?perPage=" + perPage + "&page_no=" + page_no;
    private PopupMenu popup;
    private DrawerLayout drawerLayout;
    private RelativeLayout rl_itemfan, rl_itemseller, rl_itemboth;
    private ImageView img_fan, img_seller, img__both;
    /*   handling filter */
    private int filterType = 3;
    /*  changing json accornding to filter type*/

    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private boolean isnextpage = false;
    private boolean isfirstTime = false;
    String scrollType = "AllUser";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (usersview == null) {
            usersview = inflater.inflate(R.layout.alluser_fragment, null);
            usersArrayList = new ArrayList<>();

            setUpViews(usersview);
            setUpClickListeners();

            mAdapter = new AllUser_Adapter(getActivity());
            GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
            recyclerRV.setLayoutManager(mLayoutManager);
            recyclerRV.setItemAnimator(new DefaultItemAnimator());
            recyclerRV.setAdapter(mAdapter);
            recyclerRV.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
                @Override
                protected void loadMoreItems() {
                    if (isnextpage) {
                        isLoading = false;
                        isfirstTime = true;
                        page_no += 1;
                        // mocking network delay for API call
                        isnextpage = false;
                        if (scrollType.equalsIgnoreCase("AllUser")) {
                            gettingAllUsers();
                        } else {
                            userSearchRequest(strURLSearchUSer);
                        }
                    }
                }

                @Override
                public int getTotalPageCount() {
                    return 1;
                }

                @Override
                public boolean isLastPage() {
                    return isLastPage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            });
            Utilities.showProgressDialog(getActivity());
            gettingAllUsers();
        }
        Constants.IS_ITEM_USER = "user_fragment";
        return usersview;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.homeClick();
    }

    public void setUpViews(View v) {
        ExploreFragment.imgLeftIV.setImageResource(R.drawable.icon_zoom);
        recyclerRV = v.findViewById(R.id.userRecyclerView);
        rl_back = v.findViewById(R.id.rl_back);
        filterRL = v.findViewById(R.id.filterRL);
        ll_cancel = v.findViewById(R.id.ll_cancel);
        ed_search = v.findViewById(R.id.ed_search);
        drawerLayout = v.findViewById(R.id.drawerlayout);
        rl_itemfan = v.findViewById(R.id.rl_itemfan);
        rl_itemseller = v.findViewById(R.id.rl_itemseller);
        rl_itemboth = v.findViewById(R.id.rl_itemboth);
        img_fan = v.findViewById(R.id.img_fan);
        img_seller = v.findViewById(R.id.img_seller);
        img__both = v.findViewById(R.id.img_both);
    }

    private void setUpClickListeners() {
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        rl_itemfan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterType = 1;
                drawerLayout.closeDrawer(Gravity.RIGHT);
                usersArrayList.clear();
                mAdapter.clear();
                img_fan.setVisibility(View.VISIBLE);
                img_seller.setVisibility(View.INVISIBLE);
                img__both.setVisibility(View.INVISIBLE);
                gettingAllUsers();
            }
        });
        rl_itemseller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterType = 2;
                drawerLayout.closeDrawer(Gravity.RIGHT);
                mAdapter.clear();
                img_fan.setVisibility(View.INVISIBLE);
                img_seller.setVisibility(View.VISIBLE);
                img__both.setVisibility(View.INVISIBLE);
                gettingAllUsers();

            }
        });
        rl_itemboth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterType = 3;
                drawerLayout.closeDrawer(Gravity.RIGHT);
                mAdapter.clear();
                img_fan.setVisibility(View.INVISIBLE);
                img_seller.setVisibility(View.INVISIBLE);
                img__both.setVisibility(View.VISIBLE);
                gettingAllUsers();
            }
        });
        ed_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    usersArrayList.clear();
                    mAdapter.clear();
                    page_no = 1;
                    userSearchRequest(strURLSearchUSer);
                    hideSoftKeyboard();
                    return true;
                }
                return false;
            }
        });
        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ed_search.setText("");
                page_no = 1;
                mAdapter.clear();
                Utilities.showProgressDialog(getActivity());
                gettingAllUsers();
                HomeActivity.userSerchFor = "";
            }
        });

        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    ll_cancel.setClickable(true);
                } else
                    ll_cancel.setClickable(false);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        popup = new PopupMenu(getContext(), filterRL);
        popup.getMenuInflater().inflate(R.menu.popup_filter, popup.getMenu());
        filterRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                page_no = 1;
                if (!drawerLayout.isDrawerOpen(Gravity.RIGHT))
                    drawerLayout.openDrawer(Gravity.RIGHT);
                else
                    drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                String id = menuItem.getTitle().toString();
                Log.e(TAG, "onMenuItemClick: " + id);

                return true;
            }
        });
    }

    private void userSearchRequest(String strURL) {
        scrollType = "UserSearch";
        perPage = 40;
        strURLSearchUSer = WebServicesConstants.USER_SEARCH + "perPage=" + perPage + "&page_no=" + page_no;
        Log.e(TAG, "strUrl: " + strURLSearchUSer);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("search", ed_search.getText().toString().trim());
            jsonObject.put("type", 0);
            HomeActivity.userSerchFor = ed_search.getText().toString().trim();
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        {"perPage":40,"page_no":1,"filterApply":2,"price":true,"delivery_type":true,"distance":true,"range":100000,"date":true,"pawncast":2,"cat":["ACCESSORIES","Things worn","The Experience","Balls,Bats & Bags","Things That Move","Miscellaneous","Footwear","Fashion",true],"location":{"administrative_area_level_1":"Punjab","country":"India","lat":"30.7098102","lng":"30.7098102","locality":"Sahibzada Ajit Singh Nagar"},"search":""}

        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse*********" + response.toString());

                JSONArray jsonArray1 = null;
                try {
                    jsonArray1 = new JSONArray(response);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e(TAG, "jsonArray1*********" + jsonArray1.length());
                if (jsonArray1.length() == 0) {
                    isLastPage = true;
                    mAdapter.removeLoadingFooter();
                } else if (jsonArray1.length()< perPage) {
                    isLastPage = true;
                    parseResponse(jsonArray1);
                    isnextpage = false;
                    mAdapter.removeLoadingFooter();
                } else {
                    isLastPage = false;
                    parseResponse(jsonArray1);
                    isnextpage = true;
                    Log.e(TAG, "onResponse:size " + response.length());
                    isLastPage = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "AllUserSearch");
    }

    private void gettingAllUsers() {
        scrollType = "AllUser";
        strURlAllUser = WebServicesConstants.USERS_LISTING + "?perPage=" + perPage + "&page_no=" + page_no;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strURlAllUser, new Response.Listener<JSONArray>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, "onResponse*********" + response.toString());
                Utilities.hideProgressDialog();
                if (response.length() == 0) {
                    isLastPage = true;
                    mAdapter.removeLoadingFooter();
                } else {
                    parseResponse(response);
                    isnextpage = true;
                    Log.e(TAG, "onResponse:size " + response.length());
                    isLastPage = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json = null;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "UsersRequest");
    }

    private void parseResponse(JSONArray response) {
        try {

            usersArrayList = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                JSONObject jObject = response.getJSONObject(i);
                UsersModel uModel = new UsersModel();
                uModel.setUserId(jObject.getString("user_id"));
                uModel.setName(jObject.getString("name"));
                uModel.setDescription(jObject.getString("description"));
                if (!jObject.isNull("email"))
                    uModel.setEmail(jObject.getString("email"));
                uModel.setFollowerCount(Integer.parseInt(jObject.getString("followerCount")));
                uModel.setFollowingCount(Integer.parseInt(jObject.getString("followingCount")));
                uModel.setRating(Integer.parseInt(jObject.getString("rating")));
                uModel.setRanking(Integer.parseInt(jObject.getString("ranking")));
                uModel.setIsFollowed(Boolean.parseBoolean(jObject.getString("isFollowed")));

                if (!jObject.isNull("location")) {
                    JSONObject locationObject = jObject.getJSONObject("location");
                    UserLocationModel locationModel = new UserLocationModel();
                    if (!jObject.isNull("administrative_area_level_1"))
                        locationModel.setAdministrativeAreaLevel1(locationObject.getString("administrative_area_level_1"));
                    if (!jObject.isNull("country"))
                        locationModel.setCountry(locationObject.getString("country"));
                    if (!jObject.isNull("country_code"))
                        locationModel.setCountryCode(locationObject.getString("country_code"));
                    if (!jObject.isNull("lat"))
                        locationModel.setLat(locationObject.getString("lat"));
                    if (!jObject.isNull("lng"))
                        locationModel.setLng(locationObject.getString("lng"));
                    if (!locationObject.isNull("locality"))
                        locationModel.setLocality(locationObject.getString("locality"));
                    if (!locationObject.isNull("postal_code"))
                        locationModel.setPostalCode(locationObject.getString("postal_code"));

                    uModel.setLocation(locationModel);
                }

                if (!jObject.isNull("flag"))
                    uModel.setFlag(jObject.getString("flag"));

                uModel.setIsVerified(Boolean.parseBoolean(jObject.getString("isVerified")));
                if (!jObject.isNull("user_images")) {
                    JSONObject userObject = jObject.getJSONObject("user_images");
                    UserImagesModel imagesModel = new UserImagesModel();
                    if (!userObject.isNull("original"))
                        imagesModel.setOriginal(userObject.getString("original"));

                    uModel.setUserImages(imagesModel);
                }
                if (filterType == 3) {
                    usersArrayList.add(uModel);
                }
                if (filterType == 1) {
                    if (!uModel.getIsVerified() == true) {
                        usersArrayList.add(uModel);
                        Log.e(TAG, "parseResponse: filterType" + filterType);
                    } else {
                        Log.e(TAG, "parseResponse: filterType else" + filterType);
                    }
                }
                if (filterType == 2) {
                    if (!uModel.getIsVerified() == false) {
                        usersArrayList.add(uModel);
                    }
                }
            }

            mAdapter.addAll(usersArrayList);
            mAdapter.addLoadingFooter();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyboard() {
        View view = getActivity().getCurrentFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
