package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ItemsAdapter;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemLocationModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniz on 25/7/17.
 */

public class Fragment_purchased extends Fragment {
    View itemViews;
    private static final String TAG = "Fragment_purchased";
    TextView noProductsTV;
    RecyclerView recyclerRV;
    ArrayList<ItemsModel> itemArraylist;
    ItemsAdapter mItemsAdapter;
    List<String> listTags = new ArrayList<>();

    public Fragment_purchased() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (itemViews == null) {
            itemViews = inflater.inflate(R.layout.fragment_purchased, null);
            Log.i(TAG, "onCreateView: ");
            setUpViews(itemViews);
        }
        return itemViews;
    }

    private void setUpViews(View v) {
        noProductsTV = v.findViewById(R.id.noProductsTV);
        recyclerRV = v.findViewById(R.id.recyclerRV);
    }

    @Override
    public void onResume() {
        super.onResume();
        itemArraylist = AppSingleton.getInstance().getPurchasedItemModal();
        Log.e(TAG, "onResume: " + itemArraylist);
        if (itemArraylist.size() > 0) {
            setDataAndShowsItems();
        } else {
            getAllOrderHistory();
        }
    }

    public void getAllOrderHistory() {
        String userid = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
        Utilities.showProgressDialog(getActivity());
        String strURL = WebServicesConstants.OrderHistroy + "user_id=" + HomeActivity.UserItself + "&perPage=100&page_no=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() > 0) {
                        parseDATA(response);
                    } else {
                        Utilities.hideProgressDialog();
                        noProductsTV.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error);
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void parseDATA(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject mainJsonObject = jsonArray.getJSONObject(i);
                JSONObject jsonObject = mainJsonObject.getJSONObject("item_object");
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setLastUsed(jsonObject.getString("last_used"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setIsAutographed(Boolean.parseBoolean(jsonObject.getString("isAutographed")));
                itemsModel.setCategory(jsonObject.getString("category"));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setCharityName(jsonObject.getString("charity_name"));
                itemsModel.setCurrency(jsonObject.getString("currency"));
                itemsModel.setDeliveryType(Boolean.parseBoolean(jsonObject.getString("delivery_type")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setWeight(jsonObject.getString("weight"));
                itemsModel.setCreated(jsonObject.getString("created"));
                itemsModel.setStatus(jsonObject.getString("status"));
                itemsModel.setWidth(jsonObject.getString("width"));
                itemsModel.setHeight(jsonObject.getString("height"));
                itemsModel.setLength(jsonObject.getString("length"));
                itemsModel.setPersonTransfertext(jsonObject.getString("person_transfertext"));
                itemsModel.setNewtimestamp(jsonObject.getString("newtimestamp"));
                itemsModel.setOnHold(jsonObject.getString("onHold"));
                itemsModel.setStatus2(jsonObject.getString("status2"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsFavorite(jsonObject.getBoolean("isFavorite"));

                itemsModel.setRating(jsonObject.getString("rating"));
                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));

                    itemsModel.setUserImages(imagesModel);
                }
                itemsModel.setPawnedDate(jsonObject.getString("pawned_date"));
                if (!jsonObject.isNull("tags")) {
                    JSONArray tags = jsonObject.getJSONArray("tags");
                    for (int k = 0; k < tags.length(); k++) {
                        listTags.add(tags.get(k).toString());
                    }
                }
                itemsModel.setTags(listTags);
                if (!jsonObject.isNull("location")) {
                    JSONObject location = jsonObject.getJSONObject("location");
                    ItemLocationModel locationModel = new ItemLocationModel();
                    locationModel.setAdministrativeAreaLevel1(location.getString("administrative_area_level_1"));
                    locationModel.setCountry(location.getString("country"));
                    locationModel.setCountryCode(location.getString("country_code"));
                    locationModel.setLat(Float.parseFloat(location.getString("lat")));
                    locationModel.setLng(Float.parseFloat(location.getString("lng")));
                    locationModel.setLocality(location.getString("locality"));
                    locationModel.setPostalCode(location.getString("postal_code"));

                    itemsModel.setLocation(locationModel);
                }

                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));

                    itemsModel.setItemImages(itemImagesModel);
                }

                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }
                itemsModel.setVerifieduser(Integer.parseInt(jsonObject.getString("verified")));
                itemArraylist.add(itemsModel);
            }
            Utilities.hideProgressDialog();
            setDataAndShowsItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataAndShowsItems() {

        if (itemArraylist.size() > 0) {
            noProductsTV.setVisibility(View.GONE);
        } else {
            noProductsTV.setVisibility(View.VISIBLE);
        }
//        mItemsAdapter = new ItemsAdapter(getActivity(), itemArraylist, null);
        mItemsAdapter = new ItemsAdapter(getActivity(), itemArraylist, null, "purchasedFragment");
        recyclerRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setAutoMeasureEnabled(true);
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(mItemsAdapter);
    }
}
