package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.adapters.DetailedPayment_Adapter;
import com.membilia.volley_models.DetailedTransactionModal;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniapps on 30/12/17.
 */

public class DetailedTransaction_Fragment extends Fragment {

    private static final String TAG = "detailedFragment";
    View view;
    LinearLayout tollbar;
    RelativeLayout leftRL;
    TextView textView;
    RecyclerView recyclerView;
    DetailedPayment_Adapter detailedPayment;
    ArrayList<DetailedTransactionModal> arrayList ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detailedtransaction, null);
        setView(view);
        setDefaultData();
        setOnclick();
        getPaymentHistory();
        return view;
    }


    private void setOnclick() {
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    private void setView(View view) {
        tollbar =  view.findViewById(R.id.topbar);
        leftRL =  view.findViewById(R.id.leftRL);
        textView =  view.findViewById(R.id.titleTV);
        recyclerView =  view.findViewById(R.id.recyclerRV);
    }

    private void setDefaultData() {
        tollbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        textView.setText("Detailed Payment History");

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getPaymentHistory() {
        String strUrl = WebServicesConstants.PaymentHistroy;

        final JSONObject requestJson = new JSONObject();
        try {
            requestJson.put("user_id", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, ""));
            requestJson.put("page_no", "1");
            requestJson.put("perPage", "40");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.TransactionHistory);
    }

    private void parseResponse(String response) {
        {
            arrayList = new ArrayList<>();
            try {
                JSONObject mainJson = new JSONObject(response);
                JSONArray dateJsonArray = mainJson.getJSONArray("data");
                for (int i = 0; i < dateJsonArray.length(); i++) {
                    JSONObject childJson = dateJsonArray.getJSONObject(i);
                    DetailedTransactionModal detailedTransactionModal = new DetailedTransactionModal();
                    detailedTransactionModal.setPayment_id(childJson.getString("payment_id"));
                    detailedTransactionModal.setOrder_date(childJson.getString("order_date"));
                    detailedTransactionModal.setCommission_value(childJson.getString("commission_value"));
                    detailedTransactionModal.setPrice(childJson.getString("price"));
                    detailedTransactionModal.setItem_name(childJson.getString("item_name"));
                    detailedTransactionModal.setCharity_name(childJson.getString("charity_name"));
                    detailedTransactionModal.setItem_id(childJson.getString("item_id"));
                    detailedTransactionModal.setItem_image(childJson.getString("item_image"));
                    detailedTransactionModal.setVideo_image(childJson.getString("video_image"));
                    detailedTransactionModal.setEarning(childJson.getString("earning"));

                    arrayList.add(detailedTransactionModal);
                }
                Log.e(TAG, "parseResponse: "+arrayList.get(0).getItem_name() );
                setAdapter(arrayList);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setAdapter(ArrayList<DetailedTransactionModal> arrayList) {
        detailedPayment = new DetailedPayment_Adapter(getActivity(),arrayList);
        recyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(detailedPayment);
    }
}
