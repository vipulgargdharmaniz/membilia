package com.membilia.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ChatMessageAdapter;
import com.membilia.dbModal.ChatForItemModal;
import com.membilia.volley_models.MessageHistoryModal;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.facebook.FacebookSdk.getApplicationContext;
/*
 * Created by dharmaniapps on 27/12/17.
 */
public class SendMessage_Fragment extends Fragment {

    private static final String TAG = "SendMessage_Fragment";
    View view;
    LinearLayout sendLL, topbar;
    RelativeLayout leftRL;
    EditText messageET;
    TextView title;
    ImageView settingIV;

    String friendId, timeStamp, itemID;
    int perPage = 100;
    ChatMessageAdapter mAdapter;
    ArrayList<MessageHistoryModal> arrayList = new ArrayList<>();
    RecyclerView recyclerRV;
    RelativeLayout rightRL;
    Handler handler;
    Timer timer;
    TimerTask timerTask;
    String lastMsgID="", lastMsgTime="";
    ArrayList<ChatForItemModal> chatModalList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();

        if (b != null) {
            friendId = b.getString("friend_id");
            itemID = b.getString("item_id");
            timeStamp = b.getString("timeStamp");
            Log.e(TAG, "friendId: "+friendId );
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.sendmessage_fragment, null);
            setView(view);
            setClick();
            setDefault();
            if (haveNetworkConnection()) {
                getAllChat();
            } else {
                int itemSize = HomeActivity.db.getAllChat(Constants_AddItem.ITEM_ID).size();
                Log.e(TAG, "itemSize: "+itemSize );
                if (itemSize>0){
                    parseDBdata(itemSize);
                }
            }

        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
        HomeActivity.bottombar.setVisibility(View.GONE);
        return view;
    }

    private void parseDBdata(int itemSize) {
        ArrayList<MessageHistoryModal> tempList = new ArrayList<>();
        ArrayList<ChatForItemModal> newHashMap = new ArrayList();
        newHashMap = HomeActivity.db.getAllChat(Constants_AddItem.ITEM_ID);
        for (int i=0;i<itemSize;i++){
            MessageHistoryModal modal = new MessageHistoryModal();
            ChatForItemModal chatForItemModal = new ChatForItemModal();

            modal.setDirection(newHashMap.get(i).getChatModal().isDirection());
            modal.setMessageId(newHashMap.get(i).getChatModal().getMessageId());
            modal.setMessage(newHashMap.get(i).getChatModal().getMessage());
            modal.setItemId(newHashMap.get(i).getChatModal().getItemId());
            modal.setItemName(Constants_AddItem.ITEM_NAME);

            modal.setTimeStamp(newHashMap.get(i).getChatModal().getTimeStamp());
            Log.e(TAG, "parseDBdata: "+newHashMap.get(i).getChatModal().getStrDirection() );
            if (newHashMap.get(i).getChatModal().getStrDirection().equalsIgnoreCase("true")) {
                modal.setStrDirection("true");
                modal.setSenderId(newHashMap.get(i).getChatModal().getSenderId());
                modal.setSenderName(newHashMap.get(i).getChatModal().getSenderName());
                modal.setSenderImage(newHashMap.get(i).getChatModal().getSenderImage());
                modal.setReciverId(newHashMap.get(i).getChatModal().getReciverId());
                modal.setRecivierName(newHashMap.get(i).getChatModal().getRecivierName());
                modal.setRecivierImage(newHashMap.get(i).getChatModal().getRecivierImage());
            } else {
                modal.setStrDirection("false");
                modal.setSenderId(newHashMap.get(i).getChatModal().getSenderId());
                modal.setSenderName(newHashMap.get(i).getChatModal().getSenderName());
                modal.setSenderImage(newHashMap.get(i).getChatModal().getSenderImage());
                modal.setReciverId(newHashMap.get(i).getChatModal().getReciverId());
                modal.setRecivierName(newHashMap.get(i).getChatModal().getRecivierName());
                modal.setRecivierImage(newHashMap.get(i).getChatModal().getRecivierImage());
            }
            tempList.add(modal);
            Collections.sort(tempList, MessageHistoryModal.DESCENDING_COMPARATOR);
            arrayList = tempList;
            chatForItemModal.setMainItemId(newHashMap.get(i).getChatModal().getItemId());
            chatForItemModal.setChatModal(modal);
        lastMsgID = arrayList.get(arrayList.size() - 1).getMessageId();
        lastMsgTime = arrayList.get(arrayList.size() - 1).getTimeStamp();
        Utilities.hideProgressDialog();
        setAdapter(arrayList);
        }
    }

    private void getAllChat() {
        int itemSize = HomeActivity.db.getAllChat(Constants_AddItem.ITEM_ID).size();
        if (itemSize>0){
            parseDBdata(itemSize);
        }
        if (timeStamp.equalsIgnoreCase("")){
            timeStamp = String.valueOf(System.currentTimeMillis()/1000);
        }
        Log.e(TAG, "friendId:aa "+friendId );
        String strUrl = WebServicesConstants.GetMessage + friendId + "&direction=1&time_stamp=" + timeStamp + "&perPage=" + perPage + "&item_id=" + itemID;
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                parseResponseData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return header;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.GetMessage);
    }


    private void setView(View view) {
        sendLL = view.findViewById(R.id.sendLL);
        topbar = view.findViewById(R.id.topbar);
        leftRL = view.findViewById(R.id.leftRL);
        rightRL = view.findViewById(R.id.rightRL);
        title = view.findViewById(R.id.titleTV);
        settingIV = view.findViewById(R.id.imgSettingIV);
        messageET = view.findViewById(R.id.messageET);
        recyclerRV = view.findViewById(R.id.recyclerRV);
        messageET.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    private void setClick() {
        sendLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strMessage = messageET.getText().toString();
                if (!strMessage.equalsIgnoreCase("")) {
                    try {
                        JSONObject requestJson = new JSONObject();
                        requestJson.put("friend_id", friendId);
                        requestJson.put("message", strMessage);
                        requestJson.put("message_type", "0");
                        requestJson.put("item_id", itemID);
                        sendMessage(requestJson);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                messageET.setText("");
            }
        });
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
                HomeActivity.bottombar.setVisibility(View.VISIBLE);

                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        rightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: "+itemID );
                HomeActivity.switchFragment(getActivity(), new Fragment_OrderDetails(), Constants.Buy_Fragmnet, true, null);
            }
        });
    }


    private void sendMessage(final JSONObject requestJson) {
        Log.e(TAG, "onchatrespose requestJson: " + requestJson);
        String strUrl = WebServicesConstants.Chat;
        Log.e(TAG, "onchatrespose strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onchatrespose: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    lastMsgTime = jsonObject.getString("timeStamp");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                getLastChat();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.Chat);
    }

    private void setDefault() {
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        rightRL.setVisibility(View.VISIBLE);
        Log.e(TAG, "title.ITEM_NAME: " + Constants_AddItem.ITEM_NAME);
        title.setText(Constants_AddItem.ITEM_NAME);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
        HomeActivity.bottombar.setVisibility(View.GONE);
        startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    private void parseResponseData(String response) {
        ArrayList<MessageHistoryModal> tempList = new ArrayList<>();
        chatModalList = new ArrayList<>();
        if (HomeActivity.db.getAllChat(Constants_AddItem.ITEM_ID).size()>0){
            HomeActivity.db.removeAllChat(Constants_AddItem.ITEM_ID);
        }
        try {
            JSONArray jsonArray = new JSONArray(response);
            JSONObject mainJsonObject;
            JSONObject senderJson;
            JSONObject reciverJson;
            JSONObject senderImageJson;
            JSONObject reciverImageJson;
            for (int i = 0; i < jsonArray.length(); i++) {
                mainJsonObject = jsonArray.getJSONObject(i);
                MessageHistoryModal modal = new MessageHistoryModal();
                ChatForItemModal chatForItemModal = new ChatForItemModal();
                modal.setDirection(mainJsonObject.getBoolean("direction"));
                modal.setMessageId(mainJsonObject.getString("message_id"));
                modal.setMessage(mainJsonObject.getString("text"));
                modal.setItemId(mainJsonObject.getString("item_id"));
                modal.setItemName(Constants_AddItem.ITEM_NAME);


                Log.e(TAG, "parseResponseData:direction "+mainJsonObject.getBoolean("direction") );
                modal.setTimeStamp(mainJsonObject.getString("timeStamp"));
                if (mainJsonObject.getBoolean("direction")) {
                    modal.setStrDirection("true");
                    senderJson = mainJsonObject.getJSONObject("sender");
                    modal.setSenderId(senderJson.getString("user_id"));
                    modal.setSenderName(senderJson.getString("name"));
                    senderImageJson = senderJson.getJSONObject("user_images");
                    modal.setSenderImage(senderImageJson.getString("original"));

                    reciverJson = mainJsonObject.getJSONObject("receiver");
                    modal.setReciverId(reciverJson.getString("user_id"));
                    modal.setRecivierName(reciverJson.getString("name"));
                    reciverImageJson = reciverJson.getJSONObject("user_images");
                    modal.setRecivierImage(reciverImageJson.getString("original"));

                } else {
                    modal.setStrDirection("false");
                    senderJson = mainJsonObject.getJSONObject("sender");
                    modal.setSenderId(senderJson.getString("user_id"));
                    modal.setSenderName(senderJson.getString("name"));
                    senderImageJson = senderJson.getJSONObject("user_images");
                    modal.setSenderImage(senderImageJson.getString("original"));

                    reciverJson = mainJsonObject.getJSONObject("receiver");
                    modal.setReciverId(reciverJson.getString("user_id"));
                    modal.setRecivierName(reciverJson.getString("name"));
                    reciverImageJson = reciverJson.getJSONObject("user_images");
                    modal.setRecivierImage(reciverImageJson.getString("original"));
                }
                tempList.add(modal);
                Collections.sort(tempList, MessageHistoryModal.DESCENDING_COMPARATOR);
                arrayList = tempList;

                chatForItemModal.setMainItemId(mainJsonObject.getString("item_id"));
                chatForItemModal.setChatModal(modal);
                chatModalList.add(chatForItemModal);
                HomeActivity.db.addChat(chatModalList);
            }
            if (arrayList.size()>0){
                lastMsgID = arrayList.get(arrayList.size() - 1).getMessageId();
                lastMsgTime = arrayList.get(arrayList.size() - 1).getTimeStamp();
                Log.e(TAG, "lastMsgTime: "+lastMsgTime );
                Log.e(TAG, "lastMsgTime: "+lastMsgID );
            }

            Utilities.hideProgressDialog();
            setAdapter(arrayList);
//            getDBdata();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(final ArrayList<MessageHistoryModal> arrayList) {
        String userId = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
        mAdapter = new ChatMessageAdapter(getActivity(), arrayList, userId);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.scrollToPosition(arrayList.size() - 1);
        recyclerRV.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                recyclerRV.scrollToPosition(arrayList.size() - 1);
            }
        });
        recyclerRV.setNestedScrollingEnabled(false);
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void startTimer() {
        timer = new Timer();
        handler = new Handler();
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        if (haveNetworkConnection()) {
                            getLastChat();
                        }
                        //your code is here
                    }
                });
            }
        };
        timer.schedule(timerTask, 8000, 5000);
    }

    //To stop timer
    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    private void getLastChat() {
//        if (lastMsgTime.equalsIgnoreCase("")||lastMsgTime.equalsIgnoreCase(" " )){
//            Log.e(TAG, "lastMsgTime: "+lastMsgTime );
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            lastMsgTime = ts;
//        }
        Log.e(TAG, "getLastChat:lastMsgTime "+lastMsgTime );
        String strUrl = WebServicesConstants.GetMessage + friendId + "&direction=1&time_stamp=" + lastMsgTime + "&perPage=1" + "&item_id=" + itemID + "&page_no=1";
        Log.e(TAG, "getLastChat:strUrl " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "getLastChat onResponse: " + response);
                parserLastData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization",HomeActivity.db.getUser().get(0).getUserToken());
                return header;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.GetMessage);
    }

    private void parserLastData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            JSONObject mainJsonObject;
            JSONObject senderJson;
            JSONObject reciverJson;
            JSONObject senderImageJson;
            JSONObject reciverImageJson;
            ArrayList<MessageHistoryModal> tempListLastData = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                mainJsonObject = jsonArray.getJSONObject(i);
                MessageHistoryModal modal = new MessageHistoryModal();
                ChatForItemModal chatForItemModal = new ChatForItemModal();
                modal.setDirection(mainJsonObject.getBoolean("direction"));
                modal.setMessageId(mainJsonObject.getString("message_id"));
                modal.setMessage(mainJsonObject.getString("text"));
                modal.setItemId(mainJsonObject.getString("item_id"));
                senderJson = mainJsonObject.getJSONObject("sender");
                modal.setSenderId(senderJson.getString("user_id"));
                modal.setSenderName(senderJson.getString("name"));
                senderImageJson = senderJson.getJSONObject("user_images");
                modal.setSenderImage(senderImageJson.getString("original"));
                reciverJson = mainJsonObject.getJSONObject("receiver");
                modal.setReciverId(reciverJson.getString("user_id"));
                modal.setRecivierName(reciverJson.getString("name"));
                reciverImageJson = reciverJson.getJSONObject("user_images");
                modal.setRecivierImage(reciverImageJson.getString("original"));
                modal.setTimeStamp(mainJsonObject.getString("timeStamp"));
                if (mainJsonObject.getBoolean("direction")){
                    modal.setStrDirection("true");
                }else {
                    modal.setStrDirection("false");
                }
                timeStamp = mainJsonObject.getString("timeStamp");
                tempListLastData.add(modal);
                chatForItemModal.setMainItemId(mainJsonObject.getString("item_id"));
                chatForItemModal.setChatModal(modal);
                chatModalList.add(chatForItemModal);
            }
            Utilities.hideProgressDialog();
            if (tempListLastData.size()>0){
                if (!lastMsgID.equalsIgnoreCase(tempListLastData.get(0).getMessageId())) {
                    Log.e(TAG, "sameID:NO ");
                    Log.e(TAG, "sameID:NO MSg " + tempListLastData.get(0).getMessage());
                    Log.e(TAG, "sameID:NO MSglastMsgID " + lastMsgID);
                    lastMsgID = tempListLastData.get(0).getMessageId();
                    lastMsgTime = tempListLastData.get(0).getTimeStamp();
                    arrayList.addAll(tempListLastData);
                    HomeActivity.db.addChat(chatModalList);
                    setAdapter(arrayList);
                } else {
                    Log.e(TAG, "sameID:yes MSg " + tempListLastData.get(0).getMessage());
                    Log.e(TAG, "sameID:YES ");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}
