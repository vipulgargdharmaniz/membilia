package com.membilia.Fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.membilia.R;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.CategoriesAdapter;
import com.membilia.volley_models.CategoriesModel;

import java.util.ArrayList;

/**
 * Created by dharmaniz on 25/5/17.
 */

public class CategoryFragment extends Fragment {

    View categoriesView;
    GridView categoriesGrivView;
    Resources mResources;
    AppSingleton mAppSingleton;
    String arrayCategories[] = {"THINGS WORN", "FOOTWEAR", "The EXPERIENCE", "BALLS,BATS & BAGS", "THINGS THAT MOVE", "FASHION", "ACCESSORIES", "MISCELLANEOUS"};
    int arrayCategoriesImages[] = {R.drawable.c1, R.drawable.c2, R.drawable.c3, R.drawable.c4, R.drawable.c5, R.drawable.c6, R.drawable.c7, R.drawable.c8};
    String arrayCategoriesID[] = {"3playpawn", "footwear", "9playpawn", "6playpawn", "1playpawn", "Fashion", "4playpawn", "7playpawn"};
    ArrayList<CategoriesModel> modelArrayList = new ArrayList<CategoriesModel>();
    CategoriesAdapter mAdapter;
    private static final String TAG = "CategoryFragment";

    public CategoryFragment() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        categoriesView = inflater.inflate(R.layout.fragment_category, null);
        Log.i(TAG, "onCreateView: ");
        mResources = getActivity().getResources();
        setUpViews(categoriesView);
        setclicklisteners();
        HomeActivity.isAddItemOpen = true;
        return categoriesView;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.categoryClick();
    }

    public void setUpViews(View v) {
        categoriesGrivView = (GridView) v.findViewById(R.id.categoriesGrivView);
        setCategoriesAdapter();
    }

    public void setclicklisteners() {
        categoriesGrivView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CategoriesModel model = modelArrayList.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("model",model);
                Log.e(TAG, "setclicklisteners: "+modelArrayList.get(position).getCategoryID() );
                HomeActivity.switchFragment(getActivity(),new CategoriesItemsFragment(), Constants.CATEGORIES_ITEMS_FRAGMENT,true,bundle);
            }
        });
    }

    private void setCategoriesAdapter() {
        for (int i = 0; i < arrayCategoriesImages.length; i++) {
            CategoriesModel mModel = new CategoriesModel();
            mModel.setImage(arrayCategoriesImages[i]);
            mModel.setName(arrayCategories[i]);
            mModel.setCategoryID(arrayCategoriesID[i]);

            modelArrayList.add(mModel);
        }
        Log.e("CategoryFragment", "*******Size******" + modelArrayList.size());
        mAdapter = new CategoriesAdapter(getActivity(), modelArrayList);
        categoriesGrivView.setAdapter(mAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();


        HomeActivity.onPause = "yes";
    }
}
