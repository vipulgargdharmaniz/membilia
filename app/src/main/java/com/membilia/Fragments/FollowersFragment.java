package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.Followers_Adapter;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.Follow_Model;
import com.membilia.volley_models.ItemLocationModel;
import com.membilia.volley_models.ItemUserImagesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniapps on 31/10/17.
 */

public class FollowersFragment extends Fragment {

    private static final String TAG = "FollowersFragment";
    private View            itemView;
    private RecyclerView    recyclerRV;
    private RelativeLayout  leftRL,rightRL;
    private NestedScrollView mNestedScrollView;
    private ProgressBar      progressbar;
    private TextView         titleTV;

    private int perPage     = 20;
    private int pageNo      = 1;

    private String strUrl   = WebServicesConstants.Follower+
                              HomeActivity.forProfileID+"&perPage="+perPage+"&page_no="+pageNo;

    private Followers_Adapter mfollowAdapter;
    private ArrayList<Follow_Model> modelArrayList = new ArrayList<Follow_Model>();
    private ArrayList<Follow_Model> modelArrayListAll = new ArrayList<Follow_Model>();
    private String isFollowed ="";
    private boolean isNextPage = false;
    private boolean isNew = false;
    LinearLayout topbar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       if (itemView==null){
           isNew = true;
           itemView = inflater.inflate(R.layout.follow_screen, null);

           Bundle bundle = this.getArguments();
           if (bundle != null) {
               isFollowed = bundle.getString("strFollowOrFollowing", "");
           }
           setView(itemView);
       }
       else {
           isNew = false;
       }
        return itemView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: " );
        if (isNew){
            Utilities.showProgressDialog(getActivity());
            pageNo = 1;

            modelArrayListAll.clear();
            executeServices();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeActivity.onPause = "yes";
    }

    private void setView(View itemView) {
        topbar =  itemView.findViewById(R.id.topbar);
        recyclerRV = itemView.findViewById(R.id.recyclerRV);
        leftRL =  itemView.findViewById(R.id.leftRL);
        rightRL =  itemView.findViewById(R.id.rightRL);
        mNestedScrollView = itemView.findViewById(R.id.mNestedScrollView);
        progressbar =  itemView.findViewById(R.id.progressbar);
        titleTV =  itemView.findViewById(R.id.titleTV);

        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        titleTV.setText(isFollowed);
        rightRL.setVisibility(View.GONE);

        setOnClicks();
    }

    private void setOnClicks() {

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (isNextPage){
                        ++pageNo;
                        executeServices();
                    }
                }
            }
        });

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: " );
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();
            }
        });
    }


    private void executeServices()  {
        Log.e(TAG, "forProfileID: "+HomeActivity.forProfileID );
        strUrl   = WebServicesConstants.Follower+
                HomeActivity.forProfileID+"&perPage="+perPage+"&page_no="+pageNo;
        Log.e(TAG, "followers url: "+strUrl );
        StringRequest stringRequest = new StringRequest(strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "*********onResponse*********" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()>0){
                        isNextPage = true;
                    }
                    if(jsonArray.length()==0 || jsonArray.length()<perPage){
                        isNextPage = false;
                    }


                    parseJson(jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "followersFragment");
    }

    private void parseJson(JSONArray response) throws   JSONException{
        try {
            for(int i=0 ; i<response.length() ; i++){
                JSONObject jsonObject = response.getJSONObject(i);
                Follow_Model follow_model = new Follow_Model();

                follow_model.setUserid(jsonObject.getString("user_id"));
                follow_model.setUserName(jsonObject.getString("name"));
                follow_model.setDesc(jsonObject.getString("description"));
                follow_model.setEmail(jsonObject.getString("email"));
                follow_model.setFollowersCount(jsonObject.getInt("followerCount"));
                follow_model.setFollowersCount(jsonObject.getInt("followingCount"));
                follow_model.setRanking(jsonObject.getInt("ranking"));
                follow_model.setRating(jsonObject.getInt("rating"));
                follow_model.setFollowed(jsonObject.getBoolean("isFollowed"));
                follow_model.setFlag(jsonObject.getString("flag"));
                follow_model.setVerified(jsonObject.getBoolean("isVerified"));

                if (!jsonObject.isNull("location")){
                    JSONObject location = jsonObject.getJSONObject("location");
                    ItemLocationModel locationModel = new ItemLocationModel();
                    locationModel.setAdministrativeAreaLevel1(location.getString("administrative_area_level_1"));
                    locationModel.setCountry(location.getString("country"));
                    locationModel.setCountryCode(location.getString("country_code"));
                    locationModel.setLat(Float.parseFloat(location.getString("lat")));
                    locationModel.setLng(Float.parseFloat(location.getString("lng")));
                    locationModel.setLocality(location.getString("locality"));
                    locationModel.setPostalCode(location.getString("postal_code"));

                    follow_model.setLocation(locationModel);
                }
                if (!jsonObject.isNull("user_images")){
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));

                    follow_model.setUserImages(imagesModel);
                }

                if (pageNo==1){
                    modelArrayList.add(follow_model);
                }
                else {
                    modelArrayList.clear();
                    modelArrayList.add(follow_model);
                }

            }

            if (modelArrayList.size()>0)
            modelArrayListAll.addAll(modelArrayList);
            setAdapter();

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setAdapter() {

        mfollowAdapter = new Followers_Adapter(getActivity(),modelArrayListAll,FollowersFragment.this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        mfollowAdapter.notifyDataSetChanged();
        recyclerRV.setNestedScrollingEnabled(false);
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(mfollowAdapter);
        mfollowAdapter.notifyDataSetChanged();
    }

    private void adapter(){

        mfollowAdapter.notifyDataSetChanged();
    }


}
