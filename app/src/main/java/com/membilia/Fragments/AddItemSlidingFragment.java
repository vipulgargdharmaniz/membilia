package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ItemSplashAdapter;

/*
 * Created by dharmaniapps on 2/2/18.
 */

public class AddItemSlidingFragment extends Fragment {

    TabLayout tabLayout;
    private ViewPager mViewPager;
    private ItemSplashAdapter mPagerAdapter;
    int layoutArray[] = {R.layout.item_splash1, R.layout.item_splash2, R.layout.item_splash3};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_splash_slides,null);
        setUpViews(view);
        HomeActivity.bottombar.setVisibility(View.GONE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
        HomeActivity.isAddItemOpen = true;
        return view;
    }


    protected void setUpViews(View view) {
        mViewPager = view.findViewById(R.id.viewpager);
        tabLayout =  view.findViewById(R.id.tabDots);
        setViewPagerAdapter();
    }

    private void setViewPagerAdapter() {
        mPagerAdapter = new ItemSplashAdapter(getActivity(), layoutArray, tabLayout , AddItemSlidingFragment.this);
        mViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager, true);
    }
    public void back(){
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();
        Constants.OpenAddItemFrom = "home";
        HomeActivity.switchFragment(getActivity(), new AddItemFragment(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, true, null);
    }
}
