package com.membilia.Fragments;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.UserAdapter;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.UserImagesModel;
import com.membilia.volley_models.UserLocationModel;
import com.membilia.volley_models.UsersModel;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/*
 * Created by dharmaniz on 25/5/17.
*/

public class UsersFragment extends Fragment {
    String TAG = "UsersFragment";
    Resources mResources;
    SharedPreferences _prefs;
    SharedPreferences.Editor _editor;
    View usersview;
    RecyclerView userRecyclerView;
    UserAdapter mAdapter;
    ArrayList<UsersModel> usersArrayList;
    private NestedScrollView mNestedScrollView;


    int perPage = 60;
    int page_no = 1;
    int isNextPage = 1;
    private boolean isNew = false;

    public UsersFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        if (usersview==null){
            isNew = true;
            usersview = inflater.inflate(R.layout.fragment_userslist, null);
            Log.i(TAG, "onCreateView: ");
            usersArrayList = AppSingleton.getInstance().getUserApiResponseArrayList();
            Constants.IS_ITEM_USER = "user_fragment";
            setUpViews(usersview);
            setUpClickListeners();
        }
        else {
            isNew = false;
        }

        return usersview;
    }


    public void setUpViews(View v) {
        ExploreFragment.imgLeftIV.setImageResource(R.drawable.icon_zoom);
//        ExploreFragment.imgRightIV.setImageResource(R.drawable.icon_menu);
        userRecyclerView = (RecyclerView) v.findViewById(R.id.userRecyclerView);
        mNestedScrollView = (NestedScrollView) v.findViewById(R.id.mNestedScrollView);



    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNew==true){
            if (usersArrayList.size() > 0) {
                setAdapter();
            } else {
        /*Execute User API*/
                Utilities.showProgressDialog(getActivity());
                gettingAllUsers();
            }
        }
        else {

        }

        HomeActivity.homeClick();
    }

    private void setUpClickListeners() {

//        ExploreFragment.imgRightIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDialogUsersFilter();
//            }
//        });
        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {

                    if (isNextPage==1){
                        ++page_no;
                        gettingAllUsers();
                    }
                }
            }
        });

    }

    private void setAdapter() {
        userRecyclerView.setNestedScrollingEnabled(false);
//        userRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        userRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new UserAdapter(getActivity(), usersArrayList);
        userRecyclerView.setAdapter(mAdapter);
    }

    private void showDialogUsersFilter() {
        final Dialog filterDialog = new Dialog(getActivity());
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        filterDialog.setContentView(R.layout.dialog_user_filter);
        filterDialog.setCanceledOnTouchOutside(true);


        Window window = filterDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        LinearLayout llFans = (LinearLayout) filterDialog.findViewById(R.id.llFans);
        LinearLayout llPlayers = (LinearLayout) filterDialog.findViewById(R.id.llPlayers);
        LinearLayout llBoth = (LinearLayout) filterDialog.findViewById(R.id.llBoth);
        final ImageView imgFans = (ImageView) filterDialog.findViewById(R.id.imgFans);
        final ImageView imgPlayers = (ImageView) filterDialog.findViewById(R.id.imgPlayers);
        final ImageView imgBoth = (ImageView) filterDialog.findViewById(R.id.imgBoth);
        imgBoth.setVisibility(View.VISIBLE);
        if (Constants.IS_USERS_FILTER.equals("fan")) {
            imgFans.setVisibility(View.VISIBLE);
            imgPlayers.setVisibility(View.GONE);
            imgBoth.setVisibility(View.GONE);
        } else if (Constants.IS_USERS_FILTER.equals("player")) {
            imgFans.setVisibility(View.GONE);
            imgPlayers.setVisibility(View.VISIBLE);
            imgBoth.setVisibility(View.GONE);
        } else if (Constants.IS_USERS_FILTER.equals("both")) {
            imgFans.setVisibility(View.GONE);
            imgPlayers.setVisibility(View.GONE);
            imgBoth.setVisibility(View.VISIBLE);
        }
        llFans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.dismiss();
                Constants.IS_USERS_FILTER = "fan";
                imgFans.setVisibility(View.VISIBLE);
                imgPlayers.setVisibility(View.GONE);
                imgBoth.setVisibility(View.GONE);
                /*Execute API*/
                //executeUsersAPI();
            }
        });
        llPlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.dismiss();
                Constants.IS_USERS_FILTER = "player";
                imgFans.setVisibility(View.GONE);
                imgPlayers.setVisibility(View.VISIBLE);
                imgBoth.setVisibility(View.GONE);
                /*Execute API*/
                //executeUsersAPI();
            }
        });
        llBoth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterDialog.dismiss();
                Constants.IS_USERS_FILTER = "both";
                imgFans.setVisibility(View.GONE);
                imgPlayers.setVisibility(View.GONE);
                imgBoth.setVisibility(View.VISIBLE);
                /*Execute API*/
                //executeUsersAPI();
            }
        });
        filterDialog.show();
    }




    private void gettingAllUsers() {
        String strUrl = WebServicesConstants.USERS_LISTING + "?perPage=" + perPage + "&page_no=" + page_no;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, "onResponse*********" + response.toString());
                if (response.length()>0){
                    isNextPage = 1;
                    parseResponse(response);
                }
                else {
                    isNextPage = 0;
                }
                if (response.length()<perPage){
                    isNextPage = 0;
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json = null;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "UsersRequest");
    }

    private void parseResponse(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jObject = response.getJSONObject(i);
                UsersModel uModel = new UsersModel();
                uModel.setUserId(jObject.getString("user_id"));
                uModel.setName(jObject.getString("name"));
                uModel.setDescription(jObject.getString("description"));
                if (!jObject.isNull("email"))
                    uModel.setEmail(jObject.getString("email"));
                uModel.setFollowerCount(Integer.parseInt(jObject.getString("followerCount")));
                uModel.setFollowingCount(Integer.parseInt(jObject.getString("followingCount")));
                uModel.setRating(Integer.parseInt(jObject.getString("rating")));
                uModel.setRanking(Integer.parseInt(jObject.getString("ranking")));
                uModel.setIsFollowed(Boolean.parseBoolean(jObject.getString("isFollowed")));

                if (!jObject.isNull("location")) {
                    JSONObject locationObject = jObject.getJSONObject("location");
                    UserLocationModel locationModel = new UserLocationModel();
                    if (!jObject.isNull("administrative_area_level_1"))
                        locationModel.setAdministrativeAreaLevel1(locationObject.getString("administrative_area_level_1"));
                    if (!jObject.isNull("country"))
                        locationModel.setCountry(locationObject.getString("country"));
                    if (!jObject.isNull("country_code"))
                        locationModel.setCountryCode(locationObject.getString("country_code"));
                    if (!jObject.isNull("lat"))
                        locationModel.setLat(locationObject.getString("lat"));
                    if (!jObject.isNull("lng"))
                        locationModel.setLng(locationObject.getString("lng"));
                    if (!locationObject.isNull("locality"))
                        locationModel.setLocality(locationObject.getString("locality"));
                    if (!locationObject.isNull("postal_code"))
                        locationModel.setPostalCode(locationObject.getString("postal_code"));

                    uModel.setLocation(locationModel);
                }

                if (!jObject.isNull("flag"))
                    uModel.setFlag(jObject.getString("flag"));

                uModel.setIsVerified(Boolean.parseBoolean(jObject.getString("isVerified")));
                if (!jObject.isNull("user_images")) {
                    JSONObject userObject = jObject.getJSONObject("user_images");
                    UserImagesModel imagesModel = new UserImagesModel();
                    if (!userObject.isNull("original"))
                        imagesModel.setOriginal(userObject.getString("original"));

                    uModel.setUserImages(imagesModel);
                }

                if (uModel.getIsVerified()){
                    usersArrayList.add(uModel);
                }

            }
            Utilities.hideProgressDialog();
            setAdapter();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
