package com.membilia.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aigestudio.wheelpicker.WheelPicker;
import com.aigestudio.wheelpicker.widgets.WheelDayPicker;
import com.aigestudio.wheelpicker.widgets.WheelMonthPicker;
import com.aigestudio.wheelpicker.widgets.WheelYearPicker;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.kyleduo.switchbutton.SwitchButton;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.CameraUtils;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.CharityOptions;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.AddImageListAdapter;
import com.membilia.interfaces.GetItemCountAndRemoveItem;
import com.membilia.volley.AppHelper;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley.VolleyMultipartRequest;
import com.membilia.volley_models.GenreModel;
import com.membilia.volley_models.GenreValue;
import com.membilia.volley_models.ImagesItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.EXTRA_VIDEO_QUALITY;
import static com.facebook.FacebookSdk.getApplicationContext;
/*
 * Created by dharmaniapps on 14/12/17.
*/

public class Edit_AddItem extends Fragment {

    private static final String TAG = "Edit_AddItem";
    Resources mResources;
    View addItemPPView;
    TextView txtItemCountTV;
    RecyclerView recyclerViewImagesVideo;
    EditText editItemNameET, editItemPriceET, editItemWeightET, editItemStoryET,
            editItemTagSepratedET, editPersonalTrasET;
    TextView itemLastUsedDATE, txtConfirmTV, editTypeCharityName, editLocationET, deleteTV;
    Typeface mTypeface;
    RelativeLayout rightRL, leftRL;
    String strAdminArea = "", strCountry = "",
            strLat = "", strLong, strLocalty = "", strPostalcode = "";
    /*
     * radio button and switch button
     */
    SwitchButton switchAutographed, switchForCharity;
    RadioButton radioShipping, radioPersonal;
    boolean isPersonalTransfer = false;
    boolean isAutograph = false;
    boolean isForCharity = false;
    LinearLayout firstCharityLL;   /* for showing and hiding charity layout*/
    TextView nextTV, shippingTypeTV;
    LinearLayout shppingtypeLL;
    /*
     * handling Tags
     */
    String strTags = "";
    ArrayList<String> tagArraylist = new ArrayList<>();
    String strItemName, strItemPrice, strItemWeight, strCategory, strGenre, strGenreValue, strItemStory,
            strPersonalTransfer, strLocation, strCharityName;
    int intPrice = 0;
    /**
     * textview and relative for categories
     */
    RelativeLayout categoryRL, genreRl, genreValueRL;
    TextView categoryTV, genreValueTV, genreTV;

    /**
     * ImageView for selecting images
     */
    SimpleDraweeView selectIMG;

    /**
     * Bottom sheet vaiables and Widgets
     */
    View bottomSheet;
    TextView cancelTV, sheetTitleTV, doneTV;
    List<String> monthArraylist = new ArrayList<>();
    String firstGenreItem = "";
    LinearLayout wheeldataLL, wheelpickerLL, ll_date;
    WheelDayPicker main_wheel_left;
    WheelPicker wheelViewData;
    WheelYearPicker main_wheel_right;
    WheelMonthPicker main_wheel_center;

    String strResultDATA = "";
    String strResultDATE = "";
    String strDate = "", strMonth, strYear = "";
    String strDateFinal = "";

    int intMonth = 0;


    /**
     * it helps us to detect when wheel view is open then for where have to set the values
     * 0 for choose categories , 1 for select genre , 2 for genre value , 3 for datepicker;
     */
    int setValueFor = 0;

    /**
     * month name as of hasmap
     */
    HashMap<Integer, String> monthMap = new HashMap<>();
    ArrayList<String> data = new ArrayList<>();
    /**
     * permission for camera , video and gallery
     */
    private String CameraPermission = Manifest.permission.CAMERA;
    private String ReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String WriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final int CAMERA_REQUEST = 111;
    private static final int GALLERY_REQUEST = 222;
    private static final int VIDEO_REQUEST = 333;
    private static final int VIDEO_GALLERY_REQUEST = 4444;
    private static final int GOOGLE_PLACE_API = 5555;

  Uri tempUria;
    /**
     * test
     */
    ArrayList<String> imagesEncodedList = new ArrayList<String>();
    String imageEncoded;

    /**
     * item image modal
     */
    ImagesItemModel imagesItemModel = new ImagesItemModel();

    /*ArrayList*/
    ArrayList<ImagesItemModel> mImagesVideoArrayList;
    ArrayList<String> imageBitmapArray = new ArrayList<>();

    String mCurrentPhotoPath;

    public String VIDEO_TYPE = "video_type";
    public String IMAGE_TYPE = "image_type";

    /**
     * Check for cheking and seeting for single video upload
     */
    public static boolean isVideoSelected = false;

    /**
     * categories arraylist , genre Arraylist  , genre value Arraylist
     */
    ArrayList<String> catArrayList;
    ArrayList<GenreModel> genreArrayList;
    ArrayList<String> genreList;

    List<GenreValue> genreValueList = new ArrayList<>();
    ArrayList<String> arrayListWheelData = new ArrayList<>();


    byte[] image1 = null;
    ArrayList<ImagesItemModel> strImageUrlList = new ArrayList<>();

    byte[] videoImage = null;
    byte[] VideoData = null;
    JSONObject imageJson;


    GetItemCountAndRemoveItem mGetItemCountAndRemoveItem = new GetItemCountAndRemoveItem() {
        @Override
        public void getCountAndPositionForRemove(int position) {
            mImagesVideoArrayList.remove(position);
            Log.e(TAG, "......................: ");
            setAdapter(mImagesVideoArrayList);
        }
    };

    BottomSheetDialog mBottomSheetDialog;

    boolean isUploadVideo = false;
    boolean isUploadImage = false;
    private ArrayList<String> arrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        Log.e(TAG, "onCreate: ");
        HomeActivity.bottombar.setVisibility(View.VISIBLE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        addItemPPView = inflater.inflate(R.layout.edit_additem, container, false);
        mResources = getActivity().getResources();
        mTypeface = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Light.ttf");

        mImagesVideoArrayList = AppSingleton.getInstance().getmImagesVideoArrayList();
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        setUpViews(addItemPPView);
        setUpClickListeners();


        strAdminArea = Constants_AddItem.AdminArea;
        strPostalcode = Constants_AddItem.AdminPostalCode;
        strLat = Constants_AddItem.AdminLat;
        strLong = Constants_AddItem.AdminLng;
        strCountry = Constants_AddItem.AdminCountry;
        strLocalty = Constants_AddItem.AdminLocatity;
        return addItemPPView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Utilities.hideProgressDialog();
        Log.e(TAG, "onStart: ");

        setDefaultTextnCheck();
        setDefaultWheelData();
        setWheelview();
        setWheelPicker();

        catArrayList = AppSingleton.getInstance().getCatArrayList();
        genreArrayList = AppSingleton.getInstance().getGenreArrayList();
        genreList = AppSingleton.getInstance().getGenreList();

        if (catArrayList.size() == 0) {
            getGenreService();
            getCatService();
        }
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        intMonth = month;
        strYear = String.valueOf(year);
        strDate = String.valueOf(day);
        strDateFinal = year + "-" + month + "-" + day;
        Log.e(TAG, "Date Calender: " + strDateFinal);

        wheelViewData.setData(Arrays.asList(mResources.getStringArray(R.array.array_sports)));

    }

    private void setDefaultTextnCheck() {
        String strShipping = "<u>" + "Shipping" + "</u>";
        radioShipping.setText(Html.fromHtml(strShipping));
        String strPersonalTransfer = "<u>" + "Personal transfer" + "</u>";
        radioPersonal.setText(Html.fromHtml(strPersonalTransfer));
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity.bottombar.setVisibility(View.VISIBLE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
        if (Constants.OpenEditAddItemFrom.equalsIgnoreCase("fragment_details")) {
            Log.e(TAG, "OnResume: " + Constants.OpenEditAddItemFrom);
            mImagesVideoArrayList.clear();
            getInstance();
            URL url = null;

            if (!Constants_AddItem.imagepath1.equalsIgnoreCase("")) {
                imageBitmapArray.add(Constants_AddItem.imagepath1);
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setStrImageVideoPath(Constants_AddItem.imagepath1);
                mImagesItemModel.setEditImageString(Constants_AddItem.imagepath1);
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrCameFrom("details");
                Log.e(TAG, "imagepath1: " + Constants_AddItem.imagepath1);
                mImagesVideoArrayList.add(mImagesItemModel);
            }
            if (!Constants_AddItem.imagepath2.equalsIgnoreCase("")) {
                imageBitmapArray.add(Constants_AddItem.imagepath2);
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setStrImageVideoPath(Constants_AddItem.imagepath2);
                mImagesItemModel.setEditImageString(Constants_AddItem.imagepath2);
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrCameFrom("details");
                Log.e(TAG, "imagepath2: " + Constants_AddItem.imagepath2);
                mImagesVideoArrayList.add(mImagesItemModel);
            }
            if (!Constants_AddItem.imagepath3.equalsIgnoreCase("")) {
                imageBitmapArray.add(Constants_AddItem.imagepath3);
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setStrImageVideoPath(Constants_AddItem.imagepath3);
                mImagesItemModel.setEditImageString(Constants_AddItem.imagepath3);
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrCameFrom("details");
                Log.e(TAG, "imagepath3: " + Constants_AddItem.imagepath3);
                mImagesVideoArrayList.add(mImagesItemModel);
            }
            if (!Constants_AddItem.imagepath4.equalsIgnoreCase("")) {
                imageBitmapArray.add(Constants_AddItem.imagepath4);
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setStrImageVideoPath(Constants_AddItem.imagepath4);
                mImagesItemModel.setEditImageString(Constants_AddItem.imagepath4);
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrCameFrom("details");
                Log.e(TAG, "imagepath4: " + Constants_AddItem.imagepath4);
                mImagesVideoArrayList.add(mImagesItemModel);
            }
            if (!Constants_AddItem.imagepath5.equalsIgnoreCase("")) {
                imageBitmapArray.add(Constants_AddItem.imagepath5);
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setStrImageVideoPath(Constants_AddItem.imagepath5);
                mImagesItemModel.setEditImageString(Constants_AddItem.imagepath5);
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrCameFrom("details");
                Log.e(TAG, "imagepath5: " + Constants_AddItem.imagepath5);
                mImagesVideoArrayList.add(mImagesItemModel);
            }
            if (!Constants_AddItem.imagepath6.equalsIgnoreCase("")) {
                imageBitmapArray.add(Constants_AddItem.imagepath6);
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setStrImageVideoPath(Constants_AddItem.imagepath6);
                mImagesItemModel.setEditImageString(Constants_AddItem.imagepath6);
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrCameFrom("details");
                Log.e(TAG, "imagepath6: " + Constants_AddItem.imagepath6);
                mImagesVideoArrayList.add(mImagesItemModel);
            }
            if (!Constants_AddItem.videopath.equalsIgnoreCase("")) {

                Log.e(TAG, "onResume: " + Constants_AddItem.videoimage);
                Log.e(TAG, "onResume: " + Constants_AddItem.videopath);
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setUriType(VIDEO_TYPE);
                mImagesItemModel.setStrCameFrom("details");
                mImagesItemModel.setStrVideoImage(Constants_AddItem.videoimage);
                mImagesItemModel.setStrImageVideoPath(Constants_AddItem.videopath);
                mImagesVideoArrayList.add(mImagesItemModel);
                isVideoSelected = true;
            }
            Constants.CallImageAdpaterFrom = "service";
            setAdapter(mImagesVideoArrayList);
        } else if (Constants.OpenEditAddItemFrom.equalsIgnoreCase("Charity")) {
            Log.e(TAG, "OnResume: " + Constants.OpenEditAddItemFrom);
            if (Constants_AddItem.ImageVideoArraylist != null) {
                mImagesVideoArrayList = Constants_AddItem.ImageVideoArraylist;
            }
            Constants.CallImageAdpaterFrom = "camera";
            getInstance();
            if (mImagesVideoArrayList.size() > 0) {
                setAdapter(mImagesVideoArrayList);
            }
        } else {
            Log.e(TAG, "OnResume: " + Constants.OpenEditAddItemFrom);
            if (Constants_AddItem.ImageVideoArraylist != null) {
                mImagesVideoArrayList = Constants_AddItem.ImageVideoArraylist;
            }
            Constants.CallImageAdpaterFrom = "camera";
            getInstance();
            if (mImagesVideoArrayList.size() > 0) {
                setAdapter(mImagesVideoArrayList);
            }
        }
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            Log.e(TAG, "getBitmapFromURL: " + src);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setAdapter(ArrayList<ImagesItemModel> mArrayList) {
        // i replace video count with isvideoSelected videocount also used for same purpose
        if (isVideoSelected) {
            txtItemCountTV.setText(mImagesVideoArrayList.size() + "/7");
        } else {
            txtItemCountTV.setText(mImagesVideoArrayList.size() + "/6");
        }
        AddImageListAdapter addImageListAdapter = new AddImageListAdapter(getActivity(), mArrayList, mGetItemCountAndRemoveItem);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImagesVideo.setLayoutManager(horizontalLayoutManager);
        recyclerViewImagesVideo.setAdapter(addImageListAdapter);
        addImageListAdapter.notifyDataSetChanged();
    }

    private void getInstance() {
        Log.e(TAG, "getInstance: " + Constants_AddItem.ITEM_DELVIERYTYPE);
        editItemNameET.setText(Constants_AddItem.ITEM_NAME);
        String price = Constants_AddItem.ITEM_PRICE.replace(",", "");
        editItemPriceET.setText(price);
        editItemWeightET.setText(Constants_AddItem.ITEM_WEIGHT);
        categoryTV.setText(Constants_AddItem.ITEM_CAT);
        editItemStoryET.setText(Constants_AddItem.ITEM_STORY);

        genreTV.setText(Constants_AddItem.ITEM_GENRE);
        genreValueTV.setText(Constants_AddItem.ITEM_GENREVALUE);
        editPersonalTrasET.setText(Constants_AddItem.ITEM_PERSONAL_TRANSFER);
        editLocationET.setText(Constants_AddItem.ITEM_LOCATION);
        editItemTagSepratedET.setText(Constants_AddItem.TAGS);
        itemLastUsedDATE.setText(Constants_AddItem.ITEM_LASTUSED);


        if (Constants_AddItem.ITEM_DELVIERYTYPE.equals("false")) {
            isPersonalTransfer = false;
            radioShipping.setChecked(true);
            editPersonalTrasET.setEnabled(false);
        }
        if (Constants_AddItem.ITEM_DELVIERYTYPE.equals("true")) {
            isPersonalTransfer = true;
            radioPersonal.setChecked(true);
            editPersonalTrasET.setEnabled(true);
        } else {
            isPersonalTransfer = false;
        }


        if (Constants_AddItem.ITEM_AUTO.equalsIgnoreCase("true")) {
            switchAutographed.setChecked(true);

        } else {
            switchAutographed.setChecked(false);
        }
        if (Constants_AddItem.ITEM_CHARITY.equalsIgnoreCase("true")) {
            switchForCharity.setChecked(true);
            editTypeCharityName.setText(Constants_AddItem.ITEM_CHARITYNAME);
        } else {
            switchForCharity.setChecked(false);
            editTypeCharityName.setEnabled(false);
        }
    }

    private void setUpViews(View v) {
        txtItemCountTV = v.findViewById(R.id.txtItemCountTV);
        recyclerViewImagesVideo = v.findViewById(R.id.recyclerViewImagesVideo);
        editItemNameET = v.findViewById(R.id.editItemNameET);
        editItemPriceET = v.findViewById(R.id.editItemPriceET);
        editItemWeightET = v.findViewById(R.id.editItemWeightET);
        editItemStoryET = v.findViewById(R.id.editItemStoryET);
        editItemTagSepratedET = v.findViewById(R.id.editItemTagSepratedET);
        editPersonalTrasET = v.findViewById(R.id.editPersonalTrasET);
        editLocationET = v.findViewById(R.id.editLocationET);
        editTypeCharityName = v.findViewById(R.id.editTypeCharityName);
        itemLastUsedDATE = v.findViewById(R.id.itemLastUsedDATE);
        txtConfirmTV = v.findViewById(R.id.txtConfirmTV);
        selectIMG = v.findViewById(R.id.selectIMG);

        switchAutographed = v.findViewById(R.id.switchAutographed);
        switchForCharity = v.findViewById(R.id.switchForCharity);
        rightRL = v.findViewById(R.id.rightRL);
        leftRL = v.findViewById(R.id.leftRL);

        radioPersonal = v.findViewById(R.id.radioPersonal);
        radioShipping = v.findViewById(R.id.radioShipping);
        firstCharityLL = v.findViewById(R.id.firstCharityLL);
        nextTV = v.findViewById(R.id.nextTV);
        shppingtypeLL = v.findViewById(R.id.shppingtypeLL);
        shippingTypeTV = v.findViewById(R.id.shippingTypeTV);

        deleteTV = v.findViewById(R.id.deleteTV);
        /*
         * bottomsheet ids
         */
        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.wheelview_layout, null);
        mBottomSheetDialog.setContentView(sheetView);


        bottomSheet = mBottomSheetDialog.findViewById(R.id.bottom_sheet);
        cancelTV = bottomSheet.findViewById(R.id.cancelTV);
        doneTV = bottomSheet.findViewById(R.id.doneTV);
        sheetTitleTV = bottomSheet.findViewById(R.id.sheetTitleTV);
        main_wheel_left = bottomSheet.findViewById(R.id.main_wheel_left);
        main_wheel_center = bottomSheet.findViewById(R.id.main_wheel_center);
        main_wheel_right = bottomSheet.findViewById(R.id.main_wheel_right);
        wheeldataLL = bottomSheet.findViewById(R.id.wheeldataLL);
        wheelpickerLL = bottomSheet.findViewById(R.id.wheelpickerLL);
        ll_date = bottomSheet.findViewById(R.id.ll_date);
        wheelViewData = mBottomSheetDialog.findViewById(R.id.wheelview);

        /*
         *  ids for categories
         */
        categoryTV = v.findViewById(R.id.categoryTV);
        genreTV = v.findViewById(R.id.genreTV);
        genreValueTV = v.findViewById(R.id.genreValueTV);
        categoryRL = v.findViewById(R.id.categoryRL);
        genreRl = v.findViewById(R.id.genreRl);
        genreValueRL = v.findViewById(R.id.genreValueRL);

        RoundingParams roundingParams = new RoundingParams();
        roundingParams.setCornersRadii(20, 20, 20, 20);
        roundingParams.setOverlayColor(Color.WHITE);
        selectIMG.getHierarchy().setPlaceholderImage(R.drawable.camera_additem);
        selectIMG.getHierarchy().setRoundingParams(roundingParams);
    }


    private void setUpClickListeners() {
        /*
        * Fetching All details from layout
        */
        strItemName = editItemNameET.getText().toString();
        strItemPrice = editItemPriceET.getText().toString();
        strItemPrice = strItemPrice.replace("$", "");
        strItemWeight = editItemWeightET.getText().toString();
        strItemWeight = strItemWeight.replace(" lbs", "");
        strCategory = categoryTV.getText().toString();
        strGenre = genreTV.getText().toString();
        strGenreValue = genreValueTV.getText().toString();
        strItemStory = editItemStoryET.getText().toString();
        strTags = editItemTagSepratedET.getText().toString();
        //  isAutograph
        //  isForCharity
        //  isPersonalTransfer
        strPersonalTransfer = editPersonalTrasET.getText().toString();
        strLocation = editLocationET.getText().toString();
        strCharityName = editTypeCharityName.getText().toString();
        strResultDATE = itemLastUsedDATE.getText().toString();

        if (radioPersonal.isChecked()) {
            isPersonalTransfer = false;
        } else {
            isPersonalTransfer = true;
        }

        editItemStoryET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editItemStoryET) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        editItemWeightET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {

                    if (!editItemWeightET.getText().toString().equalsIgnoreCase("")){
                        Float weight = Float.parseFloat(editItemWeightET.getText().toString());
                        if (weight > 0 && weight < 50) {
                            radioShipping.setChecked(true);
                            radioPersonal.setChecked(false);
                            Constants_AddItem.ITEM_DELVIERYTYPE = "true";
                            isPersonalTransfer = false;
                            editPersonalTrasET.setEnabled(false);
                        } else {
                            radioPersonal.setChecked(true);
                            radioShipping.setChecked(false);
                            Constants_AddItem.ITEM_DELVIERYTYPE = "false";
                            isPersonalTransfer = true;
                            editPersonalTrasET.setEnabled(true);
                        }
                    }
                }
            }
        });

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        selectIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mImagesVideoArrayList.size() < 7) {
                    checkAndroidPermissions();
                }
            }
        });
        editItemPriceET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String itemprize = editItemPriceET.getText().toString();
                    if (itemprize.length() > 0) {
                        if (itemprize.equalsIgnoreCase("$")) {
                            editItemPriceET.setText("");
                            intPrice = 0;
                        }
                        if (!itemprize.contains("$")) {
                            editItemPriceET.setText("$" + itemprize);
                            intPrice = Integer.parseInt(itemprize);
                        }
                    }
                    // code to execute when EditText loses focus
                }
            }
        });

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.hide();
            }
        });

        doneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.hide();
                Log.e(TAG, "Value from Wheel: " + wheelViewData.getData());
                int itemPostion = wheelViewData.getCurrentItemPosition();

                if (setValueFor == 0) {
                    if (strResultDATA.equalsIgnoreCase("")) {
                        strResultDATA = wheelViewData.getData().get(itemPostion).toString();
                    }
                    categoryTV.setText(strResultDATA);
                    strResultDATA = "";
                } else if (setValueFor == 1) {
                    if (strResultDATA.equalsIgnoreCase("")) {
                        strResultDATA = wheelViewData.getData().get(itemPostion).toString();
                    }
                    genreTV.setText(strResultDATA);
                    firstGenreItem = strResultDATA;
                    strResultDATA = "";
                } else if (setValueFor == 2) {

                    if (strResultDATA.equalsIgnoreCase("") || strResultDATA.equalsIgnoreCase(firstGenreItem)) {
                        strResultDATA = wheelViewData.getData().get(itemPostion).toString();
                        Log.e(TAG, "onClick: " + strResultDATA);
                        genreValueTV.setText(strResultDATA);
                    } else {
                        genreValueTV.setText(strResultDATA);
                    }
                    strResultDATA = "";
                }

                if (setValueFor == 3) {
                    strResultDATE = strDate + strMonth + strYear;
                    /*
                     * if wheel view is not moved
                     */
                    if (strResultDATE.length() == 0) {
                        strResultDATE = main_wheel_left.getSelectedDay() + " " + main_wheel_center.getSelectedMonth() + ", " + main_wheel_right.getSelectedYear();
                    }
                    /*
                     * if wheel view is moved
                     */
                    else {
                        if (strDate.length() == 0) {
                            strDate = String.valueOf(main_wheel_left.getSelectedDay());
                        }
                        if (intMonth == 0) {
                            intMonth = main_wheel_center.getSelectedMonth();
                        }
                        if (strYear.length() == 0) {
                            strYear = String.valueOf(main_wheel_right.getSelectedYear());
                        }
                        /*
                         * to change month intvalue to month name
                         */
                        Log.e(TAG, "checkMonth: " + intMonth);
                        strMonth = monthMap.get(intMonth - 1);
                        Log.e(TAG, "checkMonth: " + strMonth);
                        /*
                         * to make 1 to 01;
                         */
                        if (strDate.length() == 1) {
                            strDate = "0" + strDate;
                        }
                        strDateFinal = strYear + "-" + intMonth + "-" + strDate;
                        strResultDATE = strMonth + " " + strDate + ", " + strYear;
                    }
                    itemLastUsedDATE.setText(strResultDATE);
                }

                if (!categoryTV.getText().equals("")) {
                    categoryTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
                if (!genreTV.getText().equals("")) {
                    genreTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
                if (!genreValueTV.getText().equals("")) {
                    genreValueTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
                if (!itemLastUsedDATE.getText().equals("")) {
                    itemLastUsedDATE.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
            }
        });
        categoryRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrayListWheelData = catArrayList;
                if (arrayListWheelData.size() > 0) {
                    setValueFor = 0;
                    wheelViewData.setData(arrayListWheelData);
                    wheeldataLL.setVisibility(View.VISIBLE);
                    wheelpickerLL.setVisibility(View.GONE);
                    ll_date.setVisibility(View.GONE);
                    sheetTitleTV.setText(getActivity().getResources().getString(R.string.selectcategory));
                    mBottomSheetDialog.show();
                }

            }

        });

        genreRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    arrayListWheelData = genreList;
                    if (arrayListWheelData.size() > 0) {
                        setValueFor = 1;
                        wheelViewData.setData(arrayListWheelData);
                        wheeldataLL.setVisibility(View.VISIBLE);
                        wheelpickerLL.setVisibility(View.GONE);
                        ll_date.setVisibility(View.GONE);
                        mBottomSheetDialog.show();
                        sheetTitleTV.setText(getActivity().getResources().getString(R.string.chooseGenre));
                        genreValueTV.setText("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        genreValueRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    arrayListWheelData = new ArrayList<>();
                    String strGenre = genreTV.getText().toString();
                    if (!strGenre.equalsIgnoreCase("")) {
                        for (int k = 0; k < genreArrayList.size(); k++) {
                            if (strGenre.equalsIgnoreCase(genreArrayList.get(k).getGenreName())) {
                                for (int i = 0; i < genreArrayList.get(k).getGenreValueList().size(); i++) {
                                    arrayListWheelData.add(genreArrayList.get(k).getGenreValueList().get(i).getValueName());
                                }
                            }
                        }
                        if (arrayListWheelData.size() > 0) {
                            setValueFor = 2;
                            wheelViewData.setData(arrayListWheelData);
                            wheelViewData.setSelected(true);
                            wheeldataLL.setVisibility(View.VISIBLE);
                            wheelpickerLL.setVisibility(View.GONE);
                            ll_date.setVisibility(View.GONE);
                            sheetTitleTV.setText(getActivity().getResources().getString(R.string.chooseGenreValue));
                            mBottomSheetDialog.show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        itemLastUsedDATE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setValueFor = 3;
                wheeldataLL.setVisibility(View.GONE);
                wheelpickerLL.setVisibility(View.VISIBLE);
                ll_date.setVisibility(View.VISIBLE);
                sheetTitleTV.setText("Select Date");
                mBottomSheetDialog.show();
            }
        });

        editLocationET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
                    saveInstance();
                    startActivityForResult(intent, GOOGLE_PLACE_API);
                } catch (GooglePlayServicesRepairableException e) {
                    // Indicates that Google Play Services is either not installed or not up to date. Prompt
                    // the user to correct the issue.
                    GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), e.getConnectionStatusCode(),
                            0 /* requestCode */).show();
                } catch (GooglePlayServicesNotAvailableException e) {
                    // Indicates that Google Play Services is not available and the problem is not easily
                    // resolvable.
                    String message = "Google Play Services is not available: " +
                            GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

                    Log.e(TAG, message);
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }
            }

        });


        switchForCharity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isForCharity = true;
                    editTypeCharityName.setEnabled(true);
                } else {
                    isForCharity = false;
                    editTypeCharityName.setEnabled(false);
                }
            }
        });
        radioPersonal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    editPersonalTrasET.setEnabled(true);
                }
            }
        });
        radioShipping.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    editPersonalTrasET.setEnabled(false);
                }
            }
        });


        editTypeCharityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants_AddItem.ImageVideoArraylist = mImagesVideoArrayList;
                HomeActivity.bottombar.setVisibility(View.GONE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);

                saveInstance();

                HomeActivity.switchFragment(getActivity(), new CharityOptions(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, true, null);
            }
        });

        txtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageJson = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                JSONObject locationJson = new JSONObject();
                JSONArray tagJson = new JSONArray();

                String news = editItemTagSepratedET.getText().toString();

                String[] words = news.split(",");//splits the string based on whitespace
                for (String w : words) {
                    tagArraylist.add(w);
                    tagJson.put(w);
                }


                for (int i = 0; i < mImagesVideoArrayList.size(); i++) {
                    ImagesItemModel imagesItemModel = new ImagesItemModel();
                    if (mImagesVideoArrayList.get(i).getStrCameFrom().equalsIgnoreCase("details")) {
                        imagesItemModel.setStrImageVideoPath(mImagesVideoArrayList.get(i).getStrImageVideoPath());
                        strImageUrlList.add(imagesItemModel);
                    } else if (mImagesVideoArrayList.get(i).getStrCameFrom().equalsIgnoreCase("camera")) {
                        imagesItemModel.setStrUrlByteArray(mImagesVideoArrayList.get(i).getStrUrlByteArray());
                        imagesItemModel.setStrImageVideoPath("none");
                        strImageUrlList.add(imagesItemModel);

                    }

                    Log.e(TAG, "mImagesVideoArrayList: " + mImagesVideoArrayList.get(i).getStrImageVideoPath());
                    Log.e(TAG, "mImagesVideoArrayList: " + mImagesVideoArrayList.get(i).getStrUrlByteArray());

                }


                strItemName = editItemNameET.getText().toString();
                strItemPrice = editItemPriceET.getText().toString();
                strItemPrice = strItemPrice.replace("$", "");
                intPrice = Integer.parseInt(strItemPrice);

                strItemWeight = editItemWeightET.getText().toString();
                strItemWeight = strItemWeight.replace("lbs", "");

                strCategory = categoryTV.getText().toString();
                strGenre = genreTV.getText().toString();
                strGenreValue = genreValueTV.getText().toString();
                strItemStory = editItemStoryET.getText().toString();
                strTags = editItemTagSepratedET.getText().toString();
                strCharityName = editTypeCharityName.getText().toString();
                strLocation = editLocationET.getText().toString();
                strResultDATE = itemLastUsedDATE.getText().toString();

                if (radioPersonal.isChecked()) {
                    isPersonalTransfer = true;
                } else {
                    isPersonalTransfer = false;
                }


                if (switchForCharity.isChecked()) {
                    isForCharity = true;
                } else {
                    isForCharity = false;
                }
                if (switchAutographed.isChecked()) {
                    isAutograph = true;
                } else {
                    isAutograph = false;
                }

                Log.e(TAG, "intMonth: " + intMonth);
                int monthsize = String.valueOf(intMonth).length();
                int strDatesize = String.valueOf(strDate).length();

                Log.e(TAG, "monthsize: " + monthsize);
                Log.e(TAG, "strDatesize: " + strDatesize);
                if (monthsize == 1)
                    strDateFinal = strYear + "-" + "0" + intMonth + "-" + strDate;
                else
                    strDateFinal = strYear + "-" + intMonth + "-" + strDate;
                if (strDatesize == 1) {
                    strDateFinal = strYear + "-" + intMonth + "-" + "0" + strDate;
                }
                if (monthsize == 1 && strDatesize == 1)
                    strDateFinal = strYear + "-0" + intMonth + "-" + "0" + strDate;
                Log.e(TAG, "Date strDateFinal: " + strDateFinal);


                try {
                    jsonObject.put("item_name", strItemName);
                    jsonObject.put("price", intPrice);
                    jsonObject.put("weight", strItemWeight);
                    jsonObject.put("item_story", strItemStory);
                    jsonObject.put("category", strCategory);
                    jsonObject.put("genre", strGenre);
                    jsonObject.put("sport", strGenre);
                    jsonObject.put("genre_value", strGenreValue);
                    jsonObject.put("autographed", isAutograph);
                    jsonObject.put("charity_name", strCharityName);
                    jsonObject.put("charity", isForCharity);
                    jsonObject.put("currency", "USD");
                    jsonObject.put("last_used", strDateFinal + "T13:37:48.338+0530");

                    jsonObject.put("delivery_type", isPersonalTransfer);
                    jsonObject.put("person_transfertext", editPersonalTrasET.getText().toString());
                    jsonObject.put("tags", tagJson);

                    locationJson.put("administrative_area_level_1", strAdminArea);
                    locationJson.put("country", strCountry);
                    locationJson.put("lat", strLat);
                    locationJson.put("lng", strLong);
                    locationJson.put("locality", strLocalty);
                    locationJson.put("postal_code", strPostalcode);
                    jsonObject.put("location", locationJson);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < mImagesVideoArrayList.size(); i++) {
                    if (!mImagesVideoArrayList.get(i).getUriType().equalsIgnoreCase("video_type")) {
                        if (mImagesVideoArrayList.get(i).getStrUrlByteArray() != null) {
                            image1 = mImagesVideoArrayList.get(i).getStrUrlByteArray();
                        }
                    }
                    if (mImagesVideoArrayList.get(i).getUriType().equalsIgnoreCase("video_type")) {
                        videoImage = mImagesVideoArrayList.get(i).getStrUrlByteArray();
                    }
                }
                arrayList = new ArrayList<>();
                for (int j = 0; j < mImagesVideoArrayList.size(); j++) {
                    try {
                        String image = "";
                        if (!mImagesVideoArrayList.get(j).getEditImageString().equalsIgnoreCase("")) {
                            arrayList.add(mImagesVideoArrayList.get(j).getEditImageString());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                for (int j = 0; j < arrayList.size(); j++) {
                    String image = "";
                    image = arrayList.get(j);
                    try {
                        if (j == 0) {
                            imageJson.put("image1", image);
                            imageJson.put("image2", "");
                            imageJson.put("image3", "");
                            imageJson.put("image4", "");
                            imageJson.put("image5", "");
                            imageJson.put("image6", "");

                        } else if (j == 1) {
                            imageJson.put("image2", image);
                            imageJson.put("image3", "");
                            imageJson.put("image4", "");
                            imageJson.put("image5", "");
                            imageJson.put("image6", "");
                        }
                        if (j == 2) {
                            imageJson.put("image3", image);
                            imageJson.put("image4", "");
                            imageJson.put("image5", "");
                            imageJson.put("image6", "");
                        }
                        if (j == 3) {
                            imageJson.put("image4", image);
                            imageJson.put("image5", "");
                            imageJson.put("image6", "");
                        }
                        if (j == 4) {
                            imageJson.put("image5", image);
                            imageJson.put("image6", "");
                        }
                        if (j == 5) {
                            imageJson.put("image6", image);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Log.e(TAG, "imageJson: " + imageJson);
                Log.e(TAG, "imageJson: " + jsonObject);
                Log.e(TAG, "imageJson size: " + mImagesVideoArrayList.size());
                for (int j = 0; j < mImagesVideoArrayList.size(); j++) {
                    if (mImagesVideoArrayList.get(j).getUriType().equalsIgnoreCase("video_type")) {
                        isUploadVideo = true;
                    }
                    if (mImagesVideoArrayList.get(j).getUriType().equalsIgnoreCase("image_type")) {
                        isUploadImage = true;
                    }
                }

                if (validate(strItemName, strItemPrice, strItemWeight, strItemStory, strCategory, strGenre, strGenreValue, mImagesVideoArrayList.size(), isPersonalTransfer)) {
                    updateItem(jsonObject);
                }

            }
        });

        deleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmationDialog(getActivity(), "Membilia", "You are about to delete this offering.");
            }
        });
    }


    private boolean validate(String strItemName, String strItemPrice, String strItemWeight, String strItemStory, String strCategory, String strGenre, String strGenreValue, int imageArraySize, boolean isPersonalTransfer) {

        Double weigthint  = Double.valueOf(strItemWeight);
        if (strItemName.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Name");
            return false;
        } else if (strItemPrice.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Price");
            return false;
        } else if (strItemWeight.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Weight");
            return false;
        } else if (strItemStory.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Story");
            return false;
        } else if (strCategory.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Item Category");
            return false;
        } else if (strGenre.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Item Genre");
            return false;
        } else if (strGenreValue.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Item GenreValue");
            return false;
        } else if (imageArraySize == 0) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Atlest 1 Image or Video");
            return false;
        } else if (isPersonalTransfer) {
            String personalTransfer = editPersonalTrasET.getText().toString();
            if (personalTransfer.equalsIgnoreCase("")) {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please enter personal transfer message");
                return false;
            }
        }else if (!isPersonalTransfer){
            if (editLocationET.getText().toString().equalsIgnoreCase("")) {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please enter location");
                return false;
            }
            if (weigthint >= 50){
                showtranferDialog(getActivity(), "Membilia", "Given the weight of your item we strongly suggest you post this item as an in personal tranfer so your fan can meet you or your agent for pick up!" +
                        "Pawn away.");
                return false;
            }
        }
        if (switchForCharity.isChecked()) {
            Log.e(TAG, "validate:switchForCharity :true ");
            if (editTypeCharityName.getText().toString().equalsIgnoreCase("")) {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please enter Charity Name");
                return false;
            }else{
                Log.e(TAG, "validate:switchForCharity :true|false ");
            }
        } else {
            Log.e(TAG, "validate:switchForCharity false ");
        }
        return true;
    }

    public void showtranferDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_trasfer);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        TextView txttransfer = alertDialog.findViewById(R.id.txttransfer);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txttransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                radioPersonal.setChecked(true);
                radioShipping.setChecked(false);
                Constants_AddItem.ITEM_DELVIERYTYPE = "false";
                isPersonalTransfer = true;
                editPersonalTrasET.setEnabled(true);
            }
        });
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    public void showConfirmationDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.logout_dialog);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button

        TextView messageTV = alertDialog.findViewById(R.id.messageTV);
        TextView yes = alertDialog.findViewById(R.id.yes);
        TextView no = alertDialog.findViewById(R.id.no);

        LinearLayout yesll = alertDialog.findViewById(R.id.yesll);

        yes.setText("Cancel");
        no.setText("Ok");


        messageTV.setText(strMessage);

        yesll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                deleteItemRequest();
            }
        });

        alertDialog.show();
    }


    private void saveInstance() {

        if (isForCharity) {
            Constants_AddItem.ITEM_CHARITY = "true";
        } else {
            Constants_AddItem.ITEM_CHARITY = "false";
        }
        Constants_AddItem.ITEM_NAME = editItemNameET.getText().toString();
        Constants_AddItem.ITEM_PRICE = editItemPriceET.getText().toString();
        Constants_AddItem.ITEM_WEIGHT = editItemWeightET.getText().toString();
        Constants_AddItem.ITEM_STORY = editItemStoryET.getText().toString();
        Constants_AddItem.ITEM_CAT = categoryTV.getText().toString();
        Constants_AddItem.ITEM_GENRE = genreTV.getText().toString();
        Constants_AddItem.ITEM_GENREVALUE = genreValueTV.getText().toString();
        Constants_AddItem.ITEM_PERSONAL_TRANSFER = editPersonalTrasET.getText().toString();
        Constants_AddItem.ITEM_LOCATION = editLocationET.getText().toString();
        Constants_AddItem.ITEM_LASTUSED = itemLastUsedDATE.getText().toString();
        Log.e(TAG, "Constants_AddItem.ITEM_LOCATION: " + Constants_AddItem.ITEM_LOCATION);
        if (radioPersonal.isChecked()) {
            Constants_AddItem.ITEM_DELVIERYTYPE = "true";
        } else {
            Constants_AddItem.ITEM_DELVIERYTYPE = "false";
        }
        if (switchAutographed.isChecked()) {
            Constants_AddItem.ITEM_AUTO = "true";
        } else {
            Constants_AddItem.ITEM_AUTO = "false";
        }

        if (switchForCharity.isChecked()) {
            Constants_AddItem.ITEM_CHARITY = "true";
        } else {
            Constants_AddItem.ITEM_CHARITY = "false";
        }
        Constants_AddItem.TAGS = editItemTagSepratedET.getText().toString();
        Log.e(TAG, "saveInstance: " + Constants_AddItem.ITEM_DELVIERYTYPE);
    }

    private void setDefaultWheelData() {
        monthArraylist = Arrays.asList(getResources().getStringArray(R.array.array_months));
        for (int i = 0; i < monthArraylist.size(); i++) {
            monthMap.put(i, monthArraylist.get(i));
        }
    }

    /**
     * setting Wheelview
     */
    private void setWheelview() {

        main_wheel_right.setAtmospheric(true);
        main_wheel_right.setCyclic(true);
        main_wheel_right.setCurved(true);
        main_wheel_right.setIndicatorSize(10);
        main_wheel_right.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        main_wheel_right.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        main_wheel_left.setAtmospheric(true);
        main_wheel_left.setCyclic(true);
        main_wheel_left.setCurved(true);
        main_wheel_left.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        main_wheel_left.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        main_wheel_center.setAtmospheric(true);
        main_wheel_center.setCyclic(true);
        main_wheel_center.setCurved(true);
        main_wheel_center.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        main_wheel_center.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        wheelViewData.setAtmospheric(true);
        wheelViewData.setCyclic(true);
        wheelViewData.setCurved(true);
        wheelViewData.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        wheelViewData.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        main_wheel_center.setIndicator(true);
        main_wheel_center.setIndicatorSize(3);

        main_wheel_left.setIndicator(true);
        main_wheel_left.setIndicatorSize(3);

        main_wheel_right.setIndicator(true);
        main_wheel_right.setIndicatorSize(3);

        wheelViewData.setIndicator(true);
        wheelViewData.setIndicatorSize(3);
    }

    private void setWheelPicker() {
        main_wheel_left.getSelectedDay();


        main_wheel_left.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strDate = data.toString();
                Log.e(TAG, "date: " + strDate);
            }
        });

        main_wheel_center.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                intMonth = Integer.parseInt(data.toString());
                Log.e(TAG, "month: " + intMonth);
            }
        });

        main_wheel_right.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strYear = data.toString();
                Log.e(TAG, "year: " + strYear);
            }
        });

        wheelViewData.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strResultDATA = data.toString();
            }
        });

    }

    private void checkAndroidPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermissions()) {
                showCameraGalleryDialog();
            } else {
                requestPermission();
            }
        } else {
            showCameraGalleryDialog();
        }
    }

    private boolean checkPermissions() {
        int camera = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), CameraPermission);
        int read = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), ReadStorage);
        int write = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), WriteStorage);

        return camera == PackageManager.PERMISSION_GRANTED &&
                read == PackageManager.PERMISSION_GRANTED &&
                write == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CameraPermission, ReadStorage,
                WriteStorage}, 100);
    }


    private void showCameraGalleryDialog() {
        final Dialog cameraGalleryDialog = new Dialog(getActivity());
        cameraGalleryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraGalleryDialog.setContentView(R.layout.dialog_camera_video);
        cameraGalleryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        LinearLayout createVideoTV = cameraGalleryDialog.findViewById(R.id.createVideoLL);
        LinearLayout chooseVideoTV = cameraGalleryDialog.findViewById(R.id.chooseVideoLL);
        LinearLayout cameraTV = cameraGalleryDialog.findViewById(R.id.cameraLL);
        LinearLayout photoTV = cameraGalleryDialog.findViewById(R.id.photoLL);

        TextView cancelTV = cameraGalleryDialog.findViewById(R.id.cancelTV);

        if (isVideoSelected) {
            createVideoTV.setVisibility(View.GONE);
            chooseVideoTV.setVisibility(View.GONE);
        } else {
            createVideoTV.setVisibility(View.VISIBLE);
            chooseVideoTV.setVisibility(View.VISIBLE);
        }

        if (!isVideoSelected && mImagesVideoArrayList.size() > 6) {
            cameraTV.setVisibility(View.VISIBLE);
            photoTV.setVisibility(View.VISIBLE);
        } else if (!isVideoSelected && mImagesVideoArrayList.size() == 6) {
            cameraTV.setVisibility(View.GONE);
            photoTV.setVisibility(View.GONE);
        }
        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
            }
        });
        cameraTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Open Camera********/
                cameraGalleryDialog.dismiss();
                openCamera();

            }
        });
        photoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Open Gallery********/
                cameraGalleryDialog.dismiss();
                openGallery();
            }
        });
        createVideoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                 * Open Video Camera********/
                cameraGalleryDialog.dismiss();
                openVideoCamera();

            }
        });
        chooseVideoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                 * Open Video Gallery********/
                cameraGalleryDialog.dismiss();
                openVideoGallery();

            }
        });
        cameraGalleryDialog.show();
    }

    private void openCamera() {
        Constants.OpenEditAddItemFrom = "camera";
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) == null) {
            return;
        }
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    public void openGallery() {
        Constants.OpenEditAddItemFrom = "camera";
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void openVideoCamera() {
        Constants.OpenEditAddItemFrom = "camera";
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Environment.getExternalStorageDirectory().getPath() + "myvideo.mp4");
        startActivityForResult(intent, VIDEO_REQUEST);
    }

    private void openVideoGallery() {
        Constants.OpenEditAddItemFrom = "camera";
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, VIDEO_GALLERY_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == GOOGLE_PLACE_API && resultCode == RESULT_OK ) {
            Geocoder geocoder;
            List<Address> addresses = null;
            Log.e(TAG, "onActivityResult: " + GOOGLE_PLACE_API);
            Place place = PlaceAutocomplete.getPlace(getActivity(), intent);
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();


                strLat = String.valueOf(place.getLatLng().latitude);
                strLong = String.valueOf(place.getLatLng().longitude);

                strCountry = country;
                if (postalCode != null) {
                    strPostalcode = postalCode;
                }
                strAdminArea = address;
                strLocalty = city;
                editLocationET.setText(place.getAddress());
                Constants_AddItem.ITEM_LOCATION = String.valueOf(place.getAddress());
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.e(TAG, "onActivityResult: " + place.getAddress());
        } else if (requestCode == VIDEO_GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri videoUri = intent.getData();
            getdatapicture(videoUri);
        } else if (requestCode == VIDEO_REQUEST && resultCode == RESULT_OK) {
            Uri videoUri = intent.getData();
            getdataVideoCamera(videoUri);
        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap BitmapImages = (Bitmap) intent.getExtras().get("data");
             tempUria = CameraUtils.getImageUri(BitmapImages, getActivity());

            Log.e(TAG, "cameraURi: " + tempUria);
            setImage(BitmapImages);

        } else if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            InputStream stream = null;
            try {
                Bitmap bitmap = null;
                stream = getActivity().getContentResolver().openInputStream(intent.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                try {
                    bitmap = getCorrectlyOrientedImage(getActivity(), intent.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String strEncodedImage64 = encodeImage(bitmap);

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = CameraUtils.getImageUri(bitmap, getActivity());
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrImageVideoPath(String.valueOf(tempUri));
                mImagesItemModel.setStrVideoThumbnail(bitmap);
                mImagesItemModel.setEditImageString(strEncodedImage64);
                mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), bitmap));
                mImagesVideoArrayList.add(mImagesItemModel);
                setAdapter(mImagesVideoArrayList);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode,resultCode,intent);
    }

    private void setImage(Bitmap a) {
        try {
            Bitmap bitmap;
            bitmap = getCorrectlyOrientedImage(getActivity(), tempUria);
            String strEncodedImage64 = encodeImage(bitmap);
            Uri tempUri = CameraUtils.getImageUri(bitmap, getActivity());
            ImagesItemModel mImagesItemModel = new ImagesItemModel();
            mImagesItemModel.setUriType(IMAGE_TYPE);
            mImagesItemModel.setStrImageVideoPath(String.valueOf(tempUri));
            mImagesItemModel.setStrVideoThumbnail(bitmap);
            mImagesItemModel.setEditImageString(strEncodedImage64);
            mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), bitmap));
            mImagesVideoArrayList.add(mImagesItemModel);

            setAdapter(mImagesVideoArrayList);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public  Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();
        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);
        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }
        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 400 || rotatedHeight > 400) {
            float widthRatio = ((float) rotatedWidth) / ((float) 400);
            float heightRatio = ((float) rotatedHeight) / ((float) 400);
            float maxRatio = Math.max(widthRatio, heightRatio);
            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
        }
        return srcBitmap;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri, new String[]{
                MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }
    private void getdataVideoCamera(Uri videoUri) {
        isVideoSelected = true;
        InputStream iStream = null;
        byte[] inputCompressData = null;
        Log.i("", "" + videoUri);
        //To Show The Video Thumbnail......
        String filePath = CameraUtils.getThumbnailPathForLocalFile(getActivity(), videoUri);
        Bitmap resized = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(filePath), 400, 400, true);
        File file = new File(filePath);
        File file1 = new File(videoUri.getPath());
        file1.delete();
        if (file1.exists()) {
            try {
                file1.getCanonicalFile().delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file1.exists()) {
                getActivity().getApplicationContext().deleteFile(file1.getName());
            }
        }
        /*Convert URI to Byte Array**/
        try {
            iStream = getApplicationContext().getContentResolver().openInputStream(videoUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            byte[] inputData = CameraUtils.getBytes(iStream);
            VideoData = inputData;
            Constants_AddItem.VideoDAta = VideoData;
            inputCompressData = CameraUtils.compressArray(inputData);

        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "checkVideoPiture: " + resized);
        ImagesItemModel mImagesItemModel = new ImagesItemModel();
        mImagesItemModel.setStrImageVideoPath(filePath);
        mImagesItemModel.setUriType(VIDEO_TYPE);
        mImagesItemModel.setStrVideoThumbnail(resized);
        mImagesItemModel.setStrVideobyteArray(VideoData);
        mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), resized));
        mImagesVideoArrayList.add(mImagesItemModel);
        setAdapter(mImagesVideoArrayList);
    }

    /**
     * method is used to create tumbnail from video
     *
     * @param videoUri
     */
    private void getdatapicture(Uri videoUri) {
        isVideoSelected = true;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        Bitmap mBitmap = null;
        try {
            String scheme = videoUri.getScheme();
            Log.i("uri", "" + videoUri + " " + scheme);
            retriever.setDataSource(getActivity(), videoUri);
            mBitmap = retriever.getFrameAtTime(-1, MediaMetadataRetriever.OPTION_CLOSEST);
            Log.d("bitmappp", " " + mBitmap.getWidth() + " " + mBitmap.getHeight());
        } catch (IllegalArgumentException ex) {
            // Assume this is a corrupt video file
            Log.d("IllegalArgumentExcpt", "" + ex.getMessage());
        } catch (RuntimeException ex) {
            // Assume this is a corrupt video file.
            Log.d("RuntimeException", "" + ex.getMessage());
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
                Log.d("RuntimeException", "" + ex.getMessage());
            }
        }
        String strVideoPath = "";
        strVideoPath = CameraUtils.getRealVideoPathFromURI(videoUri);
        InputStream iStream = null;
        try {
            iStream = getApplicationContext().getContentResolver().openInputStream(videoUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            byte[] inputData = AppHelper.getBytes(iStream);
            VideoData = inputData;
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*Setting Data on Adapter*/
        Log.e(TAG, "checkVideoPiture: " + mBitmap);
        ImagesItemModel mImagesItemModel = new ImagesItemModel();
        mImagesItemModel.setStrVideoThumbnail(mBitmap);
        mImagesItemModel.setStrImageVideoPath(strVideoPath);
        mImagesItemModel.setUriType(VIDEO_TYPE);
        mImagesItemModel.setStrVideobyteArray(VideoData);
        mImagesItemModel.setStrCameFrom("camera");
        mImagesItemModel.setStrVideoImage(strVideoPath);
        mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), mBitmap));
        Constants.CallImageAdpaterFrom = "camera";
        mImagesVideoArrayList.add(mImagesItemModel);
        setAdapter(mImagesVideoArrayList);

    }

    private String getImagePathFromBitmap(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return path;
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    /**
     * service for cat and genre
     */

    private void getGenreService() {
        String url = WebServicesConstants.GET_GENRE_VALUE;
        String strConstant = Constants.GET_GENRE_VALUE;

        serviceArea(url, strConstant);
    }

    private void getCatService() {
        String url = WebServicesConstants.GET_CATEGORIES;
        String strConstant = Constants.GET_CATEGORIES;

        serviceArea(url, strConstant);
    }

    public void serviceArea(String url, final String strConstant) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****RESPONSE****" + response);
                if (strConstant.equalsIgnoreCase(Constants.GET_CATEGORIES)) {
                    parsejson_categories(response);
                } else {
                    parsejson_genre(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        // Adding request to request queue
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, strConstant);
    }

    private void parsejson_genre(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                genreValueList = new ArrayList<>();
                GenreModel genreModel = new GenreModel();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                genreModel.setGenreName(jsonObject.getString("name"));

                JSONArray jsonArrayList = jsonObject.getJSONArray("list");

                for (int j = 0; j < jsonArrayList.length(); j++) {
                    JSONObject jsonObjectList = jsonArrayList.getJSONObject(j);
                    GenreValue genreValue = new GenreValue();
                    genreValue.setCategoryID(jsonObjectList.getString("category_id"));
                    genreValue.setDescription(jsonObjectList.getString("description"));
                    genreValue.setValueName(jsonObjectList.getString("name"));
                    genreValueList.add(genreValue);
                    genreModel.setGenreValueList(genreValueList);
                }
                genreArrayList.add(genreModel);
                genreList.add(genreArrayList.get(i).getGenreName());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parsejson_categories(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                catArrayList.add(jsonObject.getString("name"));
            }
//            Log.e(TAG, "catArrayList: "+catArrayList );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateItem(final JSONObject jsonObject) {
        String strUrl = WebServicesConstants.UPDATEITEM + Constants_AddItem.ITEM_ID;
        Log.e(TAG, "strUrl: " + strUrl);
        Utilities.showProgressDialog(getActivity());
        StringRequest jsonArrayRequest = new StringRequest(Request.Method.PUT, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "onResponse: " + response);
                String totalimages = txtItemCountTV.getText().toString();
                try {
                    if (isUploadImage) {
                        executeUploadImageAPI();
                    } else if (!Constants_AddItem.videopath.equalsIgnoreCase("") && totalimages.contains("/6")) {
                        deleteVideoRequest();
                    } else if (isUploadImage) {
                        executeUploadVideoAPI();
                    }else {
                        Utilities.hideProgressDialog();
                        Constants.additemFragment = "1";
                        showAlertDialog(getActivity(), "Membilia", "Your item has been edited. it may take several minutes to appear in the market,so please do not close the app.");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                String json;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "No more data");
                            break;
                    }
                }
            }
        }) {
            //            Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, WebServicesConstants.ADDITEM);
    }

    public void executeUploadImageAPI() {
        String url = WebServicesConstants.EditMultiImages + Constants_AddItem.ITEM_ID;
        Log.e(TAG, "MultipleImages: URL " + url);
        Log.e(TAG, "MultipleImages: JSON " + imageJson);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "UploadImage Response: " + response);
                String totalimages = txtItemCountTV.getText().toString();
                if (!Constants_AddItem.videopath.equals("") && videoImage != null) {
                    deleteVideoRequest();
                } else if (!Constants_AddItem.videopath.equalsIgnoreCase("") && totalimages.contains("/6")) {
                    deleteVideoRequest();
                } else if (Constants_AddItem.videopath.equals("") && videoImage != null) {
                    executeUploadVideoAPI();
                } else {
                    Utilities.hideProgressDialog();
                    Constants.additemFragment = "1";
                    showAlertDialog(getActivity(), "Membilia", "Your item has been edited. it may take several minutes to appear in the market,so please do not close the app.");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return imageJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "UploadImageRequest");
    }

    private void executeUploadVideoAPI() {

        String url = WebServicesConstants.VideoUpload;
        Log.e(TAG, "url for videoUpload: " + url);
        final String itemid = Constants_AddItem.ITEM_ID;
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                Log.e(TAG, "UploadVideo onResponse: " + response.data);

                Utilities.hideProgressDialog();
                Constants.additemFragment = "1";
                showAlertDialog(getActivity(), "Membilia", "Your item has been edited. it may take several minutes to appear in the market,so please do not close the app.");

//                FragmentManager fm = getActivity().getSupportFragmentManager();
//                fm.popBackStack();
//                HomeActivity.SpecificFragment((FragmentActivity) getActivity(), Constants.EXPLORE_FRAGMENT, new ExploreFragment());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                Utilities.hideProgressDialog();
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", error.toString());
                error.printStackTrace();
            }
        }) {

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                Log.e(TAG, "video_image: " + videoImage);
                Log.e(TAG, "VideoData: " + VideoData);
                if (videoImage != null)
                    params.put("video_image", new DataPart("video_image.jpg", videoImage, "image/jpeg"));
                if (VideoData != null)
                    params.put("video", new DataPart("file.mp4", VideoData, "video/mp4"));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("item_id", itemid);
                return param;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(multipartRequest, "VideoUploadRequest");
    }

    private void deleteItemRequest() {
        String strUrl = WebServicesConstants.DeleteItem + Constants_AddItem.ITEM_ID;
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest jsonArrayRequest = new StringRequest(Request.Method.DELETE, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                Utilities.hideProgressDialog();
                HomeActivity.createhome = "yes";
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                HomeActivity.SpecificFragment(getActivity(), Constants.EXPLORE_FRAGMENT, new ExploreFragment());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                String json = null;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), "No more data");
                            break;
                    }
                }
            }
        }) {
            //            Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, WebServicesConstants.DeleteItem);
    }


    private void deleteVideoRequest() {

        String strUrl = WebServicesConstants.DeleteVideo + Constants_AddItem.ITEM_ID;
        Log.e(TAG, "strUrl: " + strUrl);

        StringRequest jsonArrayRequest = new StringRequest(Request.Method.DELETE, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "delete onResponse: " + response);
                Log.e(TAG, "videoImage: " + videoImage);
                if (videoImage != null) {
                    executeUploadVideoAPI();
                } else {
                    Utilities.hideProgressDialog();
                    Constants.additemFragment = "1";
                    showAlertDialog(getActivity(), "Membilia", "Your item has been edited. it may take several minutes to appear in the market,so please do not close the app.");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                String json = null;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                Toast.makeText(getActivity(), "No more data", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }) {
            //            Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, WebServicesConstants.DeleteVideo);
    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_showalert);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        txtDismiss.setText("Thank you");
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                HomeActivity.createhome = "yes";
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                HomeActivity.SpecificFragment(getActivity(), Constants.EXPLORE_FRAGMENT, new ExploreFragment());
            }
        });
        alertDialog.show();
    }
}
