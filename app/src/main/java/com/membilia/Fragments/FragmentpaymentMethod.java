package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentpaymentMethod extends Fragment {
    View itemsview;
    ImageView imgLeftIV;
    RadioButton payoutrb, addbankrb, bycheckrb;
    TextView payoutgmailtv;
    LinearLayout paypalll, addbankll, checkll;
    private static final String TAG = "FragmentPaymentMethod";

    JSONObject responseJson;
    String strPaypal = "";
    boolean isPaypallAccountAdded = false;
    boolean isBankAccountAdded = false;

    String oldDefault = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemsview = inflater.inflate(R.layout.fragment_payment_method, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemsview);
        setupclicks();

        return itemsview;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPayPalID();
    }

    private void getPayPalID() {
        String strUrl = WebServicesConstants.PayPalGet;
        Log.e(TAG, "getPayPalID: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    responseJson = jsonArray.getJSONObject(0);
                    Log.e(TAG, "onResponse: " + responseJson);
                    if (responseJson.has("paypal_id")) {
                        strPaypal = responseJson.getString("paypal_id");
                        isPaypallAccountAdded = true;
                        payoutgmailtv.setText(strPaypal);
                    } else {
                        isPaypallAccountAdded = false;
                    }
                    if (responseJson.getString("isbankAccountAdded").equalsIgnoreCase("0")) {
                        isBankAccountAdded = false;
                    }
                    else {
                        isBankAccountAdded = true;
                    }
                    if (!isPaypallAccountAdded && !isBankAccountAdded && Constants.isCheck.equalsIgnoreCase("")) {
                        makePaymentDefault("0");
                        payoutrb.setChecked(false);
                        addbankrb.setChecked(false);
                    }
                    else if (!Constants.isBankAccount.equalsIgnoreCase("") &&!isBankAccountAdded ){
                        makePaymentDefault("0");
                        payoutrb.setChecked(false);
                        addbankrb.setChecked(false);
                    }
                    else  if (!Constants.isPayPall.equalsIgnoreCase("") &&!isPaypallAccountAdded ){
                        makePaymentDefault("0");
                        payoutrb.setChecked(false);
                        addbankrb.setChecked(false);
                    }
                    else {
                        getPaymentDefault();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return hashMap;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.PayPalGet);
    }

    private void setUpViews(View v) {
        payoutgmailtv = v.findViewById(R.id.payoutgmailtv);
        payoutrb = v.findViewById(R.id.payoutrb);
        addbankrb = v.findViewById(R.id.addbankrb);
        bycheckrb = v.findViewById(R.id.bycheckrb);
        paypalll = v.findViewById(R.id.paypall);
        addbankll = v.findViewById(R.id.addbankll);
        checkll = v.findViewById(R.id.checkll);
        imgLeftIV = v.findViewById(R.id.imgLeftIV);

    }

    private void setupclicks() {
        payoutrb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPaypallAccountAdded) {
                    payoutrb.setChecked(true);
                    addbankrb.setChecked(false);
                    bycheckrb.setChecked(false);
                    makePaymentDefault("3");
                } else {
                    payoutrb.setChecked(false);
                    addbankrb.setChecked(false);
                    bycheckrb.setChecked(false);
                    Bundle bundle = new Bundle();
                    bundle.putString("strPaypal", payoutgmailtv.getText().toString());
                    HomeActivity.switchFragment(getActivity(), new Fragmenteditpaypalaccount(), Constants.Fragment_editpaypalaccount, true, bundle);

                }
            }
        });
        paypalll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("strPaypal", payoutgmailtv.getText().toString());
                HomeActivity.switchFragment(getActivity(), new Fragmenteditpaypalaccount(), Constants.Fragment_editpaypalaccount, true, bundle);
            }
        });
        addbankrb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isBankAccountAdded) {
                    addbankrb.setChecked(true);
                    bycheckrb.setChecked(false);
                    payoutrb.setChecked(false);
                    makePaymentDefault("2");
                } else {
                    addbankrb.setChecked(false);
                    payoutrb.setChecked(false);
                    bycheckrb.setChecked(false);
                    HomeActivity.switchFragment(getActivity(), new Fragmentshowbankaccount(), Constants.Fragment_showbankaccount, true, null);
                }
            }
        });
        addbankll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.switchFragment(getActivity(), new Fragmentshowbankaccount(), Constants.Fragment_showbankaccount, true, null);
            }
        });
        bycheckrb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bycheckrb.isChecked()) {
                    bycheckrb.setChecked(true);
                    addbankrb.setChecked(false);
                    payoutrb.setChecked(false);
                    makePaymentDefault("1");
                } else {
                    bycheckrb.setChecked(true);
                    addbankrb.setChecked(false);
                    payoutrb.setChecked(false);
                }
            }
        });
        imgLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }


    public void makePaymentDefault(final String i) {
        final String userid = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
        String strUrl = WebServicesConstants.MakePaymentDefault;
        Log.e(TAG, "strUrl: " + strUrl + "   userid  " + userid);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                getPaymentDefault();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return hashMap;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("default_account", i);
                hashMap.put("user_id", userid);
                return hashMap;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.MakePaymentDefault);
    }


    public void getPaymentDefault() {
        String userid =HomeActivity.db.getUser().get(0).getUserId();
        String strUrl = WebServicesConstants.GetPaymentDefault + userid;
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse:getPaymentDefault " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("message")) {
                        Log.e(TAG, "message: " + jsonObject.getString("message"));
                    } else {
                        String defaultAccount = jsonObject.getString("default_account");
                        if (defaultAccount.equalsIgnoreCase("1")) {
                            oldDefault = "bycheck";
                            Constants.isCheck = "true";
                            Constants.isBankAccount="";
                            Constants.isPayPall = "";
                            setUI(bycheckrb, payoutrb, addbankrb);
                        } else if (defaultAccount.equalsIgnoreCase("2")) {
                            oldDefault = "bybank";
                            Constants.isCheck = "";
                            Constants.isBankAccount="true";
                            Constants.isPayPall = "";
                            setUI(addbankrb, bycheckrb, payoutrb);
                        } else if (defaultAccount.equalsIgnoreCase("3")) {
                            oldDefault = "bypaypall";
                            if (isPaypallAccountAdded) {
                                Constants.isCheck = "";
                                Constants.isBankAccount="";
                                Constants.isPayPall = "true";

                                payoutrb.setChecked(true);
                                addbankrb.setChecked(false);
                                bycheckrb.setChecked(false);
                            }
                        } else {
                            payoutrb.setChecked(false);
                            addbankrb.setChecked(false);
                            bycheckrb.setChecked(false);
                            oldDefault = "";
                        }
                    }
                    Log.e(TAG, "onResponse:getdefault " + oldDefault);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return hashMap;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.GetPaymentDefault);
    }

    private void setUI(RadioButton bycheckrb, RadioButton payoutrb, RadioButton addbankrb) {
        bycheckrb.setChecked(true);
        payoutrb.setChecked(false);
        addbankrb.setChecked(false);
    }
}
