package com.membilia.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.membilia.R;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.SlidingActivityImageAdapter;
import com.membilia.volley_models.IDImagesVideoModel;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/*
 * Created by dharmaniz on 11/9/17.
 */

public class Fragment_Image extends Fragment {
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public String TAG = "ItemDetailImageActivity";
    View imageView;

    ViewPager Viewpager1;

    TabLayout tabDots;
    RelativeLayout crossRL;
    ArrayList<IDImagesVideoModel> imagesVideoArrayList = new ArrayList<>();
    int PositionItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null) {
            imagesVideoArrayList = (ArrayList<IDImagesVideoModel>) args.getSerializable("IMAGE");
            Log.i("","");
            PositionItem = args.getInt("POSITION");
            //Log.e(TAG, "******SIZE******" + imagesVideoArrayList.size());
        }
        imageView = inflater.inflate(R.layout.fragment_image, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(imageView);
        setclicklisteners();
        return imageView;
    }

    private void setUpViews(View v) {
        crossRL =  v.findViewById(R.id.crossRL);
        Viewpager1 =  v.findViewById(R.id.Viewpager1);
        tabDots =  v.findViewById(R.id.tabDots);
        Item_DetailSliding_Image();
        HomeActivity.bottombar.setVisibility(View.GONE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
    }


    private void setclicklisteners() {
        crossRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
                HomeActivity.bottombar.setVisibility(View.VISIBLE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
            }
        });
    }

    private void Item_DetailSliding_Image() {

        Viewpager1.setAdapter(new SlidingActivityImageAdapter(getActivity(), imagesVideoArrayList));
        tabDots.setupWithViewPager(Viewpager1,true);

        NUM_PAGES = imagesVideoArrayList.size();
        Log.e(TAG, "NUM_PAGES: "+NUM_PAGES );
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                    Log.e(TAG, "NUM_PAGES: currentPage "+currentPage );
                }
                Viewpager1.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 16000);

        // Pager listener over indicator
        Viewpager1.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                Log.e(TAG, "NUM_PAGES: "+currentPage );
            }
            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }
            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });
    }
}
