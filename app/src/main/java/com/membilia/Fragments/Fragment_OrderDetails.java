package com.membilia.Fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;

/*
 * Created by dharmaniapps on 21/12/17.
 */

public class Fragment_OrderDetails extends Fragment {
    View view;
    SimpleDraweeView itemImgIV;
    TextView itemNameTV, itemPrizeTV, changeAddressTV, ordernoTV, orderdateTV, senderTV,
            deliverymethodTV, recipientTV, addressTV, itemprizeBottomTV, shippingpriceTV,
            totalpriceTV, shareTV, reviewTV, sendmsgTV, titleTV, deliveryTypeTV;

    LinearLayout topbar;
    RelativeLayout leftRL;
    ImageView playIconIV;
    private static final String TAG = "Fragment_OrderDetails";
    //    Cursor res;
    String s = "";
    boolean isShipping = false, isRevived = false;

    String ItemId, UserId;

    String itemName, itemPrice, itemOrderNo, itemDate, senderName = "", senderId = "", deliveryType;
    String recipientName = "", recipientId = "", itemAddress, shippingPrice, totalPrice, transferPrice;
    String itemImage = "", senderImage = "";
    String OrderStatusId, OrderStatusType, isReviewed;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ItemId = Constants_AddItem.ITEM_ID;
        UserId = Constants_AddItem.USER_ID;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        HomeActivity.bottombar.setVisibility(View.VISIBLE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (Constants.refreshOrderDetails.equalsIgnoreCase("true")) {
            view = inflater.inflate(R.layout.fragment_orderdetails, null);
            setView(view);
            setOnClick();
            setDefaultData();
            getOrderHistory();
            Constants.refreshOrderDetails = "";
        } else {
            if (view == null) {
                view = inflater.inflate(R.layout.fragment_orderdetails, null);
                setView(view);
                setOnClick();
                setDefaultData();
                getOrderHistory();
            }
        }

        return view;
    }


    private void setView(View view) {
        topbar = view.findViewById(R.id.topbar);
        leftRL = view.findViewById(R.id.leftRL);
        titleTV = view.findViewById(R.id.titleTV);
        itemImgIV = view.findViewById(R.id.itemImgIV);
        itemNameTV = view.findViewById(R.id.itemNameTV);
        itemPrizeTV = view.findViewById(R.id.itemPrizeTV);
        changeAddressTV = view.findViewById(R.id.changeAddressTV);
        ordernoTV = view.findViewById(R.id.ordernoTV);
        orderdateTV = view.findViewById(R.id.orderdateTV);
        senderTV = view.findViewById(R.id.senderTV);
        deliverymethodTV = view.findViewById(R.id.deliverymethodTV);
        recipientTV = view.findViewById(R.id.recipientTV);
        addressTV = view.findViewById(R.id.addressTV);
        itemprizeBottomTV = view.findViewById(R.id.itemprizeBottomTV);
        shippingpriceTV = view.findViewById(R.id.shippingpriceTV);
        totalpriceTV = view.findViewById(R.id.totalpriceTV);
        shareTV = view.findViewById(R.id.shareTV);
        reviewTV = view.findViewById(R.id.reviewTV);
        sendmsgTV = view.findViewById(R.id.sendmsgTV);
        deliveryTypeTV = view.findViewById(R.id.deliveryTypeTV);
        titleTV.setText("Order Details");
        playIconIV = view.findViewById(R.id.playIconIV);
    }

    private void setDefaultData() {
        leftRL.setVisibility(View.VISIBLE);
        topbar.setVisibility(View.VISIBLE);

    }

    private void setOnClick() {
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        senderTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.clickProfile();
                HomeActivity.forProfileID = senderId;
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser", "false");
                HomeActivity.switchFragment(getActivity(), new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });

        recipientTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.clickProfile();
                HomeActivity.forProfileID = recipientId;
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser", "false");
                HomeActivity.switchFragment(getActivity(), new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });

        reviewTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRevived) {
                    senderName = senderName.replace("<u>", "");
                    Bundle bundle = new Bundle();
                    bundle.putString("senderName", senderName);
                    bundle.putString("senderId", senderId);
                    bundle.putString("senderImage", senderImage);
                    HomeActivity.switchFragment(getActivity(), new ReviewScreen(), Constants.GiveReview_Fragment, true, bundle);
                }
            }
        });

        changeAddressTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("sender_id", senderId);
                bundle.putString("recieverid", recipientId);
                bundle.putString("item_id", Constants_AddItem.ITEM_ID);
                bundle.putString("order_id", itemOrderNo);
                HomeActivity.switchFragment(getActivity(), new Customercare_Fragment(), Constants.CustomerCare, true, bundle);

            }
        });

        sendmsgTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String friendID;
               /* Log.e(TAG, "onClick:Constants_AddItem.SELLER_ID '"+Constants_AddItem.SELLER_ID );
                Log.e(TAG, "onClick:Constants_AddItem.USER_ID '"+Constants_AddItem.USER_ID );*/
                if (Constants_AddItem.USER_ID.equalsIgnoreCase(HomeActivity.UserItself)) {
                    friendID = Constants_AddItem.SELLER_ID + "playpawn";
                } else {
                    friendID = Constants_AddItem.USER_ID;
                }

                Long tsLong = System.currentTimeMillis() / 1000;
                String ts = tsLong.toString();
                Bundle bundle = new Bundle();
                bundle.putString("friend_id", friendID);
                bundle.putString("item_id", Constants_AddItem.ITEM_ID);
                bundle.putString("timeStamp", ts);
                Log.e(TAG, "timeStamponClick: " + ts);
                HomeActivity.switchFragment(getActivity(), new SendMessage_Fragment(), Constants.SendMessage, true, bundle);
            }
        });
        shareTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareLink();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void getOrderHistory() {
        Utilities.showProgressDialog(getActivity());
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("item_id", Constants_AddItem.ITEM_ID);
            jsonObject.put("user_id", HomeActivity.db.getUser().get(0).getUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String orderHistory = WebServicesConstants.OrderHistoryApi;
        Log.e(TAG, "strUrl: " + orderHistory);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, orderHistory, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                parseData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return header;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void parseData(String response) {
        Utilities.hideProgressDialog();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray orderDataArray = jsonObject.getJSONArray("orderData");
            if (orderDataArray.length() == 0) {
                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), "Sorry this item has been deleted");
            } else {
                JSONObject mainObject = orderDataArray.getJSONObject(0);
                JSONObject itemObject = mainObject.getJSONObject("item_object");
                // main
                itemOrderNo = "Membilia" + mainObject.getString("order_id");
                itemDate = mainObject.getString("order_date");
                OrderStatusId = mainObject.getString("status1");
                isReviewed = String.valueOf(mainObject.getBoolean("is_reviewed"));
                transferPrice = mainObject.getString("transfer_price");
                // item object
                itemName = itemObject.getString("item_name");
                itemPrice = itemObject.getString("price");
                double totalPrice1 = Double.parseDouble(itemPrice);
                DecimalFormat formatter = new DecimalFormat("#,###");
                String formatted = formatter.format(totalPrice1);
                itemPrice = "$" + formatted;
                JSONObject imageJsonObject = itemObject.getJSONObject("item_images");
                if (imageJsonObject.has("original_1")) {
                    itemImage = imageJsonObject.getString("original_1");
                }
                JSONObject videoJsonObject = itemObject.getJSONObject("item_video");
                if (videoJsonObject.has("original_videoimage")) {
                    itemImage = videoJsonObject.getString("original_videoimage");
                    playIconIV.setVisibility(View.VISIBLE);
                }
                deliveryType = String.valueOf(itemObject.getBoolean("delivery_type"));

                // item sender and reciever
                try {
                    JSONObject recieverObject = mainObject.getJSONObject("reciver");
                    recipientId = recieverObject.getString("user_id");
                    recipientName = recieverObject.getString("name");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject senderObject = mainObject.getJSONObject("sender");
                    senderId = senderObject.getString("user_id");
                    senderName = senderObject.getString("name");
                    JSONObject ImageObject = senderObject.getJSONObject("user_images");
                    senderImage = ImageObject.getString("original");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // address "address_line_one": "go",
                JSONObject addressJson = mainObject.getJSONObject("address");
                String addressline1 = addressJson.getString("address_line_one");
                String addressline2 = addressJson.getString("address_line_two");
                String addressCity = addressJson.getString("city");
                String addressState = addressJson.getString("state");
                String addressCountry = addressJson.getString("country");
                itemAddress = addressline1 + ",\n" +
                        addressline2 + ",\n" +
                        addressCity + ",\n" +
                        addressState + " ," +
                        addressCountry;
                Log.e(TAG, "itemImage: " + itemImage);
                itemImgIV.setImageURI(Uri.parse(itemImage));
                itemNameTV.setText(itemName);
                itemPrizeTV.setText(itemPrice);
                ordernoTV.setText(itemOrderNo);
                orderdateTV.setText(itemDate);
                senderName = "<u>" + "" + senderName + "</u>";
                senderTV.setText(Html.fromHtml(senderName));
                recipientName = "<u>" + "" + recipientName + "</u>";
                recipientTV.setText(Html.fromHtml(recipientName));
                double totalPrice2 = Double.parseDouble(itemObject.getString("price"));
                DecimalFormat formatter2 = new DecimalFormat("#,###");
                String formatted2 = formatter2.format(totalPrice2);
                String itemPriceNew = "$" + formatted2;
                itemprizeBottomTV.setText(itemPriceNew);
                if (!addressCountry.equalsIgnoreCase("")) {
                    addressTV.setText(itemAddress);
                    addressTV.setText(itemAddress);
                } else {
                    addressTV.setText("Personal transfer");
                }
                if (deliveryType.equalsIgnoreCase("true")) {
                    deliverymethodTV.setText("Personal transfer");
                } else {
                    deliverymethodTV.setText("Shipping");
                }

                if (OrderStatusId.equalsIgnoreCase("1")) {
                    deliveryTypeTV.setText("Order Placed");
                } else if (OrderStatusId.equalsIgnoreCase("2")) {
                    deliveryTypeTV.setText("Shipped");
                } else if (OrderStatusId.equalsIgnoreCase("3")) {
                    deliveryTypeTV.setText("In Transit");
                } else if (OrderStatusId.equalsIgnoreCase("4")) {
                    deliveryTypeTV.setText("Delivered");
                } else if (OrderStatusId.equalsIgnoreCase("5")) {
                    deliveryTypeTV.setText("Refund Requested");
                } else if (OrderStatusId.equalsIgnoreCase("6")) {
                    deliveryTypeTV.setText("Refunded");
                }


                if (senderId.equalsIgnoreCase(HomeActivity.UserItself)) {
                    reviewTV.setBackgroundResource(R.drawable.lightgraybutton);
                    reviewTV.setAlpha((float) .5);
                    isRevived = false;
                } else {
                    if (isReviewed.equalsIgnoreCase("true")) {
                        isRevived = false;
                        reviewTV.setBackgroundResource(R.drawable.lightgraybutton);
                        reviewTV.setAlpha((float) .5);
                    } else {
                        isRevived = true;
                    }

                }
                int TotalPrice = Integer.parseInt(transferPrice) + Integer.parseInt(itemObject.getString("price"));
                double totalPrice4 = Double.parseDouble(String.valueOf(TotalPrice));
                DecimalFormat formatter4 = new DecimalFormat("#,###");
                String formatted4 = formatter4.format(totalPrice4);
                String finalPrice = "$" + formatted4;
                totalpriceTV.setText(finalPrice);
                if (!transferPrice.equalsIgnoreCase("0")) {
                    double totalPrice3 = Double.parseDouble(transferPrice);
                    DecimalFormat formatter3 = new DecimalFormat("#,###");
                    String formatted3 = formatter3.format(totalPrice3);
                    transferPrice = "$" + formatted3;
                    shippingpriceTV.setText(transferPrice);
                } else {
                    shippingpriceTV.setText(transferPrice);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /* @Override
    public void onResume() {
        super.onResume();
        String s = "";
        try {
            if (res != null) {
                Log.e(TAG, "onResume: " + res);
                s = String.format("%,d", Long.parseLong(res.getString(3)));
                itemprizeBottomTV.setText("$" + s);
                ordernoTV.setText("Membilia" + res.getString(4));
                String strTemp = res.getString(5);
                String strArray[] = strTemp.split("T");
                String strDate = strArray[0];
                String strActualFormat = Utilities.getDate(strDate);
                orderdateTV.setText(strActualFormat);
                senderName = res.getString(6);
                String senderName1 = "<u>" + "" + res.getString(6) + "</u>";
//          locationtv.setText(Html.fromHtml(strcharity));
                senderTV.setText(Html.fromHtml(senderName1));
                String recipientName = "<u>" + "" + res.getString(7) + "</u>";
                recipientTV.setText(Html.fromHtml(recipientName));

                String address = res.getString(9);
                Log.e(TAG, "addressTV: " + address);

                if (res.getString(8).equalsIgnoreCase("false")) {
                    deliverymethodTV.setText("Shipping");

                    addressTV.setText(address);
                    isShipping = true;
                } else {
                    deliverymethodTV.setText("personal transfer");

                    addressTV.setText("Personal transfer");
                    isShipping = false;
                }

                s = String.format("%,d", Long.parseLong(res.getString(3)));


                if (isShipping) {
                    shippingpriceTV.setText("$" + res.getString(13));
                    s = s.replace(",", "");
                    String price = res.getString(13).replace(",", "");
                    int totalPrice = Integer.parseInt(s) + Integer.parseInt(price);
                    double totalPrice1 = Double.parseDouble(String.valueOf(totalPrice));
                    DecimalFormat formatter = new DecimalFormat("#,###");
                    String formatted = formatter.format(totalPrice1);
                    totalpriceTV.setText("$" + formatted);
                } else {
                    shippingpriceTV.setText("0");
                    totalpriceTV.setText("$" + s);
                }

                reciverId = res.getString(11);
                senderId = res.getString(10);
                if (senderId.equalsIgnoreCase(HomeActivity.UserItself)) {
//                reviewTV.setBackgroundResource(R.drawable.lightgraybutton);
                    reviewTV.setAlpha((float) .5);
                    isRevived = false;
                } else {
                    isRevived = true;
                }
                itemNameTV.setText(res.getString(1));
                imageUri = Uri.parse(res.getString(15));

                if (res.getString(18).equalsIgnoreCase("4")){
                    deliveryTypeTV.setText("Delivered");
                }else if (res.getString(18).equalsIgnoreCase("1")){
                    deliveryTypeTV.setText("Order Placed");
                }
                else if (res.getString(18).equalsIgnoreCase("2")){
                    deliveryTypeTV.setText("Shipped");
                }
                else if (res.getString(18).equalsIgnoreCase("3")){
                    deliveryTypeTV.setText("In Transit");
                }
                else if (res.getString(18).equalsIgnoreCase("5")){
                    deliveryTypeTV.setText("Refund Requested");
                }
                else if (res.getString(18).equalsIgnoreCase("6")){
                    deliveryTypeTV.setText("Refunded");
                }
                Log.e(TAG, "Statussssss: "+res.getString(17));
                itemImgIV.setImageURI(imageUri);
                double totalPrice1 = Double.parseDouble(res.getString(14));
                DecimalFormat formatter = new DecimalFormat("#,###");
                String formatted = formatter.format(totalPrice1);
                itemPrizeTV.setText("$" + formatted);

                Log.e(TAG, "onResume:16616161 "+res.getString(16) );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/


    private void shareLink() {
        String image;
        if (!Constants_AddItem.videoimage.equalsIgnoreCase("")) {
            image = Constants_AddItem.videoimage;
        } else {
            image = Constants_AddItem.imagepath1;
        }
        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier(Constants_AddItem.ITEM_ID)
                .setTitle(Constants_AddItem.ITEM_NAME)
                .setContentDescription(Constants_AddItem.ITEM_STORY)
                .setContentImageUrl(image)
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata()
                        .addCustomMetadata("android_deepview", "membilia_deepview_pj0")
                        .addCustomMetadata("ios_deepview", "membilia_deepview_pj0")
                        .addCustomMetadata("item_id", Constants_AddItem.ITEM_ID));

        LinkProperties lp = new LinkProperties()
                .setChannel("facebook")
                .setFeature("sharing")
                .addTag(Constants_AddItem.ITEM_ID)
                .setStage("new user")
                .setCampaign("content 123 launch")
                .addControlParameter("$desktop_url", "https://membilia.com/")
                .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));

        buo.generateShortUrl(getActivity(), lp, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    Log.i("BRANCH SDK", "got my Branch link to share: " + url);
                } else {
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ShareSheetStyle ss = new ShareSheetStyle(getActivity(), Constants_AddItem.ITEM_NAME, image)
                .setCopyUrlStyle(ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_send), "Copy", "Added to clipboard")
                .setMoreOptionStyle(ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_search), "Show more")
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.HANGOUT)
                .setAsFullWidthStyle(true)
                .setSharingTitle("Share With");

        buo.showShareSheet(getActivity(), lp, ss, new Branch.BranchLinkShareListener() {
            @Override
            public void onShareLinkDialogLaunched() {
                Log.e(TAG, "onShareLinkDialogLaunched: ");
            }

            @Override
            public void onShareLinkDialogDismissed() {
                Log.e(TAG, "onShareLinkDialogDismissed: ");
            }

            @Override
            public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
                Log.e(TAG, "onLinkShareResponse: " + sharedLink);
            }

            @Override
            public void onChannelSelected(String channelName) {
                Log.e(TAG, "onChannelSelected: ");
            }
        });
    }
}
