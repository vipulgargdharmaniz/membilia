package com.membilia.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.widget.PullRefreshLayout;

import com.membilia.MyHorizontalScrollView;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.activities.SelectionActivity;
import com.membilia.adapters.HorizontalAdapter;
import com.membilia.adapters.ItemsAdapter;
import com.membilia.fonts.TextViews.TextViewLato_Regular;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemLocationModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;
import com.membilia.volley_models.UserImagesModel;
import com.membilia.volley_models.UserLocationModel;
import com.membilia.volley_models.UsersModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.membilia.Util.Constants.IS_HOMEPOST_REFRESH;

/*
 * Created by dharmaniz on 25/5/17.
 */

public class ItemsFragment extends Fragment {
    //    SwipeRefreshLayout swipeToRefresh;
    String TAG = "ItemsFragment";

    public static NestedScrollView mNestedScrollView;

    View itemsview;

    RecyclerView recyclerviewRC;
    int perPage = 20;
    int page_noUser = 1;
    int page_noItem = 1;
    ItemsAdapter mItemsAdapter;
    ArrayList<ItemsModel> itemArraylist = new ArrayList<>();

    ArrayList<UsersModel> alluserArrayList = new ArrayList<>();
    ArrayList<UsersModel> loadMoreUser = new ArrayList<>();
    ArrayList<ItemsModel> loadMoreArrayList = new ArrayList<ItemsModel>();
    List<String> listTags = new ArrayList<String>();

    private RecyclerView horizontal_recycler_view;
    private MyHorizontalScrollView hsView;
    private HorizontalAdapter mHorizontalAdapter;

    private TextViewLato_Regular txt_seeall;
    private String searchResponse = "";


    private PullRefreshLayout swipeRefresh;

    String strURLItemUrl = "";
    private boolean isNew = false;
    private int filterType = 0;
    ProgressBar progressbarBottom;
    RelativeLayout progressRL;

    public ItemsFragment() {

    }

    @SuppressLint("ValidFragment")
    public ItemsFragment(String response) {
        searchResponse = response;
    }

    @SuppressLint("ValidFragment")
    public ItemsFragment(int filterType) {
        this.filterType = filterType;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (HomeActivity.isfilterResponse.equalsIgnoreCase("nodatafound")) {
            filterType = 1;
            itemArraylist.clear();
            loadMoreArrayList.clear();
        } else if (HomeActivity.isfilterResponse.equalsIgnoreCase("")) {
            filterType = 0;
            itemArraylist.clear();
            loadMoreArrayList.clear();
        } else {
            filterType = 2;
            HomeActivity.forSearchItem = "";
            itemArraylist.clear();
            loadMoreArrayList.clear();
        }

    }

    @SuppressLint("NewApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        if (itemsview == null) {
        // Inflate the layout for this fragment
        itemsview = inflater.inflate(R.layout.item_newfragment, container, false);

        setUpViews();
        setupclicks();
        /* filter 3 when it comes from search api*/
        Constants.IS_ITEM_USER = "item_fragment";
               /*  if data comes from search item api from explore fragment */
        if (!HomeActivity.forSearchItem.equals("")) {
            try {
                filterType = 3;
                JSONArray jsonArray = new JSONArray(searchResponse);
                parseResponse(jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        alluserArrayList = AppSingleton.getInstance().getUserApiResponseArrayList();
                      /* all users arraylist */
        if (alluserArrayList.size() > 0) {
            HomeActivity.UserType = "old";
            Log.e(TAG, "alluserArrayList size: " + alluserArrayList.size());
            setHorizontalAdapter();
        } else {
            HomeActivity.UserType = "new";
            gettingAllUsers();
        }

        if (filterType == 1) {
            recyclerviewRC.setVisibility(View.GONE);
        } else if (filterType == 2) {
            recyclerviewRC.setVisibility(View.VISIBLE);
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(HomeActivity.isfilterResponse);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jsonArray != null) {
                HomeActivity.Pageno_Item = 1;
                Log.e(TAG, "jsonArray: " + jsonArray);
                parseResponse(jsonArray);
            } else {
                HomeActivity.Pageno_Item = 0;

            }
        } else if (filterType==0){
            gettingAllItems();
            HomeActivity.isfilterResponse ="";
            recyclerviewRC.setVisibility(View.VISIBLE);
        }
        else {
            filterType= 0;
            HomeActivity.isfilterResponse ="";
            Log.e(TAG, "from search: " );
        }
        return itemsview;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNew) {

        } else {
            /* do nothing */
        }
        HomeActivity.homeClick();

    }

    @Override
    public void onPause() {
        super.onPause();
        HomeActivity.onPause = "yes";
    }

    public void setUpViews() {
        ExploreFragment.imgLeftIV.setImageResource(R.drawable.icon_zoom);



        recyclerviewRC = itemsview.findViewById(R.id.recyclerRV);
        horizontal_recycler_view = itemsview.findViewById(R.id.horizontal_recycler_view);
        hsView =  itemsview.findViewById(R.id.hsView);
        mNestedScrollView = itemsview.findViewById(R.id.mNestedScrollView);
        txt_seeall =  itemsview.findViewById(R.id.txt_seeall);
        txt_seeall.setVisibility(View.INVISIBLE);
        /* swipe_refresh_layout*/
        swipeRefresh = itemsview.findViewById(R.id.swipeRefresh);
        progressbarBottom =  itemsview.findViewById(R.id.progressbarBottom);

        progressbarBottom.setVisibility(View.INVISIBLE);

        progressRL = itemsview.findViewById(R.id.progressRL);
        progressRL.setVisibility(View.VISIBLE);
    }

    private void setupclicks() {

        swipeRefresh.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                HomeActivity.isfilterResponse ="";
                filterType =0;
                page_noItem = 1;
                itemArraylist.clear();
                progressRL.setVisibility(View.GONE);
                progressbarBottom.setVisibility(View.GONE);
                gettingAllItems();
            }
        });
//        hsView.setOnScrollChangedListener(new MyHorizontalScrollView.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged(int l, int t, int oldl, int oldt) {
//
//            }
//        });

//        @TargetApi(Build.VERSION_CODES.M)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            callhorizonalscollview();
        }
//        hsView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    if (hsView.fullScroll(ScrollView.FOCUS_RIGHT)){
//                        Log.e(TAG, "FOCUS_RIGHT: " );
//                    }
//                }
//                return false;
//            }
//        });


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            hsView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//                @Override
//                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
//                    int maxScrollX = hsView.getChildAt(0).getMeasuredWidth() - hsView.getMeasuredWidth();
//                    if (hsView.getScrollX() == maxScrollX) {
//                        if (HomeActivity.Pageno_alluser == 1) {
//                            ++page_noUser;
//                            HomeActivity.UserType = "refresh";
//                            gettingAllUsers();
//                        }
//                    }
//
//                }
//            });
//        }

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {

                    if (HomeActivity.Pageno_Item == 1&& filterType==0) {
                        ++page_noItem;
                        if (HomeActivity.forSearchItem.equals("")) {
                            HomeActivity.itemType = "refresh";

                            progressbarBottom.setVisibility(View.VISIBLE);
//                            Utilities.showProgressDialog(getActivity());
                            gettingAllItems();
                        } else {
                            HomeActivity.itemType = "refresh";

                            progressbarBottom.setVisibility(View.VISIBLE);
//                            Utilities.showProgressDialog(getActivity());
                            searchItem();
                        }
                    }
                }
            }
        });




        txt_seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("userModel", "seeall");
                HomeActivity.switchFragment(getActivity(), new AllUser_Fragment(), Constants.All_USER, true, args);
            }
        });
    }

    @SuppressLint("NewApi")
    private void callhorizonalscollview() {
        hsView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                int maxScrollX = hsView.getChildAt(0).getMeasuredWidth() - hsView.getMeasuredWidth();
                if (hsView.getScrollX() == maxScrollX) {
                    if (HomeActivity.Pageno_alluser == 1) {
                        ++page_noUser;
                        HomeActivity.UserType = "refresh";
                        gettingAllUsers();
                    }
                }
            }
        });
    }

    private void gettingAllItems() {
        HomeActivity.Pageno_Item = 0;
        String strUrl = WebServicesConstants.EXPLORE_ITEMS + "?perPage=" + perPage + "&page_no=" + page_noItem;
        Log.i(TAG, "url: " + strUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONArray response) {
                IS_HOMEPOST_REFRESH = false;
                Log.e(TAG, "onResponse*********" + response.toString());
                if (response.length() > 0) {
                    HomeActivity.Pageno_Item = 1;
                    parseResponse(response);

                } else {
                    HomeActivity.Pageno_Item = 0;
                    progressbarBottom.setVisibility(View.INVISIBLE);
//                    Utilities.hideProgressDialog();
                }
                swipeRefresh.setRefreshing(false);
                if (HomeActivity.itemType.equalsIgnoreCase("refresh")) {

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json ;
                if (HomeActivity.itemType.equalsIgnoreCase("refresh")) {
                }
                swipeRefresh.setRefreshing(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                if (json.contains("failed")){
                                    getActivity().startActivity(new Intent(getActivity(),SelectionActivity.class));
                                    getActivity().finish();
                                }
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ItemsRequest");
    }

    private void searchItem() {

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("item_name", HomeActivity.itemSearch);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        HomeActivity.Pageno_Item = 0;
        strURLItemUrl = WebServicesConstants.ITEM_SEARCH + "perPage=" + perPage + "&page_no=" + page_noItem;
        Log.i(TAG, "url: " + strURLItemUrl);
        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, strURLItemUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse*********" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (response.length() > 0) {
                        HomeActivity.Pageno_Item = 1;
                        parseResponse(jsonArray);
                    } else {
                        HomeActivity.Pageno_Item = 0;
                        progressbarBottom.setVisibility(View.INVISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json ;
                progressbarBottom.setVisibility(View.GONE);
                if (HomeActivity.itemType.equalsIgnoreCase("refresh")) {
//                    Utilities.hideProgressDialog();
                }
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                if (json.equalsIgnoreCase("no result found")) {
                                    Toast.makeText(getActivity(), "No more data", Toast.LENGTH_SHORT).show();
                                } else
                                    Toast.makeText(getActivity(), json, Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization",  PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ItemSearch");
    }

    private void gettingAllUsers() {
        HomeActivity.Pageno_alluser = 0;
        String strUrl = WebServicesConstants.USERS_LISTING + "?perPage=" + perPage + "&page_no=" + page_noUser;
        Log.i(TAG, "url: " + strUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, "onResponse*********" + response.toString());
                if (response.length() > 0) {
                    HomeActivity.Pageno_alluser = 1;
                    parseAllUser(response);
                } else {
                    HomeActivity.Pageno_alluser = 0;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json = null;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
//                                Toast.makeText(getActivity(), json, Toast.LENGTH_SHORT).show();
//                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "UsersRequest");
    }

    private void parseAllUser(JSONArray response) {

        if (HomeActivity.UserType.equalsIgnoreCase("old")) {
            Log.e(TAG, "parseAllUser: old");
        } else if (HomeActivity.UserType.equalsIgnoreCase("refresh")) {
            loadMoreUser.clear();
            Log.e(TAG, "parseAllUser: new");
        }
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jObject = response.getJSONObject(i);
                UsersModel uModel = new UsersModel();
                uModel.setUserId(jObject.getString("user_id"));
                uModel.setName(jObject.getString("name"));
                uModel.setDescription(jObject.getString("description"));
                if (!jObject.isNull("email"))
                    uModel.setEmail(jObject.getString("email"));
                uModel.setFollowerCount(Integer.parseInt(jObject.getString("followerCount")));
                uModel.setFollowingCount(Integer.parseInt(jObject.getString("followingCount")));
                uModel.setRating(Integer.parseInt(jObject.getString("rating")));
                uModel.setRanking(Integer.parseInt(jObject.getString("ranking")));
                uModel.setIsFollowed(Boolean.parseBoolean(jObject.getString("isFollowed")));

                if (!jObject.isNull("location")) {
                    JSONObject locationObject = jObject.getJSONObject("location");
                    UserLocationModel locationModel = new UserLocationModel();
                    if (!jObject.isNull("administrative_area_level_1"))
                        locationModel.setAdministrativeAreaLevel1(locationObject.getString("administrative_area_level_1"));
                    if (!jObject.isNull("country"))
                        locationModel.setCountry(locationObject.getString("country"));
                    if (!jObject.isNull("country_code"))
                        locationModel.setCountryCode(locationObject.getString("country_code"));
                    if (!jObject.isNull("lat"))
                        locationModel.setLat(locationObject.getString("lat"));
                    if (!jObject.isNull("lng"))
                        locationModel.setLng(locationObject.getString("lng"));
                    if (!locationObject.isNull("locality"))
                        locationModel.setLocality(locationObject.getString("locality"));
                    if (!locationObject.isNull("postal_code"))
                        locationModel.setPostalCode(locationObject.getString("postal_code"));

                    uModel.setLocation(locationModel);
                }

                if (!jObject.isNull("flag"))
                    uModel.setFlag(jObject.getString("flag"));

                uModel.setIsVerified(Boolean.parseBoolean(jObject.getString("isVerified")));
                if (!jObject.isNull("user_images")) {
                    JSONObject userObject = jObject.getJSONObject("user_images");
                    UserImagesModel imagesModel = new UserImagesModel();
                    if (!userObject.isNull("original"))
                        imagesModel.setOriginal(userObject.getString("original"));

                    uModel.setUserImages(imagesModel);
                }

                if (page_noUser == 1)
                    alluserArrayList.add(uModel);
                else {
                    loadMoreUser.add(uModel);
                }
            }
            if (loadMoreUser.size() > 0) {
                alluserArrayList.addAll(loadMoreUser);
            }

            setHorizontalAdapter();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setHorizontalAdapter() {

        horizontal_recycler_view.setNestedScrollingEnabled(false);
        mHorizontalAdapter = new HorizontalAdapter(getActivity(), alluserArrayList);
        horizontal_recycler_view.setHasFixedSize(false);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);


        horizontal_recycler_view.setAdapter(mHorizontalAdapter);

        if (horizontal_recycler_view.isAttachedToWindow()) {

            txt_seeall.setVisibility(View.VISIBLE);
        }
    }

    public void parseResponse(JSONArray response) {

        if (HomeActivity.itemType.equalsIgnoreCase("search")) {
            itemArraylist.clear();
            loadMoreArrayList.clear();
        } else if (HomeActivity.itemType.equalsIgnoreCase("relativeClick")) {
            page_noItem = 1;
            itemArraylist.clear();
            loadMoreArrayList.clear();
        } else if (HomeActivity.itemType.equalsIgnoreCase("refresh")) {
            loadMoreArrayList.clear();
        }
        try {

            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setLastUsed(jsonObject.getString("last_used"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setIsAutographed(Boolean.parseBoolean(jsonObject.getString("isAutographed")));
                itemsModel.setCategory(jsonObject.getString("category"));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setCharityName(jsonObject.getString("charity_name"));
                itemsModel.setCurrency(jsonObject.getString("currency"));
                itemsModel.setDeliveryType(Boolean.parseBoolean(jsonObject.getString("delivery_type")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setWeight(jsonObject.getString("weight"));
                itemsModel.setCreated(jsonObject.getString("created"));
                itemsModel.setStatus(jsonObject.getString("status"));
                itemsModel.setWidth(jsonObject.getString("width"));
                itemsModel.setHeight(jsonObject.getString("height"));
                itemsModel.setLength(jsonObject.getString("length"));
                itemsModel.setPersonTransfertext(jsonObject.getString("person_transfertext"));
                itemsModel.setNewtimestamp(jsonObject.getString("newtimestamp"));
                itemsModel.setOnHold(jsonObject.getString("onHold"));
                itemsModel.setStatus2(jsonObject.getString("status2"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsFavorite(Boolean.parseBoolean(jsonObject.getString("isFavorite")));
                itemsModel.setRating(jsonObject.getString("rating"));
                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));

                    itemsModel.setUserImages(imagesModel);
                }
                itemsModel.setPawnedDate(jsonObject.getString("pawned_date"));
                if (!jsonObject.isNull("tags")) {
                    JSONArray tags = jsonObject.getJSONArray("tags");
                    for (int k = 0; k < tags.length(); k++) {
                        listTags.add(tags.get(k).toString());
                    }

                }
                itemsModel.setTags(listTags);
                if (!jsonObject.isNull("location")) {
                    JSONObject location = jsonObject.getJSONObject("location");
                    ItemLocationModel locationModel = new ItemLocationModel();
                    locationModel.setAdministrativeAreaLevel1(location.getString("administrative_area_level_1"));
                    locationModel.setCountry(location.getString("country"));
                    locationModel.setCountryCode(location.getString("country_code"));
                    locationModel.setLat(Float.parseFloat(location.getString("lat")));
                    locationModel.setLng(Float.parseFloat(location.getString("lng")));
                    locationModel.setLocality(location.getString("locality"));
                    locationModel.setPostalCode(location.getString("postal_code"));

                    itemsModel.setLocation(locationModel);
                }

                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));

                    itemsModel.setItemImages(itemImagesModel);
                }

                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }


                itemsModel.setItemIndx(Integer.parseInt(jsonObject.getString("item_indx")));


                if (page_noItem == 1) {
                    itemArraylist.add(itemsModel);
                } else {
                    loadMoreArrayList.add(itemsModel);
                }

            }
            if (loadMoreArrayList.size()==0){
                setDataAndShowsItems();
            }
            else {
                itemArraylist.addAll(loadMoreArrayList);
                mItemsAdapter.notifyDataSetChanged();
            }
//            if (loadMoreArrayList.size() > 0) {
//                itemArraylist.addAll(loadMoreArrayList);
//            }


            progressbarBottom.setVisibility(View.VISIBLE);
            progressRL.setVisibility(View.GONE);
//            Utilities.hideProgressDialog();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDataAndShowsItems() {
        if (itemArraylist.size() > 0) {
            mNestedScrollView.setVisibility(View.VISIBLE);
        } else {
            mNestedScrollView.setVisibility(View.GONE);
        }
        mItemsAdapter = new ItemsAdapter(getActivity(), itemArraylist, null);
        recyclerviewRC.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setAutoMeasureEnabled(true);
        recyclerviewRC.setLayoutManager(mLayoutManager);
        recyclerviewRC.setItemAnimator(new DefaultItemAnimator());
        recyclerviewRC.setAdapter(mItemsAdapter);
    }


    @Override
    public void onDestroyView() {
        if (itemsview.getParent() != null) {
            ((ViewGroup) itemsview.getParent()).removeView(itemsview);
        }
        super.onDestroyView();
    }




}
