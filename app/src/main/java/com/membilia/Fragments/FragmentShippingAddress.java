package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley.UtilsVolley;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
/*
 * Created by dharmaniz on 11/7/17.
 */
public class FragmentShippingAddress extends Fragment {
    String TAG = "FragmentShippingAddress";
    View itemsview;
    EditText streetET, aprtET, cityET, stateET, postalcodeET;
    TextView countryET, titleTV;
    LinearLayout topbar;
    RelativeLayout leftRL;
    Button saveb;
    Bundle bundle = new Bundle();
    String countryCode = "";
    String strstreet, straprt, strcity, strcountry, strpostal, strstate, strAddressId = "";
    boolean isEdit = false;
    //    CountryModel model;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            isEdit = true;
            strstreet = args.getString("street");
            straprt = args.getString("aprt");
            strcity = args.getString("city");
            strstate = args.getString("state");
            strcountry = args.getString("country");
            Constants.SelectedCountry = strcountry;
            strpostal = args.getString("zipcode");
            strAddressId = args.getString("addressId");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        itemsview = inflater.inflate(R.layout.fragment_shipping_address, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemsview);
        setupclicks();
        if (isEdit) {
            streetET.setText(strstreet);
            aprtET.setText(straprt);
            cityET.setText(strcity);
            stateET.setText(strstate);
            countryET.setText(strcountry);
            postalcodeET.setText(strpostal);
        }
        return itemsview;
    }

    private void setUpViews(View v) {
        topbar =  v.findViewById(R.id.topbar);
        leftRL =  v.findViewById(R.id.leftRL);
        titleTV =  v.findViewById(R.id.titleTV);
        if (!isEdit) {
            titleTV.setText("Add Shipping Address");
        } else {
            titleTV.setText("Edit Address");
        }
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        streetET =  v.findViewById(R.id.streetET);
        streetET.requestFocus();
        aprtET =  v.findViewById(R.id.aprtET);
        cityET =  v.findViewById(R.id.cityET);
        stateET =  v.findViewById(R.id.stateET);
        countryET =  v.findViewById(R.id.countryET);
        postalcodeET =  v.findViewById(R.id.postalcodeET);
        saveb = v.findViewById(R.id.saveb);
    }

    private void setupclicks() {
        saveb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strstreet = streetET.getText().toString();
                straprt = aprtET.getText().toString();
                strcity = cityET.getText().toString();
                strstate = stateET.getText().toString();
                strcountry = countryET.getText().toString();
                strpostal = postalcodeET.getText().toString();
                if (strstreet.equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.streetfield));
                } else if (strcity.equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.cityfield));
                } else if (strstate.equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.statefield));
                } else if (strcountry.equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.countryfield));
                } else if (strpostal.equals("")) {
                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.postalfield));
                } else {
                    executePostAddressAPI();
                }
            }
        });
        countryET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                HomeActivity.switchFragment(getActivity(), new Fragment_Country_List(), Constants.Fragment_Country_List, true, null);
                HomeActivity.switchFragment(getActivity(), new Country_Fragment(), Constants.Fragment_Country_List, true, null);
            }
        });

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SelectedCountryCode = "";
                Constants.SelectedCountry = "";
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        countryET.setText(Constants.SelectedCountry);
        countryCode = Constants.SelectedCountryCode;
    }

    private void executePostAddressAPI() {
        Utilities.showProgressDialog(getActivity());
        int requestType;
        String strUrl ;
        if (isEdit) {
            requestType = Request.Method.PUT;
            strUrl = WebServicesConstants.SAVE_USER_ADDRESS + "/" + strAddressId;
        } else {
            requestType = Request.Method.POST;
            strUrl = WebServicesConstants.SAVE_USER_ADDRESS;
        }
        StringRequest stringRequest = new StringRequest(requestType, strUrl,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utilities.hideProgressDialog();
                        Log.i("VOLLEY", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("address_id").length() > 0) {
                                getActivity().onBackPressed();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Utilities.hideProgressDialog();
                Log.e(TAG, "****onErrorResponse****" + error.toString());
                String json ;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, ""));
                params.put("address_line_one", strstreet);
                params.put("address_line_two", straprt);
                params.put("city", strcity);
                params.put("country", strcountry);
                params.put("state", strstate);
                params.put("zipcode", strpostal);
                params.put("country_code", countryCode);

                return params;
            }

            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "SignInRequest");
    }
}