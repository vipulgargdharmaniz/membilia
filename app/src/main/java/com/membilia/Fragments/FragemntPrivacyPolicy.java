package com.membilia.Fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;

import com.membilia.R;


/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragemntPrivacyPolicy extends Fragment {
    View itemsview;
    ImageView imgLeftIV;

    WebView webView;
    private static final String TAG = "FragemntPrivacyPolicy";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        itemsview = inflater.inflate(R.layout.fragment_privacy_policy, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemsview);
        setupclicks();
        return itemsview;
    }

    private void setUpViews(View v) {
        imgLeftIV =  v.findViewById(R.id.imgLeftIV);
        webView =  v.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl("https://membilia.com/terms-conditions/");
         }

    private void setupclicks() {

        imgLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

}