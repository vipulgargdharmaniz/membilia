package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.membilia.R;
import com.membilia.Util.Countries;
import com.membilia.adapters.CountriesAdapter;
import com.membilia.volley_models.CountryModel;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniapps on 30/11/17.
 */

public class Country_Fragment extends Fragment {

    private RecyclerView recyclerRV;
    private LinearLayout topbarLL;
    private RelativeLayout leftRL, progressbarRL;
    private TextView titleTV;
    EditText editSearch;
    CountriesAdapter mAdapter;
    private String strCountriesInfo = "";
    private ArrayList<CountryModel> countriesArrayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.countrylist, null);
        setView(view);
        strCountriesInfo = Countries.COUNTRIES_INFO;
        return view;
    }

    private void setView(View view) {
        recyclerRV = view.findViewById(R.id.recyclerRV);
        topbarLL = view.findViewById(R.id.topbarLL);
        leftRL = view.findViewById(R.id.leftRL);
        titleTV = view.findViewById(R.id.titleTV);
        editSearch = view.findViewById(R.id.editSearch);
        progressbarRL = view.findViewById(R.id.progressbarRL);
    }

    private void setupClick() {
        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //after the change calling the method and passing the search input
                filter(s.toString());
            }
        });

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<CountryModel> filterdNames = new ArrayList<CountryModel>();
        //looping through existing elements
        for (CountryModel s : countriesArrayList) {
            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        setAdapter(filterdNames);
    }

    @Override
    public void onResume() {
        super.onResume();
        topbarLL.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        titleTV.setVisibility(View.VISIBLE);
        progressbarRL.setVisibility(View.GONE);
        editSearch.setVisibility(View.VISIBLE);
        setupClick();
        setUpCountriesAdapter();
    }

    private void setUpCountriesAdapter() {
        try {
            JSONArray jsonArray = new JSONArray(strCountriesInfo);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CountryModel mModel = new CountryModel();
                mModel.setName(jsonObject.getString("name"));
                mModel.setNameImage(jsonObject.getString("nameImage"));
                mModel.setDailCode(jsonObject.getString("dial_code"));
                mModel.setCode(jsonObject.getString("code"));
                if (!jsonObject.isNull("Language")) {
                    mModel.setLanguage(jsonObject.getString("Language"));
                }
                countriesArrayList.add(mModel);
            }
            setAdapter(countriesArrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(ArrayList<CountryModel> arrayList) {
        mAdapter = new CountriesAdapter(getActivity(), arrayList, Country_Fragment.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    public void clearFragment() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
