package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.volley.UtilsVolley;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniz on 14/7/17.
 */

public class Fragmenteditpaypalsubmit extends Fragment {
    View itemView;
    ImageView imgLeftIV, imgRightIV;
    RelativeLayout searchRL, rightRL;
    TextView submitTV;
    private static final String TAG = "paypalsubmit";
    JSONObject requestJson;
    EditText emailET;
    TextView titleTV;
    String strPayPalAccount = "";
    boolean isDeleteVisible = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String strPayPal = bundle.getString("strPaypal", "");
            if (!strPayPal.equalsIgnoreCase("")) {
                this.strPayPalAccount = strPayPal;
                isDeleteVisible = true;
            } else {
                isDeleteVisible = false;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemView = inflater.inflate(R.layout.fragment_edit_paypal_account_submit, null);
        Log.e(TAG, "onCreateView: ");
        setUpViews(itemView);
        setupclicks();
        return itemView;
    }

    public void setUpViews(View v) {
        imgLeftIV = v.findViewById(R.id.imgLeftIV);
        imgRightIV = v.findViewById(R.id.imgRightIV);
        searchRL = v.findViewById(R.id.searchRL);
        submitTV = v.findViewById(R.id.submitTV);
        emailET = v.findViewById(R.id.emailET);
        rightRL = v.findViewById(R.id.rightRL);
        titleTV = v.findViewById(R.id.titleTV);

        if (!isDeleteVisible) {
            rightRL.setVisibility(View.GONE);
            submitTV.setText(getActivity().getResources().getString(R.string.add));
            titleTV.setText(getActivity().getResources().getString(R.string.addpaypal));
        } else {
            titleTV.setText(getActivity().getResources().getString(R.string.editpaypal));
        }
        if (!strPayPalAccount.equalsIgnoreCase("")) {
            emailET.setText(strPayPalAccount);
        }
    }

    public void setupclicks() {

        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailET.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (strPayPalAccount.equalsIgnoreCase("")) {
                    if (email.matches(emailPattern)) {
                        try {
                            requestJson = new JSONObject();
                            requestJson.put("user_id", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, ""));
                            requestJson.put("paypal_id", emailET.getText().toString());
                            servicePayPal();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else
                        AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), "Please enter valid email id.");
                } else {
                    try {
                        String strUrl = WebServicesConstants.PayPalEdit;
                        requestJson = new JSONObject();
                        requestJson.put("user_id", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, ""));
                        requestJson.put("paypal_id", emailET.getText().toString());
                        serviceEditPayPal(strUrl, Request.Method.POST);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        rightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceDeletePayPal();
            }
        });
    }

    private void servicePayPal() {
        Utilities.showProgressDialog(getActivity());
        String strUrl = WebServicesConstants.PayPalAdd;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject responseJson = new JSONObject(response);
                    String strMessage = responseJson.getString("message");
                    if (strMessage.equalsIgnoreCase("true")) {
                        Utilities.hideProgressDialog();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        fm.popBackStackImmediate(Constants.Fragment_PaymentMethod, 2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.PayPalAdd);
    }


    private void serviceDeletePayPal() {
        Utilities.showProgressDialog(getActivity());
//        String strUrl = WebServicesConstants.PayPalDelete;
        String strUrl = WebServicesConstants.PayPalDeleteNew;
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, ""));
        JsonObjectRequest request_json = new JsonObjectRequest(strUrl, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "onResponse: "+response );
                            try {
                                String strMessage = response.getString("message");
                                if (strMessage.equalsIgnoreCase("true")) {
                                    Utilities.hideProgressDialog();
                                    FragmentManager fm = getActivity().getSupportFragmentManager();
                                    fm.popBackStackImmediate(Constants.Fragment_PaymentMethod, 2);
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)  {
                VolleyLog.e("Error: ", error.getMessage());
                NetworkResponse response = error.networkResponse;
                String json;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                hashMap.put("Content-Type", "application/json");
                return hashMap;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(request_json);
    }


    private void serviceEditPayPal(String strUrl, int delete) {
        Log.e(TAG, "strUrl: " + strUrl + "    " + requestJson.toString());
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject responseJson = new JSONObject(response);
                    String strMessage = responseJson.getString("message");
                    if (strMessage.equalsIgnoreCase("true")) {
                        Utilities.hideProgressDialog();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        fm.popBackStackImmediate(Constants.Fragment_PaymentMethod, 2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.PayPalAdd);
    }

}
