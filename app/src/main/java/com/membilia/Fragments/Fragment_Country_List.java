package com.membilia.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.membilia.R;
import com.membilia.Util.Countries;
import com.membilia.adapters.CountriesAdapter;
import com.membilia.volley_models.CountryModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dharmaniz on 19/7/17.
 */

public class Fragment_Country_List extends Fragment {

    View itemView;
    EditText editSearchET;
    RecyclerView countriesRecyclerView;
    String strCountriesInfo = "";
    ArrayList<CountryModel> countriesArrayList = new ArrayList<CountryModel>();
    CountriesAdapter mAdapter;
    ImageView imgLeftIV;
    private static final String TAG = "Fragment_Country_List";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemView = inflater.inflate(R.layout.fragment_country_list, null);
        Log.i(TAG, "onCreateView: ");
        strCountriesInfo = Countries.COUNTRIES_INFO;
        setUpViews(itemView);
        setupclicks();
        return itemView;
    }

    private void setUpViews(View v) {
        editSearchET = (EditText) v.findViewById(R.id.editSearchET);
        countriesRecyclerView = (RecyclerView) v.findViewById(R.id.countriesRecyclerView);
        imgLeftIV = (ImageView) v.findViewById(R.id.imgLeftIV);
        setUpCountriesAdapter();
    }


    private void setupclicks() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //after the change calling the method and passing the search input
                filter(s.toString());
            }
        });


//        countriesRecyclerView.addOnItemTouchListener(new RecyclerClickListener(getActivity(), new RecyclerClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
//                Log.d("click item", String.valueOf(position));
//
//            }
//        }));
        imgLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });


    }


    private void setUpCountriesAdapter() {
        try {
            JSONArray jsonArray = new JSONArray(strCountriesInfo);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CountryModel mModel = new CountryModel();
                mModel.setName(jsonObject.getString("name"));
                mModel.setNameImage(jsonObject.getString("nameImage"));
                mModel.setDailCode(jsonObject.getString("dial_code"));
                mModel.setCode(jsonObject.getString("code"));
                if (!jsonObject.isNull("Language")) {
                    mModel.setLanguage(jsonObject.getString("Language"));
                }

                countriesArrayList.add(mModel);
            }
            setAdapter(countriesArrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<CountryModel> filterdNames = new ArrayList<CountryModel>();

        //looping through existing elements
        for (CountryModel s : countriesArrayList) {
            //if the existing elements contains the search input
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        setAdapter(filterdNames);
    }

    private void setAdapter(ArrayList<CountryModel> arrayList) {
        mAdapter = new CountriesAdapter(getActivity(), arrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        countriesRecyclerView.setLayoutManager(mLayoutManager);
        countriesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        countriesRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    /****** Recycler Click Listener *******/
    public static class RecyclerClickListener implements RecyclerView.OnItemTouchListener {

        private OnItemClickListener mListener;
        GestureDetector mGestureDetector;

        public interface OnItemClickListener {
            void onItemClick(View view, int position);
        }

        public RecyclerClickListener(Context context, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildLayoutPosition(childView));
                return true;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) { }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

    }
}
