package com.membilia.Fragments;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ReviewsAdapter;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ReviewReceiverModel;
import com.membilia.volley_models.ReviewRecevierLocationModel;
import com.membilia.volley_models.ReviewRecieverImages;
import com.membilia.volley_models.ReviewSenderImages;
import com.membilia.volley_models.ReviewSenderLocationModel;
import com.membilia.volley_models.ReviewSenderModel;
import com.membilia.volley_models.ReviewsModel;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class ReviewFragment extends Fragment {
    View reviewView;
    RecyclerView recyclerRV;
    int perPage = 10;
    int page_no = 1;
    ReviewsAdapter mAdapter;
    ArrayList<ReviewsModel> reviewsModelArrayList = new ArrayList<>();
    private String TAG = "ReviewFragment";
    LinearLayout nodataLL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        reviewView = inflater.inflate(R.layout.fragment_review, container, false);
        Log.i(TAG, "onCreateView: ");
        setUpViews(reviewView);
        return reviewView;
    }

    private void setUpViews(View v) {
        recyclerRV = v.findViewById(R.id.recyclerRV);
        nodataLL = v.findViewById(R.id.nodataLL);
        gettingAllItemsReviews();
    }

    private void gettingAllItemsReviews() {
        Utilities.showProgressDialog(getActivity());
        String strUrl = WebServicesConstants.USER_REVIEW + HomeActivity.forProfileID + "&perPage=" + perPage + "&page_no=" + page_no;
        Log.e(TAG, "url: "+strUrl );
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONArray response) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onResponse" + response.toString());
                Log.e(TAG, "onResponse" + response.length());
                if (response.length()>0){
                    parseResponse(response);
                    nodataLL.setVisibility(View.GONE);
                }else
                    nodataLL.setVisibility(View.VISIBLE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "PawnRequest");
    }

    private void parseResponse(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ReviewsModel reviewmodel = new ReviewsModel();
                reviewmodel.setItemName(jsonObject.getString("item_name"));
                reviewmodel.setReview(jsonObject.getString("review"));
                reviewmodel.setRating(jsonObject.getString("rating"));
                reviewmodel.setIsReviewer(jsonObject.getString("is_reviewer"));

                if (!jsonObject.isNull("sender")) {
                    JSONObject senderObject = jsonObject.getJSONObject("sender");
                    ReviewSenderModel senderReviewModel = new ReviewSenderModel();
                    senderReviewModel.setUserId(senderObject.getString("user_id"));
                    senderReviewModel.setName(senderObject.getString("name"));
                    if (!senderObject.isNull("description"))
                        senderReviewModel.setDescription(senderObject.getString("description"));
                    if (!senderObject.isNull("email"))
                        senderReviewModel.setEmail(senderObject.getString("email"));
                    if (!senderObject.isNull("followerCount"))
                        senderReviewModel.setFollowerCount(senderObject.getInt("followerCount"));
                    if (!senderObject.isNull("followingCount"))
                        senderReviewModel.setFollowingCount(senderObject.getInt("followingCount"));
                    if (!senderObject.isNull("rating"))
                        senderReviewModel.setRating(senderObject.getInt("rating"));
                    if (!senderObject.isNull("ranking"))
                        senderReviewModel.setRanking(senderObject.getInt("ranking"));
                    senderReviewModel.setIsFollowed(senderObject.getBoolean("isFollowed"));

                    if (!senderObject.isNull("location")) {
                        JSONObject senderLocationObj = senderObject.getJSONObject("location");
                        ReviewSenderLocationModel senderLocationModel = new ReviewSenderLocationModel();
                        if (!senderLocationObj.isNull("administrative_area_level_1"))
                            senderLocationModel.setAdministrativeAreaLevel1(senderLocationObj.getString("administrative_area_level_1"));
                        if (!senderLocationObj.isNull("country"))
                            senderLocationModel.setCountry(senderLocationObj.getString("country"));
                        if (!senderLocationObj.isNull("country_code"))
                            senderLocationModel.setCountryCode(senderLocationObj.getString("country_code"));
                        if (!senderLocationObj.isNull("lat"))
                            senderLocationModel.setLat(senderLocationObj.getInt("lat"));
                        if (!senderLocationObj.isNull("lng"))
                            senderLocationModel.setLng(senderLocationObj.getInt("lng"));
                        if (!senderLocationObj.isNull("locality"))
                            senderLocationModel.setLocality(senderLocationObj.getString("locality"));
                        if (!senderLocationObj.isNull("postal_code"))
                            senderLocationModel.setPostalCode(senderLocationObj.getString("postal_code"));
                        senderReviewModel.setLocation(senderLocationModel);
                    }
                    if (!senderObject.isNull("flag"))
                        senderReviewModel.setFlag(senderObject.getString("flag"));
                    if (!senderObject.isNull("isVerified"))
                        senderReviewModel.setIsVerified(senderObject.getBoolean("isVerified"));
                    if (!senderObject.isNull("user_images")) {
                        JSONObject senderImagesObj = senderObject.getJSONObject("user_images");
                        ReviewSenderImages imgModel = new ReviewSenderImages();
                        if (!senderImagesObj.isNull("original"))
                            imgModel.setOriginal(senderImagesObj.getString("original"));
                        senderReviewModel.setUserImages(imgModel);
                    }
                    reviewmodel.setSender(senderReviewModel);
                }
                if (!jsonObject.isNull("receiver")) {
                    JSONObject receiverObject = jsonObject.getJSONObject("receiver");
                    ReviewReceiverModel receiverModel = new ReviewReceiverModel();
                    receiverModel.setUserId(receiverObject.getString("user_id"));
                    receiverModel.setName(receiverObject.getString("name"));
                    if (!receiverObject.isNull("description"))
                        receiverModel.setDescription(receiverObject.getString("description"));
                    if (!receiverObject.isNull("email"))
                        receiverModel.setEmail(receiverObject.getString("email"));
                    if (!receiverObject.isNull("followerCount"))
                        receiverModel.setFollowerCount(receiverObject.getInt("followerCount"));
                    if (!receiverObject.isNull("followingCount"))
                        receiverModel.setFollowingCount(receiverObject.getInt("followingCount"));
                    if (!receiverObject.isNull("rating"))
                        receiverModel.setRating(receiverObject.getInt("rating"));
                    if (!receiverObject.isNull("ranking"))
                        receiverModel.setRanking(receiverObject.getInt("ranking"));
                    if (!receiverObject.isNull("isFollowed"))
                        receiverModel.setIsFollowed(receiverObject.getBoolean("isFollowed"));
                    if (!receiverObject.isNull("location")) {
                        JSONObject rLocationObject = receiverObject.getJSONObject("location");
                        ReviewRecevierLocationModel rLocationM = new ReviewRecevierLocationModel();
                        if (!rLocationObject.isNull("administrative_area_level_1"))
                            rLocationM.setAdministrativeAreaLevel1(rLocationObject.getString("administrative_area_level_1"));
                        if (!rLocationObject.isNull("country"))
                            rLocationM.setCountry(rLocationObject.getString("country"));
                        if (!rLocationObject.isNull("country_code"))
                            rLocationM.setCountryCode(rLocationObject.getString("country_code"));
                        if (!rLocationObject.isNull("lat"))
                            rLocationM.setLat(rLocationObject.getInt("lat"));
                        if (!rLocationObject.isNull("lng"))
                            rLocationM.setLng(rLocationObject.getInt("lng"));
                        if (!rLocationObject.isNull("locality"))
                            rLocationM.setLocality(rLocationObject.getString("locality"));
                        if (!rLocationObject.isNull("postal_code"))
                            rLocationM.setPostalCode(rLocationObject.getString("postal_code"));
                        receiverModel.setLocation(rLocationM);
                    }
                    if (!receiverObject.isNull("flag"))
                        receiverModel.setFlag(receiverObject.getString("flag"));
                    if (!receiverObject.isNull("isVerified"))
                        receiverModel.setIsVerified(receiverObject.getBoolean("isVerified"));

                    if (!receiverObject.isNull("user_images")) {
                        JSONObject rImagesObject = receiverObject.getJSONObject("user_images");
                        ReviewRecieverImages rImageModel = new ReviewRecieverImages();
                        if (!rImagesObject.isNull("original"))
                            rImageModel.setOriginal(rImagesObject.getString("original"));
                    }
                    reviewmodel.setReceiver(receiverModel);
                }
                if (!jsonObject.isNull("date"))
                    reviewmodel.setDate(jsonObject.getString("date"));

                reviewsModelArrayList.add(reviewmodel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setAdapter();
    }
    private void setAdapter() {
        mAdapter = new ReviewsAdapter(getActivity(), reviewsModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(mAdapter);
        recyclerRV.setNestedScrollingEnabled(false);
    }
}
