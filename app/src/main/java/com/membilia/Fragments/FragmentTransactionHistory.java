package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentTransactionHistory extends Fragment {
    View itemsview;
    RelativeLayout searchRL;
    TextView detailpaymenttv , amountEarnedTV ,charityTV ,soldTV;
    private static final String TAG = "FragmentTransactionHist";

    String soldItems ="", amountRaised = "",amountEarned ="";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
      if (itemsview==null){
          itemsview = inflater.inflate(R.layout.fragment_transaction_history, null);
          Log.i(TAG, "onCreateView: ");
          setUpViews(itemsview);
          setupclicks();
          getTransactionHistory();
      }
        return itemsview;
    }

    private void getTransactionHistory() {
        Utilities.showProgressDialog(getActivity());
        String strUrl = WebServicesConstants.TransactionHistory;
        Log.e(TAG, "strUrl: " + strUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: "+response );
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    soldItems  = jsonObject.getString("pawnedAndExp");
                    amountRaised = jsonObject.getString("charity");
                    amountEarned = jsonObject.getString("amountEarned");

                    setData();
                } catch (Exception e){
                  e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, ""));
                return params;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.TransactionHistory);

    }

    private void setData() {
        charityTV.setText("$"+amountRaised);
        amountEarnedTV.setText("$"+amountEarned);
        soldTV.setText(soldItems);
        Utilities.hideProgressDialog();
    }

    private void setUpViews(View v) {
        searchRL =  v.findViewById(R.id.searchRL);
        detailpaymenttv =  v.findViewById(R.id.detailpaymenttv);
        soldTV =  v.findViewById(R.id.soldTV);
        amountEarnedTV =  v.findViewById(R.id.amountEarnedTV);
        charityTV  =  v.findViewById(R.id.charityTV);
    }

    private void setupclicks() {
        String strDetails = "<u>" + "Detailed Payment History" + "</u>";

        detailpaymenttv.setText(Html.fromHtml(strDetails));
        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        detailpaymenttv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.switchFragment(getActivity(),new DetailedTransaction_Fragment(),Constants.Fragment_Aboutus, true,null);
            }
        });
    }
}
