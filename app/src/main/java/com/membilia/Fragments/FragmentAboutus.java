package com.membilia.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.membilia.R;


/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentAboutus extends Fragment {

    View itemsview;
    RelativeLayout searchRL;
    WebView webView;
    private static final String TAG = "FragmentAboutus";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        itemsview= inflater.inflate(R.layout.fragment_aboutus, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemsview);
        setupclicks();
        return itemsview;
    }

    private void setUpViews(View v) {
        searchRL=v.findViewById(R.id.searchRL);
        webView =  v.findViewById(R.id.webView);
        webView.loadUrl("https://membilia.com/");
        webView.getSettings().setJavaScriptEnabled(true);
    }

    private void setupclicks() {
        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }
}

