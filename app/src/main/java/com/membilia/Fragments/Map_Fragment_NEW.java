package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.membilia.R;

/**
 * Created by dharmaniapps on 28/11/17.
 */

public class Map_Fragment_NEW extends Fragment implements OnMapReadyCallback{

    View view;
    private static final String TAG = "Map_Fragment_NEW";
    String lat="" , log="";
    LinearLayout topbar;
    RelativeLayout leftRL;
    TextView titleTV;
    RelativeLayout map_RL;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args!=null){
            Log.e(TAG, "onCreate: "+args.getString("lat") );
            lat = args.getString("lat");
            log = args.getString("log");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mapfragment,null);
        topbar = (LinearLayout) view.findViewById(R.id.topbar);
        leftRL = (RelativeLayout) view.findViewById(R.id.leftRL);
        titleTV = (TextView) view.findViewById(R.id.titleTV);
        map_RL = (RelativeLayout) view.findViewById(R.id.map_RL);
        titleTV.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        topbar.setVisibility(View.VISIBLE);
        map_RL.setVisibility(View.VISIBLE);
        SupportMapFragment mapFragment =
                (SupportMapFragment)this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        onclick();
        return view;
    }

    private void onclick() {
        titleTV.setText(getActivity().getResources().getString(R.string.itemLocation));
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                map_RL.setVisibility(View.INVISIBLE);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Double lat1;
        Double log2 ;
        if (!lat.equalsIgnoreCase("")&&!log.equalsIgnoreCase("")){
            lat1 = Double.parseDouble(lat);
            log2 = Double.parseDouble(log);
        }
        else {
            lat1 = 0.0;
            log2 = 0.0;
        }
        LatLng sydney = new LatLng(lat1, log2);
        Log.e(TAG, "onMapReady: "+lat1);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney , 15.0f));
        googleMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        map_RL.setVisibility(View.INVISIBLE);
//        FragmentManager fm = getActivity().getSupportFragmentManager();
//        fm.popBackStack();
    }

}
