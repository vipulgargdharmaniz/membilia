package com.membilia.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ProfileItem_Adapter;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemLocationModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AvailableFragment extends Fragment {
    View availableView;
    ProfileItem_Adapter mProfileItemsAdapter;
    ArrayList<ItemsModel> modelArrayList = new ArrayList<>();
    List<String> listTags = new ArrayList<>();
    LinearLayout txtInfoItemsTV;
    int perPage = 20;
    int page_no = 1;
    RecyclerView recyclerRV;
    private String TAG = "AvailableFragment";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        availableView = inflater.inflate(R.layout.fragment_available, container, false);
        Log.i(TAG, "onCreateView: ");
        setUpViews(availableView);
        setupclicks();
        return availableView;
    }

    private void setUpViews(View v) {
        recyclerRV = v.findViewById(R.id.recyclerRV);
        txtInfoItemsTV = v.findViewById(R.id.txtInfoItemsTV);
        gettingAllItems();
    }

    private void setupclicks() {
    }

    private void gettingAllItems() {
        Utilities.showProgressDialog(getActivity());
        String strUrl = WebServicesConstants.AVAILABLE_ITEMS + "?user_id=" + HomeActivity.forProfileID + "&perPage=" + perPage + "&page_no=" + page_no;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(JSONArray response) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onResponse*********" + response.toString());
                Log.e(TAG, "onResponse: " + response.length());
                if (response.length() > 0) {
                    parseResponse(response);
                } else {
                    txtInfoItemsTV.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "AvailableRequest");
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void parseResponse(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setLastUsed(jsonObject.getString("last_used"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setIsAutographed(Boolean.parseBoolean(jsonObject.getString("isAutographed")));
                itemsModel.setCategory(jsonObject.getString("category"));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setCharityName(jsonObject.getString("charity_name"));
                itemsModel.setCurrency(jsonObject.getString("currency"));
                itemsModel.setDeliveryType(Boolean.parseBoolean(jsonObject.getString("delivery_type")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setWeight(jsonObject.getString("weight"));
                itemsModel.setCreated(jsonObject.getString("created"));
                itemsModel.setStatus(jsonObject.getString("status"));
                itemsModel.setWidth(jsonObject.getString("width"));
                itemsModel.setHeight(jsonObject.getString("height"));
                itemsModel.setLength(jsonObject.getString("length"));
                itemsModel.setPersonTransfertext(jsonObject.getString("person_transfertext"));
                itemsModel.setNewtimestamp(jsonObject.getString("newtimestamp"));
                itemsModel.setOnHold(jsonObject.getString("onHold"));
                itemsModel.setStatus2(jsonObject.getString("status2"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsFavorite(Boolean.parseBoolean(jsonObject.getString("isFavorite")));
                itemsModel.setRating(jsonObject.getString("rating"));
                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));

                    itemsModel.setUserImages(imagesModel);
                }
                itemsModel.setPawnedDate(jsonObject.getString("pawned_date"));
                if (!jsonObject.isNull("tags")) {
                    JSONArray tags = jsonObject.getJSONArray("tags");
                    for (int k = 0; k < tags.length(); k++) {
                        listTags.add(tags.get(k).toString());
                    }

                }
                itemsModel.setTags(listTags);
                if (!jsonObject.isNull("location")) {
                    JSONObject location = jsonObject.getJSONObject("location");
                    ItemLocationModel locationModel = new ItemLocationModel();
                    locationModel.setAdministrativeAreaLevel1(location.getString("administrative_area_level_1"));
                    locationModel.setCountry(location.getString("country"));
                    locationModel.setCountryCode(location.getString("country_code"));
                    locationModel.setLat(Float.parseFloat(location.getString("lat")));
                    locationModel.setLng(Float.parseFloat(location.getString("lng")));
                    locationModel.setLocality(location.getString("locality"));
                    locationModel.setPostalCode(location.getString("postal_code"));

                    itemsModel.setLocation(locationModel);
                }

                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));

                    itemsModel.setItemImages(itemImagesModel);
                }

                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }


                itemsModel.setItemIndx(Integer.parseInt(jsonObject.getString("item_indx")));
                itemsModel.setVerifieduser(jsonObject.getInt("is_verified_user"));


                modelArrayList.add(itemsModel);

            }

            /*Setting Up & Show Items Data*/
            setDataAndShowsItems();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setDataAndShowsItems() {

        if (modelArrayList.size() > 0) {
            txtInfoItemsTV.setVisibility(View.GONE);
            recyclerRV.setVisibility(View.VISIBLE);
        } else {
            txtInfoItemsTV.setVisibility(View.VISIBLE);
            recyclerRV.setVisibility(View.GONE);
        }
        mProfileItemsAdapter = new ProfileItem_Adapter(getActivity(), modelArrayList, null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(mProfileItemsAdapter);
        recyclerRV.setNestedScrollingEnabled(false);

    }
}
