package com.membilia.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.volley.UtilsVolley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentChangePassword extends Fragment {
    View itemsview;
    ImageView imgLeftIV;
    private static final String TAG = "FragmentChangePassword";
    EditText currentpasswordET , newpasswordET,confirmpasswordET;
    String strCurrentPassword , strNewPassword , strConfirmPassword;
    Button submitBT;
    JSONObject requestJson;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        itemsview = inflater.inflate(R.layout.fragment_change_password, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemsview);
        setupclicks();
        return itemsview;
    }

    private void setUpViews(View v) {
        imgLeftIV = (ImageView) v.findViewById(R.id.imgLeftIV);
        currentpasswordET = (EditText) v.findViewById(R.id.currentpasswordET);
        newpasswordET = (EditText) v.findViewById(R.id.newpasswordET);
        confirmpasswordET = (EditText) v.findViewById(R.id.confirmpasswordET);
        submitBT = (Button) v.findViewById(R.id.submitBT);
    }

    private void setupclicks() {

        imgLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestJson = new JSONObject();
                strCurrentPassword = currentpasswordET.getText().toString();
                strNewPassword =  newpasswordET.getText().toString();
                strConfirmPassword = confirmpasswordET.getText().toString();
                if (Validate(strCurrentPassword, strNewPassword , strConfirmPassword)){
                    try {
                        requestJson.put("currentPassword",strCurrentPassword);
                        requestJson.put("newPassword",strConfirmPassword);
                        changePasswordService(requestJson);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        });

    }

    private void changePasswordService(final JSONObject requestJson1) {
        Utilities.showProgressDialog(getActivity());
        String strURl = WebServicesConstants.ChangePassword;
        Log.e(TAG, "strURl: "+strURl );
        Log.e(TAG, "changePasswordService: "+requestJson );
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strURl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*******RESPONSE*********: "+response );
                Utilities.hideProgressDialog();
                try {
                    JSONObject responseJson = new JSONObject(response);
                    String message = responseJson.getString("message");
                    if (message.equalsIgnoreCase("true")){
                        showMessage("Password Changed!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                error.printStackTrace();
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                if (json.equalsIgnoreCase("no result found")) {
                                    Toast.makeText(getActivity(), "No more data", Toast.LENGTH_SHORT).show();
                                } else
//                                    Toast.makeText(getActivity(), json, Toast.LENGTH_SHORT).show();
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String,String> header= new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestJson1.toString().getBytes();
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest,WebServicesConstants.ChangePassword);
    }

    private boolean Validate(String strCurrentPassword, String strNewPassword, String strConfirmPassword) {

        if (strCurrentPassword.equalsIgnoreCase("")){
            showMessage("please enter current password");
            return false;
        }
       else if (strCurrentPassword.length()<6){
            showMessage("your current password is wrong");
            return false;
        }
        else if (strNewPassword.equalsIgnoreCase("")){
            showMessage("please enter new password");
            return false;
        }
        else if (strNewPassword.length()<8){
            showMessage("new password should be minimum of 8 letters");
            return false;
        }
        else if (strConfirmPassword.equalsIgnoreCase("")){
            showMessage("please enter confirm password");
            return false;
        }
        else if (strConfirmPassword.length()<8){
            showMessage("new password and current password does not match");
            return false;
        }
        else if (!strNewPassword.equalsIgnoreCase(strConfirmPassword)){
            showMessage("new password and current password does not match");
            return false;
        }
        else {
            return  true;
        }
    }

    private void showMessage(String s) {
        Toast.makeText(getActivity(),s,Toast.LENGTH_SHORT).show();
    }

}
