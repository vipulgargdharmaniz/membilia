package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;

import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniz on 14/7/17.
 */

public class Fragmentaddbankaccount extends Fragment {
    View itemView;
    RelativeLayout searchRL;
    private static final String TAG = "Fragmentaddbankaccount";
    private LinearLayout checkingLL, savingLL;
    private CheckBox checkingCB, savingCB;
    private EditText accountNameET, bankNameET, accountnoET, routingnoET;
    private TextView submitTV, titleTV;
    private String strAccountName, strBankName, strAccountNo, strRountingNo;
    private boolean isSavingAccount = false;
    private String strAccountId;
    private boolean isAddType = false;
    String type;
    static FragmentActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemView = inflater.inflate(R.layout.fragment_add_bank_account, null);
        setUpviews(itemView);
        setupclicks();
        activity = getActivity();
        Bundle args = getArguments();
        if (args != null) {
            try {
                strAccountName = args.getString("accountName");
                strBankName = args.getString("bankName");
                strRountingNo = args.getString("rountingNo");
                strAccountNo = args.getString("accountNo");
                strAccountId = args.getString("accountId");
                isSavingAccount = args.getBoolean("Type");

                accountNameET.setText(strAccountName);
                bankNameET.setText(strBankName);
                accountnoET.setText(strAccountNo);
                routingnoET.setText(strRountingNo);
                if (isSavingAccount) {
                    savingCB.setChecked(true);
                    checkingCB.setChecked(false);
                    savingCB.setTextColor(getActivity().getResources().getColor(R.color.black));
                    checkingCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));


                } else {
                    savingCB.setChecked(false);
                    checkingCB.setChecked(true);
                    checkingCB.setTextColor(getActivity().getResources().getColor(R.color.black));
                    savingCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }
                titleTV.setText(getString(R.string.editbankaccount));

            } catch (Exception e) {
                e.printStackTrace();
            }
            isAddType = false;
        } else {
            titleTV.setText(getString(R.string.addbankaccount));
            isAddType = true;
        }
        return itemView;
    }

    private void setUpviews(View v) {
        searchRL = v.findViewById(R.id.searchRL);
        checkingLL = v.findViewById(R.id.checkingLL);
        savingLL = v.findViewById(R.id.savingLL);
        checkingCB = v.findViewById(R.id.checkingCB);
        savingCB = v.findViewById(R.id.savingCB);
        submitTV = v.findViewById(R.id.submitTV);
        accountNameET = v.findViewById(R.id.accountNameET);
        bankNameET = v.findViewById(R.id.bankNameET);
        accountnoET = v.findViewById(R.id.accountnoET);
        routingnoET = v.findViewById(R.id.routingnoET);
        titleTV = v.findViewById(R.id.titleTV);
    }

    private void setupclicks() {
        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = activity.getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        checkingCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    checkingCB.setChecked(true);
                    savingCB.setChecked(false);
                    checkingCB.setEnabled(false);
                    savingCB.setEnabled(true);
                    isSavingAccount = false;
                    checkingCB.setTextColor(getActivity().getResources().getColor(R.color.black));
                    savingCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }
            }
        });

        savingCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    checkingCB.setChecked(false);
                    savingCB.setChecked(true);
                    checkingCB.setEnabled(true);
                    savingLL.setEnabled(false);
                    isSavingAccount = true;
                    savingCB.setTextColor(getActivity().getResources().getColor(R.color.black));
                    checkingCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }

            }
        });

        savingLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkingCB.setChecked(false);
                savingCB.setChecked(true);
                checkingLL.setEnabled(true);
                savingLL.setEnabled(false);
                isSavingAccount = true;

            }
        });


        submitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strAccountName = accountNameET.getText().toString();
                strBankName = bankNameET.getText().toString();
                strAccountNo = accountnoET.getText().toString();
                strRountingNo = routingnoET.getText().toString();
                if (isValid(strAccountName, strBankName, strAccountNo, strRountingNo)) {

                    if (isAddType) {
                        String strUserID = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
                        addBankAccount(strUserID);
                    } else {
                        String strUserID = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
                        editBankAccount(strUserID);
                    }
                }
            }
        });
    }


    private void setCheckBox(CheckBox trueCB, CheckBox falseCB, boolean b) {
        trueCB.setChecked(true);
        falseCB.setChecked(false);
        isSavingAccount = b;
    }

    private boolean isValid(String strAccountName, String strBankName, String strAccountNo, String strRountingNo) {
        if (strAccountName.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "All fields are required.");
            return false;
        } else if (strBankName.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "All fields are required.");
            return false;
        } else if (strAccountNo.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "All fields are required.");
            return false;
        } else if (strRountingNo.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "All fields are required.");
            return false;
        }
        return true;
    }

    private void printMessage(String s) {
        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
    }

    private void addBankAccount(final String strUserid) {
        String strUrl = WebServicesConstants.AddBankAccount;
        final String type = String.valueOf(isSavingAccount);
        Log.e(TAG, "strUrl: " + strUrl + type);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", strUserid);
                params.put("account_number", strAccountNo);
                params.put("name_on_account", strAccountName);
                params.put("name_on_bank", strBankName);
                params.put("routing_number", strRountingNo);
                params.put("isAccountSaving", type);
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.AddBankAccount);
    }

    private void editBankAccount(String strUserID) {
          type = String.valueOf(isSavingAccount);
        String strUrl = WebServicesConstants.EditBackAccount + strAccountId;
        Log.e(TAG, "strUrl: " + strUrl + type);
        if (type.equalsIgnoreCase("true")){
            type="1";
        }else {
            type = "0";
        }
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("account_id", strAccountId);
                params.put("account_number", strAccountNo);
                params.put("name_on_account", strAccountName);
                params.put("name_on_bank", strBankName);
                params.put("routing_number", strRountingNo);
                params.put("isAccountSaving", type);
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.AddBankAccount);
    }

}
