package com.membilia.Fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.views.RatingBarView;
import com.membilia.volley.UtilsVolley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dharmaniapps on 10/4/18.
 */

public class ReviewScreen extends Fragment {

    private static final String TAG = "GiveReview_Fragment";
    View view;
    RelativeLayout ratingBarRL;
    SimpleDraweeView profileIV, borderRing;
    TextView usernameTV, titleTV;
    ImageView imgVerityUnverity, imgSettingIV;
    RatingBarView ratingBarNew;
    EditText reviewET;
    LinearLayout topbar;
    RelativeLayout leftRL, rightRL;
    String strReview = "", strReviewCount = "";
    JSONObject requestJson;
    String senderName , senderId ,senderImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.give_review, container, false);

        Bundle args = getArguments();
        if (args != null) {
            senderName = args.getString("senderName");
            senderId = args.getString("senderId");
            senderImage = args.getString("senderImage");
        }

        setUpViews(view);
        setupclicks();
        setDeafoultData();
        return view;
    }

    private void setDeafoultData() {
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        rightRL.setVisibility(View.VISIBLE);
        titleTV.setVisibility(View.VISIBLE);
        imgSettingIV.setVisibility(View.VISIBLE);
        titleTV.setText("Leave Review");
        ratingBarRL.setVisibility(View.GONE);
        borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);
        profileIV.setImageURI(Uri.parse(senderImage));
        usernameTV.setTextColor(getActivity().getResources().getColor(R.color.green));
        senderName =senderName.replace("</u>","");
        usernameTV.setText(senderName);
        imgSettingIV.setImageResource(R.drawable.donetick);
    }

    private void setUpViews(View view) {
        ratingBarNew = view.findViewById(R.id.ratingBarNew);
        ratingBarRL =  view.findViewById(R.id.ratingBarRL);
        profileIV =  view.findViewById(R.id.profileIV);
        borderRing = view.findViewById(R.id.borderRing);
        usernameTV = view.findViewById(R.id.usernameTV);
        imgVerityUnverity = view.findViewById(R.id.imgVerityUnverity);
        reviewET =  view.findViewById(R.id.reviewET);
        topbar = view.findViewById(R.id.topbar);
        leftRL =  view.findViewById(R.id.leftRL);
        titleTV =  view.findViewById(R.id.titleTV);
        rightRL =  view.findViewById(R.id.rightRL);
        imgSettingIV = view.findViewById(R.id.imgSettingIV);
    }

    private void setupclicks() {

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        rightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestJson = new JSONObject();
                strReview = reviewET.getText().toString();
                strReviewCount = String.valueOf(ratingBarNew.getStarCount());
                try {
                    requestJson.put("review", strReview);
                    requestJson.put("rating", strReviewCount);
                    requestJson.put("item_id", Constants_AddItem.ITEM_ID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                giveReview();
            }
        });
    }
    private void giveReview() {
        Utilities.showProgressDialog(getActivity());
        String userId = senderId;
        String strUrl = WebServicesConstants.Review + userId + "/review";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject responseJson  = new JSONObject(response);
                    String strMessage = responseJson.getString("messgae");
                    if (strMessage.equalsIgnoreCase("true")){
                        Constants.refreshOrderDetails = "true";
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        fm.popBackStack();
                    }
                    Utilities.hideProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Utilities.hideProgressDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                if (json.equalsIgnoreCase("no result found")) {
                                    Toast.makeText(getActivity(), "No more data", Toast.LENGTH_SHORT).show();
                                } else
                                    Toast.makeText(getActivity(), json, Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.Review);
    }




}
