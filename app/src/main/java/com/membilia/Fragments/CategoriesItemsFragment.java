package com.membilia.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.RecyclerSectionItemDecoration;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.Categories_ItemAdapter;
import com.membilia.adapters.CategoriesItem_Rest;
import com.membilia.volley_models.CategoriesModel;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemLocationModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class CategoriesItemsFragment extends Fragment {
    View itemsView;
    String TAG = "CategoriesItemsFragment";
    RecyclerView recyclerviewRC, recyclerExplore;
    int perPage = 50;
    int page_no = 1;
    private Categories_ItemAdapter mItemsAdapter;
    private CategoriesItem_Rest mRest_Categories;

    ArrayList<ItemsModel> modelArrayList = new ArrayList<>();
    ArrayList<ItemsModel> modelArrayListRest = new ArrayList<>();
    ArrayList<ItemsModel> modelArrayListRestAll = new ArrayList<>();
    List<String> listTags = new ArrayList<String>();
    CategoriesModel mCategoriesModel;
    String strCategoryType = "3playpawn";
    private NestedScrollView mNestedScrollView;
    private TextView txtTitle;
    private RelativeLayout searchRL;

    private LinearLayout ll_progressbar;
    private CoordinatorLayout coordinatorLayout;
    private boolean isNextScroll = true;
    private Toolbar toolbar;
    private ProgressBar progressbar;
    public boolean isRefresh= false;
    JSONObject requestObject ;
    public CategoriesItemsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategoriesModel = (CategoriesModel) getArguments().getSerializable("model");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       if(itemsView==null){
           itemsView = inflater.inflate(R.layout.fragment_categories_items, container, false);
           Log.i(TAG, "onCreateView: ");
           setUpViews();
           setupclicks();
           modelArrayList.clear();
           modelArrayListRest.clear();
           modelArrayListRestAll.clear();

           HomeActivity.categoryClick();
           gettingAllItems();
           isRefresh = true;
       }
       else {
           isRefresh = false;
       }

        return itemsView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isRefresh){
            isRefresh =false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeActivity.onPause = "yes";
    }

    public void setUpViews() {
        recyclerviewRC =  itemsView.findViewById(R.id.recyclerRV);
        mNestedScrollView =  itemsView.findViewById(R.id.mNestedScrollView);


        txtTitle =  itemsView.findViewById(R.id.title);
        searchRL =  itemsView.findViewById(R.id.searchRL);
        recyclerExplore =  itemsView.findViewById(R.id.recyclerExplore);
        toolbar = itemsView.findViewById(R.id.toolbar);
        ll_progressbar =  itemsView.findViewById(R.id.ll_progressbar);
        coordinatorLayout =  itemsView.findViewById(R.id.coardinateLayout);
        progressbar =  itemsView.findViewById(R.id.progressbar);
        /*
         * Getting All Items
         * ****/
        toolbar.setVisibility(View.GONE);
        ll_progressbar.setVisibility(View.VISIBLE);
        coordinatorLayout.setVisibility(View.GONE);
        progressbar.getIndeterminateDrawable().setColorFilter(getActivity().getResources().getColor(R.color.green), android.graphics.PorterDuff.Mode.SRC_ATOP);
    }

    private void setupclicks() {

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                  if (isNextScroll){
                      Log.e(TAG, "onScrollChange: ");
                      ++page_no;
                      gettingAllItems();
                  }}
            }
        });

        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.switchFragment(getActivity(), new CategoryFragment(), Constants.CATEGORIES_FRAGMENT, false, null);

            }
        });
    }


    private void gettingAllItems() {
        if(mCategoriesModel.getName().equalsIgnoreCase("FOOTWEAR")){
            txtTitle.setText("Footwear");
        }
        else if(mCategoriesModel.getName().equalsIgnoreCase("THINGS WORN")){
            txtTitle.setText("Things Worn");
        }
        else if(mCategoriesModel.getName().equalsIgnoreCase("The EXPERIENCE")){
            txtTitle.setText("The Experience");
        }
        else if(mCategoriesModel.getName().equalsIgnoreCase("BALLS,BATS & BAGS")){
            txtTitle.setText("Balls,Bats & Bags");
        }
        else if(mCategoriesModel.getName().equalsIgnoreCase("THINGS THAT MOVE")){
            txtTitle.setText("Things That Move");
        }
        else if(mCategoriesModel.getName().equalsIgnoreCase("FASHION")){
            txtTitle.setText("Fashion");
        }
        else if(mCategoriesModel.getName().equalsIgnoreCase("ACCESSORIES")){
            txtTitle.setText("Accessories");
        }
        else if(mCategoriesModel.getName().equalsIgnoreCase("MISCELLANEOUS")){
            txtTitle.setText("Miscellaneous");
        }

        strCategoryType = mCategoriesModel.getName();
        getAllItems123();

//        String strUrl = WebServicesConstants.EXPLORE_ITEMS + "?perPage=" + perPage + "&page_no=" + page_no;
//        Log.e(TAG, "strUrl: " + strUrl);
//        StringRequest postRequest = new StringRequest(Request.Method.GET, strUrl,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        // response
//                        Log.e("Response", response);
//                        try {
//                            JSONArray jsonArray = new JSONArray(response);
//                            if (jsonArray.length()>0){
//                                isNextScroll = true;
//                            }
//                            else if (jsonArray.length()==0 || jsonArray.length()<perPage) {
//                                isNextScroll = false;
//                            }
//                            parseResponse(jsonArray);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        // error
//                        Log.d("Error.Response", error.toString());
//
//                        String json;
//
//                        ll_progressbar.setVisibility(View.GONE);
//
//                        NetworkResponse response = error.networkResponse;
//                        if (response != null && response.data != null) {
//                            switch (response.statusCode) {
//                                case 403:
//                                    json = new String(response.data);
//                                    json = UtilsVolley.trimMessage(json, "message");
//                                    if (json != null)
//                                         AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
//                                        Log.e(TAG, "******ERROR******" + json);
//                                    break;
//                            }
//                        }
//                    }
//                }
//        ) {
//
//            @Override
//            public Map getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
//                headers.put("Content-Type", "application/json");
//                Log.d(TAG, "toke: " + PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
//                return headers;
//            }
//        };
//        PlayerPawnApplication.getInstance().
//
//                addToRequestQueue(postRequest);

    }

    private void getAllItems123() {
        String strUrl = WebServicesConstants.NotVerifiedUsers;
        Log.e(TAG, "strUrl: " + strUrl + "....." + page_no);

        try {
            requestObject = new JSONObject();
            requestObject.put("perPage", perPage);
            requestObject.put("page_no", page_no);
            requestObject.put("filterApply", "1");
            requestObject.put("verified", "0");
            Log.e(TAG, "requestObject: " + requestObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length()>0){
                        isNextScroll = true;
                    }
                    else if (jsonArray.length()==0 || jsonArray.length()<perPage) {
                        isNextScroll = false;
                    }
                    parseResponse(jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.NotVerifiedUsers);
    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    private void parseResponse(JSONArray response) {

        String catname = mCategoriesModel.getName();
        if (page_no>1){
            modelArrayListRest.clear();
        }

        Log.e(TAG, "catgegory selected: "+catname );
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setLastUsed(jsonObject.getString("last_used"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setIsAutographed(Boolean.parseBoolean(jsonObject.getString("isAutographed")));
                itemsModel.setCategory(jsonObject.getString("category"));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setCharityName(jsonObject.getString("charity_name"));
                itemsModel.setCurrency(jsonObject.getString("currency"));
                itemsModel.setDeliveryType(Boolean.parseBoolean(jsonObject.getString("delivery_type")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setWeight(jsonObject.getString("weight"));
                itemsModel.setCreated(jsonObject.getString("created"));
                itemsModel.setStatus(jsonObject.getString("status"));
                itemsModel.setWidth(jsonObject.getString("width"));
                itemsModel.setHeight(jsonObject.getString("height"));
                itemsModel.setLength(jsonObject.getString("length"));
                itemsModel.setPersonTransfertext(jsonObject.getString("person_transfertext"));
                itemsModel.setNewtimestamp(jsonObject.getString("newtimestamp"));
                itemsModel.setOnHold(jsonObject.getString("onHold"));
                itemsModel.setStatus2(jsonObject.getString("status2"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsFavorite(Boolean.parseBoolean(jsonObject.getString("isFavorite")));
                itemsModel.setRating(jsonObject.getString("rating"));
                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));

                    itemsModel.setUserImages(imagesModel);
                }
                itemsModel.setPawnedDate(jsonObject.getString("pawned_date"));
                if (!jsonObject.isNull("tags")) {
                    JSONArray tags = jsonObject.getJSONArray("tags");
                    for (int k = 0; k < tags.length(); k++) {
                        listTags.add(tags.get(k).toString());
                    }

                }
                itemsModel.setTags(listTags);
                if (!jsonObject.isNull("location")) {
                    JSONObject location = jsonObject.getJSONObject("location");
                    ItemLocationModel locationModel = new ItemLocationModel();
                    locationModel.setAdministrativeAreaLevel1(location.getString("administrative_area_level_1"));
                    locationModel.setCountry(location.getString("country"));
                    locationModel.setCountryCode(location.getString("country_code"));
                    locationModel.setLat(Float.parseFloat(location.getString("lat")));
                    locationModel.setLng(Float.parseFloat(location.getString("lng")));
                    locationModel.setLocality(location.getString("locality"));
                    locationModel.setPostalCode(location.getString("postal_code"));

                    itemsModel.setLocation(locationModel);
                }

                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));

                    itemsModel.setItemImages(itemImagesModel);
                }

                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }


                itemsModel.setItemIndx(Integer.parseInt(jsonObject.getString("item_indx")));
                itemsModel.setVerifieduser(jsonObject.getInt("verified"));


                if (page_no==1){
                    if (itemsModel.getCategory().equalsIgnoreCase(catname)){
                        modelArrayList.add(itemsModel);
                    }
                    else {
                    modelArrayListRest.add(itemsModel);
                    }
                }
                else {
                    if (!itemsModel.getCategory().equalsIgnoreCase(catname)){
                        Log.e(TAG, "refresh rest: " );
                        modelArrayListRest.add(itemsModel);
                    }
//                    else {
//                        Log.e(TAG, "refresh  " );
//                    }
                }

            }
            if (modelArrayListRest.size()>0){
                modelArrayListRestAll.addAll(modelArrayListRest);
            }


            Log.e(TAG, "modelArrayListRest: "+modelArrayListRest.size() );
            Log.e(TAG, "modelArrayList: "+modelArrayList.size() );


            /*Setting Up & Show Items Data*/
            setDataAndShowsItems();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDataAndShowsItems() {
        RecyclerSectionItemDecoration sectionItemDecoration =
                new RecyclerSectionItemDecoration(getResources().getDimensionPixelSize(R.dimen.size_40_dp),
                        true,
                        getSectionCallback("OtherPost"));


        Log.e(TAG, "All::modelArrayList: "+modelArrayList.size() );
        mItemsAdapter = new Categories_ItemAdapter(getActivity(), modelArrayList, null , CategoriesItemsFragment.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerviewRC.setNestedScrollingEnabled(false);
        recyclerviewRC.setLayoutManager(mLayoutManager);
//        recyclerviewRC.setItemAnimator(new DefaultItemAnimator());
        recyclerviewRC.setAdapter(mItemsAdapter);




        Log.e(TAG, "All::modelArrayListRest: "+modelArrayListRest.size() );
        mRest_Categories = new CategoriesItem_Rest(getActivity(), modelArrayListRestAll, null,CategoriesItemsFragment.this);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        recyclerExplore.setNestedScrollingEnabled(false);
        mLayoutManager1.setAutoMeasureEnabled(true);
        recyclerExplore.setLayoutManager(mLayoutManager1);

        recyclerExplore.setItemAnimator(new DefaultItemAnimator());
        recyclerExplore.setAdapter(mRest_Categories);

        ll_progressbar.setVisibility(View.GONE);
        coordinatorLayout.setVisibility(View.VISIBLE);
        toolbar.setVisibility(View.VISIBLE);
    }
    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final String people) {
        return new RecyclerSectionItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int position) {
                return position == 0;
            }

            @Override
            public CharSequence getSectionHeader(int position) {
                String data = "Other Post";
                return data;

            }
        };
    }

//    public void setHeader(boolean visibility){
//        Log.e(TAG, "setHeader: " +visibility);
//        if (visibility){
//            txt_otherpost.setVisibility(View.VISIBLE);
//        }
//        else {
//            txt_otherpost.setVisibility(View.GONE);
//        }
//
//    }

}
