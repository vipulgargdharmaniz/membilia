package com.membilia.Fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.SwipeHelper;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ShippingAddressAdatper;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ShippingAddressModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentShowShippingAddress extends Fragment {
    String TAG = "ShowShippingAddress";
    View itemsview;

    TextView addshipping, titleTV;
    RecyclerView showaddressRV;
    LinearLayout topbar;
    RelativeLayout leftRL;
    Bundle bundle = new Bundle();
    ArrayList<ShippingAddressModel> modelArrayList = new ArrayList<>();
    ShippingAddressAdatper mShippingAddressAdatper;
    String strstreet, straprt, strcity, strcountry, strpostal, strstate;
    SwipeHelper swipeHelper;
    String strCameFrom;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        itemsview = inflater.inflate(R.layout.fragment_show_shipping_address, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemsview);
        setupclicks();
        Bundle args = getArguments();

        if (args != null) {
           try {
               strCameFrom = args.getString("from");
           }catch (Exception e){
               e.printStackTrace();
           }
        }

        if (args != null) {
            strstreet = args.getString("street");
            straprt = args.getString("aprt");
            strcity = args.getString("city");
            strstate = args.getString("state");
            strcountry = args.getString("country");
            strpostal = args.getString("postal");
        }
        return itemsview;
    }

    private void setUpViews(View v) {

        addshipping = v.findViewById(R.id.addshipping);
        showaddressRV = v.findViewById(R.id.showaddressRV);
        topbar = v.findViewById(R.id.topbar);
        leftRL = v.findViewById(R.id.leftRL);
        titleTV = v.findViewById(R.id.titleTV);
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        titleTV.setText(getActivity().getResources().getString(R.string.shippingaddress));
    }

    @Override
    public void onResume() {
        super.onResume();
        gettingAllAddresses();
    }

    private void setupclicks() {

        addshipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.SelectedCountry = "";
                HomeActivity.switchFragment(getActivity(), new FragmentShippingAddress(), Constants.Fragment_Shipping_Address, true, null);
            }
        });
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    public void gettingAllAddresses() {
        Utilities.showProgressDialog(getActivity());
        modelArrayList.clear();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(WebServicesConstants.ALL_ADDRESSES, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, "onResponse All Address*********" + response.toString());
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ItemsRequest");
    }


    private void parseResponse(JSONArray response) {
        try {

            if (response.length()==1){
                JSONObject jsonObject = response.getJSONObject(0);
                ShippingAddressModel mShippingAddressModel = new ShippingAddressModel();
                mShippingAddressModel.setAddress_id(jsonObject.getString("address_id"));
                mShippingAddressModel.setUser_id(jsonObject.getString("user_id"));
                if (!jsonObject.isNull("address_line_one"))
                    mShippingAddressModel.setAddress_line_one(jsonObject.getString("address_line_one"));
                if (!jsonObject.isNull("address_line_two"))
                    mShippingAddressModel.setAddress_line_two(jsonObject.getString("address_line_two"));
                mShippingAddressModel.setCity(jsonObject.getString("city"));
                mShippingAddressModel.setState(jsonObject.getString("state"));
                mShippingAddressModel.setCountry(jsonObject.getString("country"));
                mShippingAddressModel.setCountry_code(jsonObject.getString("country_code"));
                mShippingAddressModel.setZipcode(jsonObject.getString("zipcode"));
                mShippingAddressModel.setStatus(jsonObject.getString("status"));
                mShippingAddressModel.setCreated(jsonObject.getString("created"));
                mShippingAddressModel.setIsDefault(jsonObject.getString("isDefault"));
                modelArrayList.add(mShippingAddressModel);
                setAddressDefault(modelArrayList.get(0).getAddress_id());

            }
            else {
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObject = response.getJSONObject(i);
                    ShippingAddressModel mShippingAddressModel = new ShippingAddressModel();
                    mShippingAddressModel.setAddress_id(jsonObject.getString("address_id"));
                    mShippingAddressModel.setUser_id(jsonObject.getString("user_id"));
                    if (!jsonObject.isNull("address_line_one"))
                        mShippingAddressModel.setAddress_line_one(jsonObject.getString("address_line_one"));
                    if (!jsonObject.isNull("address_line_two"))
                        mShippingAddressModel.setAddress_line_two(jsonObject.getString("address_line_two"));
                    mShippingAddressModel.setCity(jsonObject.getString("city"));
                    mShippingAddressModel.setState(jsonObject.getString("state"));
                    mShippingAddressModel.setCountry(jsonObject.getString("country"));
                    mShippingAddressModel.setCountry_code(jsonObject.getString("country_code"));
                    mShippingAddressModel.setZipcode(jsonObject.getString("zipcode"));
                    mShippingAddressModel.setStatus(jsonObject.getString("status"));
                    mShippingAddressModel.setCreated(jsonObject.getString("created"));
                    mShippingAddressModel.setIsDefault(jsonObject.getString("isDefault"));
                    modelArrayList.add(mShippingAddressModel);
                }
                //Set Up Adatper
                settingUpAdatper();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void settingUpAdatper() {
        mShippingAddressAdatper = new ShippingAddressAdatper(getActivity(), modelArrayList, FragmentShowShippingAddress.this,strCameFrom);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        showaddressRV.setLayoutManager(mLayoutManager);
        showaddressRV.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                showaddressRV.getContext(),
                mLayoutManager.getOrientation());
        showaddressRV.addItemDecoration(mDividerItemDecoration);
        showaddressRV.setAdapter(mShippingAddressAdatper);
        Utilities.hideProgressDialog();

        swipeHelper = new SwipeHelper(getActivity(), showaddressRV) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(
                        "Delete",
                        0,
                        Color.parseColor("#d74e59"),
                        new SwipeHelper.UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                // TODO: onDelete
                                dialogbox(pos);
                            }
                        }
                ));
            }
        };
    }


    private void dialogbox(final int pos) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        // set the custom dialog components - text, image and button
        LinearLayout yesll =  dialog.findViewById(R.id.yesll);
        LinearLayout noll =  dialog.findViewById(R.id.noll);
        TextView messageTV =  dialog.findViewById(R.id.messageTV);
        messageTV.setText("Are you sure, you want to delete Shipping address?");
        noll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        yesll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                deleteAccount(pos);
            }
        });
        dialog.show();
    }


    private void deleteAccount(final int pos) {
        String strUrl = WebServicesConstants.SAVE_USER_ADDRESS + "/" +modelArrayList.get(pos).getAddress_id();
        Utilities.showProgressDialog(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "onResponse: "+response );
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    if (message.equalsIgnoreCase("true")){
                        modelArrayList.remove(pos);
                        mShippingAddressAdatper.notifyDataSetChanged();
                        if (modelArrayList.size()>0){
                            setAddressDefault(modelArrayList.get(0).getAddress_id());
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return header;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.DeleteAccount);
    }

    public void backstack() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();
    }

    public void editAddress(String street, String aprtTV, String cityTV, String stateTV, String countryTV, String zipCode, String addressId) {
        Bundle bundle = new Bundle();
        bundle.putString("street", street);
        bundle.putString("aprt", aprtTV);
        bundle.putString("city", cityTV);
        bundle.putString("state", stateTV);
        bundle.putString("country", countryTV);
        bundle.putString("zipcode", zipCode);
        bundle.putString("addressId", addressId);
        HomeActivity.switchFragment(getActivity(), new FragmentShippingAddress(), Constants.Fragment_Shipping_Address, true, bundle);
    }


    private void setAddressDefault(String address_id) {
        String url = WebServicesConstants.DefaultAddress + address_id;
//        Log.e(TAG, "url: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse DefaultAdrress " + response);
                JSONObject jsonObject ;
                try {
                    jsonObject = new JSONObject(response);
                    boolean message = jsonObject.getBoolean("message");
                    if (message) {
                        settingUpAdatper();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Utilities.showProgressDialog(getActivity());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                Utilities.hideProgressDialog();
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.DefaultAddress);
    }
}