package com.membilia.Fragments;

/*
 * Created by dharmaniapps on 2/2/18.
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayPawnPref_Once;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley.UtilsVolley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NonVerifyUserFragment extends Fragment {

    private static final String TAG = "NonVerifyUserFragment";
    LinearLayout cancelLL;
    TextView postTV, txtSubmitTV;
    EditText editNameET, editReasonOfEnqiryET, editPhoneNoET, editEmailAddressET;
    String strName, strReasonEnquiry, strPhoneNumber, strEmailAddress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_add_item_for_not_verify_user, null);
        setView(view);
        setOnClick();
        HomeActivity.bottombar.setVisibility(View.GONE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
        HomeActivity.isAddItemOpen = true;
        return view;
    }


    private void setView(View view) {
        cancelLL = view.findViewById(R.id.layoutCancel);
        postTV = view.findViewById(R.id.postTV);
        txtSubmitTV = view.findViewById(R.id.txtSubmitTV);
        editNameET = view.findViewById(R.id.editNameET);
        editReasonOfEnqiryET = view.findViewById(R.id.editReasonOfEnqiryET);
        editPhoneNoET = view.findViewById(R.id.editPhoneNoET);
        editEmailAddressET = view.findViewById(R.id.editEmailAddressET);
    }

    private void setOnClick() {
        cancelLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();
                HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
                HomeActivity.bottombar.setVisibility(View.VISIBLE);
            }
        });

        postTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlayPawnPref_Once.writeString(getActivity(), PlayPawnPref_Once.ApplyasFan, "true");
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();
                Constants.OpenAddItemFrom = "home";
                HomeActivity.switchFragment(getActivity(), new AddItemFragment(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, true, null);
            }
        });

        txtSubmitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyForVerify();
            }
        });
    }

    private void applyForVerify() {
        strName = editNameET.getText().toString();
        strReasonEnquiry = editReasonOfEnqiryET.getText().toString();
        strPhoneNumber = editPhoneNoET.getText().toString();
        strEmailAddress = editEmailAddressET.getText().toString();
        if (strName.equals("")) {
            AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), "Name is required.");
        } else if (strReasonEnquiry.equals("")) {
            AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), "Reason of inquiry is required.");
        } else if (strPhoneNumber.equals("")) {
            AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), "Phone number is required.");
        } else if (strEmailAddress.equals("")) {
            AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), "Email is required.");
        } else if (!Utilities.emailValidator(strEmailAddress)) {
            AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), getActivity().getResources().getString(R.string.entervalidemailaddress));
        } else {
                    /*Execute API*/
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", strName);
                jsonObject.put("email", strEmailAddress);
                jsonObject.put("phone", strPhoneNumber);
                jsonObject.put("reason", strReasonEnquiry);
                hitService(jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void hitService(final JSONObject jsonObject) {
        String strUrl = WebServicesConstants.APPLY_AS_SELLER;

        Utilities.showProgressDialog(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                Utilities.hideProgressDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("messgae")) {
                        String message = jsonObject.getString("messgae");
                        AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                String json;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), "You have already requested for seller access. We are reviewing your account and will get back to your soon.");
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

}
