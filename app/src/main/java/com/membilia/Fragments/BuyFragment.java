package com.membilia.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ShippingAddressModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * Created by dharmaniapps on 19/12/17.
 */

public class BuyFragment extends Fragment {
    View view;
    LinearLayout topbar, shippingaddressLL, shippingCostLL, purchasepopLL, tollbarLL;
    RelativeLayout leftRL;
    TextView itemNameTV, itemPrizeTV, itemStoryTV, shippingClick, continueBT;
    TextView streetTV, aprtTV, cityTV, districtTV, countryTV, postalcodeTV, changeAddressTV, deliverymethodTV;
    TextView priceTV, shippingPriceTV, payprizetxt, paynowTXT, titleTV;
    SimpleDraweeView itemImgIV;
    private static final String TAG = "BuyFragment";
    ArrayList<ShippingAddressModel> modelArrayList = new ArrayList<ShippingAddressModel>();
    private String countryname = "";
    boolean isCountryUSA = false;
    int shippingCost = 25;
    int finalCost, actualCost;
    String strShippingCost = "";
    String shippingAdsID = "";
    HomeActivity activity;
    boolean isShipping = false;

    String finalPrizetopay;
    String number;
    double amount;
    DecimalFormat formatter;
    String formatted;
    /*
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    //private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
    //    private static final String CONFIG_CLIENT_ID = "AdVKNkLizxsgP1HvS2yoBWIwrqpOviAKnOWMyXVJh8hAwlz4pY69l28aWu6Xqrl7ju1t4uQWueL9xg9u";
    //private static final String CONFIG_CLIENT_ID = "AdVKNkLizxsgP1HvS2yoBWIwrqpOviAKnOWMyXVJh8hAwlz4pY69l28aWu6Xqrl7ju1t4uQWueL9xg9u";

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    private static final String CONFIG_CLIENT_ID = "AQmveErGD0pon6xSe8uMXDYweS80a4e1LxXLSpNi9Hqru2ABUwEEu39yN6PAsezlweLf1LG2-vdqVZux";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.//.merchantName("Player Pawn")
            .merchantName("Player Pawn")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));//Player Pawn

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.buy_fragment, null);
        onSetView(view);
        onSetClick();
        activity = new HomeActivity();
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);

        return view;
    }

    private void onSetView(View view) {
        topbar = view.findViewById(R.id.topbar);
        leftRL = view.findViewById(R.id.leftRL);
        shippingaddressLL = view.findViewById(R.id.shippingaddressLL);
        shippingCostLL = view.findViewById(R.id.shippingCostLL);
        itemNameTV = view.findViewById(R.id.itemNameTV);
        itemPrizeTV = view.findViewById(R.id.itemPrizeTV);
        itemStoryTV = view.findViewById(R.id.itemStoryTV);
//        itemStoryTV.setMovementMethod(new ScrollingMovementMethod());
        streetTV = view.findViewById(R.id.streetTV);
        aprtTV = view.findViewById(R.id.aprtTV);
        cityTV = view.findViewById(R.id.cityTV);
        districtTV = view.findViewById(R.id.districtTV);
        countryTV = view.findViewById(R.id.countryTV);
        postalcodeTV = view.findViewById(R.id.postalcodeTV);
        changeAddressTV = view.findViewById(R.id.changeAddressTV);

        deliverymethodTV = view.findViewById(R.id.deliverymethodTV);
        priceTV = view.findViewById(R.id.priceTV);
        shippingPriceTV = view.findViewById(R.id.shippingPriceTV);
        payprizetxt = view.findViewById(R.id.payprizetxt);
        paynowTXT = view.findViewById(R.id.paynowTXT);
        titleTV = view.findViewById(R.id.titleTV);
        itemImgIV = view.findViewById(R.id.itemImgIV);
        shippingClick = view.findViewById(R.id.shippingClick);
        continueBT = view.findViewById(R.id.continueBT);
        purchasepopLL = view.findViewById(R.id.purchasepopLL);
        tollbarLL = view.findViewById(R.id.tollbarLL);
        String strcharity = "<u>" + "Shipping" + "</u>";

        shippingClick.setText(Html.fromHtml(strcharity));
        tollbarLL.setVisibility(View.VISIBLE);
    }

    private void onSetClick() {
        changeAddressTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("from", "buy");
                HomeActivity.switchFragment(getActivity(), new FragmentShowShippingAddress(), Constants.Fragment_Show_Shipping_Address, true, bundle);
            }
        });
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUnHoldItem();

            }
        });
        paynowTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAvailbility();
            }
        });

        shippingClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.switchFragment(getActivity(), new ShippingMsgFragment(), "ShippingFragment", true, null);
            }
        });

        continueBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        onSetDefaultData();
        gettingAllAddresses();
    }

    private void checkAvailbility() {
        String strUrl = WebServicesConstants.CheckItem;
        Log.e(TAG, "strUrl: " + strUrl);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("item_id", Constants_AddItem.ITEM_ID);
            jsonObject.put("user_id", HomeActivity.UserItself);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "checkAvailbility:jsonObject " + jsonObject);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                setHoldItem();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                try {
                    NetworkResponse response = error.networkResponse;
                    String json;
                    if (response != null && response.data != null) {
                        switch (response.statusCode) {
                            case 403:
                                json = new String(response.data);
                                json = UtilsVolley.trimMessage(json, "message");
                                Log.e(TAG, "onErrorResponse: " + json);
                                if (json != null)
                                    showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                                break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void setHoldItem() {
        String strUrl = WebServicesConstants.HoldItem;
        Log.e(TAG, "strUrl: " + strUrl + Constants_AddItem.ITEM_ID);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("item_id", Constants_AddItem.ITEM_ID);
            jsonObject.put("hold", "1");
            jsonObject.put("user_id", HomeActivity.UserItself);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
        if (isShipping) {
            if (!shippingAdsID.equalsIgnoreCase("")) {
                ifAvailable();
            } else {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please add shipping address");
            }
        } else {
            ifAvailable();
        }

    }


    private void setUnHoldItem() {
        String strUrl = WebServicesConstants.HoldItem;
        Log.e(TAG, "strUrl: " + strUrl);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("item_id", Constants_AddItem.ITEM_ID);
            jsonObject.put("hold", "2");
            jsonObject.put("user_id", HomeActivity.UserItself);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);

        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();
    }

    private void ifAvailable() {
//                Intent intent = new Intent(getActivity(), Payment_Activity.class);
//                getActivity().startActivity(intent);
//                getActivity().overridePendingTransition(R.anim.in_from_right, R.anim.animback);
          /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */

        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    public void onResume() {
        super.onResume();
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        titleTV.setText(getActivity().getResources().getString(R.string.Buy));
    }

    private void onSetDefaultData() {
        itemNameTV.setText(Constants_AddItem.ITEM_NAME);
        if (Constants_AddItem.ITEM_DELVIERYTYPE.equalsIgnoreCase("true")) {
            deliverymethodTV.setText(getActivity().getResources().getString(R.string.personaltransfer));
            shippingaddressLL.setVisibility(View.GONE);
            shippingCostLL.setVisibility(View.GONE);
            shippingAdsID = "";
            isShipping = false;
        } else {
            deliverymethodTV.setText(getActivity().getResources().getString(R.string.shipping));
            shippingaddressLL.setVisibility(View.VISIBLE);
            shippingCostLL.setVisibility(View.VISIBLE);
            isShipping = true;
        }
        number = String.valueOf(Constants_AddItem.ITEM_PRICE.replace("$", ""));
        number = number.replace(",", "");
        amount = Double.parseDouble(number);
        formatter = new DecimalFormat("#,###");
        formatted = formatter.format(amount);
        itemPrizeTV.setText("$" + formatted);


        if (Constants_AddItem.ITEM_PRICE != null) {
            itemStoryTV.setText(Constants_AddItem.ITEM_STORY);
            number = String.valueOf(Constants_AddItem.ITEM_PRICE.replace("$", ""));
            number = number.replace(",", "");
            amount = Double.parseDouble(number);
            formatter = new DecimalFormat("#,###");
            formatted = formatter.format(amount);
            payprizetxt.setText(formatted);

            amount = Double.parseDouble(number);
            formatter = new DecimalFormat("#,###");
            formatted = formatter.format(amount);
            priceTV.setText("$" + formatted);

        }
        String itemPrize = Constants_AddItem.ITEM_PRICE.replace("$", "");
        itemPrize = itemPrize.replace(",", "");
        actualCost = Integer.parseInt(itemPrize);


        if (!Constants_AddItem.videoimage.equalsIgnoreCase("")) {
            Uri image = Uri.parse(Constants_AddItem.videoimage);
            itemImgIV.setImageURI(image);
        } else {
            Uri image = Uri.parse(Constants_AddItem.imagepath1);
            itemImgIV.setImageURI(image);
        }
    }

    private void gettingAllAddresses() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(WebServicesConstants.ALL_ADDRESSES, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, "onResponse*********" + response.toString());
                Log.e(TAG, "onResponse: " + response.length());
                if (response.length() == 0) {
                    changeAddressTV.setText("Add Address");
                    changeAddressTV.setVisibility(View.VISIBLE);
                } else {
                    parseResponse(response);
                    changeAddressTV.setText("Change Address");
                    changeAddressTV.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, WebServicesConstants.ALL_ADDRESSES);
    }

    private void parseResponse(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ShippingAddressModel mShippingAddressModel = new ShippingAddressModel();
                mShippingAddressModel.setAddress_id(jsonObject.getString("address_id"));
                if (i == 0) {
                    shippingAdsID = jsonObject.getString("address_id");
                }
                mShippingAddressModel.setUser_id(jsonObject.getString("user_id"));
                if (!jsonObject.isNull("address_line_one"))
                    mShippingAddressModel.setAddress_line_one(jsonObject.getString("address_line_one"));
                if (!jsonObject.isNull("address_line_two"))
                    mShippingAddressModel.setAddress_line_two(jsonObject.getString("address_line_two"));
                mShippingAddressModel.setCity(jsonObject.getString("city"));
                mShippingAddressModel.setState(jsonObject.getString("state"));
                mShippingAddressModel.setCountry(jsonObject.getString("country"));
                mShippingAddressModel.setCountry_code(jsonObject.getString("country_code"));
                mShippingAddressModel.setZipcode(jsonObject.getString("zipcode"));
                mShippingAddressModel.setStatus(jsonObject.getString("status"));
                mShippingAddressModel.setCreated(jsonObject.getString("created"));
                mShippingAddressModel.setIsDefault(jsonObject.getString("isDefault"));
                modelArrayList.add(mShippingAddressModel);
            }
            //Set Up Adatper
            setAddressFeilds();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAddressFeilds() {
        for (int i = 0; i < modelArrayList.size(); i++) {
            if (modelArrayList.get(i).getIsDefault().equalsIgnoreCase("true")) {
                streetTV.setText(modelArrayList.get(i).getAddress_line_one());
                aprtTV.setText(modelArrayList.get(i).getAddress_line_two());
                cityTV.setText(modelArrayList.get(i).getCity());
                districtTV.setText(modelArrayList.get(i).getState());
                countryTV.setText(modelArrayList.get(i).getCountry());
                postalcodeTV.setText(modelArrayList.get(i).getZipcode());
                countryname = modelArrayList.get(i).getCountry();
                if (countryname.contains("United")) {
                    isCountryUSA = true;
                }
            }
        }
        if (isCountryUSA) {
            if (Constants_AddItem.ITEM_COUNTRY.equalsIgnoreCase(countryname)) {
                shippingPriceTV.setText("$25");
                shippingCost = 25;
                strShippingCost = "25";
            } else {
                shippingPriceTV.setText("$40");
                strShippingCost = "40";
                shippingCost = 40;
            }
        } else {
            shippingPriceTV.setText("$40");
            strShippingCost = "40";
            shippingCost = 40;
        }
        if (isShipping) {
            finalCost = shippingCost + actualCost;

            number = String.valueOf(finalCost);
            amount = Double.parseDouble(number);
            formatter = new DecimalFormat("#,###");
            formatted = formatter.format(amount);
            payprizetxt.setText("$" + formatted);

            finalPrizetopay = String.valueOf(finalCost);
        } else {

            number = String.valueOf(actualCost);
            amount = Double.parseDouble(number);
            formatter = new DecimalFormat("#,###");
            formatted = formatter.format(amount);
            payprizetxt.setText("$" + formatted);
            finalPrizetopay = String.valueOf(actualCost);
            shippingCost = 0;
        }
    }

    /*
   * This method shows use of optional payment details and item list.
   */
    private PayPalPayment getStuffToBuy(String paymentIntent) {
        //--- include an item list, payment amount details
        Log.e(TAG, "actualCost: " + actualCost);
        PayPalItem[] items =
                {
                        new PayPalItem(Constants_AddItem.ITEM_NAME, 1, new BigDecimal(actualCost), "USD",
                                Constants_AddItem.ITEM_ID),
                };
        Log.e(TAG, "strShippingCost: " + finalPrizetopay);
        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal(shippingCost);
        BigDecimal tax = new BigDecimal("0.0");
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);

        PayPalPayment payment = new PayPalPayment(amount, "USD", Constants_AddItem.ITEM_NAME, paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);

        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
        payment.custom("This is text that will be associated with the payment that the app can use.");
        return payment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        confirm.getProofOfPayment().toJSONObject().toString();
                        Log.e(TAG, "confirm.getProofOfPayment(): " + confirm.getProofOfPayment().toJSONObject().toString());
                        Log.i(TAG, "confirm " + confirm.toJSONObject().toString(4));
                        Log.i(TAG, "getpayment" + confirm.getPayment().toJSONObject().toString(4));
                        /*
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */


                        parseData(confirm.toJSONObject().toString());

//                        displayResultText("PaymentConfirmation info received from PayPal");

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private void parseData(String jsonObject) {
        try {
            Utilities.showProgressDialog(getActivity());
            JSONObject mainJson = new JSONObject(jsonObject);
            String response = mainJson.getString("response");
            JSONObject jsonRespone = new JSONObject(response);
            String id = jsonRespone.getString("id");
            Log.e(TAG, "parseData: " + id);
            JSONObject requestJson = new JSONObject();
            requestJson.put("item_id", Constants_AddItem.ITEM_ID);
            requestJson.put("transaction_id", id);
            requestJson.put("shippo_id", "12345");
            requestJson.put("address_id", shippingAdsID);
            requestJson.put("transfer_price", shippingCost);

            Log.e(TAG, "request Json: " + requestJson);
            if (!id.equalsIgnoreCase("")) {
                paymentService(requestJson);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utilities.hideProgressDialog();
        }
    }

    private void paymentService(final JSONObject requestJson) {
        String strURl = WebServicesConstants.Payment;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strURl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                Utilities.hideProgressDialog();
//                activity.getAllOrderHistory();
                Constants.FragmentRefresh = "true";
                purchasepopLL.setVisibility(View.VISIBLE);
                HomeActivity.bottombar.setVisibility(View.GONE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
                tollbarLL.setVisibility(View.VISIBLE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            Log.e(TAG, "onErrorResponse: " + json);
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                return requestJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.Payment);
    }


    public void showAlertDialog(final Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_showalert);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
        alertDialog.show();
    }
}
