package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dharmaniapps on 23/11/17.
 */

public class Report_Fragment extends Fragment {

    private View view;
    LinearLayout ll1,ll2,ll3,ll4,ll5,ll6 , submitll ,topbar;
    ImageView chk1,chk2,chk3,chk4,chk5,chk6;
    TextView txt1, txt2,txt3,txt4,txt5,txt6 , titleTV;
    RelativeLayout leftRL;
    String strReport = "",strItemId="";
    String strURL = WebServicesConstants.BASE_URL + "report.php";
    private static final String TAG = "Report_Fragment";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            strItemId = args.getString("useritemid");
            Log.e(TAG, "**ITEM_ID***" + strItemId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view =  inflater.inflate(R.layout.report_fragment, null);
       setView(view);
       setitemVisible(view);
       setonclick();

        return view;
    }

    private void setView(View view) {
        ll1 = (LinearLayout) view.findViewById(R.id.ll1);
        ll2 = (LinearLayout) view.findViewById(R.id.ll2);
        ll3 = (LinearLayout) view.findViewById(R.id.ll3);
        ll4 = (LinearLayout) view.findViewById(R.id.ll4);
        ll5 = (LinearLayout) view.findViewById(R.id.ll5);
        ll6 = (LinearLayout) view.findViewById(R.id.ll6);

        submitll= (LinearLayout) view.findViewById(R.id.submitll);
        topbar = (LinearLayout) view.findViewById(R.id.topbar);
        leftRL = (RelativeLayout) view.findViewById(R.id.leftRL);

        chk1 = (ImageView) view.findViewById(R.id.chk1);
        chk2 = (ImageView) view.findViewById(R.id.chk2);
        chk3 = (ImageView) view.findViewById(R.id.chk3);
        chk4 = (ImageView) view.findViewById(R.id.chk4);
        chk5 = (ImageView) view.findViewById(R.id.chk5);
        chk6 = (ImageView) view.findViewById(R.id.chk6);


        txt1 = (TextView) view.findViewById(R.id.txt1);
        txt2 = (TextView) view.findViewById(R.id.txt2);
        txt3 = (TextView) view.findViewById(R.id.txt3);
        txt4 = (TextView) view.findViewById(R.id.txt4);
        txt5 = (TextView) view.findViewById(R.id.txt5);
        txt6 = (TextView) view.findViewById(R.id.txt6);
        titleTV = (TextView) view.findViewById(R.id.titleTV);
    }

    private void setitemVisible(View view) {
        topbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        titleTV.setVisibility(View.VISIBLE);
        titleTV.setText(getActivity().getResources().getString(R.string.reportabuse));
        chk1.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.check_peach));
        chk2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
        chk3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
        chk4.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
        chk5.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
        chk6.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
        strReport = txt1.getText().toString();
    }

    private void setonclick() {
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chk1.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.check_peach));
                chk2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk4.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk5.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk6.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                strReport = txt1.getText().toString();
            }
        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chk2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.check_peach));
                chk1.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk4.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk5.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk6.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                strReport = txt2.getText().toString();
            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chk3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.check_peach));
                chk2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk1.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk4.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk5.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk6.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                strReport = txt3.getText().toString();
            }
        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chk4.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.check_peach));
                chk2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk1.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk5.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk6.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                strReport = txt4.getText().toString();
            }
        });
        ll5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chk5.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.check_peach));
                chk2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk4.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk1.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk6.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                strReport = txt5.getText().toString();
            }
        });
        ll6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chk6.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.check_peach));
                chk2.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk3.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk4.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk5.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                chk1.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.unchecknew));
                strReport = txt6.getText().toString();
            }
        });

        submitll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 hitService();
            }
        });
    }

    private void hitService() {
        Utilities.showProgressDialog(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Utilities.hideProgressDialog();
                Log.e(TAG, "********ISSUE_RESPONSE*********: "+response );
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), jsonObject.getString("message"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "********ERROR_RESPONSE*********: "+error.toString() );
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("type", "0");
                params.put("reason", strReport);
                params.put("user_item_id", strItemId);
                params.put("user_id", HomeActivity.UserItself);
                return params;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);

    }
}
