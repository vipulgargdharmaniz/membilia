package com.membilia.Fragments;

/*
 * Created by dharmaniapps on 9/3/18.
 */

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.dbModal.DbProfile;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ProfileDataModel;
import com.membilia.volley_models.ProfileImageModel;
import com.membilia.volley_models.ProfileLocationModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;

public class NewProfileFragment extends Fragment {

    private static String TAG = "NewProfileFragment";
    View itemsView;
    SimpleDraweeView profileIV, borderRing;
    TextView txtNameTV, txtCountryTV, profiledescriptionTV, txtMessageTV, profileFollowTV, txtRankingValueTV, txtFollowersValueTV, txtFollowingValueTV;
    ImageView checkIV, imgCountry;
    LinearLayout topbar, llCountry, descriptionLL, messageLL, followLL;

    HorizontalScrollView horizontalScrollview;
    RelativeLayout llFavorite, llPawned, llAvailable, llReviews;
    View favoriteFooter, pawnedFooter, availableFooter, reviewsFooter;
    RelativeLayout leftRL, rightRL;
    TextView titleTV;
    ImageView imgLeftIV, imgSettingIV;
    CoordinatorLayout coordinatorLayout;

    LinearLayout ll_followers, ll_following;

    String strToken, strOwnerId;
    String strProfileId;

    ArrayList<DbProfile> dbProfileArrayList;
    ProfileDataModel profileDataModel;

    boolean isFollow = false;
    public String strUrlFOLLOW = WebServicesConstants.BASE_URL + "user/" + HomeActivity.forProfileID + "/follow";
    public String strUrlUNFOLLOW = WebServicesConstants.BASE_URL + "user/" + HomeActivity.forProfileID + "/unfollow";

    String linkName, linkImg, linkDescription, linkUserID;
    public String CurrentUserId, subFragment = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        strToken = HomeActivity.db.getUser().get(0).getUserToken();
        strOwnerId = HomeActivity.forProfileID;
        profileDataModel = new ProfileDataModel();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (HomeActivity.FromSetting.equalsIgnoreCase("")) {
            if (itemsView == null) {
                itemsView = inflater.inflate(R.layout.new_fragment_profile, null);
                Bundle args = getArguments();
                if (args != null) {
                    try {
                        subFragment = args.getString("subFragment");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Log.e(TAG, "subFragment: " + subFragment);
                setView(itemsView);
                setClick();
                if (haveNetworkConnection()) {
                    if (subFragment != null) {
                        if (subFragment.equalsIgnoreCase("review")) {
                            setUpReviewFragment();
                        } else {
                            switchFragment(getActivity(), new FavoriteFragment(), false);
                        }
                    } else {
                        switchFragment(getActivity(), new FavoriteFragment(), false);
                    }
                    executeProfileData();
                } else {
                    if (HomeActivity.db.getProfile(strOwnerId).size() > 0) {
                        parseDataBaseResponse(HomeActivity.db.getProfile(strOwnerId));
                    } else {
                        AlertDialogManager.showAlertDialogOK(getActivity(), "Membilia", "Please connect with internet");
                    }
                }
            }
        } else {
            HomeActivity.FromSetting = "";
            itemsView = inflater.inflate(R.layout.new_fragment_profile, null);
            setView(itemsView);
            setClick();
            if (haveNetworkConnection()) {
                executeProfileData();
                switchFragment(getActivity(), new FavoriteFragment(), false);
            } else {
                if (HomeActivity.db.getProfile(strOwnerId).size() > 0) {
                    parseDataBaseResponse(HomeActivity.db.getProfile(strOwnerId));
                } else {
                    AlertDialogManager.showAlertDialogOK(getActivity(), "Membilia", "Please connect with internet");
                }
            }
        }

        return itemsView;
    }


    private void setUpReviewFragment() {
        horizontalScrollview.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
        favoriteFooter.setVisibility(View.INVISIBLE);
        pawnedFooter.setVisibility(View.INVISIBLE);
        availableFooter.setVisibility(View.INVISIBLE);
        reviewsFooter.setVisibility(View.VISIBLE);
        switchFragment(getActivity(), new ReviewFragment(), false);
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.clickProfile();
        if (!Constants.NewMessage.equalsIgnoreCase("0")) {
            txtMessageTV.setText(Constants.NewMessage);
            txtMessageTV.setVisibility(View.VISIBLE);
        } else {
            txtMessageTV.setVisibility(View.GONE);
        }

//        horizontalScrollview.fullScroll(HorizontalScrollView.FOCUS_LEFT);
//        favoriteFooter.setVisibility(View.VISIBLE);
//        pawnedFooter.setVisibility(View.GONE);
//        reviewsFooter.setVisibility(View.GONE);
//        availableFooter.setVisibility(View.GONE);
//        switchFragment(getActivity(), new FavoriteFragment(), false);
    }

    private void setView(View itemsview) {
        topbar = itemsview.findViewById(R.id.topbar);
        leftRL = itemsview.findViewById(R.id.leftRL);
        rightRL = itemsview.findViewById(R.id.rightRL);
        titleTV = itemsview.findViewById(R.id.titleTV);
        imgLeftIV = itemsview.findViewById(R.id.imgLeftIV);
        imgSettingIV = itemsview.findViewById(R.id.imgSettingIV);

        coordinatorLayout = itemsview.findViewById(R.id.coardinateLayout);

        profileIV = itemsview.findViewById(R.id.profileIV);
        borderRing = itemsview.findViewById(R.id.borderRing);

        checkIV = itemsview.findViewById(R.id.checkIV);
        imgCountry = itemsview.findViewById(R.id.imgCountry);

        txtNameTV = itemsview.findViewById(R.id.txtNameTV);
        txtCountryTV = itemsview.findViewById(R.id.txtCountryTV);
        profiledescriptionTV = itemsview.findViewById(R.id.profiledescriptionTV);
        txtMessageTV = itemsview.findViewById(R.id.txtMessageTV);
        profileFollowTV = itemsview.findViewById(R.id.profileFollowTV);
        txtRankingValueTV = itemsview.findViewById(R.id.txtRankingValueTV);
        txtFollowersValueTV = itemsview.findViewById(R.id.txtFollowersValueTV);
        txtFollowingValueTV = itemsview.findViewById(R.id.txtFollowingValueTV);

        llCountry = itemsview.findViewById(R.id.llCountry);
        descriptionLL = itemsview.findViewById(R.id.descriptionll);
        messageLL = itemsview.findViewById(R.id.llMessage);
        followLL = itemsview.findViewById(R.id.followll);


        horizontalScrollview = itemsview.findViewById(R.id.horizontalScrollview);
        llFavorite = itemsview.findViewById(R.id.llFavorite);
        llPawned = itemsview.findViewById(R.id.llPawned);
        llAvailable = itemsview.findViewById(R.id.llAvailable);
        llReviews = itemsview.findViewById(R.id.llReviews);

        favoriteFooter = itemsview.findViewById(R.id.favoriteFooter);
        pawnedFooter = itemsview.findViewById(R.id.pawnedFooter);
        availableFooter = itemsview.findViewById(R.id.availableFooter);
        reviewsFooter = itemsview.findViewById(R.id.reviewsFooter);

        titleTV.setVisibility(View.VISIBLE);
        topbar.setVisibility(View.VISIBLE);

        ll_following = itemsview.findViewById(R.id.ll_following);
        ll_followers = itemsview.findViewById(R.id.ll_followers);

        borderRing.getHierarchy().setPlaceholderImage(R.drawable.border_ring);

        coordinatorLayout.setVisibility(View.GONE);
        favoriteFooter.setVisibility(View.VISIBLE);
        pawnedFooter.setVisibility(View.GONE);
        availableFooter.setVisibility(View.GONE);
        reviewsFooter.setVisibility(View.GONE);
        rightRL.setVisibility(View.VISIBLE);
        imgSettingIV.setImageDrawable(null);
    }

    private void setClick() {

        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                    Log.e(TAG, "onClick:11111 ");
                } else {
                    Log.e(TAG, "onClick:00000 ");
                    HomeActivity.forProfileID = HomeActivity.UserItself;
                    Bundle bundle = new Bundle();
                    bundle.putString("isSameUser", "false");
                    HomeActivity.switchFragment(getActivity(), new NewProfileFragment(), Constants.PROFILE_FRAGMENT, false, bundle);

                }

            }
        });
        rightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (HomeActivity.forProfileID.equalsIgnoreCase(HomeActivity.UserItself)) {
                    Constants.UserImage = "";
                    Constants.UserDesc ="";
                    Constants.UserCountry="";
                    Constants.UserName = "";
                    HomeActivity.switchFragment(getActivity(), new FragmentSetting_New(), Constants.Fragment_Setting, true, null);
                } else {
                    shareLink();
                }
            }
        });
        ll_followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (haveNetworkConnection()) {
                    if (!txtFollowersValueTV.getText().toString().equalsIgnoreCase("0")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("strFollowOrFollowing", "Followers");
                        HomeActivity.switchFragment(getActivity(), new FollowersFragment(), Constants.Fragment_Follower, true, bundle);
                    }
                } else {
                    AlertDialogManager.showAlertDialogOK(getActivity(), "Membilia", "Please connect with internet");
                }
            }
        });
        ll_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (haveNetworkConnection()) {
                    if (!txtFollowingValueTV.getText().toString().equalsIgnoreCase("0")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("strFollowOrFollowing", "Following");
                        HomeActivity.switchFragment(getActivity(), new FollowingFragment(), Constants.Fragment_Follower, true, bundle);
                    }
                } else {
                    AlertDialogManager.showAlertDialogOK(getActivity(), "Membilia", "Please connect with internet");
                }
            }
        });

        llFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                horizontalScrollview.fullScroll(HorizontalScrollView.FOCUS_LEFT);
                favoriteFooter.setVisibility(View.VISIBLE);
                pawnedFooter.setVisibility(View.GONE);
                reviewsFooter.setVisibility(View.GONE);
                availableFooter.setVisibility(View.GONE);
                switchFragment(getActivity(), new FavoriteFragment(), false);
            }
        });

        llPawned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favoriteFooter.setVisibility(View.GONE);
                pawnedFooter.setVisibility(View.VISIBLE);
                reviewsFooter.setVisibility(View.GONE);
                availableFooter.setVisibility(View.GONE);
                switchFragment(getActivity(), new PawnFragmentProfile(), false);
            }
        });
        llAvailable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favoriteFooter.setVisibility(View.GONE);
                pawnedFooter.setVisibility(View.GONE);
                availableFooter.setVisibility(View.VISIBLE);
                reviewsFooter.setVisibility(View.GONE);
                switchFragment(getActivity(), new AvailableFragment(), false);
            }
        });
        llReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                horizontalScrollview.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                favoriteFooter.setVisibility(View.GONE);
                pawnedFooter.setVisibility(View.GONE);
                availableFooter.setVisibility(View.GONE);
                reviewsFooter.setVisibility(View.VISIBLE);
                switchFragment(getActivity(), new ReviewFragment(), false);
            }
        });
        messageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.switchFragment(getActivity(), new FragmentMessageHistory(), Constants.Fragment_Message_History, true, null);
            }
        });

        followLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (haveNetworkConnection()) {
                    Utilities.showProgressDialog(getActivity());
                    if (!isFollow) {
                        followServices();
                    } else {
                        unFollowServices();
                    }
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Connect with internet!");
                }
            }
        });
    }


    private void executeProfileData() {
        String strUrl = WebServicesConstants.USER_PROFILE + HomeActivity.forProfileID;
        StringRequest stringRequest = new StringRequest(strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*********onResponse*********" + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", strToken);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "USER_PROFILE");
    }

    private void parseResponse(String response) {
        try {
            Log.e(TAG, "db.getAllProfile().size(): " + HomeActivity.db.getAllProfile().size());
            dbProfileArrayList = new ArrayList<>();
            JSONObject emp = new JSONObject(response);
            DbProfile dbProfile = new DbProfile();
            strProfileId = emp.getString("user_id");
            dbProfile.setUserId(strProfileId);
            CurrentUserId = strProfileId;
            HomeActivity.forProfileID = CurrentUserId;

            profileDataModel.setUserId(emp.getString("user_id"));
            profileDataModel.setName(emp.getString("name"));
            profileDataModel.setEmail(emp.getString("email"));
            profileDataModel.setDescription(emp.getString("description"));
            profileDataModel.setRanking(emp.getInt("ranking"));
            profileDataModel.setFollowerCount(emp.getInt("followerCount"));
            profileDataModel.setFollowingCount(emp.getInt("followingCount"));
            profileDataModel.setIsFollowed(emp.getBoolean("isFollowed"));

            JSONObject imageObject = emp.getJSONObject("user_images");
            ProfileImageModel imageModel = new ProfileImageModel();
            if (!imageObject.isNull("original")) {
                imageModel.setOriginal(imageObject.getString("original"));
                profileDataModel.setProfileImageModel(imageModel);
            }

            linkName = profileDataModel.getName();
            linkDescription = profileDataModel.getDescription();
            linkImg = profileDataModel.getProfileImageModel().getOriginal();
            linkUserID = profileDataModel.getUserId();
            if (!emp.isNull("location")) {
                JSONObject locationObject = emp.getJSONObject("location");
                ProfileLocationModel locationModel = new ProfileLocationModel();
                if (!locationObject.isNull("administrative_area_level_1"))
                    locationModel.setAdministrativeAreaLevel1(locationObject.getString("administrative_area_level_1"));
                if (!locationObject.isNull("country")) {
                    locationModel.setCountry(locationObject.getString("country"));
                    dbProfile.setUserCountry(locationObject.getString("country"));
                }
                if (!locationObject.isNull("country_code"))
                    locationModel.setCountryCode(locationObject.getString("country_code"));
                if (!locationObject.isNull("lat"))
                    locationModel.setLat(locationObject.getString("lat"));
                if (!locationObject.isNull("lng"))
                    locationModel.setLng(locationObject.getString("lng"));
                if (!locationObject.isNull("locality"))
                    locationModel.setLocality(locationObject.getString("locality"));
                if (!locationObject.isNull("postal_code"))
                    locationModel.setPostalCode(locationObject.getString("postal_code"));
                profileDataModel.setLocationModel(locationModel);
            }

            dbProfile.setUserId(emp.getString("user_id"));
            dbProfile.setUsername(emp.getString("name"));
            dbProfile.setUserDecp(emp.getString("description"));
            dbProfile.setUserRanking(String.valueOf(emp.getInt("rating")));
            dbProfile.setUserFollower(String.valueOf(emp.getInt("followingCount")));
            dbProfile.setUserFollowing(String.valueOf(emp.getInt("followerCount")));
            dbProfile.setUserPic(profileDataModel.getProfileImageModel().getOriginal());

            Log.e(TAG, "followerCount: " + profileDataModel.getFollowerCount());
            Log.e(TAG, "followingCount: " + profileDataModel.getFollowingCount());
            String strFollow;
            if (emp.getBoolean("isFollowed")) {
                dbProfile.setUserFollowed("true");
                strFollow = "Unfollow";
                isFollow = false;
                profileFollowTV.setText(strFollow);
            } else {
                dbProfile.setUserFollowed("false");
                strFollow = "Follow";
                isFollow = true;
                profileFollowTV.setText(strFollow);
            }

            isFollow = emp.getBoolean("isFollowed");
            Log.e(TAG, "isFollow: " + isFollow);
            if (HomeActivity.db.getProfile(strProfileId).size() != 0) {
                HomeActivity.db.UpdateProfile(strProfileId);
                dbProfileArrayList.add(dbProfile);
                HomeActivity.db.addProfile(dbProfileArrayList);
            } else if (HomeActivity.db.getProfile(strProfileId).size() == 0) {
                dbProfileArrayList.add(dbProfile);
                HomeActivity.db.addProfile(dbProfileArrayList);
            }
            coordinatorLayout.setVisibility(View.VISIBLE);
            setDataOnWidgets(profileDataModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseDataBaseResponse(ArrayList<DbProfile> profile) {
        coordinatorLayout.setVisibility(View.VISIBLE);
        String userName = "@" + profile.get(0).getUsername();
        titleTV.setText(userName);
        txtNameTV.setText(profile.get(0).getUsername());
        Constants.currentUserId = profile.get(0).getUsername();
        profiledescriptionTV.setText(profile.get(0).getUserDecp().trim());
        if (profile.get(0).getUserRanking().equalsIgnoreCase("0")){
            txtRankingValueTV.setText("-");
        }else {
            txtRankingValueTV.setText(profile.get(0).getUserRanking());
        }

        txtFollowingValueTV.setText(profile.get(0).getUserFollowing());
        txtFollowersValueTV.setText(profile.get(0).getUserFollower());
        if (profile.get(0).getUserDecp().equalsIgnoreCase("")) {
            descriptionLL.setVisibility(View.GONE);
        }
        profileIV.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        profileIV.setImageURI(Uri.parse(profile.get(0).getUserPic()));

        linkName = profile.get(0).getUsername();
        linkDescription = profile.get(0).getUserDecp().trim();
        linkImg = profile.get(0).getUserPic();
        linkUserID = profile.get(0).getUserId();

        if (profile.get(0).getUserVerified().equalsIgnoreCase("true")) {
            checkIV.setVisibility(View.VISIBLE);
        } else {
            checkIV.setVisibility(View.GONE);
        }
        isFollow = Boolean.parseBoolean(profile.get(0).getUserFollowed());
        Log.e(TAG, "isFollow: " + isFollow);

        String strFollow;
        if (profile.get(0).getUserFollowed().equalsIgnoreCase("true")) {
            strFollow = "Follow";
            isFollow = true;
            profileFollowTV.setText(strFollow);
        } else {
            isFollow = false;
            strFollow = "Unfollow";
            profileFollowTV.setText(strFollow);
        }
        String countryImage = profile.get(0).getUserCountry();
        txtCountryTV.setText(countryImage);
        if (!countryImage.equalsIgnoreCase("")) {
            Resources res = null;
            String c = "";
            Drawable drawable;
            res = getActivity().getResources();
            String a = countryImage.replace(" ", "_").toLowerCase();
            String b = a.replace("(", "");
            c = b.replace(")", "");
            int resID = res.getIdentifier(c, "drawable", getActivity().getPackageName());
            drawable = res.getDrawable(resID);
            imgCountry.setImageDrawable(drawable);
        } else {
            imgCountry.setVisibility(View.INVISIBLE);
        }
        if (!HomeActivity.forProfileID.equalsIgnoreCase(HomeActivity.UserItself)) {
            followLL.setVisibility(View.VISIBLE);
            leftRL.setVisibility(View.VISIBLE);
            messageLL.setVisibility(View.GONE);
            imgSettingIV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.share1));
        } else {
            leftRL.setVisibility(View.GONE);
            messageLL.setVisibility(View.VISIBLE);
            imgSettingIV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.setting));
        }
    }

    private void setDataOnWidgets(ProfileDataModel profileDataModel) {
        txtNameTV.setText(profileDataModel.getName());
        String strDescription = profileDataModel.getDescription().trim();
        profiledescriptionTV.setText(strDescription);
        if (strDescription.equalsIgnoreCase("")) {
            descriptionLL.setVisibility(View.GONE);
        }
        if (!strProfileId.equalsIgnoreCase(strOwnerId)) {
            followLL.setVisibility(View.VISIBLE);
        }
        String ranking = profileDataModel.getRanking().toString();
        String followerCount = profileDataModel.getFollowingCount().toString();
        String followingCount = profileDataModel.getFollowerCount().toString();

        Log.e(TAG, "setDataOnWidgets: " + followerCount);
        Log.e(TAG, "setDataOnWidgets: " + followingCount);
        String userName = "@" + profileDataModel.getName();
        String userImage = profileDataModel.getProfileImageModel().getOriginal();
        if (ranking.equalsIgnoreCase("0")){
            txtRankingValueTV.setText("-");
        }else {
            txtRankingValueTV.setText(ranking);
        }
        txtFollowersValueTV.setText(followerCount);
        txtFollowingValueTV.setText(followingCount);
        titleTV.setText(userName);
        profileIV.getHierarchy().setPlaceholderImage(R.drawable.default_img);
        profileIV.setImageURI(Uri.parse(userImage));

        if (profileDataModel.getIsVerified()) {
            checkIV.setVisibility(View.VISIBLE);
        } else {
            checkIV.setVisibility(View.GONE);
        }

        String countryImage = profileDataModel.getLocationModel().getCountry();
        txtCountryTV.setText(countryImage);
        if (!countryImage.equalsIgnoreCase("")) {
            Resources res = null;
            String c = "";
            Drawable drawable;
            res = getActivity().getResources();
            String a = countryImage.replace(" ", "_").toLowerCase();
            String b = a.replace("(", "");
            c = b.replace(")", "");
            int resID = res.getIdentifier(c, "drawable", getActivity().getPackageName());
            drawable = res.getDrawable(resID);
            imgCountry.setImageDrawable(drawable);

        } else {
            imgCountry.setVisibility(View.INVISIBLE);
        }
        if (!HomeActivity.forProfileID.equalsIgnoreCase(HomeActivity.UserItself)) {
            followLL.setVisibility(View.VISIBLE);
            leftRL.setVisibility(View.VISIBLE);

            messageLL.setVisibility(View.GONE);
            imgSettingIV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.share1));
        } else {
            leftRL.setVisibility(View.GONE);
            messageLL.setVisibility(View.VISIBLE);
            imgSettingIV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.setting));
        }

    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private void switchFragment(FragmentActivity activity, Fragment fragment, boolean addToStack) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            ft.replace(R.id.layoutContainerFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            ft.commit();
        }
    }


    private void followServices() {
        Log.e(TAG, "followServices: "+strUrlFOLLOW );
        Log.e(TAG, "followServices: "+strToken );
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrlFOLLOW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*********onResponse*********" + response);
                executeProfileData();
                Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", strToken);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "FollowRequest");
    }


    private void unFollowServices() {
        Log.e(TAG, "unFollowServices: " + strUrlUNFOLLOW);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrlUNFOLLOW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*********onResponse*********" + response);
                executeProfileData();
                Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.e(TAG, "**********onErrorResponse*********" + error.toString());
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", strToken);
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, "FollowRequest");
    }


    private void shareLink() {
        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier("content/12345")
                .setTitle(linkName)
                .setContentDescription(linkDescription)
                .setContentImageUrl(linkImg)
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata().addCustomMetadata("android_deepview", "membilia_deepview_pj0").addCustomMetadata("ios_deepview", "membilia_deepview_pj0").addCustomMetadata("user_id", linkUserID));

        LinkProperties lp = new LinkProperties()
                .setChannel("facebook")
                .setFeature("sharing")
                .addTag(linkUserID)
                .setStage("new user")
                .setCampaign("content 123 launch")
                .addControlParameter("$desktop_url", "https://membilia.com/")
                .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));

        buo.generateShortUrl(getActivity(), lp, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
//                    Log.i("BRANCH SDK", "got my Branch link to share: " + url);
                } else {
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ShareSheetStyle ss = new ShareSheetStyle(getActivity(), linkName, linkImg)
                .setCopyUrlStyle(ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_send), "Copy", "Added to clipboard")
                .setMoreOptionStyle(ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_search), "Show more")
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.HANGOUT)
                .setAsFullWidthStyle(true)
                .setSharingTitle("Share With");

        buo.showShareSheet(getActivity(), lp, ss, new Branch.BranchLinkShareListener() {
            @Override
            public void onShareLinkDialogLaunched() {
                Log.e(TAG, "onShareLinkDialogLaunched: ");
            }

            @Override
            public void onShareLinkDialogDismissed() {
                Log.e(TAG, "onShareLinkDialogDismissed: ");
            }

            @Override
            public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
                Log.e(TAG, "onLinkShareResponse: " + sharedLink);
            }

            @Override
            public void onChannelSelected(String channelName) {
                Log.e(TAG, "onChannelSelected: ");
            }
        });
    }
}
