package com.membilia.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.adapters.ItemsAdapter;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemLocationModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniz on 11/9/17.
 */

public class Fragments_Tags extends Fragment {

    View tagsView;
    LinearLayout llToolbar, llnoitem;
    String strName;
    String i;
    RelativeLayout leftRL;
    TextView titleTV;
    ItemsAdapter itemsAdapter;
    List<String> listTags = new ArrayList<>();
    ArrayList<ItemsModel> itemArraylist = new ArrayList<>();
    ArrayList<ItemsModel> loadmoreArraylist = new ArrayList<>();
    NestedScrollView mNestedScrollView;
    RecyclerView recyclerRV;
    int perPage = 20;

    int Pageno = 1;
    boolean isNextPage = false;
    private static final String TAG = "Fragments_Tags";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            strName = args.getString("name");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (tagsView == null) {
            tagsView = inflater.inflate(R.layout.fragment_favorite, null);
            Log.i(TAG, "onCreateView: ");
            setUpViews(tagsView);
            setupclicks();
            setitemVisibilty();
            Utilities.showProgressDialog(getActivity());
            gettingAllItems();
        }
        return tagsView;
    }

    private void setitemVisibilty() {
        llnoitem.setVisibility(View.GONE);
        llToolbar.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);

        titleTV.setText(strName);
    }

    private void setUpViews(View v) {
        llToolbar = v.findViewById(R.id.topbar);
        leftRL = v.findViewById(R.id.leftRL);
        titleTV = v.findViewById(R.id.titleTV);
        llnoitem = v.findViewById(R.id.llnoitem);
        mNestedScrollView = v.findViewById(R.id.mNestedScrollView);
        recyclerRV = v.findViewById(R.id.recyclerRV);
    }

    private void setupclicks() {
        leftRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (isNextPage = true) {
                        ++Pageno;
                        gettingAllItems();
                    }
                }
            }
        });
    }

    private void gettingAllItems() {
        final String strUrl = WebServicesConstants.SerchTag + "perPage=" + perPage + "&page_no=" + Pageno;
//        final String strUrl = "http://35.166.11.148/staging/api/item/search/tags?perPage=20&page_no=" + Pageno;
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tag", strName);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);
            StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, response);
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length() < 20) {
                            isNextPage = false;
                        } else {
                            isNextPage = true;
                        }
                        parseResponse(jsonArray);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    Utilities.hideProgressDialog();
                }
            }) {
                //**Passing some request headers
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                    return headers;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    return jsonObject.toString().getBytes();
                }
            };
            PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseResponse(JSONArray response) {
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setLastUsed(jsonObject.getString("last_used"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setIsAutographed(Boolean.parseBoolean(jsonObject.getString("isAutographed")));
                itemsModel.setCategory(jsonObject.getString("category"));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setCharityName(jsonObject.getString("charity_name"));
                itemsModel.setCurrency(jsonObject.getString("currency"));
                itemsModel.setDeliveryType(Boolean.parseBoolean(jsonObject.getString("delivery_type")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setWeight(jsonObject.getString("weight"));
                itemsModel.setCreated(jsonObject.getString("created"));
                itemsModel.setStatus(jsonObject.getString("status"));
                itemsModel.setWidth(jsonObject.getString("width"));
                itemsModel.setHeight(jsonObject.getString("height"));
                itemsModel.setLength(jsonObject.getString("length"));
                itemsModel.setPersonTransfertext(jsonObject.getString("person_transfertext"));
                itemsModel.setNewtimestamp(jsonObject.getString("newtimestamp"));
                itemsModel.setOnHold(jsonObject.getString("onHold"));
                itemsModel.setStatus2(jsonObject.getString("status2"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsFavorite(Boolean.parseBoolean(jsonObject.getString("isFavorite")));
                itemsModel.setRating(jsonObject.getString("rating"));
                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));

                    itemsModel.setUserImages(imagesModel);
                }
                itemsModel.setPawnedDate(jsonObject.getString("pawned_date"));
                if (!jsonObject.isNull("tags")) {
                    JSONArray tags = jsonObject.getJSONArray("tags");
                    for (int k = 0; k < tags.length(); k++) {
                        listTags.add(tags.get(k).toString());
                    }
                }
                itemsModel.setTags(listTags);
                if (!jsonObject.isNull("location")) {
                    JSONObject location = jsonObject.getJSONObject("location");
                    ItemLocationModel locationModel = new ItemLocationModel();
                    locationModel.setAdministrativeAreaLevel1(location.getString("administrative_area_level_1"));
                    locationModel.setCountry(location.getString("country"));
                    locationModel.setCountryCode(location.getString("country_code"));
                    locationModel.setLat(Float.parseFloat(location.getString("lat")));
                    locationModel.setLng(Float.parseFloat(location.getString("lng")));
                    locationModel.setLocality(location.getString("locality"));
                    locationModel.setPostalCode(location.getString("postal_code"));

                    itemsModel.setLocation(locationModel);
                }

                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));

                    itemsModel.setItemImages(itemImagesModel);
                }

                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }
                itemsModel.setItemIndx(Integer.parseInt(jsonObject.getString("item_indx")));
                itemsModel.setVerifieduser(jsonObject.getInt("is_verified_user"));

                if (Pageno == 1) {
                    itemArraylist.add(itemsModel);
                } else {
                    loadmoreArraylist.add(itemsModel);
                }
            }
            if (loadmoreArraylist.size() > 0) {
                itemArraylist.addAll(loadmoreArraylist);
            }
            Utilities.hideProgressDialog();
            setDataAndShowsItems();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDataAndShowsItems() {
        itemsAdapter = new ItemsAdapter(getActivity(), itemArraylist, null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        recyclerRV.setAdapter(itemsAdapter);
        recyclerRV.setNestedScrollingEnabled(false);
    }
}

