package com.membilia.Fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.SwipeHelper;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ShowBankAccount_Adapter;
import com.membilia.volley_models.AccountDetailsModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/*
 * Created by dharmaniz on 14/7/17.
 */

public class Fragmentshowbankaccount extends Fragment {
    View itemView;
    TextView addbankaccounttv;
    ImageView imgLeftIV;
    RecyclerView recyclerRV;
    private static final String TAG = "Fragmentshowbankaccount";
    ShowBankAccount_Adapter mShowBankAdapter;
    ArrayList<AccountDetailsModal> modelArrayList = new ArrayList<>();
    SwipeHelper swipeHelper;
    public static int defaultPostion;

    boolean isMakeDefaultCall = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemView = inflater.inflate(R.layout.fragment_show_bank_account, null);
        Log.i(TAG, "onCreateView: ");
        setUpviews(itemView);
        setUpclicks();
        return itemView;
    }

    private void setUpviews(View v) {
        addbankaccounttv = v.findViewById(R.id.addbankaccounttv);
        imgLeftIV = v.findViewById(R.id.imgLeftIV);
        recyclerRV = v.findViewById(R.id.recyclerRV);
    }

    private void setUpclicks() {
        addbankaccounttv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.switchFragment(getActivity(), new Fragmentaddbankaccount(), Constants.Fragment_Add_Bank_Account, true, null);
            }
        });

        imgLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        modelArrayList.clear();
        getAllAcount();
    }

    private void getAllAcount() {
        if (!isMakeDefaultCall) {
            Utilities.showProgressDialog(getActivity());
        }
        String strUrl = WebServicesConstants.GetAllBankAccount;
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return hashMap;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.GetAllBankAccount);
    }

    private void parseResponse(String response) {
        JSONObject jsonObject;
        JSONArray jsonArray;
        String accountName, nameOfBank, accountId, routingId, accountNo, created;
        boolean isDefault, isSavingAccount;
        try {
            jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                AccountDetailsModal accountDetailsModal = new AccountDetailsModal();
                jsonObject = jsonArray.getJSONObject(i);
                accountId = jsonObject.getString("account_id");
                accountName = jsonObject.getString("name_on_account");
                nameOfBank = jsonObject.getString("name_on_bank");
                routingId = jsonObject.getString("routing_number");
                accountNo = jsonObject.getString("account_number");
                created = jsonObject.getString("created");
                isDefault = jsonObject.getBoolean("defult");
                isSavingAccount = jsonObject.getBoolean("isAccountSaving");

                accountDetailsModal.setAccountId(accountId);
                accountDetailsModal.setAccountName(accountName);
                accountDetailsModal.setBankName(nameOfBank);
                accountDetailsModal.setRoutingNo(routingId);
                accountDetailsModal.setAccountNo(accountNo);
                accountDetailsModal.setCreated(created);
                accountDetailsModal.setDefault(isDefault);
                accountDetailsModal.setSavingAccount(isSavingAccount);

                modelArrayList.add(accountDetailsModal);
            }
            Log.e(TAG, "modelArrayList: " + modelArrayList);
            settingUpAdatper();
        } catch (Exception e) {
            e.printStackTrace();
            Utilities.hideProgressDialog();
        }
    }


    private void settingUpAdatper() {
        mShowBankAdapter = new ShowBankAccount_Adapter(getActivity(), modelArrayList, Fragmentshowbankaccount.this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerRV.setLayoutManager(mLayoutManager);
        recyclerRV.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(
                recyclerRV.getContext(),
                mLayoutManager.getOrientation());
        recyclerRV.addItemDecoration(mDividerItemDecoration);
        recyclerRV.setAdapter(mShowBankAdapter);
        mShowBankAdapter.notifyDataSetChanged();
        Utilities.hideProgressDialog();

        swipeHelper = new SwipeHelper(getActivity(), recyclerRV) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(
                        "Delete",
                        0,
                        Color.parseColor("#d74e59"),
                        new SwipeHelper.UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                // TODO: onDelete
                                dialogbox(pos);
                            }
                        }
                ));
            }
        };
    }

    private void dialogbox(final int pos) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        LinearLayout yesll = dialog.findViewById(R.id.yesll);
        LinearLayout noll = dialog.findViewById(R.id.noll);
        TextView messageTV = dialog.findViewById(R.id.messageTV);
        messageTV.setText("Are you sure, you want to delete Bank Account details?");
        noll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        yesll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (modelArrayList.size() > 1 && defaultPostion != pos) {
                    deleteAccount(pos);
                } else if (defaultPostion == pos && modelArrayList.size() == 1) {
                    deleteAccount(pos);
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Hi! this is your default option, first make another as default then delete this.");
                }
            }
        });
        dialog.show();
    }

    public void deleteAccount(final int pos) {
        String strUrl = WebServicesConstants.DeleteAccount + modelArrayList.get(pos).getAccountId();
        Log.e(TAG, "strUrl: " + strUrl);
        Utilities.showProgressDialog(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    if (message.equalsIgnoreCase("true")) {
                        modelArrayList.remove(pos);
                        mShowBankAdapter.notifyDataSetChanged();
                        Utilities.hideProgressDialog();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Utilities.hideProgressDialog();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return header;
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.DeleteAccount);
    }


    public void MakeDefault(final String accountNo) {
        final JSONObject requestJson = new JSONObject();
        try {
            requestJson.put("account_id", accountNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String strUrl = WebServicesConstants.MakeBankAccountDefault;
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                isMakeDefaultCall = true;
                modelArrayList.clear();
                getAllAcount();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                header.put("Content-Type", "application/x-www-form-urlencoded");
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.DeleteAccount);
    }
}
