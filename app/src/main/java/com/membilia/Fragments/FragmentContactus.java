package com.membilia.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import com.membilia.R;

/**
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentContactus extends Fragment {
    View itemsview;
    RelativeLayout searchRL;
    WebView webView;
    private static final String TAG = "FragmentContactus";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        itemsview = inflater.inflate(R.layout.fragment_contactus, null);
        Log.i(TAG, "onCreateView: ");
        setUpViews(itemsview);
        setupclicks();
        return itemsview;
    }

    private void setUpViews(View v) {
        searchRL = (RelativeLayout) v.findViewById(R.id.searchRL);
        webView = (WebView) v.findViewById(R.id.webView);
        webView.loadUrl("https://membilia.com/join");
    }

    private void setupclicks() {

        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

    }

}
