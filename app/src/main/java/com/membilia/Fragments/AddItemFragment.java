package com.membilia.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aigestudio.wheelpicker.WheelPicker;
import com.aigestudio.wheelpicker.widgets.WheelDayPicker;
import com.aigestudio.wheelpicker.widgets.WheelMonthPicker;
import com.aigestudio.wheelpicker.widgets.WheelYearPicker;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.kyleduo.switchbutton.SwitchButton;
import com.membilia.PlayPawnPref_Once;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.CameraUtils;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.CharityOptions;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.AddImageListAdapter;
import com.membilia.interfaces.GetItemCountAndRemoveItem;
import com.membilia.volley.AppHelper;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley.VolleyMultipartRequest;
import com.membilia.volley_models.GenreModel;
import com.membilia.volley_models.GenreValue;
import com.membilia.volley_models.ImagesItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.ACTION_VIDEO_CAPTURE;
import static android.provider.MediaStore.EXTRA_VIDEO_QUALITY;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.membilia.Util.PlayerPawnPreference.latitude;
import static com.membilia.Util.PlayerPawnPreference.longitude;

public class AddItemFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, PlaceSelectionListener {
    Resources mResources;
    View addItemPPView;
    TextView txtItemCountTV;
    RecyclerView recyclerViewImagesVideo;
    EditText editItemNameET, editItemPriceET, editItemWeightET, editItemStoryET,
            editItemTagSepratedET, editPersonalTrasET;
    TextView itemLastUsedDATE, txtConfirmTV, editTypeCharityName, editLocationET;
    String arrayCategories[];
    String arraySports[];
    Typeface mTypeface;
    RelativeLayout rightRL, leftRL;
    private static final String TAG = "AddItemFragment";
    String strAdminArea = "", strCountry = "",
            strLat = "", strLong, strLocalty = "", strPostalcode = "";
    SwitchButton switchAutographed, switchForCharity;
    RadioButton radioShipping, radioPersonal;
    boolean isPersonalTransfer = false;
    boolean isAutograph = false;
    boolean isForCharity = false;
    LinearLayout firstCharityLL;   /* for showing and hiding charity layout*/
    CoordinatorLayout rootLayout;
    TextView nextTV, shippingTypeTV;
    LinearLayout shppingtypeLL, llok;
    RelativeLayout item_splash2RL;
    HomeActivity activity;
    /* handling Tags */
    String strTags = "";
    ArrayList<String> tagArraylist = new ArrayList<>();
    String strItemName, strItemPrice, strItemWeight, strCategory, strGenre, strGenreValue, strItemStory,
            strPersonalTransfer, strLocation, strCharityName;
    int intPrice = 0;
    /* textview and relative for categories*/
    RelativeLayout categoryRL, genreRl, genreValueRL;
    TextView categoryTV, genreValueTV, genreTV;
    /**
     * ImageView for selecting images
     */
    SimpleDraweeView selectIMG;
    /*     * Bottom sheet vaiables and Widgets     */
    View bottomSheet;
    TextView cancelTV, sheetTitleTV, doneTV;

    List<String> monthArraylist = new ArrayList<>();
    String firstGenreItem = "";
    LinearLayout wheeldataLL, wheelpickerLL, ll_date;
    WheelDayPicker main_wheel_left;
    WheelPicker wheelViewData;
    WheelYearPicker main_wheel_right;
    WheelMonthPicker main_wheel_center;
    String strResultDATA = "";
    String strResultDATE = "";
    String strDate = "", strMonth, strYear = "";
    String strDateFinal = "";
    int intMonth = 0;
    boolean locationEnable = false;
    /*
     * it helps us to detect when wheel view is open then for where have to set the values
     * 0 for choose categories , 1 for select genre , 2 for genre value , 3 for datepicker;
     */
    int setValueFor = 0;

    /*
     * month name as of hasmap
     */
    HashMap<Integer, String> monthMap = new HashMap<>();

    /*
     * permission for camera , video and gallery
     */
    private String CameraPermission = Manifest.permission.CAMERA;
    private String ReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String WriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final int CAMERA_REQUEST = 111;
    private static final int GALLERY_REQUEST = 222;
    private static final int VIDEO_REQUEST = 333;
    private static final int VIDEO_GALLERY_REQUEST = 4444;

    /*ArrayList*/
    ArrayList<ImagesItemModel> mImagesVideoArrayList;
    String mCurrentPhotoPath;
    public String VIDEO_TYPE = "video_type";
    public String IMAGE_TYPE = "image_type";
    /*
     * Check for cheking and seeting for single video upload
     */
    public static boolean isVideoSelected = false;
    /*
     * categories arraylist , genre Arraylist  , genre value Arraylist
     */
    ArrayList<String> catArrayList = new ArrayList<>();
    ArrayList<GenreModel> genreArrayList = new ArrayList<>();
    ArrayList<String> genreList = new ArrayList<>();
    List<GenreValue> genreValueList = new ArrayList<>();
    ArrayList<String> arrayListWheelData = new ArrayList<>();
    int deleteItemPosition;
    byte[] image1 = null;
    byte[] image2 = null, image3 = null, image4 = null, image5 = null, image6 = null;
    byte[] videoImage = null;
    byte[] VideoData = null;

    GetItemCountAndRemoveItem mGetItemCountAndRemoveItem = new GetItemCountAndRemoveItem() {
        @Override
        public void getCountAndPositionForRemove(int position) {
            deleteItemPosition = position;
            mImagesVideoArrayList.remove(position);
            setAdapter(mImagesVideoArrayList);
        }
    };
    BottomSheetDialog mBottomSheetDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = new HomeActivity();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        HomeActivity.AddItemClick();
        HomeActivity.bottombar.setVisibility(View.VISIBLE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
        addItemPPView = inflater.inflate(R.layout.fragment_add_item_p, container, false);
        mResources = getActivity().getResources();
        arrayCategories = mResources.getStringArray(R.array.array_category);
        arraySports = mResources.getStringArray(R.array.array_sports);
        mTypeface = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Light.ttf");
        mBottomSheetDialog = new BottomSheetDialog(getActivity());
        setUpViews(addItemPPView);
        setUpClickListeners();
        mImagesVideoArrayList = AppSingleton.getInstance().getmImagesVideoArrayList();
        setWidgetsVisibity();
        setDefaultTextnCheck();
        setWheelview();
        setBottomSheet();
        setWheelPicker();
        /* service for getting categories and genre name and values*/
        getCatService();
        getGenreService();
        /* important for data setting*/
        setDefaultWheelData();
        setAddItemOperations();
        if (!PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.ADDRESS, "").equalsIgnoreCase("")) {
            String strlocation = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.ADDRESS, "");
            editLocationET.setText(strlocation);
        }
        if (!PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.CITY, "").equalsIgnoreCase("")) {
            strAdminArea = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.CITY, "");
        }
        if (!PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.STATE, "").equalsIgnoreCase("")) {
            strLocalty = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.STATE, "");
        }
        if (!PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.COUNTRY, "").equalsIgnoreCase("")) {
            strCountry = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.COUNTRY, "");
        }
        if (!PlayerPawnPreference.readString(getActivity(), latitude, "").equalsIgnoreCase("")) {
            strLat = PlayerPawnPreference.readString(getActivity(), latitude, "");
        }
        if (!PlayerPawnPreference.readString(getActivity(), longitude, "").equalsIgnoreCase("")) {
            strLong = PlayerPawnPreference.readString(getActivity(), longitude, "");

        }
        if (!PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.POSTALCODE, "").equalsIgnoreCase("")) {
            strPostalcode = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.POSTALCODE, "");
        }
        checkAccountAdded();
        //                 HomeActivity.switchFragment(getActivity(), new FragmentpaymentMethod(), Constants.Fragment_PaymentMethod, true, null);
        return addItemPPView;
    }

    private void checkAccountAdded() {
        Utilities.showProgressDialog(getActivity());
        String userid = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "");
        String strUrl = WebServicesConstants.GetPaymentDefault + userid;
        Log.e(TAG, "strUrl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.hideProgressDialog();

                Log.e(TAG, "onResponse:getPaymentDefault " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("default_account")) {
                        if (jsonObject.getString("default_account").equalsIgnoreCase("0")) {
                            showPopForAccount();
                        }else {
                            HomeActivity.isAddItemOpen = true;
                        }
                    } else {
                        showPopForAccount();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
                showPopForAccount();
                HomeActivity.isAddItemOpen = true;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                Log.e(TAG, "getHeaders: " + HomeActivity.db.getUser().get(0).getUserToken());
                hashMap.put("Authorization", HomeActivity.db.getUser().get(0).getUserToken());
                return hashMap;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }


    private void setAddItemOperations() {
        StringTokenizer st = new StringTokenizer(strTags, ",");
        while (st.hasMoreTokens()) {
            tagArraylist.add(st.nextToken());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);


        HomeActivity.bottombar.setVisibility(View.VISIBLE);
        HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);

        Log.e(TAG, "onResume: " + mImagesVideoArrayList.size());
        intMonth = main_wheel_center.getCurrentMonth();
        Log.e(TAG, "intMonth: " + intMonth);
        strMonth = monthMap.get(intMonth - 1);
        strDate = String.valueOf(main_wheel_left.getCurrentDay());
        strYear = String.valueOf(main_wheel_right.getCurrentYear());
        itemLastUsedDATE.setText(strMonth + " " + strDate + ", " + strYear);
        Constants.CallImageAdpaterFrom = "AddItem";


        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        strDateFinal = year + "-" + month + "-" + day;
        if (mImagesVideoArrayList.size() > 0) {
            setAdapter(mImagesVideoArrayList);
        }
    }

    private void setDefaultTextnCheck() {
        String strShipping = "<u>" + "Shipping" + "</u>";
        radioShipping.setText(Html.fromHtml(strShipping));


        String strPersonalTransfer = "<u>" + "Personal transfer" + "</u>";
        radioPersonal.setText(Html.fromHtml(strPersonalTransfer));
        if (isPersonalTransfer) {
            editPersonalTrasET.setEnabled(true);
        } else {
            editPersonalTrasET.setEnabled(false);
        }
        if (isForCharity) {
            editTypeCharityName.setEnabled(true);
        } else {
            editTypeCharityName.setEnabled(false);
        }
        if (Constants.OpenAddItemFrom.equalsIgnoreCase("home")) {

            mImagesVideoArrayList.clear();
            isVideoSelected = false;
        } else {
            editItemNameET.setText(Constants_AddItem.ITEM_NAME);
            editItemPriceET.setText(Constants_AddItem.ITEM_PRICE);
            editItemWeightET.setText(Constants_AddItem.ITEM_WEIGHT);
            categoryTV.setText(Constants_AddItem.ITEM_CAT);
            genreTV.setText(Constants_AddItem.ITEM_GENRE);
            genreValueTV.setText(Constants_AddItem.ITEM_GENREVALUE);
            editItemStoryET.setText(Constants_AddItem.ITEM_STORY);
            editItemTagSepratedET.setText(Constants_AddItem.TAGS);
            editTypeCharityName.setText(Constants_AddItem.ITEM_CHARITYNAME);
            VideoData = Constants_AddItem.VideoDAta;
            if (Constants_AddItem.ITEM_DELVIERYTYPE.equals("true")) {
                isPersonalTransfer = false;
                radioShipping.setChecked(true);
            }
            if (Constants_AddItem.ITEM_DELVIERYTYPE.equals("false")) {
                isPersonalTransfer = true;
                radioPersonal.setChecked(true);
            }
            editPersonalTrasET.setText(Constants_AddItem.ITEM_PERSONAL_TRANSFER);
            if (Constants_AddItem.ITEM_AUTO.equals("auto")) {
                switchAutographed.setChecked(true);
            }
            if (Constants_AddItem.ITEM_CHARITY.equals("charity")) {
                switchForCharity.setChecked(true);
            }
            if (switchAutographed.isChecked()) {
                Constants_AddItem.ITEM_AUTO = "auto";
            }
            editTypeCharityName.setText(Constants_AddItem.ITEM_CHARITYNAME);
            itemLastUsedDATE.setText(Constants_AddItem.ITEM_LASTUSED);
        }
    }


    private void setDefaultWheelData() {
        monthArraylist = Arrays.asList(getResources().getStringArray(R.array.array_months));
        for (int i = 0; i < monthArraylist.size(); i++) {
            monthMap.put(i, monthArraylist.get(i));
        }
    }


    private void setWheelPicker() {
        main_wheel_left.getSelectedDay();
        main_wheel_left.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strDate = data.toString();
                Log.e(TAG, "date: " + strDate);
            }
        });
        main_wheel_center.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                intMonth = Integer.parseInt(data.toString());
                Log.e(TAG, "month: " + intMonth);
            }
        });

        main_wheel_right.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strYear = data.toString();
                Log.e(TAG, "year: " + strYear);
            }
        });

        wheelViewData.setOnItemSelectedListener(new WheelPicker.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                strResultDATA = data.toString();
            }
        });

    }


    /*
     * setting Wheelview
     */
    private void setWheelview() {

        main_wheel_right.setAtmospheric(true);
        main_wheel_right.setCyclic(true);
        main_wheel_right.setCurved(true);
        main_wheel_right.setIndicatorSize(10);
        main_wheel_right.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        main_wheel_right.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        main_wheel_left.setAtmospheric(true);
        main_wheel_left.setCyclic(true);
        main_wheel_left.setCurved(true);
        main_wheel_left.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        main_wheel_left.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        main_wheel_center.setAtmospheric(true);
        main_wheel_center.setCyclic(true);
        main_wheel_center.setCurved(true);
        main_wheel_center.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        main_wheel_center.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        wheelViewData.setAtmospheric(true);
        wheelViewData.setCyclic(true);
        wheelViewData.setCurved(true);
        wheelViewData.setSelectedItemTextColor(getActivity().getResources().getColor(R.color.black));
        wheelViewData.setIndicatorColor(getActivity().getResources().getColor(R.color.black));

        main_wheel_center.setIndicator(true);
        main_wheel_center.setIndicatorSize(3);

        main_wheel_left.setIndicator(true);
        main_wheel_left.setIndicatorSize(3);

        main_wheel_right.setIndicator(true);
        main_wheel_right.setIndicatorSize(3);

        wheelViewData.setIndicator(true);
        wheelViewData.setIndicatorSize(3);
    }

    /*
     * Bottom sheet handler and clicklistners
     */
    private void setBottomSheet() {

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        doneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Log.e(TAG, "Value from Wheel: " + wheelViewData.getData());
                int itemPostion = wheelViewData.getCurrentItemPosition();

                if (setValueFor == 0) {
                    if (strResultDATA.equalsIgnoreCase("")) {
                        strResultDATA = wheelViewData.getData().get(itemPostion).toString();
                    }
                    categoryTV.setText(strResultDATA);
                    strResultDATA = "";
                } else if (setValueFor == 1) {
                    if (strResultDATA.equalsIgnoreCase("")) {
                        strResultDATA = wheelViewData.getData().get(itemPostion).toString();
                    }
                    genreTV.setText(strResultDATA);
                    firstGenreItem = strResultDATA;
                    strResultDATA = "";
                } else if (setValueFor == 2) {

                    if (strResultDATA.equalsIgnoreCase("") || strResultDATA.equalsIgnoreCase(firstGenreItem)) {
                        strResultDATA = wheelViewData.getData().get(itemPostion).toString();
                        Log.e(TAG, "onClick: " + strResultDATA);
                        genreValueTV.setText(strResultDATA);
                    } else {
                        genreValueTV.setText(strResultDATA);
                    }
                    strResultDATA = "";
                }

                if (setValueFor == 3) {
                    strResultDATE = strDate + strMonth + strYear;

                    /*
                     * if wheel view is not moved
                     */
                    if (strResultDATE.length() == 0) {
                        strResultDATE = main_wheel_left.getSelectedDay() + " " + main_wheel_center.getSelectedMonth() + ", " + main_wheel_right.getSelectedYear();
                    }
                    /*
                     * if wheel view is moved
                     */
                    else {
                        if (strDate.length() == 0) {
                            strDate = String.valueOf(main_wheel_left.getSelectedDay());
                        }
                        if (intMonth == 0) {
                            intMonth = main_wheel_center.getSelectedMonth();
                        }
                        if (strYear.length() == 0) {
                            strYear = String.valueOf(main_wheel_right.getSelectedYear());
                        }
                        /*
                         * to change month intvalue to month name
                         */
                        strMonth = monthMap.get(intMonth - 1);
                        /*
                         * to make 1 to 01;
                         */
                        if (strDate.length() == 1) {
                            strDate = "0" + strDate;
                        }

                        strDateFinal = strYear + "-" + intMonth + "-" + strDate;
                        strResultDATE = strMonth + " " + strDate + ", " + strYear;
                    }

                    itemLastUsedDATE.setText(strResultDATE);
                }

                if (!categoryTV.getText().equals("")) {
                    categoryTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
                if (!genreTV.getText().equals("")) {
                    genreTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
                if (!genreValueTV.getText().equals("")) {
                    genreValueTV.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
                if (!itemLastUsedDATE.getText().equals("")) {
                    itemLastUsedDATE.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                }
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUpViews(View v) {
        txtItemCountTV = v.findViewById(R.id.txtItemCountTV);
        recyclerViewImagesVideo = v.findViewById(R.id.recyclerViewImagesVideo);
        editItemNameET = v.findViewById(R.id.editItemNameET);
//        editItemNameET.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        editItemPriceET = v.findViewById(R.id.editItemPriceET);
        editItemWeightET = v.findViewById(R.id.editItemWeightET);
        editItemStoryET = v.findViewById(R.id.editItemStoryET);
        editItemTagSepratedET = v.findViewById(R.id.editItemTagSepratedET);
        editPersonalTrasET = v.findViewById(R.id.editPersonalTrasET);
        editLocationET = v.findViewById(R.id.editLocationET);
        editTypeCharityName = v.findViewById(R.id.editTypeCharityName);
        itemLastUsedDATE = v.findViewById(R.id.itemLastUsedDATE);
        txtConfirmTV = v.findViewById(R.id.txtConfirmTV);
        selectIMG = v.findViewById(R.id.selectIMG);
        switchAutographed = v.findViewById(R.id.switchAutographed);
        switchForCharity = v.findViewById(R.id.switchForCharity);
        rightRL = v.findViewById(R.id.rightRL);
        leftRL = v.findViewById(R.id.leftRL);
        radioPersonal = v.findViewById(R.id.radioPersonal);
        radioShipping = v.findViewById(R.id.radioShipping);
        firstCharityLL = v.findViewById(R.id.firstCharityLL);
        rootLayout = v.findViewById(R.id.rootLayout);
        nextTV = v.findViewById(R.id.nextTV);
        shppingtypeLL = v.findViewById(R.id.shppingtypeLL);
        item_splash2RL = v.findViewById(R.id.item_splash2RL);
        llok = v.findViewById(R.id.llok);
        shippingTypeTV = v.findViewById(R.id.shippingTypeTV);


//        editItemStoryET.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (v.getId() == R.id.editItemStoryET) {
//                    v.getParent().requestDisallowInterceptTouchEvent(true);
//                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
//                        case MotionEvent.ACTION_UP:
//                            v.getParent().requestDisallowInterceptTouchEvent(false);
//                            break;
//                    }
//                }
//                return false;
//            }
//        });


        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.wheelview_layout, null);
        mBottomSheetDialog.setContentView(sheetView);

        bottomSheet = mBottomSheetDialog.findViewById(R.id.bottom_sheet);
        cancelTV = bottomSheet.findViewById(R.id.cancelTV);
        doneTV = bottomSheet.findViewById(R.id.doneTV);
        sheetTitleTV = bottomSheet.findViewById(R.id.sheetTitleTV);
        main_wheel_left = bottomSheet.findViewById(R.id.main_wheel_left);
        main_wheel_center = bottomSheet.findViewById(R.id.main_wheel_center);
        main_wheel_right = bottomSheet.findViewById(R.id.main_wheel_right);
        wheeldataLL = bottomSheet.findViewById(R.id.wheeldataLL);
        wheelpickerLL = bottomSheet.findViewById(R.id.wheelpickerLL);
        ll_date = bottomSheet.findViewById(R.id.ll_date);
        wheelViewData = mBottomSheetDialog.findViewById(R.id.wheelview);

        wheelViewData.setData(Arrays.asList(arraySports));
        /*
         *  ids for categories
         */
        categoryTV = v.findViewById(R.id.categoryTV);
        genreTV = v.findViewById(R.id.genreTV);
        genreValueTV = v.findViewById(R.id.genreValueTV);
        categoryRL = v.findViewById(R.id.categoryRL);
        genreRl = v.findViewById(R.id.genreRl);
        genreValueRL = v.findViewById(R.id.genreValueRL);

        RoundingParams roundingParams = new RoundingParams();
        roundingParams.setCornersRadii(20, 20, 20, 20);
        roundingParams.setOverlayColor(Color.WHITE);
        selectIMG.getHierarchy().setPlaceholderImage(R.drawable.camera_additem);
        selectIMG.getHierarchy().setRoundingParams(roundingParams);


    }

    private void setWidgetsVisibity() {
        ll_date.setVisibility(View.GONE);
        wheeldataLL.setVisibility(View.GONE);
        wheelpickerLL.setVisibility(View.GONE);
        firstCharityLL.setVisibility(View.GONE);
        rootLayout.setVisibility(View.VISIBLE);
        shppingtypeLL.setVisibility(View.GONE);
        item_splash2RL.setVisibility(View.GONE);
        llok.setVisibility(View.VISIBLE);
        shippingTypeTV.setVisibility(View.VISIBLE);

        /*
         * topbar
         */
        leftRL.setVisibility(View.GONE);
        rightRL.setVisibility(View.GONE);
    }

    private void setUpClickListeners() {
        strItemName = editItemNameET.getText().toString();
        strItemPrice = editItemPriceET.getText().toString();

        strItemPrice = strItemPrice.replace("$", "");


        strItemWeight = editItemWeightET.getText().toString();
        strCategory = categoryTV.getText().toString();
        strGenre = genreTV.getText().toString();
        strGenreValue = genreValueTV.getText().toString();
        strItemStory = editItemStoryET.getText().toString();
        strTags = editItemTagSepratedET.getText().toString();

        //  isAutograph
        //  isForCharity
        //  isPersonalTransfer

        strCharityName = editTypeCharityName.getText().toString();
        strLocation = editLocationET.getText().toString();
        strResultDATE = itemLastUsedDATE.getText().toString();


        editItemPriceET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String itemprize = editItemPriceET.getText().toString();
                    if (itemprize.length() > 0) {
                        if (itemprize.equalsIgnoreCase("$")) {
                            editItemPriceET.setText("");
                            intPrice = 0;
                        }
                        if (!itemprize.contains("$")) {
                            editItemPriceET.setFilters(new InputFilter[]{new InputFilter.LengthFilter(7)});
                            editItemPriceET.setText("$" + itemprize);
                            intPrice = Integer.parseInt(itemprize);
                        }
                    }
                    // code to execute when EditText loses focus
                }
            }
        });
        editItemPriceET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i1 == 1) {
                    editItemPriceET.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editItemWeightET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {

                    if (!editItemWeightET.getText().toString().equalsIgnoreCase("")) {
                        Float weight = Float.parseFloat(editItemWeightET.getText().toString());
                        if (weight > 0 && weight < 50) {
                            radioShipping.setChecked(true);
                            radioPersonal.setChecked(false);
                            Constants_AddItem.ITEM_DELVIERYTYPE = "true";
                            isPersonalTransfer = false;
                            editPersonalTrasET.setEnabled(false);
                        } else {
                            radioPersonal.setChecked(true);
                            radioShipping.setChecked(false);
                            Constants_AddItem.ITEM_DELVIERYTYPE = "false";
                            isPersonalTransfer = true;
                            editPersonalTrasET.setEnabled(true);
                        }
                    }
                }
            }
        });

        txtConfirmTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                JSONObject locationJson = new JSONObject();
                JSONArray tagJson = new JSONArray();
                JSONArray imageJson = new JSONArray();

                String news = editItemTagSepratedET.getText().toString();

                String[] words = news.split(",");//splits the string based on whitespace
                for (String w : words) {
                    tagArraylist.add(w);
                    tagJson.put(w);
                }

                ArrayList<byte[]> imagesList = new ArrayList<>();

                for (int i = 0; i < mImagesVideoArrayList.size(); i++) {
                    if (mImagesVideoArrayList.get(i).getUriType().equalsIgnoreCase("video_type")) {
                        videoImage = mImagesVideoArrayList.get(i).getStrUrlByteArray();
                        VideoData = mImagesVideoArrayList.get(i).getStrVideobyteArray();
                    } else {
                        imagesList.add(mImagesVideoArrayList.get(i).getStrUrlByteArray());
                    }
                }
                for (int i = 0; i < imagesList.size(); i++) {
                    if (i == 0) {
                        image1 = imagesList.get(i);
                    } else if (i == 1) {
                        image2 = imagesList.get(i);
                    } else if (i == 2) {
                        image3 = imagesList.get(i);
                    } else if (i == 3) {
                        image4 = imagesList.get(i);
                    } else if (i == 4) {
                        image5 = imagesList.get(i);
                    } else if (i == 5) {
                        image6 = imagesList.get(i);
                    }
                }
                try {
                    strItemName = editItemNameET.getText().toString();
                    strItemPrice = editItemPriceET.getText().toString();
                    strItemWeight = editItemWeightET.getText().toString();
                    strCategory = categoryTV.getText().toString();
                    strGenre = genreTV.getText().toString();
                    strGenreValue = genreValueTV.getText().toString();
                    strItemStory = editItemStoryET.getText().toString();
                    strTags = editItemTagSepratedET.getText().toString();

                    strCharityName = editTypeCharityName.getText().toString();
                    strLocation = editLocationET.getText().toString();
                    strResultDATE = itemLastUsedDATE.getText().toString();

                    if (isPersonalTransfer) {
                        strPersonalTransfer = "personal transfer";
                    } else {
                        strPersonalTransfer = "shipping";
                    }
                    Log.e(TAG, "intMonth: " + intMonth);
                    int monthsize = String.valueOf(intMonth).length();
                    int strDatesize = String.valueOf(strDate).length();


                    Log.e(TAG, "monthsize: " + monthsize);
                    Log.e(TAG, "strDatesize: " + strDatesize);
                    if (monthsize == 1)
                        strDateFinal = strYear + "-" + "0" + intMonth + "-" + strDate;
                    else
                        strDateFinal = strYear + "-" + intMonth + "-" + strDate;
                    if (strDatesize == 1) {
                        strDateFinal = strYear + "-" + intMonth + "-" + "0" + strDate;
                    }
                    if (monthsize == 1 && strDatesize == 1)
                        strDateFinal = strYear + "-0" + intMonth + "-" + "0" + strDate;

                    Log.e(TAG, "strDateFinal: " + strDateFinal);

                   /* if (strPostalcode.equalsIgnoreCase("")) {
                      openPostacodeDialog();
                    }
                   else*/
                    if (validate(strItemName, strItemPrice, strItemWeight, strItemStory, strCategory, strGenre, strGenreValue, mImagesVideoArrayList.size(), isPersonalTransfer)) {

                        strItemPrice = strItemPrice.replace("$", "");
                        intPrice = Integer.parseInt(strItemPrice);

                        jsonObject.put("item_name", strItemName);
                        jsonObject.put("price", intPrice);
                        jsonObject.put("weight", strItemWeight);

                        jsonObject.put("item_story", strItemStory);
                        jsonObject.put("sport", strGenre);
                        jsonObject.put("charity_name", strCharityName);
                        jsonObject.put("currency", "USD");
                        jsonObject.put("category", strCategory);
                        jsonObject.put("genre", strGenre);
                        jsonObject.put("genre_value", strGenreValue);

                        jsonObject.put("autographed", isAutograph);
                        jsonObject.put("charity", isForCharity);
                        jsonObject.put("last_used", strDateFinal + "T13:37:48.338+0530");

                        jsonObject.put("delivery_type", isPersonalTransfer);
                        jsonObject.put("person_transfertext", editPersonalTrasET.getText().toString());
                        jsonObject.put("tags", tagJson);

                        locationJson.put("administrative_area_level_1", strAdminArea);
                        locationJson.put("country", strCountry);
                        locationJson.put("lat", strLat);
                        locationJson.put("lng", strLong);
                        locationJson.put("locality", strLocalty);
                        locationJson.put("postal_code", strPostalcode);
                        jsonObject.put("location", locationJson);

                        Log.e(TAG, "strMonth: " + strMonth);
                        Log.e(TAG, "onClick: " + jsonObject);
                        addItemService(jsonObject);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        rightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        categoryRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrayListWheelData = catArrayList;
                if (arrayListWheelData.size() > 0) {
                    setValueFor = 0;
                    wheelViewData.setData(arrayListWheelData);
                    wheeldataLL.setVisibility(View.VISIBLE);
                    wheelpickerLL.setVisibility(View.GONE);
                    ll_date.setVisibility(View.GONE);
                    sheetTitleTV.setText(getActivity().getResources().getString(R.string.selectcategory));
                    mBottomSheetDialog.show();
                }
            }

        });

        genreRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    arrayListWheelData = genreList;
                    if (arrayListWheelData.size() > 0) {
                        setValueFor = 1;
                        wheelViewData.setData(arrayListWheelData);
                        wheeldataLL.setVisibility(View.VISIBLE);
                        wheelpickerLL.setVisibility(View.GONE);
                        ll_date.setVisibility(View.GONE);
                        sheetTitleTV.setText(getActivity().getResources().getString(R.string.chooseGenre));
                        mBottomSheetDialog.show();
                        genreValueTV.setText("");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        genreValueRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    arrayListWheelData = new ArrayList<>();
                    String strGenre = genreTV.getText().toString();
                    if (!strGenre.equalsIgnoreCase("")) {
                        for (int k = 0; k < genreArrayList.size(); k++) {
                            if (strGenre.equalsIgnoreCase(genreArrayList.get(k).getGenreName())) {
                                for (int i = 0; i < genreArrayList.get(k).getGenreValueList().size(); i++) {
                                    arrayListWheelData.add(genreArrayList.get(k).getGenreValueList().get(i).getValueName());
                                }
                            }
                        }

                        if (arrayListWheelData.size() > 0) {
                            setValueFor = 2;
                            wheelViewData.setData(arrayListWheelData);
                            wheelViewData.setSelected(true);
                            wheeldataLL.setVisibility(View.VISIBLE);
                            wheelpickerLL.setVisibility(View.GONE);
                            ll_date.setVisibility(View.GONE);
                            sheetTitleTV.setText(getActivity().getResources().getString(R.string.chooseGenreValue));
                            mBottomSheetDialog.show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        itemLastUsedDATE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setValueFor = 3;
                wheeldataLL.setVisibility(View.GONE);
                wheelpickerLL.setVisibility(View.VISIBLE);
                ll_date.setVisibility(View.VISIBLE);
                sheetTitleTV.setText("Select Date");
                mBottomSheetDialog.show();
            }
        });

        selectIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e(TAG, "selectIMG: " + mImagesVideoArrayList.size());
                Log.e(TAG, "selectIMG: isVideoSelected  " + isVideoSelected);
                if (mImagesVideoArrayList.size() < 7) {
                    checkAndroidPermissions();
                }
            }
        });

        /*
         * Radio buttons and Check buttons
         */
        radioPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = 0;
                hideKeyboard(radioPersonal);
                if (PlayPawnPref_Once.readInteger(getActivity(), PlayPawnPref_Once.FirstPersonal, 0) <= 2) {
                    shppingtypeLL.setVisibility(View.GONE);
                    item_splash2RL.setVisibility(View.VISIBLE);
                    llok.setVisibility(View.VISIBLE);
                    nextTV.setVisibility(View.VISIBLE);
                    shippingTypeTV.setVisibility(View.VISIBLE);
                    HomeActivity.bottombar.setVisibility(View.GONE);
                    HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
                    count = PlayPawnPref_Once.readInteger(getActivity(), PlayPawnPref_Once.FirstPersonal, 0);

                    PlayPawnPref_Once.writeInteger(getActivity(), PlayPawnPref_Once.FirstPersonal, ++count);
                }
                isPersonalTransfer = true;
                editPersonalTrasET.setEnabled(true);
            }
        });

        radioShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count;
                hideKeyboard(radioShipping);
                if (PlayPawnPref_Once.readInteger(getActivity(), PlayPawnPref_Once.FirstShipping, 0) <= 2) {
                    shppingtypeLL.setVisibility(View.VISIBLE);
                    nextTV.setVisibility(View.VISIBLE);
                    shippingTypeTV.setVisibility(View.VISIBLE);
                    HomeActivity.bottombar.setVisibility(View.GONE);
                    HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
                    count = PlayPawnPref_Once.readInteger(getActivity(), PlayPawnPref_Once.FirstPersonal, 0);

                    PlayPawnPref_Once.writeInteger(getActivity(), PlayPawnPref_Once.FirstShipping, ++count);
                }
                isPersonalTransfer = false;
                editPersonalTrasET.setText("");
                editPersonalTrasET.setEnabled(false);
            }
        });

        switchAutographed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                hideKeyboard(switchAutographed);
                isAutograph = b;
            }
        });

        switchForCharity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                hideKeyboard(switchForCharity);
                if (b) {
                    int count;
                    if (PlayPawnPref_Once.readInteger(getActivity(), PlayPawnPref_Once.FirstForCharity, 0) <= 2) {
                        firstCharityLL.setVisibility(View.VISIBLE);
                        HomeActivity.bottombar.setVisibility(View.GONE);
                        HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);
                        count = PlayPawnPref_Once.readInteger(getActivity(), PlayPawnPref_Once.FirstForCharity, 0);
                        PlayPawnPref_Once.writeInteger(getActivity(), PlayPawnPref_Once.FirstForCharity, ++count);
                    }
                    isForCharity = true;
                    editTypeCharityName.setEnabled(true);
                } else {
                    isForCharity = false;
                    editTypeCharityName.setEnabled(false);
                }
            }
        });

        nextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstCharityLL.setVisibility(View.GONE);
                shppingtypeLL.setVisibility(View.GONE);
                HomeActivity.bottombar.setVisibility(View.VISIBLE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
            }
        });
        shippingTypeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstCharityLL.setVisibility(View.GONE);
                shppingtypeLL.setVisibility(View.GONE);
                HomeActivity.bottombar.setVisibility(View.VISIBLE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
            }
        });
        llok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item_splash2RL.setVisibility(View.GONE);
                firstCharityLL.setVisibility(View.GONE);
                shppingtypeLL.setVisibility(View.GONE);
                HomeActivity.bottombar.setVisibility(View.VISIBLE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
            }
        });

        editTypeCharityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(editTypeCharityName);
                HomeActivity.bottombar.setVisibility(View.GONE);
                HomeActivity.imgAddPlayerPawn.setVisibility(View.GONE);

                Constants_AddItem.ITEM_NAME = editItemNameET.getText().toString();
                Constants_AddItem.ITEM_PRICE = editItemPriceET.getText().toString();

                Constants_AddItem.ITEM_WEIGHT = editItemWeightET.getText().toString();
                Constants_AddItem.ITEM_CAT = categoryTV.getText().toString();
                Constants_AddItem.ITEM_GENRE = genreTV.getText().toString();
                Constants_AddItem.ITEM_GENREVALUE = genreValueTV.getText().toString();

                Constants_AddItem.ITEM_STORY = editItemStoryET.getText().toString();

                Constants_AddItem.TAGS = editItemTagSepratedET.getText().toString();
                Constants_AddItem.VideoDAta = VideoData;
                if (radioShipping.isChecked()) {
                    Constants_AddItem.ITEM_DELVIERYTYPE = "true";
                }
                if (radioPersonal.isChecked()) {
                    Constants_AddItem.ITEM_DELVIERYTYPE = "false";
                }
                Constants_AddItem.ITEM_PERSONAL_TRANSFER = editPersonalTrasET.getText().toString();

                if (switchAutographed.isChecked()) {
                    Constants_AddItem.ITEM_AUTO = "auto";
                }

                if (switchForCharity.isChecked()) {
                    Constants_AddItem.ITEM_CHARITY = "charity";
                }

                Constants_AddItem.ITEM_CHARITYNAME = editTypeCharityName.getText().toString();

                Constants_AddItem.ITEM_LASTUSED = itemLastUsedDATE.getText().toString();

                HomeActivity.switchFragment(getActivity(), new CharityOptions(), Constants.ADD_ITEM_PLAYERPAWN_FRAGMENT, true, null);
            }
        });


        editLocationET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!locationEnable) {
                    locationEnable = true;
                    try {
                        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                                .build();
                        Intent intent =
                                new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                        .build(getActivity());
                        startActivityForResult(intent, 1);
                    } catch (GooglePlayServicesRepairableException e) {
                        // Indicates that Google Play Services is either not installed or not up to date. Prompt
                        // the user to correct the issue.
                        GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), e.getConnectionStatusCode(),
                                0 /* requestCode */).show();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        // Indicates that Google Play Services is not available and the problem is not easily
                        // resolvable.
                        String message = "Google Play Services is not available: " +
                                GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

                        Log.e(TAG, message);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    private void openPostacodeDialog() {
        final Dialog pincodeDialog = new Dialog(getActivity());
        pincodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pincodeDialog.setContentView(R.layout.pincode_layout);
        pincodeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        LinearLayout cancel = pincodeDialog.findViewById(R.id.cancelLL);
        LinearLayout confirm = pincodeDialog.findViewById(R.id.confirmLL);
        final EditText postalcodeET = pincodeDialog.findViewById(R.id.postalcodeET);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pincodeDialog.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strPostalcode = postalcodeET.getText().toString();
                pincodeDialog.dismiss();
            }
        });
        pincodeDialog.show();
    }

   /* private boolean validate(String strItemName, String strItemPrice, String strItemWeight, String strItemStory, String strCategory, String strGenre, String strGenreValue, int imageArraySize, boolean isPersonalTransfer) {

        if (strItemName.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please insert Item Name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (strItemPrice.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please insert Item Price", Toast.LENGTH_SHORT).show();

            return false;
        } else if (strItemWeight.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please insert Item Weight", Toast.LENGTH_SHORT).show();

            return false;
        } else if (strItemStory.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please insert Item Story", Toast.LENGTH_SHORT).show();

            return false;
        } else if (strCategory.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please Select Item Category", Toast.LENGTH_SHORT).show();

            return false;
        } else if (strGenre.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please Select Item Genre", Toast.LENGTH_SHORT).show();

            return false;
        } else if (strGenreValue.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Please Select Item GenreValue", Toast.LENGTH_SHORT).show();

            return false;
        } else if (imageArraySize == 0) {
            Toast.makeText(getActivity(), "Please Select Atlest 1 Image or Video", Toast.LENGTH_SHORT).show();
            return false;
        } else if (isPersonalTransfer) {
            String personalTransfer = editPersonalTrasET.getText().toString();
            if (personalTransfer.equalsIgnoreCase("")) {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please enter personal transfer message");
                return false;
            }
        }
        Log.e(TAG, "validate: " + strPostalcode);
        if (strPostalcode.equals("")) {
            final Dialog pincodeDialog = new Dialog(getActivity());
            pincodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pincodeDialog.setContentView(R.layout.pincode_layout);
            pincodeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


            LinearLayout cancel = pincodeDialog.findViewById(R.id.cancelLL);
            LinearLayout confirm =  pincodeDialog.findViewById(R.id.confirmLL);
            final EditText postalcodeET =  pincodeDialog.findViewById(R.id.postalcodeET);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pincodeDialog.dismiss();
                }
            });

            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    strPostalcode = postalcodeET.getText().toString();
                    pincodeDialog.dismiss();
                }
            });
        }
        return true;
    }*/

    private boolean validate(String strItemName, String strItemPrice, String strItemWeight, String strItemStory, String strCategory, String strGenre, String strGenreValue, int imageArraySize, boolean isPersonalTransfer) {
        Double weigthint = 0.0;
        if (!strItemWeight.equalsIgnoreCase("")) {
            weigthint = Double.valueOf(strItemWeight);
        }
        Log.e(TAG, "validate:switchForCharity " + switchForCharity.isChecked());
        if (strItemName.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Name");
            return false;
        } else if (strItemPrice.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Price");
            return false;
        } else if (strItemWeight.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Weight");
            return false;
        } else if (strItemStory.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please insert Item Story");
            return false;
        } else if (strCategory.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Item Category");
            return false;
        } else if (strGenre.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Item Genre");
            return false;
        } else if (strGenreValue.equalsIgnoreCase("")) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Item GenreValue");
            return false;
        } else if (imageArraySize == 0) {
            AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please Select Atlest 1 Image or Video");
            return false;
        } else if (isPersonalTransfer) {
            String personalTransfer = editPersonalTrasET.getText().toString();
            if (personalTransfer.equalsIgnoreCase("")) {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please enter personal transfer message");
                return false;
            }
        } else if (!isPersonalTransfer) {
            Log.w(TAG, "validate: " + weigthint);
            if (editLocationET.getText().toString().equalsIgnoreCase("")) {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please enter location");
                return false;
            }
            if (weigthint >= 50) {
                showtranferDialog(getActivity(), "Membilia", "Given the weight of your item we strongly suggest you post this item as an in personal tranfer so your fan can meet you or your agent for pick up!" +
                        "Pawn away.");
                return false;
            }
        }

        if (switchForCharity.isChecked()) {
            Log.e(TAG, "validate:switchForCharity :true ");
            if (editTypeCharityName.getText().toString().equalsIgnoreCase("")) {
                AlertDialogManager.showAlertDialog(getActivity(), "Membilia", "Please enter Charity Name");
                return false;
            } else {
                Log.e(TAG, "validate:switchForCharity :true|false ");
            }
        } else {
            Log.e(TAG, "validate:switchForCharity false ");
        }


        Log.e(TAG, "validate:strPostalcode " + strPostalcode);
        if (strPostalcode.equals("")) {
            openPostacodeDialog();
            return false;
        }
        return true;
    }

    public void showtranferDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_trasfer);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        TextView txttransfer = alertDialog.findViewById(R.id.txttransfer);

        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txttransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                radioPersonal.setChecked(true);
                radioShipping.setChecked(false);
                Constants_AddItem.ITEM_DELVIERYTYPE = "false";
                isPersonalTransfer = true;
                editPersonalTrasET.setEnabled(true);
            }
        });
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    private void checkAndroidPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermissions()) {
                showCameraGalleryDialog();
            } else {
                requestPermission();
            }
        } else {
            // write your logic here
            showCameraGalleryDialog();
        }
    }

    private boolean checkPermissions() {
        int camera = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), CameraPermission);
        int read = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), ReadStorage);
        int write = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), WriteStorage);

        return camera == PackageManager.PERMISSION_GRANTED &&
                read == PackageManager.PERMISSION_GRANTED &&
                write == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CameraPermission, ReadStorage,
                WriteStorage}, 100);
    }


    private void showCameraGalleryDialog() {
        final Dialog cameraGalleryDialog = new Dialog(getActivity());
        cameraGalleryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraGalleryDialog.setContentView(R.layout.dialog_camera_video);
        cameraGalleryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        LinearLayout createVideoTV = cameraGalleryDialog.findViewById(R.id.createVideoLL);
        LinearLayout chooseVideoTV = cameraGalleryDialog.findViewById(R.id.chooseVideoLL);
        LinearLayout cameraTV = cameraGalleryDialog.findViewById(R.id.cameraLL);
        LinearLayout photoTV = cameraGalleryDialog.findViewById(R.id.photoLL);

        TextView cancelTV = cameraGalleryDialog.findViewById(R.id.cancelTV);

        if (isVideoSelected) {
            createVideoTV.setVisibility(View.GONE);
            chooseVideoTV.setVisibility(View.GONE);
        } else {
            createVideoTV.setVisibility(View.VISIBLE);
            chooseVideoTV.setVisibility(View.VISIBLE);
        }

        if (!isVideoSelected && mImagesVideoArrayList.size() > 6) {
            cameraTV.setVisibility(View.VISIBLE);
            photoTV.setVisibility(View.VISIBLE);
        } else if (!isVideoSelected && mImagesVideoArrayList.size() == 6) {
            cameraTV.setVisibility(View.GONE);
            photoTV.setVisibility(View.GONE);
        }

        cancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
            }
        });
        cameraTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                 * Open Camera********/
                cameraGalleryDialog.dismiss();
                dispatchTakePictureIntent();
//                openCamera1();

            }
        });
        photoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
                openGallery();

            }
        });
        createVideoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
                openVideoCamera();

            }
        });
        chooseVideoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
                openVideoGallery();

            }
        });
        cameraGalleryDialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.membilia.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }

    private void openCamera1() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) == null) {
            return;
        }
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private void openVideoCamera() {
        Constants.OpenAddItemFrom = "Charity";
        Intent takeVideoIntent = new Intent(ACTION_VIDEO_CAPTURE);
        takeVideoIntent.putExtra(EXTRA_VIDEO_QUALITY, 0);
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, VIDEO_REQUEST);
        }
    }

    private void openVideoGallery() {
        Constants.OpenAddItemFrom = "Charity";
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_PICK);
        intent.putExtra(EXTRA_VIDEO_QUALITY, 0);
        startActivityForResult(intent, VIDEO_GALLERY_REQUEST);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PNG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".png",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode,resultCode,intent);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Geocoder geocoder;
                List<Address> addresses = null;

                Place place = PlaceAutocomplete.getPlace(getActivity(), intent);
                Log.e(TAG, "onActivityResult: " + place.getLatLng());
                geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    strLat = String.valueOf(place.getLatLng().latitude);
                    strLong = String.valueOf(place.getLatLng().longitude);
                    strCountry = country;
                    if (postalCode != null) {
                        strPostalcode = postalCode;
                    } else {
                        strPostalcode = "";
                    }
                    strAdminArea = address;
                    strLocalty = city;

                } catch (IOException e) {
                    e.printStackTrace();
                }


//                Log.e(TAG, "strAdminArea: " + strAdminArea);
//                Log.e(TAG, "strLocalty: " + strLocalty);
//                Log.e(TAG, "place.getName(): " + place.getName());
//                Log.e(TAG, "address: " + address);
//                Log.e(TAG, "place.getAddress(): " + place.getAddress());
//                Log.e(TAG, "strPostcode: " + strPostalcode);
                editLocationET.setText(place.getAddress());
                locationEnable = false;
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), intent);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            locationEnable = false;
        }

        if (requestCode == VIDEO_GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri videoUri = intent.getData();
            isVideoSelected = true;
            getdatapicture(videoUri);
        } else if (requestCode == VIDEO_REQUEST && resultCode == RESULT_OK) {
            Uri videoUri = intent.getData();
            isVideoSelected = true;
            getdataVideoCamera(videoUri);
        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            try {

                Bitmap BitmapImages = CameraUtils.getBitmap(mCurrentPhotoPath);
                Uri tempUria = CameraUtils.getImageUri(BitmapImages, getActivity());
//            try {
//                BitmapImages = getCorrectlyOrientedImage(getActivity(), tempUria);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
//            Uri tempUri = CameraUtils.getImageUri(BitmapImages, getActivity());
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrImageVideoPath(String.valueOf(tempUria));
                mImagesItemModel.setStrVideoThumbnail(BitmapImages);
                mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), BitmapImages));
                mImagesVideoArrayList.add(mImagesItemModel);
                setAdapter(mImagesVideoArrayList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            InputStream stream = null;
            try {
                Bitmap bitmap;
                stream = getActivity().getContentResolver().openInputStream(intent.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                try {
                    bitmap = getCorrectlyOrientedImage(getActivity(), intent.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = CameraUtils.getImageUri(bitmap, getActivity());
                ImagesItemModel mImagesItemModel = new ImagesItemModel();
                mImagesItemModel.setUriType(IMAGE_TYPE);
                mImagesItemModel.setStrImageVideoPath(String.valueOf(tempUri));
                mImagesItemModel.setStrVideoThumbnail(bitmap);
                mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), bitmap));
                mImagesVideoArrayList.add(mImagesItemModel);
                setAdapter(mImagesVideoArrayList);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    public  Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();
        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);
        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }
        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 400 || rotatedHeight > 400) {
            float widthRatio = ((float) rotatedWidth) / ((float) 400);
            float heightRatio = ((float) rotatedHeight) / ((float) 400);
            float maxRatio = Math.max(widthRatio, heightRatio);
            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
        }
        return srcBitmap;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri, new String[]{
                MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    /**
     * method is used to create tumbnail from video
     *
     * @param videoUri
     */
    private void getdatapicture(Uri videoUri) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        Bitmap mBitmap = null;
        try {
            String scheme = videoUri.getScheme();
            Log.i("uri", "" + videoUri + " " + scheme);
            retriever.setDataSource(getActivity(), videoUri);
            mBitmap = retriever.getFrameAtTime(-1, MediaMetadataRetriever.OPTION_CLOSEST);
            Log.d("bitmap", " " + mBitmap.getWidth() + " " + mBitmap.getHeight());
        } catch (IllegalArgumentException ex) {
            // Assume this is a corrupt video file
            Log.d("IllegalArgument", "" + ex.getMessage());
        } catch (RuntimeException ex) {
            // Assume this is a corrupt video file.
            Log.d("RuntimeException", "" + ex.getMessage());
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
                Log.d("RuntimeException", "" + ex.getMessage());
            }
        }
        String strVideoPath = "";
        strVideoPath = CameraUtils.getRealVideoPathFromURI(videoUri);
        InputStream iStream = null;
        try {
            iStream = getApplicationContext().getContentResolver().openInputStream(videoUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            byte[] inputData = AppHelper.getBytes(iStream);
            VideoData = inputData;
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*Setting Data on Adapter*/
        ImagesItemModel mImagesItemModel = new ImagesItemModel();
        mImagesItemModel.setStrVideoThumbnail(mBitmap);
        mImagesItemModel.setStrImageVideoPath(strVideoPath);
        mImagesItemModel.setUriType(VIDEO_TYPE);
        mImagesItemModel.setStrVideobyteArray(VideoData);
        mImagesItemModel.setStrCameFrom("camera");
        mImagesItemModel.setStrVideoImage(strVideoPath);
        mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), mBitmap));
//        Constants.CallImageAdpaterFrom = "camera";
        mImagesVideoArrayList.add(mImagesItemModel);
        setAdapter(mImagesVideoArrayList);
    }

    private void getdataVideoCamera(Uri videoUri) {
        InputStream iStream = null;
        byte[] inputCompressData = null;
        Log.i("", "" + videoUri);
        //To Show The Video Thumbnail......
        String filePath = CameraUtils.getThumbnailPathForLocalFile(getActivity(), videoUri);
        Bitmap resized = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(filePath), 400, 400, true);
        File file1 = new File(videoUri.getPath());
        file1.delete();
        if (file1.exists()) {
            try {
                file1.getCanonicalFile().delete();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file1.exists()) {
                getActivity().getApplicationContext().deleteFile(file1.getName());
            }
        }
        /*Convert URI to Byte Array**/
        try {
            iStream = getApplicationContext().getContentResolver().openInputStream(videoUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            byte[] inputData = CameraUtils.getBytes(iStream);
            VideoData = inputData;
            Constants_AddItem.VideoDAta = VideoData;
            inputCompressData = CameraUtils.compressArray(inputData);

        } catch (IOException e) {
            e.printStackTrace();
        }
        ImagesItemModel mImagesItemModel = new ImagesItemModel();
        mImagesItemModel.setStrImageVideoPath(filePath);
        mImagesItemModel.setUriType(VIDEO_TYPE);
        mImagesItemModel.setStrVideoThumbnail(resized);
        mImagesItemModel.setStrVideobyteArray(VideoData);
        mImagesItemModel.setStrUrlByteArray(AppHelper.getFileDataFromBitmap(getActivity(), resized));
        mImagesVideoArrayList.add(mImagesItemModel);
        setAdapter(mImagesVideoArrayList);
    }


    public void setAdapter(ArrayList<ImagesItemModel> mArrayList) {
        // i replace video count with isvideoSelected videocount also used for same purpose
        if (isVideoSelected) {
            txtItemCountTV.setText(mImagesVideoArrayList.size() + "/7");
        } else {
            txtItemCountTV.setText(mImagesVideoArrayList.size() + "/6");
        }
        AddImageListAdapter addImageListAdapter = new AddImageListAdapter(getActivity(), mArrayList, mGetItemCountAndRemoveItem);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewImagesVideo.setLayoutManager(horizontalLayoutManager);
        recyclerViewImagesVideo.setAdapter(addImageListAdapter);
        addImageListAdapter.notifyDataSetChanged();

    }

    @Override
    public void onPause() {
        super.onPause();
        HomeActivity.onPause = "yes";
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onPlaceSelected(Place place) {
        strLat = String.valueOf(place.getLatLng().latitude);
        strLong = String.valueOf(place.getLatLng().longitude);
        place.getAddress();
        Log.e(TAG, "onPlaceSelected: " + place.getAddress());
    }

    @Override
    public void onError(Status status) {
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /*
     * service for cat and genre
     */
    private void getGenreService() {
        String url = WebServicesConstants.GET_GENRE_VALUE;
        String strConstant = Constants.GET_GENRE_VALUE;
        serviceArea(url, strConstant);
    }

    private void getCatService() {
        String url = WebServicesConstants.GET_CATEGORIES;
        String strConstant = Constants.GET_CATEGORIES;
        serviceArea(url, strConstant);
    }

    public void serviceArea(String url, final String strConstant) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "*****RESPONSE****" + response);
                if (strConstant.equalsIgnoreCase(Constants.GET_CATEGORIES)) {
                    parsejson_categories(response);
                } else {
                    parsejson_genre(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        // Adding request to request queue
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, strConstant);
    }

    private void parsejson_genre(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                genreValueList = new ArrayList<>();
                GenreModel genreModel = new GenreModel();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                genreModel.setGenreName(jsonObject.getString("name"));
                JSONArray jsonArrayList = jsonObject.getJSONArray("list");
                for (int j = 0; j < jsonArrayList.length(); j++) {
                    JSONObject jsonObjectList = jsonArrayList.getJSONObject(j);
                    GenreValue genreValue = new GenreValue();
                    genreValue.setCategoryID(jsonObjectList.getString("category_id"));
                    genreValue.setDescription(jsonObjectList.getString("description"));
                    genreValue.setValueName(jsonObjectList.getString("name"));
                    genreValueList.add(genreValue);
                    genreModel.setGenreValueList(genreValueList);
                }
                genreArrayList.add(genreModel);
                genreList.add(genreArrayList.get(i).getGenreName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parsejson_categories(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                catArrayList.add(jsonObject.getString("name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addItemService(final JSONObject jsonObject) {
        String strUrl = WebServicesConstants.ADDITEM;
        Utilities.showProgressDialog(getActivity());
        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                JSONObject jsonArray;
                try {
                    jsonArray = new JSONObject(response);
                    String itemid = jsonArray.getString("item_id");

                    if (image2 != null || image1 != null) {
                        executeUploadImageAPI(itemid);
                    } else if (videoImage != null) {
                        executeUploadVideoAPI(itemid);
                    }
//                    else {
//                        Constants.additemFragment = "1";
//                        FragmentManager fm = getActivity().getSupportFragmentManager();
//                        fm.popBackStack();
//                        HomeActivity.SpecificFragment((FragmentActivity) getActivity(), Constants.EXPLORE_FRAGMENT, new ExploreFragment());
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                String json;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                Toast.makeText(getActivity(), "No more data", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }) {
            //            Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                Log.e(TAG, "getBody: " + jsonObject);
                Log.e(TAG, "getBody: " + jsonObject.toString().getBytes());
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, WebServicesConstants.ADDITEM);
    }


    public void executeUploadImageAPI(final String itemid) {
        Log.e(TAG, "MultipleImages: URL " + WebServicesConstants.MultipleImages);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, WebServicesConstants.MultipleImages, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                if (videoImage != null) {
                    executeUploadVideoAPI(itemid);
                } else {
                    Utilities.hideProgressDialog();
                    Constants.additemFragment = "1";
                    showAlertDialog(getActivity(), "Membilia", "Your Item has been submitted. it may take several minutes to appear in the market, so please do not close the app.");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                Log.e(TAG, "image1: " + image1);
                Log.e(TAG, "image2: " + image2);
                Log.e(TAG, "image3: " + image3);
                Log.e(TAG, "image4: " + image4);
                Log.e(TAG, "image5: " + image5);
                Log.e(TAG, "image6: " + image6);

                if (image1 != null)
                    params.put("image1", new DataPart("file_avatar.jpg", image1, "image/jpeg"));

                if (image2 != null)
                    params.put("image2", new DataPart("file_avatar1.jpg", image2, "image/jpeg"));

                if (image3 != null)
                    params.put("image3", new DataPart("file_avatar2.jpg", image3, "image/jpeg"));

                if (image4 != null)
                    params.put("image4", new DataPart("file_avatar3.jpg", image4, "image/jpeg"));

                if (image5 != null)
                    params.put("image5", new DataPart("file_avatar4.jpg", image5, "image/jpeg"));

                if (image6 != null)
                    params.put("image6", new DataPart("file_avatar5.jpg", image6, "image/jpeg"));

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("item_id", itemid);
                return param;
            }
        };

        PlayerPawnApplication.getInstance().addToRequestQueue(multipartRequest, "UploadImageRequest");
    }

    private void executeUploadVideoAPI(final String itemid) {
        Log.e(TAG, "UploadVideo: URL " + WebServicesConstants.VideoUpload);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, WebServicesConstants.VideoUpload, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                Log.e(TAG, "onResponse: " + response.data.toString());
                Utilities.hideProgressDialog();
//              Constants.SplashAddItemOn = false;
                Constants.additemFragment = "1";
                showAlertDialog(getActivity(), "Membilia", "Your Item has been submitted. it may take several minutes to appear in the market, so please do not close the app.");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                if (videoImage != null)
                    params.put("video_image", new DataPart("video_image.jpg", videoImage, "image/jpeg"));
                if (VideoData != null)
                    params.put("video", new DataPart("file.mp4", VideoData, "video/mp4"));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("item_id", itemid);
                return param;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(multipartRequest, "VideoUploadRequest");
    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void showAlertDialog(Activity mActivity, String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_showalert);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        txtDismiss.setText("Thank you");
        txtTitle.setText(strTitle);
        txtMessage.setText(strMessage);
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                HomeActivity.createhome = "yes";
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                HomeActivity.SpecificFragment(getActivity(), Constants.EXPLORE_FRAGMENT, new ExploreFragment());
            }
        });
        alertDialog.show();
    }


    public void showPopForAccount() {
        final Dialog alertDialog = new Dialog(getActivity());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_showalert);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        // set the custom dialog components - text, image and button
        TextView txtTitle = alertDialog.findViewById(R.id.txtTitle);
        TextView txtMessage = alertDialog.findViewById(R.id.txtMessage);
        TextView txtDismiss = alertDialog.findViewById(R.id.txtDismiss);
        txtTitle.setText(getActivity().getResources().getString(R.string.app_name));
        txtMessage.setText("Please add Payout Method");
        txtDismiss.setText("Ok");
        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                HomeActivity.isAddItemOpen = true;
                HomeActivity.switchFragment(getActivity(), new FragmentpaymentMethod(), Constants.Fragment_PaymentMethod, true, null);
            }
        });
        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBottomSheetDialog.hide();
    }
}
