package com.membilia.Fragments;

/*
 * Created by dharmaniapps on 23/1/18.
 */

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.baoyz.widget.PullRefreshLayout;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.AppSingleton;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.HorizontalAdapter;
import com.membilia.adapters.ItemListAdapter;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;
import com.membilia.volley_models.UsersModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

@SuppressLint("ValidFragment")
public class NonVerifiedSellerFragment extends Fragment {
    private String TAG = "NonVerifiedSellerFragment";
    View view;
    PullRefreshLayout swipeRefresh;
    RecyclerView recyclerHRV, recyclerRV;
    HorizontalAdapter mHorizontalAdapter;
    NestedScrollView mNestedScrollView;
    ItemListAdapter mAdapter;
    ArrayList<UsersModel> allUserList;
    ArrayList<ItemsModel> itemList;
    TextView nodataTV, seeallTV;
    ProgressBar progressBar;
    boolean isFromSearch = false;
    boolean isFromFilter = false;

    private boolean isnextpage = false;

    int page_no = 1;
    int perPage = 20;
    JSONObject requestObject;

    public NonVerifiedSellerFragment() {
        isFromSearch = false;
        isFromFilter = false;
    }

    public NonVerifiedSellerFragment(ArrayList<ItemsModel> response) {
        Log.e(TAG, "NonVerifiedSellerFragment: " + response.size());
        itemList = new ArrayList<>();
        itemList = response;
        isFromSearch = true;
        isFromFilter = false;
    }

    public NonVerifiedSellerFragment(String filter) {
        isFromSearch = false;
        isFromFilter = true;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.itemfragmentlist, container, false);
        setView(view);
        setOnClick();
        allUserList = AppSingleton.getInstance().getUserApiResponseArrayList();
        Log.e(TAG, "onCreateView: " + allUserList.size());
        if (allUserList.size() > 0) {
            setHorizontalAdapter();
        }
        if (AlertDialogManager.haveNetworkConnection(getActivity())){
            recyclerRV.setNestedScrollingEnabled(false);
            mAdapter = new ItemListAdapter(getActivity());
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerRV.setLayoutManager(mLayoutManager);
            recyclerRV.setItemAnimator(new DefaultItemAnimator());
            recyclerRV.setAdapter(mAdapter);

            if (isFromSearch && !isFromFilter) {
                mAdapter.addAll(itemList);
                progressBar.setVisibility(View.GONE);
            } else if (isFromFilter && !isFromSearch) {
                Log.e(TAG, "filterJson: " + Constants.filterJson);
                if (Constants.filterJson.length() > 2) {
                    filterData(Constants.filterJson);
                }
            } else {
                getAllItems();
            }
        }else {
            recyclerRV.setNestedScrollingEnabled(false);
            mAdapter = new ItemListAdapter(getActivity());
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerRV.setLayoutManager(mLayoutManager);
            recyclerRV.setItemAnimator(new DefaultItemAnimator());
            recyclerRV.setAdapter(mAdapter);
            if (HomeActivity.db.getListItem("0").size()>0){
                mAdapter.addAll(HomeActivity.db.getListItem("0"));
            }
            Log.e(TAG, "getListItem: " + HomeActivity.db.getListItem().size());
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);


    }


    private void setView(View view) {
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        recyclerHRV = view.findViewById(R.id.horizontal_recycler_view);
        recyclerRV = view.findViewById(R.id.recyclerRV);
        mNestedScrollView = view.findViewById(R.id.mNestedScrollView);
        nodataTV = view.findViewById(R.id.nodataTV);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        seeallTV = view.findViewById(R.id.seeallTV);
    }

    private void setOnClick() {
        mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (isnextpage) {
                        page_no += 1;
                        Log.e(TAG, "loadMoreItems: " + page_no);
                        isnextpage = false;
                        if (!isFromFilter && !isFromSearch)
                            getAllItems();
                        else if (!isFromSearch && isFromFilter) {
                            filterData(Constants.filterJson);
                        }

                    }
                }
            }
        });

        swipeRefresh.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                HomeActivity.isfilterResponse = "";
                mAdapter.clear();
                getAllItems();

            }
        });
        seeallTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString("userModel", "seeall");
                HomeActivity.switchFragment(getActivity(), new AllUser_Fragment(), Constants.All_USER, true, args);
            }
        });
    }

    private void setHorizontalAdapter() {
        mHorizontalAdapter = new HorizontalAdapter(getActivity(), allUserList);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerHRV.setLayoutManager(horizontalLayoutManagaer);
        recyclerHRV.setAdapter(mHorizontalAdapter);
    }

    private void getAllItems() {
//        mAdapter.addLoadingFooter();
        String strUrl = WebServicesConstants.NotVerifiedUsers;
        Log.e(TAG, "strUrl: " + strUrl + "....." + page_no);
        try {
            requestObject = new JSONObject();
            requestObject.put("perPage", perPage);
            requestObject.put("page_no", page_no);
            requestObject.put("filterApply", "1");
            requestObject.put("verified", "2");
            Log.e(TAG, "requestObject: " + requestObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                progressBar.setVisibility(View.INVISIBLE);
                swipeRefresh.setRefreshing(false);

                if (page_no==1){
                    HomeActivity.db.removeAllList("0");
                }
                Log.e(TAG, "getListItem: " + HomeActivity.db.getListItem().size());

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.e(TAG, "jsonArray: " + jsonArray.length());
                    if (jsonArray.length() > 0) {
                        parseResponse(jsonArray);
                    } else {
                        isnextpage = false;
                        nodataTV.setVisibility(View.VISIBLE);
                        mAdapter.removeLoadingFooter();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                swipeRefresh.setRefreshing(false);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.NotVerifiedUsers);
    }

/*    private void LoadMoreItem() {
        String strUrl = WebServicesConstants.NotVerifiedUsers;
        Log.e(TAG, "strUrl: " + strUrl + "....." + page_no);
        try {
            requestObject = new JSONObject();
            requestObject.put("perPage", perPage);
            requestObject.put("page_no", page_no);
            requestObject.put("filterApply", "1");
            requestObject.put("verified", "1");
            Log.e(TAG, "requestObject: " + requestObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.e(TAG, "jsonArray: " + jsonArray.length());
                    if (response.length() > 0) {
                        if (response.length() < perPage) {
                            isnextpage = false;
                            mAdapter.removeLoadingFooter();
                        }
                        parseResponse(jsonArray);
                    } else {
                        isnextpage = false;
                        mAdapter.removeLoadingFooter();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    mAdapter.removeLoadingFooter();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return requestObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.NotVerifiedUsers);
    }*/

    private void parseResponse(JSONArray jsonArray) {

        itemList = new ArrayList<>();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setRating(jsonObject.getString("rating"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));
                itemsModel.setIsFavorite(Boolean.parseBoolean(jsonObject.getString("isFavorite")));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setVerifieduser(jsonObject.getInt("is_verified_user"));
                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));
                    itemsModel.setUserImages(imagesModel);
                }
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));

                    itemsModel.setItemImages(itemImagesModel);
                }

                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }
                itemList.add(itemsModel);
                HomeActivity.db.addListItem(itemList, "0");
            }
            mAdapter.addAll(itemList);
            if (jsonArray.length() < perPage) {
                isnextpage = false;
                progressBar.setVisibility(View.GONE);
//                mAdapter.removeLoadingFooter();
            } else {
//                mAdapter.addLoadingFooter();
                isnextpage = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filterData(final JSONObject filterJson) {
        progressBar.setVisibility(View.GONE);
        String urlString = WebServicesConstants.BASE_URL + "filteritemNew.php";
        Log.e(TAG, "urlString: " + urlString);
        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, urlString, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "filter onResponse: " + response);
                JSONArray jsonArray;
                try {
                    jsonArray = new JSONArray(response);
                    parseFilterResponse(jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: " + error.getMessage());
                String json;
                Utilities.hideProgressDialog();
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {


                return filterJson.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "filter");
    }


    private void parseFilterResponse(JSONArray jsonArray) {

        itemList = new ArrayList<>();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setRating(jsonObject.getString("rating"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));
                itemsModel.setIsFavorite(Boolean.parseBoolean(jsonObject.getString("isFavorite")));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setVerifieduser(jsonObject.getInt("is_verified_user"));
                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));
                    itemsModel.setUserImages(imagesModel);
                }
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));

                    itemsModel.setItemImages(itemImagesModel);
                }

                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }

                if (itemsModel.getVerifieduser() == 0) {
                    itemList.add(itemsModel);
                }
            }
            if (itemList.size() == 0) {
                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getResources().getString(R.string.app_name), "no Record Found");
            } else {
                mAdapter.addAll(itemList);
                if (jsonArray.length() < perPage) {
                    isnextpage = false;
                    progressBar.setVisibility(View.GONE);
                } else {
                    mAdapter.addLoadingFooter();
                    isnextpage = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
