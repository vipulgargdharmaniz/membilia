package com.membilia.Fragments;


import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ItemImagesModel;
import com.membilia.volley_models.ItemUserImagesModel;
import com.membilia.volley_models.ItemVideoModel;
import com.membilia.volley_models.ItemsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.INPUT_METHOD_SERVICE;

/*
 * Created by dharmaniz on 25/5/17.
 */

public class ExploreFragment extends Fragment {

    public static ImageView imgLeftIV;
    Resources mResources;
    View viewExploreFragment;
    RelativeLayout itemsRL, userRL, filterRL;
    TextView itemTV, userTV;
    View itemFooter, userFooter;
    private static final String TAG = "ExploreFragment";
    private RelativeLayout searchRL;
    public LinearLayout tabsLL, ll_search, ll_cancel, ll_searchNtab, ll_smallcancel;

    RelativeLayout layoutTabsFragmentRL, layoutGifRL;
    private boolean isSearchOn = false;
    private EditText ed_search;
    private int perPage = 20;
    private int page_no = 1;
    ArrayList<ItemsModel> itemVerifyList;
    ArrayList<ItemsModel> itemNonVerifyList;
    boolean isFromFilter = false;

    public ExploreFragment() {

    }

    @SuppressLint("ValidFragment")
    public ExploreFragment(String response, String s) {
    }

    @SuppressLint("ValidFragment")
    public ExploreFragment(String s) {
        isFromFilter = true;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (viewExploreFragment == null) {
            viewExploreFragment = inflater.inflate(R.layout.fragment_explore, null);
            mResources = getActivity().getResources();
            setUpViews(viewExploreFragment);

            //// resume /////
            if (Constants.IS_FIRSTIME) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // do stuff
                        layoutGifRL.setVisibility(View.GONE);
                        layoutTabsFragmentRL.setVisibility(View.VISIBLE);
                        Constants.IS_FIRSTIME = false;
                    }
                }, 5000);
            } else {
                layoutGifRL.setVisibility(View.GONE);
                layoutTabsFragmentRL.setVisibility(View.VISIBLE);
            }
            setclicklisteners();
            //Default Fragment:
            if (isFromFilter) {
                if (Constants.ExploreFragmentCurrent.equalsIgnoreCase(Constants.VerifiedSeller)) {
                    itemFooter.setVisibility(View.VISIBLE);
                    userFooter.setVisibility(View.INVISIBLE);
                    Constants.IS_ITEM_USER = "item_fragment";
                    itemTV.setTextColor(mResources.getColor(R.color.peach));
                    userTV.setTextColor(mResources.getColor(R.color.gray_darkcoal));
                    switchFragment(getActivity(), new VerifiedSellerFragment("Filter"), false, Constants.VerifiedSeller);
                } else {
                    itemFooter.setVisibility(View.INVISIBLE);
                    userFooter.setVisibility(View.VISIBLE);
                    Constants.IS_ITEM_USER = "user_fragment";
                    itemTV.setTextColor(mResources.getColor(R.color.gray_darkcoal));
                    userTV.setTextColor(mResources.getColor(R.color.peach));
                    switchFragment(getActivity(), new NonVerifiedSellerFragment("Filter"), false, Constants.NonVerifiedSeller);
                }
            } else {
                setUpByDefaultFragment();
            }
        }
        return viewExploreFragment;
    }

    public static void switchFragment(FragmentActivity activity, Fragment fragment, boolean addToStack, String tag) {
        Constants.ExploreFragmentCurrent = tag;
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            ft.replace(R.id.framelayoutinexplore, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            ft.commit();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeActivity.onPause = "yes";
    }

    public void setUpViews(View v) {
        itemsRL = v.findViewById(R.id.itemRL);
        userRL = v.findViewById(R.id.usersRL);
        itemFooter = v.findViewById(R.id.itemFooter);
        userFooter = v.findViewById(R.id.userFooter);
        itemTV = v.findViewById(R.id.itemTV);
        userTV = v.findViewById(R.id.userTV);
        imgLeftIV = v.findViewById(R.id.imgLeftIV);
        searchRL = v.findViewById(R.id.searchRL);
        tabsLL = v.findViewById(R.id.tabsLL);
        layoutTabsFragmentRL = v.findViewById(R.id.layoutTabsFragmentRL);
        layoutGifRL = v.findViewById(R.id.layoutGifRL);
        filterRL = v.findViewById(R.id.filterRL);
        ll_search = v.findViewById(R.id.ll_search);
        ed_search = v.findViewById(R.id.ed_search);
        ll_cancel = v.findViewById(R.id.ll_cancel);
        ll_searchNtab = v.findViewById(R.id.ll_searchNtab);
        ll_smallcancel = v.findViewById(R.id.ll_smallcancel);

    }

    private void setUpByDefaultFragment() {
        itemTV.setTextColor(mResources.getColor(R.color.peach));
        userTV.setTextColor(mResources.getColor(R.color.gray_darkcoal));
        itemFooter.setVisibility(View.VISIBLE);
        userFooter.setVisibility(View.INVISIBLE);
        HomeActivity.forSearchItem = "";
        HomeActivity.itemType = "relativeClick";
        switchFragment(getActivity(), new VerifiedSellerFragment(), false, Constants.VerifiedSeller);
    }

    public void setclicklisteners() {
        itemsRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.IS_ITEM_USER = "item_fragment";
                itemTV.setTextColor(mResources.getColor(R.color.peach));
                userTV.setTextColor(mResources.getColor(R.color.gray_darkcoal));
                itemFooter.setVisibility(View.VISIBLE);
                userFooter.setVisibility(View.INVISIBLE);
                HomeActivity.forSearchItem = "";
                HomeActivity.itemType = "relativeClick";
                switchFragment(getActivity(), new VerifiedSellerFragment(), false, Constants.VerifiedSeller);
            }
        });
        userRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.IS_ITEM_USER = "user_fragment";
                itemTV.setTextColor(mResources.getColor(R.color.gray_darkcoal));
                userTV.setTextColor(mResources.getColor(R.color.peach));
                itemFooter.setVisibility(View.INVISIBLE);
                userFooter.setVisibility(View.VISIBLE);
                switchFragment(getActivity(), new NonVerifiedSellerFragment(), false, Constants.NonVerifiedSeller);
            }
        });


        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ed_search.setText("");
                if (!isSearchOn) {
                    tabsLL.setVisibility(View.GONE);
                    ll_search.setVisibility(View.VISIBLE);
                    ed_search.setFocusable(true);
                    isSearchOn = true;
                } else {
                    tabsLL.setVisibility(View.VISIBLE);
                    ll_search.setVisibility(View.GONE);
                    isSearchOn = false;
                    HomeActivity.forSearchItem = "";
                    HomeActivity.itemType = "search";
                    if (Constants.ExploreFragmentCurrent.equalsIgnoreCase(Constants.VerifiedSeller)) {
                        switchFragment(getActivity(), new VerifiedSellerFragment(), false, Constants.VerifiedSeller);
                    } else
                        switchFragment(getActivity(), new NonVerifiedSellerFragment(), false, Constants.NonVerifiedSeller);
                }
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tabsLL.setVisibility(View.VISIBLE);
                ll_search.setVisibility(View.GONE);
                isSearchOn = false;
                ed_search.setText("");
                HomeActivity.forSearchItem = "";
                HomeActivity.itemType = "search";
                if (Constants.ExploreFragmentCurrent.equalsIgnoreCase(Constants.VerifiedSeller)) {
                    switchFragment(getActivity(), new VerifiedSellerFragment(), false, Constants.VerifiedSeller);
                } else
                    switchFragment(getActivity(), new NonVerifiedSellerFragment(), false, Constants.NonVerifiedSeller);

//                switchFragment(getActivity(), new ItemsFragment(), false, Constants.VerifiedSeller);
            }
        });

        ed_search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    String strURL = WebServicesConstants.ITEM_SEARCH + "perPage=" + perPage + "&page_no=" + page_no;
                    itemSearchRequest(strURL);
                    hideSoftKeyboard();
                    return true;
                }
                return false;
            }
        });

        ll_smallcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ed_search.setText("");
                ll_smallcancel.setVisibility(View.INVISIBLE);
            }
        });

        ed_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    ll_smallcancel.setVisibility(View.VISIBLE);
                } else {
                    ll_smallcancel.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        filterRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.switchFragment(getActivity(), new FragmentFilter(), Constants.Fragment_filter, true, null);
            }
        });
    }

    private void itemSearchRequest(String strURL) {
        Utilities.showProgressDialog(getActivity());
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("item_name", ed_search.getText().toString().trim());
            HomeActivity.itemSearch = ed_search.getText().toString().trim();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StringRequest jsonArrayRequest = new StringRequest(Request.Method.POST, strURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.hideProgressDialog();
                Log.i(TAG, "onResponse*********" + response.toString());
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() > 0) {
                        HomeActivity.forSearchItem = "forSearchItem";
                        HomeActivity.itemType = "search";
                        parseResponse(jsonArray);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //                switchFragment(getActivity(), new ItemsFragment(response), false, Constants.VerifiedSeller);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.hideProgressDialog();
                Log.i(TAG, "onErrorResponse*********" + error.toString());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ItemSearch");
    }

    private void parseResponse(JSONArray jsonArray) {
        itemVerifyList = new ArrayList<>();
        itemNonVerifyList = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setItemId(jsonObject.getString("item_id"));
                itemsModel.setUserId(jsonObject.getString("user_id"));
                itemsModel.setPrice(Integer.parseInt(jsonObject.getString("price")));
                itemsModel.setItemName(jsonObject.getString("item_name"));
                itemsModel.setItemStory(jsonObject.getString("item_story"));
                itemsModel.setUserName(jsonObject.getString("user_name"));
                itemsModel.setRating(jsonObject.getString("rating"));
                itemsModel.setSellerId(jsonObject.getString("seller_id"));
                itemsModel.setIsFavorite(Boolean.parseBoolean(jsonObject.getString("isFavorite")));
                itemsModel.setCharity(Boolean.parseBoolean(jsonObject.getString("charity")));
                itemsModel.setVerifieduser(jsonObject.getInt("is_verified_user"));
                itemsModel.setIsSold(Boolean.parseBoolean(jsonObject.getString("isSold")));

                if (!jsonObject.isNull("user_images")) {
                    JSONObject user_images = jsonObject.getJSONObject("user_images");
                    ItemUserImagesModel imagesModel = new ItemUserImagesModel();
                    imagesModel.setOriginal(user_images.getString("original"));
                    itemsModel.setUserImages(imagesModel);
                }
                if (!jsonObject.isNull("item_images")) {
                    JSONObject item_images = jsonObject.getJSONObject("item_images");
                    ItemImagesModel itemImagesModel = new ItemImagesModel();
                    if (!item_images.isNull("original_1"))
                        itemImagesModel.setOriginal1(item_images.getString("original_1"));
                    itemsModel.setItemImages(itemImagesModel);
                }
                if (!jsonObject.isNull("item_video")) {
                    JSONObject item_video = jsonObject.getJSONObject("item_video");
                    ItemVideoModel videoModel = new ItemVideoModel();
                    if (!item_video.isNull("original_1")) {
                        videoModel.setOriginal1(item_video.getString("original_1"));
                        videoModel.setOriginal_videoimage(item_video.getString("original_videoimage"));
                    }
                    itemsModel.setItemVideo(videoModel);
                }
                if (itemsModel.getVerifieduser() == 1)
                    itemVerifyList.add(itemsModel);
                else
                    itemNonVerifyList.add(itemsModel);


            }
            if (Constants.ExploreFragmentCurrent.equalsIgnoreCase(Constants.VerifiedSeller)) {
                if (itemVerifyList.size() > 0) {
                    switchFragment(getActivity(), new VerifiedSellerFragment(itemVerifyList), false, Constants.VerifiedSeller);
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), "No data Found");
                }
            } else {
                if (itemNonVerifyList.size() > 0) {
                    switchFragment(getActivity(), new NonVerifiedSellerFragment(itemNonVerifyList), false, Constants.NonVerifiedSeller);
                } else {
                    AlertDialogManager.showAlertDialog(getActivity(), getActivity().getString(R.string.app_name), "No data Found");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        View view = getActivity().getCurrentFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
