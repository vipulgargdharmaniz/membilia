package com.membilia.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.baoyz.widget.PullRefreshLayout;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.AlertDialogManager;
import com.membilia.Util.PaginationScrollListener;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.ActivityAdapters;
import com.membilia.signleton.PlayerPawnSignleton;
import com.membilia.volley.UtilsVolley;
import com.membilia.volley_models.ActModel;
import com.membilia.volley_models.Act_ActivityDetailsModel;
import com.membilia.volley_models.Act_UserImagesModel;
import com.membilia.volley_models.Act_UserLocationModel;
import com.membilia.volley_models.Act_UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;
/*
 * Created by dharmaniz on 25/5/17.
 */

public class ActivityFragment extends Fragment {

    View itemsview;
    private String TAG = "ActivityFragment";
    ArrayList<ActModel> actModelArrayList;
    int perPage = 20;
    int page_no = 1;
    ActivityAdapters mAdapter;
    RecyclerView recyclerRV;
    TextView noDataTV;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private boolean isnextpage = false;
    LinearLayout topbar;
    TextView titleTV;
    PullRefreshLayout swipeRefresh;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (itemsview == null) {
            itemsview = inflater.inflate(R.layout.fragment_activitylist, null);
            Log.i(TAG, "onCreateView: ");

            actModelArrayList = PlayerPawnSignleton.getInstance().getmActivityArrayList();

            setUpViews(itemsview);

            mAdapter = new ActivityAdapters(getActivity());
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerRV.setLayoutManager(mLayoutManager);
            recyclerRV.setItemAnimator(new DefaultItemAnimator());
            recyclerRV.setAdapter(mAdapter);
            recyclerRV.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
                @Override
                protected void loadMoreItems() {
                    if (isnextpage) {
                        isLoading = false;
                        page_no += 1;
                        Log.e(TAG, "loadMoreItems: " + page_no);
                        // mocking network delay for API call
                        isnextpage = false;
                        loadNextPage();
                    }
                }

                @Override
                public int getTotalPageCount() {
                    return 1;
                }

                @Override
                public boolean isLastPage() {
                    return isLastPage;
                }

                @Override
                public boolean isLoading() {
                    return isLoading;
                }
            });

            if (actModelArrayList.size()==0) {
                gettingAllItems();
            }else{
                mAdapter.addAll(actModelArrayList);
                if (actModelArrayList.size() > 19) {
                    mAdapter.addLoadingFooter();
                }

                isnextpage = true;
            }

        }

        swipeRefresh.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page_no = 1;
                mAdapter.clear();
                gettingAllItems();
            }
        });
        return itemsview;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.activityClick();
    }

    public void setUpViews(View v) {
        recyclerRV = v.findViewById(R.id.recyclerRV);
        recyclerRV.setNestedScrollingEnabled(false);
        topbar = itemsview.findViewById(R.id.topbar);
        titleTV = itemsview.findViewById(R.id.titleTV);
        noDataTV = itemsview.findViewById(R.id.nodataTV);
        topbar.setVisibility(View.VISIBLE);
        titleTV.setText("Activity");
        swipeRefresh = itemsview.findViewById(R.id.swipeRefresh);
    }

    private void loadNextPage() {
        String strUrl = WebServicesConstants.ACTIVITY_FRAGMENTS + "user_id=" + PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "") + "&perPage=" + perPage + "&page_no=" + page_no;
        Log.i(TAG, "******************" + strUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response.length() > 0) {
                    mAdapter.removeLoadingFooter();
                    isLoading = false;
                    parseResponse(response);
                } else {
                    mAdapter.removeLoadingFooter();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse*********" + error.toString());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            //**Passing some request headers
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ActivityFragment");
    }

    private void gettingAllItems() {
        Utilities.showProgressDialog(getActivity());
        String strUrl = WebServicesConstants.ACTIVITY_FRAGMENTS + "user_id=" + PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_USER_ID, "") + "&perPage=" + perPage + "&page_no=" + page_no;
        Log.e(TAG, "strUrl" + strUrl);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(strUrl, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                HomeActivity.isAddItemOpen = true;
                swipeRefresh.setRefreshing(false);
                Utilities.hideProgressDialog();
                Log.e(TAG, "onResponse" + response.toString());
                if (response.length() == 0)
                    noDataTV.setVisibility(View.VISIBLE);
                else
                    parseResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                HomeActivity.isAddItemOpen = true;
                swipeRefresh.setRefreshing(false);
                Utilities.hideProgressDialog();
                Log.e(TAG, "onErrorResponse" + error.toString());
                String json;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            json = new String(response.data);
                            json = UtilsVolley.trimMessage(json, "message");
                            if (json != null)
                                AlertDialogManager.showAlertDialog(getActivity(), getString(R.string.app_name), json);
                            break;
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonArrayRequest, "ActivityFragment");
    }

    private void parseResponse(JSONArray response) {
       // actModelArrayList = new ArrayList<>();
        if (actModelArrayList.size()>0){
            actModelArrayList.clear();
        }
        try {
            for (int i = 0; i < response.length(); i++) {
                JSONObject jsonObject = response.getJSONObject(i);

                //
                if (!jsonObject.isNull("user")) {
                    JSONObject jsonActUser = jsonObject.getJSONObject("user");

                   if (!jsonActUser.getString("user_id").equalsIgnoreCase("playpawn")){

                       ActModel actModel = new ActModel();
                       if (!jsonObject.isNull("type")) {
                           actModel.setType(jsonObject.getString("type"));
                       }
                       if (!jsonObject.isNull("text")) {
                           actModel.setText(jsonObject.getString("text"));
                       }
                       if (!jsonObject.isNull("activity_id")) {
                           actModel.setActivity_id(jsonObject.getString("activity_id"));
                       }
                       if (!jsonObject.isNull("created")) {
                           actModel.setCreated(jsonObject.getString("created"));
                       }
                       if (!jsonObject.isNull("user")) {
                           Act_UserModel act_userModel = new Act_UserModel();
                           act_userModel.setUser_id(jsonActUser.getString("user_id"));
                           act_userModel.setName(jsonActUser.getString("name"));
                           if (jsonActUser.has("description")) {
                               act_userModel.setDescription(jsonActUser.getString("description"));
                           }
                           if (jsonActUser.has("email")) {
                               act_userModel.setDescription(jsonActUser.getString("email"));
                           }
                           act_userModel.setFollowerCount(jsonActUser.getString("followerCount"));
                           act_userModel.setFollowingCount(jsonActUser.getString("followingCount"));
                           act_userModel.setRating(jsonActUser.getString("rating"));
                           act_userModel.setRanking(jsonActUser.getString("ranking"));
                           act_userModel.setIsFollowed(jsonActUser.getBoolean("isFollowed"));
                           act_userModel.setFlag(jsonActUser.getString("flag"));
                           act_userModel.setIsVerified(jsonActUser.getString("isVerified"));

                           if (!jsonActUser.isNull("location")) {
                               JSONObject jsonLocation = jsonActUser.getJSONObject("location");
                               if (jsonLocation.has("administrative_area_level_1")) {
                                   Act_UserLocationModel act_userLocationModel = new Act_UserLocationModel();
                                   act_userLocationModel.setAdministrative_area_level_1(jsonLocation.getString("administrative_area_level_1"));
                                   act_userLocationModel.setCountry(jsonLocation.getString("country"));
                                   act_userLocationModel.setCountry_code(jsonLocation.getString("country_code"));
                                   act_userLocationModel.setLat(jsonLocation.getString("lat"));
                                   act_userLocationModel.setLng(jsonLocation.getString("lng"));
                                   act_userLocationModel.setLocality(jsonLocation.getString("locality"));
                                   act_userLocationModel.setPostal_code(jsonLocation.getString("postal_code"));
                                   act_userModel.setAct_UserLocationModel(act_userLocationModel);
                               }

                           }
                           if (!jsonActUser.isNull("user_images")) {
                               JSONObject jsonUserImages = jsonActUser.getJSONObject("user_images");
                               Act_UserImagesModel act_userImagesModel = new Act_UserImagesModel();
                               act_userImagesModel.setOriginal(jsonUserImages.getString("original"));
                               act_userModel.setAct_UserImagesModel(act_userImagesModel);
                           }
                           actModel.setAct_UserModel(act_userModel);
                       }
                       if (!jsonObject.isNull("ActivityDetail")) {
                           JSONObject jsonItemObject = jsonObject.getJSONObject("ActivityDetail");
                           Act_ActivityDetailsModel detailsModel = new Act_ActivityDetailsModel();
                           detailsModel.setItem_id(jsonItemObject.getString("item_id"));
                           detailsModel.setSeller_id(jsonItemObject.getString("seller_id"));
                           if (!jsonObject.getString("type").equalsIgnoreCase("4") && !jsonObject.getString("type").equalsIgnoreCase("12")) {
                               if (jsonItemObject.has("item_name")) {
                                   detailsModel.setItem_name(jsonItemObject.getString("item_name"));
                               }
                           }


                           if (!jsonItemObject.isNull("location")) {
                               JSONObject jsonLocation = jsonItemObject.getJSONObject("location");
                               if (jsonLocation.has("administrative_area_level_1")) {
                                   Act_UserLocationModel act_userLocationModel = new Act_UserLocationModel();
                                   act_userLocationModel.setAdministrative_area_level_1(jsonLocation.getString("administrative_area_level_1"));
                                   act_userLocationModel.setCountry(jsonLocation.getString("country"));
                                   act_userLocationModel.setCountry_code(jsonLocation.getString("country_code"));
                                   act_userLocationModel.setLat(jsonLocation.getString("lat"));
                                   act_userLocationModel.setLng(jsonLocation.getString("lng"));
                                   act_userLocationModel.setLocality(jsonLocation.getString("locality"));
                                   act_userLocationModel.setPostal_code(jsonLocation.getString("postal_code"));
                                   detailsModel.setAct_UserLocationModel(act_userLocationModel);
                               }
                           }
                           actModel.setAct_ActivityDetailsModel(detailsModel);
                       }
                       actModelArrayList.add(actModel);
                   }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utilities.hideProgressDialog();
        mAdapter.addAll(actModelArrayList);
        if (actModelArrayList.size() > 19) {
            mAdapter.addLoadingFooter();
        }

        isnextpage = true;
    }
}
