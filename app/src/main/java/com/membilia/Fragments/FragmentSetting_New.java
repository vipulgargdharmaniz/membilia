package com.membilia.Fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.drawee.view.SimpleDraweeView;
import com.kyleduo.switchbutton.SwitchButton;
import com.membilia.PlayerPawnApplication;
import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.AddItemForNotVerifyUser;
import com.membilia.activities.HomeActivity;
import com.membilia.activities.SelectionActivity;
import com.membilia.dbModal.DbProfile;
import com.membilia.volley.AppHelper;
import com.membilia.volley.VolleyMultipartRequest;
import com.membilia.volley_models.UserLocationModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class FragmentSetting_New extends Fragment {

    View itemsview;
    LinearLayout changepasswordRl, shippingaddressRl, aboutusRL, orderhistoryRL, privacypolicyRl, notificationRL, messagehistoryRl, transactionurl, paypalaccountRL, contactusRL;

    LinearLayout applyverifiedsellerLL;
    TextView logoutBT, Updateprofiletv, countryTV;
    Intent intent;
    RelativeLayout searchRL;
    SwitchButton notificationswitchb;
    private static final String TAG = "FragmentSetting_New";
    private EditText firstnameET, lastnameET, descpET;
    private JSONObject profileJsonObj = new JSONObject();

    private JSONObject locationJsonObj = new JSONObject();
    UserLocationModel userLocationModel = new UserLocationModel();
    FrameLayout profileFrameIV;
    private static final int CAMERA_REQUEST = 111;
    private static final int GALLERY_REQUEST = 222;

    String strImage = "", strUsername = "", strDesc = "", strLocation = "";
    private String CameraPermission = Manifest.permission.CAMERA;
    private String ReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String WriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public Bitmap BitmapImages = null;
    SimpleDraweeView profileIV;
    String ImgURI = "";
    LinearLayout changeAddressLL;
    boolean isOnActivityRequest = false;
    ArrayList<DbProfile> dbProfiles;
    RelativeLayout topView;
    String loginType = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        itemsview = inflater.inflate(R.layout.fragment_setting, null);
        setUpViews(itemsview);
        setupclicks();
        dbProfiles = HomeActivity.db.getProfile(HomeActivity.UserItself);
        Log.e(TAG, "onCreateView: "+dbProfiles.get(0).getUserPic() );
        Log.e(TAG, "onCreateView: "+dbProfiles.get(0).getUsername() );
        Log.e(TAG, "onCreateView: "+dbProfiles.get(0).getUserDecp() );
        Log.e(TAG, "onCreateView: "+dbProfiles.get(0).getUserCountry() );
        setData();
        return itemsview;
    }


    private void setUpViews(View v) {
        logoutBT = v.findViewById(R.id.logoutBT);
        changepasswordRl = v.findViewById(R.id.changepasswordRL);
        orderhistoryRL = v.findViewById(R.id.orderhistoryRL);
        shippingaddressRl = v.findViewById(R.id.shippingaddressRL);
        privacypolicyRl = v.findViewById(R.id.privacypolicyRL);
        notificationRL = v.findViewById(R.id.notificationRL);
        messagehistoryRl = v.findViewById(R.id.messagehistoryRL);
        notificationswitchb = v.findViewById(R.id.notificationswitchb);
        notificationRL = v.findViewById(R.id.notificationRL);
        transactionurl = v.findViewById(R.id.transactionurl);
        paypalaccountRL = v.findViewById(R.id.paypalaccountRL);
        aboutusRL = v.findViewById(R.id.aboutusRL);
        contactusRL = v.findViewById(R.id.contactusRL);
        Updateprofiletv = v.findViewById(R.id.Updateprofiletv);
        searchRL = v.findViewById(R.id.searchRL);
        firstnameET = v.findViewById(R.id.firstnameET);
        lastnameET = v.findViewById(R.id.lastnameET);
        descpET = v.findViewById(R.id.descpET);
        countryTV = v.findViewById(R.id.countryTV);
        profileFrameIV = v.findViewById(R.id.profileFrameIV);
        profileIV = v.findViewById(R.id.profileIV);
        changeAddressLL = v.findViewById(R.id.changeAddressLL);
        applyverifiedsellerLL = v.findViewById(R.id.applyverifiedsellerLL);
        topView = v.findViewById(R.id.topview);
        topView.setFocusable(true);
        topView.setBackgroundColor(getActivity().getResources().getColor(R.color.settingbg));
        loginType = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.LoginType, "");
        Log.e(TAG, "setUpViews: " + loginType);
        if (loginType.equalsIgnoreCase("social") ||loginType.equalsIgnoreCase("") ) {
            changeAddressLL.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        locationData();
        getNotification();
    }

    private void setData() {
        if (Constants.UserName.equalsIgnoreCase("")) {
            strUsername = dbProfiles.get(0).getUsername();
        } else {
            strUsername = Constants.UserName;
        }

        if (Constants.UserDesc.equalsIgnoreCase("")) {
            strDesc = dbProfiles.get(0).getUserDecp();
        } else {
            strDesc = Constants.UserDesc;
        }

        if (Constants.UserCountry.equalsIgnoreCase("")) {
            strLocation = dbProfiles.get(0).getUserCountry();
        } else {
            strLocation = Constants.UserCountry;
        }

        if (Constants.UserImage.equalsIgnoreCase("")) {
            strImage = dbProfiles.get(0).getUserPic();
        } else {
            strImage = Constants.UserImage;
        }

        if (!strUsername.equalsIgnoreCase("")) {
            String[] parts = strUsername.split(" ");
            String FirstName = parts[0]; // 004
            String LastName = "";
            if (parts.length == 2) {
                LastName = parts[1];
            }
            if (parts.length == 3) {
                LastName = parts[1] + " " + parts[2];
            }
            firstnameET.setText(FirstName);
            lastnameET.setText(LastName);
        }
        if (!strLocation.equalsIgnoreCase("")) {
            countryTV.setText(strLocation);
        }
        if (!strDesc.equalsIgnoreCase("")) {
            descpET.setText(strDesc);
        }
        if (!strImage.equalsIgnoreCase("")) {
            profileIV.setImageURI(Uri.parse(strImage));
        }
        countryTV.setText(strLocation);
    }

    private void locationData() {
        try {
            locationJsonObj.put("administrative_area_level_1", userLocationModel.getAdministrativeAreaLevel1());
            locationJsonObj.put("country", userLocationModel.getCountry());
            locationJsonObj.put("lat", userLocationModel.getLat());
            locationJsonObj.put("lng", userLocationModel.getLng());
            locationJsonObj.put("locality", userLocationModel.getLocality());
            locationJsonObj.put("postal_code", userLocationModel.getPostalCode());
            profileJsonObj.put("name", "");
            profileJsonObj.put("description", "");
            profileJsonObj.put("location", "");
            profileJsonObj.put("verified", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupclicks() {
        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });

        Updateprofiletv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strUsername = firstnameET.getText().toString() + " " + lastnameET.getText().toString();
                strLocation = countryTV.getText().toString();
                strDesc = descpET.getText().toString();
                Log.e(TAG, "onClick: " + strDesc + "..." + strLocation + ".." + strUsername + "..." + ImgURI);
                updateService(strUsername, strLocation, strDesc, ImgURI);
            }
        });

        profileFrameIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAndroidPermissions();
            }
        });
        countryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.UserImage = strImage;
                Constants.UserDesc = descpET.getText().toString();
                Constants.UserName = firstnameET.getText().toString() + " " + lastnameET.getText().toString();
                HomeActivity.switchFragment(getActivity(), new Country_Fragment(), Constants.Fragment_Country_List, true, null);
            }
        });

        logoutBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox();
//                String strAuthorization = PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, "");
//                  executeLogoutAPI(strAuthorization);
            }
        });
        changepasswordRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.switchFragment(getActivity(), new FragmentChangePassword(), Constants.Fragment_Change_Password, true, null);
            }
        });
        orderhistoryRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.switchFragment(getActivity(), new FragmentOrderHistory(), Constants.Fragment_Order_History, true, null);
            }
        });
        shippingaddressRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("from", "setting");
                HomeActivity.switchFragment(getActivity(), new FragmentShowShippingAddress(), Constants.Fragment_Show_Shipping_Address, true, bundle);
            }
        });
        privacypolicyRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://membilia.com/terms-conditions/"));
                startActivity(intent);
            }
        });
        messagehistoryRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.switchFragment(getActivity(), new FragmentMessageHistory(), Constants.Fragment_Message_History, true, null);
            }
        });
        transactionurl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.switchFragment(getActivity(), new FragmentTransactionHistory(), Constants.Fragment_Transaction_History, true, null);
            }
        });
        paypalaccountRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.switchFragment(getActivity(), new FragmentpaymentMethod(), Constants.Fragment_PaymentMethod, true, null);
            }
        });
        aboutusRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://membilia.com/"));
                startActivity(intent);
            }
// }
        });
        contactusRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://membilia.com/join"));
                startActivity(intent);
            }
        });

        notificationswitchb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    serviceForNotification(0);
                } else {
                    serviceForNotification(1);
                }

            }
        });

        applyverifiedsellerLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addintent = new Intent(getActivity(), AddItemForNotVerifyUser.class);
                startActivity(addintent);
            }
        });
    }

    private void checkAndroidPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermissions()) {
                Constants.UserName = firstnameET.getText().toString() + " " + lastnameET.getText().toString();
                Constants.UserDesc = descpET.getText().toString();
                Constants.UserCountry = countryTV.getText().toString();
                showCameraGalleryDialog();
            } else {
                requestPermission();
            }
        } else {
            // write your logic here
            showCameraGalleryDialog();
        }
    }

    private boolean checkPermissions() {
        int camera = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), CameraPermission);
        int read = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), ReadStorage);
        int write = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), WriteStorage);

        return camera == PackageManager.PERMISSION_GRANTED &&
                read == PackageManager.PERMISSION_GRANTED &&
                write == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CameraPermission, ReadStorage,
                WriteStorage}, 100);
    }


    private void showCameraGalleryDialog() {
        final Dialog cameraGalleryDialog = new Dialog(getActivity());
        cameraGalleryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraGalleryDialog.setContentView(R.layout.dialog_camra_gallery);
        cameraGalleryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtCamera = cameraGalleryDialog.findViewById(R.id.txtCamera);
        TextView txtGallery = cameraGalleryDialog.findViewById(R.id.txtGallery);
        TextView txtCancel = cameraGalleryDialog.findViewById(R.id.txtCancel);

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraGalleryDialog.dismiss();
            }
        });
        txtCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Open Camera********/
                openCamera();
                cameraGalleryDialog.dismiss();
            }
        });
        txtGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                cameraGalleryDialog.dismiss();
            }
        });
        cameraGalleryDialog.show();
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) == null) {
            return;
        }
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    public static void switchFragment(FragmentActivity mActivity, final Fragment fragment, final String Tag, final boolean addToStack, final Bundle bundle) {
        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.framelayoutContainer, fragment, Tag);
            if (addToStack)
                fragmentTransaction.addToBackStack(Tag);
            if (bundle != null)
                fragment.setArguments(bundle);

            fragmentTransaction.commit();

            fragmentManager.executePendingTransactions();
        }
    }

    private void dialogBox() {
        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
        LinearLayout yesll = dialog.findViewById(R.id.yesll);
        LinearLayout noll = dialog.findViewById(R.id.noll);
        TextView messageTV = dialog.findViewById(R.id.messageTV);
        messageTV.setText("Are you sure, you want to logout?");
        noll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        yesll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                HomeActivity.db.onDelete();
                intent = new Intent(getActivity(), SelectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                SharedPreferences preferences = PlayerPawnPreference.getPreferences(getActivity());
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();

            }
        });
        dialog.show();
    }

    private void updateService(final String strusername, String strcountry, final String strdesc, final String imgURI) {
        String url = WebServicesConstants.UPDATE_USER;
        Log.i(TAG, "***SERVICE_URL***: " + url);
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonLocation = new JSONObject();
        this.strUsername = strusername;
        this.strLocation = strcountry;
        this.strDesc = strdesc;
        try {
            jsonObject.put("name", this.strUsername);
            jsonObject.put("description", this.strDesc);
            jsonLocation.put("country", strLocation);
            jsonObject.put("location", jsonLocation);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utilities.showProgressDialog(getActivity());
        JsonObjectRequest jsonsonObjReq = new JsonObjectRequest(Request.Method.PUT,
                url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        Log.e(TAG, "****EXCUTE_RESPONSE**** " + response.toString());

                        HomeActivity.FromSetting = "true";
                        Constants.UserName = "";
                        Constants.UserName = "";
                        Constants.UserCountry = "";
                        Constants.UserImage = "";

                        if (BitmapImages != null) {
                            executeUploadImageAPI();
                        } else {
                            Utilities.hideProgressDialog();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            fragmentManager.popBackStack();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        // Adding request to request queue
        PlayerPawnApplication.getInstance().addToRequestQueue(jsonsonObjReq, "UPDATE_USER");
    }


    public void executeUploadImageAPI() {
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, WebServicesConstants.UPLOAD_IMAGE_ON_SERVER, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                Utilities.hideProgressDialog();
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    String message = result.getString("message");
                    Log.e(TAG, "*******IMAGE UPLOAD SUCCESSFULLY******" + resultResponse);
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.popBackStack();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Utilities.hideProgressDialog();
            }
        }) {
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("image", new DataPart("file_avatar.jpg", AppHelper.getFileDataFromBitmap(getActivity(), BitmapImages), "image/jpeg"));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(multipartRequest, "UploadImageRequest");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        InputStream stream = null;
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
             /*Getting Image in onActivityResult
            * that is select from Gallery*/
            isOnActivityRequest = true;
            try {
                Bitmap bitmap = null;
                stream = getActivity().getContentResolver().openInputStream(data.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                try {
                    bitmap = getCorrectlyOrientedImage(getActivity(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                BitmapImages = bitmap;
                Uri tempUri = getImageUri(BitmapImages);
                ImgURI = String.valueOf(tempUri);
                strImage = String.valueOf(ImgURI);
                Constants.UserImage = strImage;
                profileIV.setImageURI(tempUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            isOnActivityRequest = true;
            BitmapImages = (Bitmap) data.getExtras().get("data");
            Uri tempUria = getImageUri(BitmapImages);
            ImgURI = String.valueOf(tempUria);
            Log.e(TAG, "cameraURi: " + tempUria);
            try {
                BitmapImages = getCorrectlyOrientedImage(getActivity(), tempUria);

            } catch (IOException e) {
                e.printStackTrace();
            }
//            profileIV.setImageBitmap(BitmapImages);
            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            Uri tempUri = getImageUri(BitmapImages);
            strImage = String.valueOf(tempUri);
            Constants.UserImage = strImage;
            profileIV.setImageURI(tempUri);
        }
    }

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    /***
     *   for orientation
     */
    public  Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();
        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);
        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }
        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 400 || rotatedHeight > 400) {
            float widthRatio = ((float) rotatedWidth) / ((float) 400);
            float heightRatio = ((float) rotatedHeight) / ((float) 400);
            float maxRatio = Math.max(widthRatio, heightRatio);
            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
        }
        return srcBitmap;
    }

    public static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri, new String[]{
                MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        if (cursor.getCount() != 1) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getInt(0);
    }


    private void serviceForNotification(int b) {
        String strUrl = WebServicesConstants.NotificationOn;
        Log.e(TAG, "strURl: " + strUrl);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("deviceToken", "");
            jsonObject.put("type", b);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "serviceForNotification: " + b);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getNotification();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return header;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.NotificationOn);
    }

    private void getNotification() {
        String strUrl = WebServicesConstants.NotificationStatus;
        Log.e(TAG, "strURl: " + strUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("notification_status");
                    if (status.equalsIgnoreCase("active")) {
                        notificationswitchb.setChecked(true);
                    } else {
                        notificationswitchb.setChecked(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return header;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest, WebServicesConstants.NotificationStatus);
    }

}
