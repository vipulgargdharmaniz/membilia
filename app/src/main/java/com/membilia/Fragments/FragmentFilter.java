package com.membilia.Fragments;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * Created by dharmaniz on 11/7/17.
 */

public class FragmentFilter extends Fragment {
    View itemsview;
    CheckBox thingsWornCB, accessoriesCB, experienceCb, ballsBatsCB, thingsThatMovesCB, miscellaneousCB, footwareCb,
            fashionCb, autographedCB;
    CheckBox distanceNearestCB, distanceFarestCB;
    CheckBox byPriceLowestCB, byPriceHighCB;
    CheckBox byPublishingNearestCB, fromPublishingFartest;
    CheckBox delShippingCB, delPersonalTransfer;
    CheckBox bySoldCB, byAvailabilityCB, pawnCostCB;
    TextView txtSaveTV;
    Typeface mTypeface;
    SeekBar seekBar;
    ImageView imgLeftIV;
    private RelativeLayout refreshRl, searchRL;

    /* values */

    int perPage = 40;
    int page_no = 1;
    int filterApply = 2;
    boolean price = true;
    boolean delivery_type = true;
    private boolean date = true;
    private boolean distance = true;
    private int range = 100000;
    private String administrative_area_level_1 = "";
    private String country = "";
    private String latitude;
    private String longitude;
    private String locality = "";

    String search = "";

    private static final String TAG = "FragmentFilter";

    int pawncast = 0;

    private String urlString = WebServicesConstants.BASE_URL + "filteritem.php";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (itemsview == null) {
            itemsview = inflater.inflate(R.layout.fragment_filter, null);
            Log.i(TAG, "onCreateView: ");
            mTypeface = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Bold.ttf");
            setUpViews(itemsview);
            setupclicks();
        }
        return itemsview;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void setUpViews(View v) {
        thingsWornCB = v.findViewById(R.id.thingsWornCB);
        thingsWornCB.setTypeface(mTypeface);
        accessoriesCB = v.findViewById(R.id.accessoriesCB);
        accessoriesCB.setTypeface(mTypeface);
        experienceCb = v.findViewById(R.id.experienceCb);
        experienceCb.setTypeface(mTypeface);
        ballsBatsCB = v.findViewById(R.id.ballsBatsCB);
        ballsBatsCB.setTypeface(mTypeface);
        thingsThatMovesCB = v.findViewById(R.id.thingsThatMovesCB);
        thingsThatMovesCB.setTypeface(mTypeface);
        miscellaneousCB = v.findViewById(R.id.miscellaneousCB);
        miscellaneousCB.setTypeface(mTypeface);
        footwareCb = v.findViewById(R.id.footwareCb);
        footwareCb.setTypeface(mTypeface);
        fashionCb = v.findViewById(R.id.fashionCb);
        fashionCb.setTypeface(mTypeface);
        autographedCB = v.findViewById(R.id.autographedCB);
        autographedCB.setTypeface(mTypeface);

        distanceNearestCB = v.findViewById(R.id.distanceNearestCB);
        distanceNearestCB.setTypeface(mTypeface);
        distanceFarestCB = v.findViewById(R.id.distanceFarestCB);
        distanceFarestCB.setTypeface(mTypeface);

        byPriceLowestCB = v.findViewById(R.id.byPriceLowestCB);
        byPriceLowestCB.setTypeface(mTypeface);
        byPriceHighCB = v.findViewById(R.id.byPriceHighCB);
        byPriceHighCB.setTypeface(mTypeface);

        byPublishingNearestCB = v.findViewById(R.id.byPublishingNearestCB);
        byPublishingNearestCB.setTypeface(mTypeface);
        fromPublishingFartest = v.findViewById(R.id.fromPublishingFartest);
        fromPublishingFartest.setTypeface(mTypeface);

        delShippingCB = v.findViewById(R.id.delShippingCB);
        delShippingCB.setTypeface(mTypeface);
        delPersonalTransfer = v.findViewById(R.id.delPersonalTransfer);
        delPersonalTransfer.setTypeface(mTypeface);

        bySoldCB = v.findViewById(R.id.bySoldCB);
        bySoldCB.setTypeface(mTypeface);
        byAvailabilityCB = v.findViewById(R.id.byAvailabilityCB);
        byAvailabilityCB.setTypeface(mTypeface);
        pawnCostCB = v.findViewById(R.id.pawnCostCB);
        pawnCostCB.setTypeface(mTypeface);


        txtSaveTV = v.findViewById(R.id.txtSaveTV);
        seekBar = v.findViewById(R.id.seekBar);
        imgLeftIV = v.findViewById(R.id.imgLeftIV);
        refreshRl = v.findViewById(R.id.refreshRl);
        searchRL = v.findViewById(R.id.searchRL);
    }

    private void setupclicks() {

        /*
         *Setting Up Seek Progress fix
         ********/
        setUpSeekBarFixedProgress();
        /*
         * Filter By Categories
         * **********/
        setUpCheckedChangeListner();
        /*
         * Sorting By Distance
         * **********/
        setUpSortingByDistance();
        //**********Sorting By Price* **********//*
        setUpSortingByPrice();
        //*********** Sorting By Publishing Date* **********//*
        setUpSortingByPublishingDate();
        //*********** Sorting By Delivery Method* **********//*
        setUpSortingByDeliveryMethod();
        //*********** Sorting By Delivery Method* **********//*
        setUpSortingByAvailability();
         /*     refresh all */
        refreshRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshLayout();
            }
        });
        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createJson();
            }
        });

        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        country = PlayerPawnPreference.readString(getActivity(), "country", "");
        locality = PlayerPawnPreference.readString(getActivity(), "city", "");
        administrative_area_level_1 = PlayerPawnPreference.readString(getActivity(), "state", "");
//        country = PlayerPawnPreference.readString(getActivity(),"postalCode","");
        latitude = (PlayerPawnPreference.readString(getActivity(), "latitude", ""));
        longitude = (PlayerPawnPreference.readString(getActivity(), "latitude", ""));
        Log.e(TAG, "onResume: " + latitude);
        Log.e(TAG, "onResume: " + longitude);
        Log.e(TAG, "Constants_AddItem.FilterPrize: " + Constants_AddItem.FilterPrize);

        if (Constants_AddItem.FilterPrize) {
            byPriceHighCB.setChecked(true);
            byPriceHighCB.setEnabled(false);
            byPriceLowestCB.setEnabled(true);
        } else {
            byPriceLowestCB.setChecked(true);
            byPriceLowestCB.setEnabled(false);
            byPriceHighCB.setEnabled(true);
        }

        if (Constants_AddItem.FilterDistance) {
            distanceFarestCB.setChecked(true);
            distanceFarestCB.setEnabled(false);
            distanceNearestCB.setEnabled(true);
        } else {
            distanceFarestCB.setEnabled(true);
            distanceFarestCB.setChecked(false);
            distanceNearestCB.setEnabled(false);
            distanceNearestCB.setChecked(true);
        }

        if (Constants_AddItem.FiterDate) {
            fromPublishingFartest.setChecked(true);
            fromPublishingFartest.setEnabled(false);
            byPublishingNearestCB.setEnabled(true);
            byPublishingNearestCB.setChecked(false);
        } else {
            fromPublishingFartest.setChecked(false);
            fromPublishingFartest.setEnabled(true);
            byPublishingNearestCB.setEnabled(false);
            byPublishingNearestCB.setChecked(true);
        }

        if (Constants_AddItem.FilterDeliveryType) {
            delShippingCB.setEnabled(true);
            delShippingCB.setChecked(false);
            delPersonalTransfer.setChecked(true);
            delPersonalTransfer.setEnabled(false);
        } else {
            delShippingCB.setEnabled(false);
            delShippingCB.setChecked(true);
            delPersonalTransfer.setChecked(false);
            delPersonalTransfer.setEnabled(true);
        }
        if (Constants_AddItem.FilterPawncast == 0) {
            bySoldCB.setChecked(true);
            bySoldCB.setEnabled(false);
            byAvailabilityCB.setEnabled(true);
            pawnCostCB.setEnabled(true);
            byAvailabilityCB.setChecked(false);
            pawnCostCB.setChecked(false);
        } else if (Constants_AddItem.FilterPawncast == 1) {
            bySoldCB.setChecked(false);
            bySoldCB.setEnabled(true);
            byAvailabilityCB.setEnabled(false);
            pawnCostCB.setEnabled(true);
            byAvailabilityCB.setChecked(true);
            pawnCostCB.setChecked(false);
        } else {
            bySoldCB.setChecked(false);
            bySoldCB.setEnabled(true);
            byAvailabilityCB.setEnabled(true);
            pawnCostCB.setEnabled(false);
            byAvailabilityCB.setChecked(false);
            pawnCostCB.setChecked(true);
        }

        if (Constants_AddItem.FilterRange == 100) {
            seekBar.setProgress(42);
        } else if (Constants_AddItem.FilterRange == 1000) {
            seekBar.setProgress(78);
        } else {
            seekBar.setProgress(120);
        }

        if (!Constants_AddItem.FilterThingsWorn) {
            thingsWornCB.setChecked(false);
        }
        else {
            thingsWornCB.setChecked(true);
        }
        if (!Constants_AddItem.FilterAccess) {
            accessoriesCB.setChecked(false);
        }else {
            accessoriesCB.setChecked(true);
        }
        if (!Constants_AddItem.FilterExperince) {
            experienceCb.setChecked(false);
        }else {
            experienceCb.setChecked(true);
        }
        if (!Constants_AddItem.FilterBallBat) {
            ballsBatsCB.setChecked(false);
        }else {
            ballsBatsCB.setChecked(true);
        }
        if (!Constants_AddItem.FilterThingsMove) {
            thingsThatMovesCB.setChecked(false);
        }else {
            thingsThatMovesCB.setChecked(true);
        }
        if (!Constants_AddItem.FilterMiscl) {
            miscellaneousCB.setChecked(false);
        }else {
            miscellaneousCB.setChecked(true);
        }
        if (!Constants_AddItem.FilterFootwear) {
            footwareCb.setChecked(false);
        }else {
            footwareCb.setChecked(true);
        }
        if (!Constants_AddItem.FilterFashion) {
            fashionCb.setChecked(false);
        }else {
            fashionCb.setChecked(true);
        }
        if (!Constants_AddItem.FilterAutograph) {
            autographedCB.setChecked(false);
        }else {
            autographedCB.setChecked(true);
        }
    }

    private void createJson() {
        Utilities.showProgressDialog(getActivity());
        JSONObject filterJson = new JSONObject();
        JSONObject locationJson = new JSONObject();
        JSONArray newCatJson = new JSONArray();

        if (accessoriesCB.isChecked()) {
            newCatJson.put("ACCESSORIES");
            Constants_AddItem.FilterAccess = true;
        } else {
            Constants_AddItem.FilterAccess = false;
        }
        if (autographedCB.isChecked()) {
            autographedCB.setChecked(true);
            Constants_AddItem.FilterAutograph = true;
        } else {
            Constants_AddItem.FilterAutograph = false;
        }
        if (thingsWornCB.isChecked()) {
            newCatJson.put("Things worn");
            Constants_AddItem.FilterThingsWorn = true;
        } else {
            Constants_AddItem.FilterThingsWorn = false;
        }
        if (experienceCb.isChecked()) {
            newCatJson.put("The Experience");
            Constants_AddItem.FilterExperince = true;
        } else {
            Constants_AddItem.FilterExperince = false;
        }
        if (ballsBatsCB.isChecked()) {
            newCatJson.put("Balls,Bats & Bags");
            Constants_AddItem.FilterBallBat = true;
        } else {
            Constants_AddItem.FilterBallBat = false;
        }
        if (thingsThatMovesCB.isChecked()) {
            newCatJson.put("Things That Move");
            Constants_AddItem.FilterThingsMove = true;
        } else {
            Constants_AddItem.FilterThingsMove = false;
        }
        if (miscellaneousCB.isChecked()) {
            newCatJson.put("Miscellaneous");
            Constants_AddItem.FilterMiscl = true;
        } else {
            Constants_AddItem.FilterMiscl = false;
        }
        if (footwareCb.isChecked()) {
            newCatJson.put("Footwear");
            Constants_AddItem.FilterFootwear = true;
        } else {
            Constants_AddItem.FilterFootwear = false;
        }
        if (fashionCb.isChecked()) {
            newCatJson.put("Fashion");
            Constants_AddItem.FilterFashion = true;
        } else {
            Constants_AddItem.FilterFashion = false;
        }
        if (autographedCB.isChecked()) {
            newCatJson.put(true);
            Constants_AddItem.FilterAutograph = true;
        } else {
            newCatJson.put(false);
            Constants_AddItem.FilterAutograph = false;
        }
        distance = distanceFarestCB.isChecked();
        price = byPriceHighCB.isChecked();
        date = fromPublishingFartest.isChecked();
        delivery_type = delPersonalTransfer.isChecked();
        if (bySoldCB.isChecked()) {
            pawncast = 0;
        }
        if (byAvailabilityCB.isChecked()) {
            pawncast = 1;
        }
        if (pawnCostCB.isChecked()) {
            pawncast = 2;
        }
                /*  range*/
        if (seekBar.getProgress() > 98) {
            range = 100000;
        } else if (seekBar.getProgress() >= 45 && seekBar.getProgress() <= 98) {
            range = 1000;
        } else if (seekBar.getProgress() < 45) {
            range = 100;
        }
        try {
            if (Constants.ExploreFragmentCurrent.equalsIgnoreCase(Constants.VerifiedSeller)) {
                filterJson.put("verified", "1");
            } else {
                filterJson.put("verified", "2");
            }
            filterJson.put("perPage", perPage);
            filterJson.put("page_no", page_no);
            filterJson.put("filterApply", filterApply);
            filterJson.put("price", price);
            filterJson.put("delivery_type", delivery_type);
            filterJson.put("distance", distance);
            filterJson.put("range", range);
            filterJson.put("date", date);
            filterJson.put("pawncast", pawncast);

            filterJson.put("cat", newCatJson);
                        /* location object*/
            locationJson.put("administrative_area_level_1", administrative_area_level_1);
            locationJson.put("country", country);
            locationJson.put("lat", latitude);
            locationJson.put("lng", longitude);
            locationJson.put("locality", locality);
            filterJson.put("location", locationJson);
            filterJson.put("page_no", page_no);
            filterJson.put("page_no", page_no);
            filterJson.put("search", search);
            Log.e(TAG, "filterJson: " + filterJson);

            Constants.filterJson = filterJson;
            Utilities.hideProgressDialog();
            Constants_AddItem.FilterPrize = price;
            Constants_AddItem.FilterDistance = distance;
            Constants_AddItem.FilterDeliveryType = delivery_type;
            Constants_AddItem.FiterDate = date;
            Constants_AddItem.FilterRange = range;
            Constants_AddItem.FilterPawncast = pawncast;

            HomeActivity.switchFragment(getActivity(), new ExploreFragment("Filer"), "ExploreFragment", false, null);
//            hitService(filterJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void refreshLayout() {

        thingsWornCB.setChecked(true);
        thingsWornCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

        accessoriesCB.setChecked(true);
        accessoriesCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
        experienceCb.setChecked(true);

        experienceCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

        ballsBatsCB.setChecked(true);
        ballsBatsCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

        thingsThatMovesCB.setChecked(true);
        thingsThatMovesCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

        miscellaneousCB.setChecked(true);
        miscellaneousCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

        footwareCb.setChecked(true);
        footwareCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

        fashionCb.setChecked(true);
        fashionCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));


        autographedCB.setChecked(true);
        autographedCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

        seekBar.setProgress(120);
        range = 1000000;

        distanceNearestCB.setChecked(false);
        distanceNearestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
        distanceFarestCB.setChecked(true);
        distanceFarestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
        distance = false;

        byPriceLowestCB.setChecked(false);
        byPriceLowestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
        byPriceHighCB.setChecked(true);
        byPriceHighCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
        price = true;

        byPublishingNearestCB.setChecked(false);
        byPublishingNearestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
        fromPublishingFartest.setChecked(true);
        fromPublishingFartest.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
        date = false;

        delShippingCB.setChecked(false);
        delShippingCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
        delPersonalTransfer.setChecked(true);
        delPersonalTransfer.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
        delivery_type = true;

        bySoldCB.setChecked(false);
        bySoldCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
        byAvailabilityCB.setChecked(true);
        byAvailabilityCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
        pawnCostCB.setChecked(false);
        pawnCostCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
        pawncast = 2;

    }


    private void setUpSeekBarFixedProgress() {
        seekBar.setMax(120);
        seekBar.setProgress(120);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                progress=10;
//                progress=progress*10;
//                seekBar.setProgress(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (seekBar.getProgress() < 55) {
                    seekBar.setProgress(42);
                    range = 100;
                } else if (seekBar.getProgress() >= 58 && seekBar.getProgress() < 98) {
                    seekBar.setProgress(78);
                    range = 1000;
                } else
                    seekBar.setProgress(120);
                range = 1000000;

            }
        });
    }

    private void setUpCheckedChangeListner() {

        thingsWornCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    thingsWornCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));

                } else {
                    thingsWornCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                }
            }
        });

        accessoriesCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    accessoriesCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    accessoriesCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }
            }
        });
        experienceCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    experienceCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    experienceCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                }

            }
        });
        ballsBatsCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ballsBatsCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    ballsBatsCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                }
            }
        });
        thingsThatMovesCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    thingsThatMovesCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    thingsThatMovesCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                }
            }
        });
        miscellaneousCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    miscellaneousCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    miscellaneousCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }
            }
        });
        footwareCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    footwareCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    footwareCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }
            }
        });
        fashionCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    fashionCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    fashionCb.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }
            }
        });
        autographedCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    autographedCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                } else {
                    autographedCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));

                }
            }
        });
    }


    private void setUpSortingByDistance() {
        distanceFarestCB.setEnabled(false);
        byPriceHighCB.setEnabled(false);
        fromPublishingFartest.setEnabled(false);
        delPersonalTransfer.setEnabled(false);
        byAvailabilityCB.setEnabled(false);
        distanceNearestCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    distanceNearestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    distanceFarestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    distanceFarestCB.setChecked(false);
                    distanceFarestCB.setEnabled(true);
                    distanceNearestCB.setEnabled(false);
                }

            }
        });
        distanceFarestCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.e(TAG, "true: ");
                    distanceFarestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    distanceNearestCB.setChecked(false);
                    distanceNearestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    distanceFarestCB.setEnabled(false);
                    distanceNearestCB.setEnabled(true);
                } else {
                    Log.e(TAG, "false: ");
                }
            }
        });
    }

    private void setUpSortingByPrice() {
        byPriceLowestCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    byPriceLowestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    byPriceHighCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    byPriceHighCB.setChecked(false);
                    byPriceLowestCB.setEnabled(false);
                    byPriceHighCB.setEnabled(true);
                }

            }
        });
        byPriceHighCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    byPriceHighCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    byPriceLowestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    byPriceLowestCB.setChecked(false);
                    byPriceHighCB.setEnabled(false);
                    byPriceLowestCB.setEnabled(true);
                }

            }
        });
    }

    private void setUpSortingByPublishingDate() {
        byPublishingNearestCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    byPublishingNearestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    fromPublishingFartest.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    fromPublishingFartest.setChecked(false);
                    byPublishingNearestCB.setEnabled(false);
                    fromPublishingFartest.setEnabled(true);
                }
            }
        });
        fromPublishingFartest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    fromPublishingFartest.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    byPublishingNearestCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    byPublishingNearestCB.setChecked(false);
                    fromPublishingFartest.setEnabled(false);
                    byPublishingNearestCB.setEnabled(true);
                }

            }
        });
    }

    private void setUpSortingByDeliveryMethod() {
        delShippingCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    delShippingCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    delPersonalTransfer.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    delPersonalTransfer.setChecked(false);
                    delShippingCB.setEnabled(false);
                    delPersonalTransfer.setEnabled(true);
                }


            }
        });
        delPersonalTransfer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    delPersonalTransfer.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    delShippingCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    delShippingCB.setChecked(false);
                    delPersonalTransfer.setEnabled(false);
                    delShippingCB.setEnabled(true);
                }


            }
        });
    }

    private void setUpSortingByAvailability() {
        bySoldCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bySoldCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    byAvailabilityCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    pawnCostCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    byAvailabilityCB.setChecked(false);
                    pawnCostCB.setChecked(false);
                    bySoldCB.setEnabled(false);
                    byAvailabilityCB.setEnabled(true);
                    pawnCostCB.setEnabled(true);
                }

            }
        });
        byAvailabilityCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bySoldCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    byAvailabilityCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    pawnCostCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    bySoldCB.setChecked(false);
                    pawnCostCB.setChecked(false);
                    byAvailabilityCB.setEnabled(false);
                    bySoldCB.setEnabled(true);
                    pawnCostCB.setEnabled(true);
                }
            }
        });
        pawnCostCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bySoldCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    byAvailabilityCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkslate));
                    pawnCostCB.setTextColor(getActivity().getResources().getColor(R.color.gray_darkcoal));
                    bySoldCB.setChecked(false);
                    byAvailabilityCB.setChecked(false);
                    byAvailabilityCB.setEnabled(true);
                    bySoldCB.setEnabled(true);
                    pawnCostCB.setEnabled(false);
                }
            }
        });

    }

}
