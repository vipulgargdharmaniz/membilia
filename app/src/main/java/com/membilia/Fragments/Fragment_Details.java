package com.membilia.Fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.membilia.CustomTypefaceSpan;
import com.membilia.PlayerPawnApplication;

import com.membilia.R;
import com.membilia.Util.Constants;
import com.membilia.Util.Constants_AddItem;
import com.membilia.Util.PlayerPawnPreference;
import com.membilia.Util.Utilities;
import com.membilia.Util.WebServicesConstants;
import com.membilia.activities.HomeActivity;
import com.membilia.adapters.Sliding_I_D_ImageAdapter;
import com.membilia.adapters.TagsAdapter;
import com.membilia.views.RatingBarView;
import com.membilia.volley_models.IDImagesVideoModel;
import com.membilia.volley_models.IDItemImageModel;
import com.membilia.volley_models.IDItemVideoModel;
import com.membilia.volley_models.IDTagModel;
import com.membilia.volley_models.IDUserImageModel;
import com.membilia.volley_models.ItemDetailLocationModel;
import com.membilia.volley_models.ItemDetailMoldel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import co.lujun.androidtagview.TagContainerLayout;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.SharingHelper;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import io.branch.referral.util.ShareSheetStyle;

import static com.membilia.Util.Constants.IS_HOMEPOST_REFRESH;

/*
* Created by dharmaniz on 28/6/17.
*/
public class Fragment_Details extends Fragment implements OnMapReadyCallback {
    private String TAG = "Fragment_Details";
    private int currentPage = 0;
    private int NUM_PAGES = 0;
    /* layout widgets*/
    RelativeLayout leftRL, rightRL, rl_details1, rl_details2;
    FrameLayout mapRL;
    TextView titleTV, itemstoryTV, genreTV, personalTransferTV, lastusedtv, categorytv, autographedtv,
            locationtv, dilerymethodtv, personaltransferGreen, itemstory, CharityTV;
    LinearLayout editll, sharell, tollbar, ll12;
    SimpleDraweeView profileIV, borderRing;
    TabLayout pageIndicator;
    RelativeLayout topviewRV;
    FrameLayout frameIV;
    ImageView imgSettingIV;
    ViewPager Viewpager;
    String strDetailsType = "edit";
    /* Variables*/
    String strPrfileUrl = "";
    Uri uriProfile;
    View itemsview;
    TextView itemNameTv, editTv, itempricetv, usernameTV, itenweighttv, tagTV;
    RatingBarView ratingBarRB;
    ItemDetailMoldel itemDetailMoldel;
    ItemDetailLocationModel itemDetailLocationModel;
    IDUserImageModel IDUserImageModel;
    IDItemImageModel IDItemImageModel;
    IDItemVideoModel IDItemVideoModel;
    ImageView imgLeftIV, imgVerityUnverity;
    Dialog mShareDialog;
    String strItemId;
    LinearLayout mLL;
    RecyclerView recycler_viewTAGS;
    NestedScrollView nSV;
    ArrayList<String> tagsArrayList = new ArrayList<>();
    ArrayList<String> newTag = new ArrayList<>();
    ImageView imageviewmap;
    ScrollView scrollview;

    ArrayList<IDImagesVideoModel> detailImagesArray = new ArrayList<>();

    private GoogleMap mMap;
    String lat = "", log = "";
    SupportMapFragment mapFragment;
    boolean isrefresh = false;

    TagContainerLayout tagContainerLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            strItemId = args.getString("Object");
            Log.e(TAG, "**ID***" + strItemId);
        }
    }

    Handler handler;
    Runnable update;
    HomeActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        activity = new HomeActivity();
        if (itemsview == null) {
            isrefresh = true;
            itemsview = inflater.inflate(R.layout.itemdetails_fragment, null);
            tagContainerLayout = itemsview.findViewById(R.id.tag_group);
            tagContainerLayout.setTagBackgroundColor(Color.RED);
            tagContainerLayout.setTagTextColor(Color.WHITE);
            tagContainerLayout.setBackgroundColor(Color.WHITE);
            tagContainerLayout.setTagBorderRadius(1);

        } else
            isrefresh = false;
        if (Constants.FragmentRefresh.equalsIgnoreCase("true")) {
            isrefresh = true;
            Constants.FragmentRefresh = "";
        }
        return itemsview;
    }


    @Override
    public void onResume() {
        super.onResume();
//        activity.getAllOrderHistory();
        if (isrefresh) {
            setUpViews(itemsview);
            HomeActivity.bottombar.setVisibility(View.VISIBLE);
            HomeActivity.imgAddPlayerPawn.setVisibility(View.VISIBLE);
            tagsArrayList.clear();
            setupclicks();
            setVisibity();
            getItemsDetails();
        }
    }


    private void setVisibity() {
        rightRL.setVisibility(View.VISIBLE);
        leftRL.setVisibility(View.VISIBLE);
        tollbar.setVisibility(View.VISIBLE);
        imgVerityUnverity.setVisibility(View.VISIBLE);
        rl_details1.setVisibility(View.INVISIBLE);
        rl_details2.setVisibility(View.INVISIBLE);
        imgSettingIV.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.filtercircle));

        titleTV.setText(getActivity().getString(R.string.itemdetails));
        borderRing.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);
        profileIV.getHierarchy().setPlaceholderImage(R.drawable.ringwhite);
        topviewRV.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        frameIV.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
    }


    private void setUpViews(View v) {
        /*new inflates*/
        titleTV = v.findViewById(R.id.titleTV);
        rightRL = v.findViewById(R.id.rightRL);
        leftRL = v.findViewById(R.id.leftRL);
        sharell = v.findViewById(R.id.sharell);
        editll = v.findViewById(R.id.editll);
        profileIV = v.findViewById(R.id.profileIV);
        borderRing = v.findViewById(R.id.borderRing);
        pageIndicator = v.findViewById(R.id.pageIndicator);
        topviewRV = v.findViewById(R.id.topviewRV);
        frameIV = v.findViewById(R.id.frameIV);
        genreTV = v.findViewById(R.id.genreTV);
        personalTransferTV = v.findViewById(R.id.personalTransferTV);
        imgSettingIV = v.findViewById(R.id.imgSettingIV);
        tollbar = v.findViewById(R.id.topbar);
        imgVerityUnverity = v.findViewById(R.id.imgVerityUnverity);
        personaltransferGreen = v.findViewById(R.id.personaltransferGreen);
        mapRL = v.findViewById(R.id.mapRL);
        tagTV = v.findViewById(R.id.tagTV);

        rl_details1 = v.findViewById(R.id.rl_details1);
        rl_details2 = v.findViewById(R.id.rl_details2);

        imageviewmap = v.findViewById(R.id.imageviewmap);
        /*For sliding image Array**/
        Viewpager = v.findViewById(R.id.Viewpager);
        ll12 = v.findViewById(R.id.ll12);
        CharityTV = v.findViewById(R.id.CharityTV);
        itemNameTv = v.findViewById(R.id.itemNameTv);
        itempricetv = v.findViewById(R.id.itempricetv);
        editTv = v.findViewById(R.id.editTv);
        lastusedtv = v.findViewById(R.id.lastusedtv);
        itenweighttv = v.findViewById(R.id.itenweighttv);
        autographedtv = v.findViewById(R.id.autographedtv);
        dilerymethodtv = v.findViewById(R.id.dilerymethodtv);
        locationtv = v.findViewById(R.id.locationtv);
        categorytv = v.findViewById(R.id.categorytv);
        usernameTV = v.findViewById(R.id.usernameTV);
        itemstoryTV = v.findViewById(R.id.itemstoryTV);
        itemstory = v.findViewById(R.id.itemstory);
        imgLeftIV = v.findViewById(R.id.imgLeftIV);
        ratingBarRB = v.findViewById(R.id.ratingBarRB);
        mLL = v.findViewById(R.id.mLL);
        recycler_viewTAGS = v.findViewById(R.id.recycler_viewTAGS);
        nSV = v.findViewById(R.id.mNestedScrollView);
        scrollview = v.findViewById(R.id.scrollview);
        scrollview.setFocusableInTouchMode(true);
        scrollview.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
    }

    private void setupclicks() {
        rightRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Report_Fragment fragment = new Report_Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("useritemid", itemDetailMoldel.getItem_id());
                fragment.setArguments(bundle);
                HomeActivity.switchFragment(getActivity(), new Report_Fragment(), "reportFragment", true, bundle);

            }
        });
        imgLeftIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IS_HOMEPOST_REFRESH = true;
                Constants.BranchIO = "";
                getActivity().onBackPressed();
            }
        });
        sharell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showshare();
                shareLink();
            }
        });
        topviewRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.forProfileID = itemDetailMoldel.getUser_id();
                Bundle bundle = new Bundle();
                bundle.putString("isSameUser", "false");
                HomeActivity.switchFragment(getActivity(), new NewProfileFragment(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });
        editll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (strDetailsType.equalsIgnoreCase("EDIT")) {
                    Utilities.showProgressDialog(getActivity());
                    Constants.OpenEditAddItemFrom = "fragment_details";
                    HomeActivity.switchFragment(getActivity(), new Edit_AddItem(), Constants.Edit_AddItem, true, null);
                } else if (strDetailsType.equalsIgnoreCase("BUY")) {
                    HomeActivity.switchFragment(getActivity(), new BuyFragment(), Constants.Buy_Fragmnet, true, null);
                } else if (strDetailsType.equalsIgnoreCase("History")) {
                    HomeActivity.switchFragment(getActivity(), new Fragment_OrderDetails(), Constants.Buy_Fragmnet, true, null);
                }
            }
        });
    }

    private void shareLink() {

        String messageData = itemDetailMoldel.getItem_name() + "\n" + itemDetailMoldel.getIDItemImageModel().getOriginal_1();
        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier(itemDetailMoldel.getItem_id())
                .setTitle("Check this out on Membilia")
                .setContentDescription(messageData)
                .setContentImageUrl(itemDetailMoldel.getIDItemImageModel().getOriginal_1())
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata().addCustomMetadata("android_deepview", "membilia_deepview_pj0")
                        .addCustomMetadata("ios_deepview", "membilia_deepview_pj0")
                        .addCustomMetadata("item_id", itemDetailMoldel.getItem_id()));

        LinkProperties lp = new LinkProperties()
                .setChannel("facebook")
                .setFeature("sharing")
                .addTag(itemDetailMoldel.getItem_id())
                .setStage("new user")
                .setCampaign("content 123 launch")
                .addControlParameter("$desktop_url", "https://membilia.com/")
                .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));
        Log.e(TAG, "shareLink: " + messageData);
        buo.generateShortUrl(getActivity(), lp, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    Log.i("BRANCH SDK", "got my Branch link to share: " + url);
                } else {
                    Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ShareSheetStyle ss = new ShareSheetStyle(getActivity(), "Check this out on Membilia", messageData)
                .setCopyUrlStyle(ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_send), "Copy", "Added to clipboard")
                .setMoreOptionStyle(ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_search), "Show more")
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.HANGOUT)
                .setAsFullWidthStyle(true)
                .setSharingTitle("Share With");
        buo.showShareSheet(getActivity(), lp, ss, new Branch.BranchLinkShareListener() {
            @Override
            public void onShareLinkDialogLaunched() {
                Log.e(TAG, "onShareLinkDialogLaunched: ");
            }

            @Override
            public void onShareLinkDialogDismissed() {
                Log.e(TAG, "onShareLinkDialogDismissed: ");
            }

            @Override
            public void onLinkShareResponse(String sharedLink, String sharedChannel, BranchError error) {
                Log.e(TAG, "onLinkShareResponse: " + sharedLink);
            }

            @Override
            public void onChannelSelected(String channelName) {
                Log.e(TAG, "onChannelSelected: ");
            }
        });
    }

    private void getItemsDetails() {
        String strUrl = WebServicesConstants.ITEM_DETAILS_ALONE + strItemId;
        Utilities.showProgressDialog(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, strUrl,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Utilities.hideProgressDialog();
                        Log.i("VOLLEY", response);
                        parseResponse(response);
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                Utilities.hideProgressDialog();
                Log.e(TAG, "****onErrorResponse****" + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", PlayerPawnPreference.readString(getActivity(), PlayerPawnPreference.VALUE_TOKEN, ""));
                return headers;
            }
        };
        PlayerPawnApplication.getInstance().addToRequestQueue(stringRequest);
    }

    private void parseResponse(String response) {
        detailImagesArray.clear();
        if (tagsArrayList.size() > 0) {
            tagsArrayList.clear();
        }
        if (newTag.size() > 0) {
            newTag.clear();
        }
        try {
            JSONObject jsondetailObject = (new JSONObject(response));
            itemDetailMoldel = new ItemDetailMoldel();
            if (!jsondetailObject.isNull("item_id")) {
                itemDetailMoldel.setItem_id(jsondetailObject.getString("item_id"));
                strItemId = jsondetailObject.getString("item_id");
            }
            if (!jsondetailObject.isNull("status"))
                itemDetailMoldel.setStatus(jsondetailObject.getString("status"));
            if (!jsondetailObject.isNull("category"))
                itemDetailMoldel.setCategory(jsondetailObject.getString("category"));
            if (!jsondetailObject.isNull("charity"))
                itemDetailMoldel.setCharity(jsondetailObject.getBoolean("charity"));
            if (!jsondetailObject.isNull("charity_name"))
                itemDetailMoldel.setCharity_name(jsondetailObject.getString("charity_name"));
            if (!jsondetailObject.isNull("created"))
                itemDetailMoldel.setCreated(jsondetailObject.getString("created"));
            if (!jsondetailObject.isNull("currency"))
                itemDetailMoldel.setCurrency(jsondetailObject.getString("currency"));
            if (!jsondetailObject.isNull("delivery_type"))
                itemDetailMoldel.setDelivery_type(jsondetailObject.getBoolean("delivery_type"));
            if (!jsondetailObject.isNull("height"))
                itemDetailMoldel.setHeight(jsondetailObject.getString("height"));
            if (!jsondetailObject.isNull("isAutographed"))
                itemDetailMoldel.setIsAutographed(jsondetailObject.getBoolean("isAutographed"));
            if (!jsondetailObject.isNull("isFavorite"))
                itemDetailMoldel.setIsFavorite(jsondetailObject.getBoolean("isFavorite"));
            if (!jsondetailObject.isNull("isSold"))
                itemDetailMoldel.setIsSold(jsondetailObject.getBoolean("isSold"));
            if (!jsondetailObject.isNull("item_name")) {
                itemDetailMoldel.setItem_name(jsondetailObject.getString("item_name"));
            }
            if (!jsondetailObject.isNull("item_story"))
                itemDetailMoldel.setItem_story(jsondetailObject.getString("item_story"));
            if (!jsondetailObject.isNull("last_used"))
                itemDetailMoldel.setLast_used(jsondetailObject.getString("last_used"));
            if (!jsondetailObject.isNull("length"))
                itemDetailMoldel.setLength(jsondetailObject.getString("length"));
            if (!jsondetailObject.isNull("newtimestamp"))
                itemDetailMoldel.setNewtimestamp(jsondetailObject.getString("newtimestamp"));
            if (!jsondetailObject.isNull("onHold"))
                itemDetailMoldel.setOnHold(jsondetailObject.getString("onHold"));
            if (!jsondetailObject.isNull("pawned_date"))
                itemDetailMoldel.setPawned_date(jsondetailObject.getString("pawned_date"));

            if (!jsondetailObject.isNull("tags")) {
                IDTagModel IDTagModel = new IDTagModel();
                JSONArray jsonArray = jsondetailObject.getJSONArray("tags");
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (!jsonArray.getString(i).equalsIgnoreCase("")) {
                        tagsArrayList.add(jsonArray.getString(i).toString().trim());
                    }

                }
                IDTagModel.setTagsArrayList(tagsArrayList);
                itemDetailMoldel.setIDTagModel(IDTagModel);
            }

            if (!jsondetailObject.isNull("person_transfertext"))
                itemDetailMoldel.setPerson_transfertext(jsondetailObject.getString("person_transfertext"));
            if (!jsondetailObject.isNull("price"))
                itemDetailMoldel.setPrice(jsondetailObject.getString("price"));
            if (!jsondetailObject.isNull("rating"))
                itemDetailMoldel.setRating(jsondetailObject.getString("rating"));
            if (!jsondetailObject.isNull("seller_id"))
                itemDetailMoldel.setSeller_id(jsondetailObject.getString("seller_id"));
            if (!jsondetailObject.isNull("sport"))
                itemDetailMoldel.setSport(jsondetailObject.getString("sport"));
            if (!jsondetailObject.isNull("status"))
                itemDetailMoldel.setStatus(jsondetailObject.getString("status"));

            if (!jsondetailObject.isNull("genre")) {
                itemDetailMoldel.setGenre(jsondetailObject.getString("genre"));
            }
            if (!jsondetailObject.isNull("genre_value")) {
                itemDetailMoldel.setGenre_value(jsondetailObject.getString("genre_value"));
            }
            if (!jsondetailObject.isNull("status2"))
                itemDetailMoldel.setStatus2(jsondetailObject.getString("status2"));
            if (!jsondetailObject.isNull("user_id"))
                itemDetailMoldel.setUser_id(jsondetailObject.getString("user_id"));
            if (!jsondetailObject.isNull("user_name"))
                itemDetailMoldel.setUser_name(jsondetailObject.getString("user_name"));
            itemDetailMoldel.setVerified(jsondetailObject.getString("verified"));
            if (itemDetailMoldel.getVerified().equalsIgnoreCase("0")) {
                imgVerityUnverity.setVisibility(View.GONE);
            }
            if (!jsondetailObject.isNull("weight"))
                itemDetailMoldel.setweight(jsondetailObject.getString("weight"));
            if (!jsondetailObject.isNull("location")) {
                JSONObject locationObject = jsondetailObject.getJSONObject("location");
                itemDetailLocationModel = new ItemDetailLocationModel();
                if (!locationObject.isNull("administrative_area_level_1")) {
                    Constants_AddItem.AdminArea = locationObject.getString("administrative_area_level_1");
                    itemDetailLocationModel.setAdministrative_area_level_1(locationObject.getString("administrative_area_level_1"));
                }
                if (!locationObject.isNull("country")) {
                    Constants_AddItem.AdminCountry = locationObject.getString("country");
                    itemDetailLocationModel.setCountry(locationObject.getString("country"));
                }
                if (!locationObject.isNull("country_code")) {
                    Constants_AddItem.AdminCountryCode = locationObject.getString("country_code");
                    itemDetailLocationModel.setCountry_code(locationObject.getString("country_code"));
                }
                if (!locationObject.isNull("lat")) {
                    Constants_AddItem.AdminLat = locationObject.getString("lat");
                    itemDetailLocationModel.setLat(locationObject.getString("lat"));
                }
                if (!locationObject.isNull("lng")) {
                    Constants_AddItem.AdminLng = locationObject.getString("lng");
                    itemDetailLocationModel.setLng(locationObject.getString("lng"));
                }
                if (!locationObject.isNull("locality")) {
                    Constants_AddItem.AdminLocatity = locationObject.getString("locality");
                    itemDetailLocationModel.setCountry(locationObject.getString("locality"));

                }
                if (!locationObject.isNull("postal_code")) {
                    Constants_AddItem.AdminPostalCode = locationObject.getString("postal_code");
                    itemDetailLocationModel.setPostal_code(locationObject.getString("postal_code"));
                }
                itemDetailMoldel.setItemDetailLocationModel(itemDetailLocationModel);
            }

            if (!jsondetailObject.isNull("user_images")) {
                JSONObject imageObject = jsondetailObject.getJSONObject("user_images");
                IDUserImageModel = new IDUserImageModel();
                if (!imageObject.isNull("original")) {
                    IDUserImageModel.setOriginal(imageObject.getString("original"));
                    itemDetailMoldel.setIDUserImageModel(IDUserImageModel);
                }
            }
//             else
            if (!jsondetailObject.isNull("item_images")) {
                JSONObject itemImagesObject = jsondetailObject.getJSONObject("item_images");
                IDItemImageModel = new IDItemImageModel();
                if (!itemImagesObject.isNull("original_1")) {
                    IDItemImageModel.setOriginal_1(itemImagesObject.getString("original_1"));

                    IDImagesVideoModel mIDIdImagesVideoModel = new IDImagesVideoModel();
                    mIDIdImagesVideoModel.setStrImageUrl(itemImagesObject.getString("original_1"));
                    mIDIdImagesVideoModel.setImageType("image");
                    detailImagesArray.add(mIDIdImagesVideoModel);
                }

                if (!itemImagesObject.isNull("original_2")) {
                    IDItemImageModel.setOriginal_2(itemImagesObject.getString("original_2"));
                    if (itemImagesObject.getString("original_2").length() > 0) {

                        IDImagesVideoModel mIDIdImagesVideoModel = new IDImagesVideoModel();
                        mIDIdImagesVideoModel.setStrImageUrl(itemImagesObject.getString("original_2"));
                        mIDIdImagesVideoModel.setImageType("image");
                        detailImagesArray.add(mIDIdImagesVideoModel);
                    }
                }


                if (!itemImagesObject.isNull("original_3")) {
                    IDItemImageModel.setOriginal_3(itemImagesObject.getString("original_3"));
                    if (itemImagesObject.getString("original_3").length() > 0) {
                        IDImagesVideoModel mIDIdImagesVideoModel = new IDImagesVideoModel();
                        mIDIdImagesVideoModel.setStrImageUrl(itemImagesObject.getString("original_3"));
                        mIDIdImagesVideoModel.setImageType("image");
                        detailImagesArray.add(mIDIdImagesVideoModel);
                    }
                }
                if (!itemImagesObject.isNull("original_4")) {
                    IDItemImageModel.setOriginal_4(itemImagesObject.getString("original_4"));
                    if (itemImagesObject.getString("original_4").length() > 0) {
                        IDImagesVideoModel mIDIdImagesVideoModel = new IDImagesVideoModel();
                        mIDIdImagesVideoModel.setStrImageUrl(itemImagesObject.getString("original_4"));
                        mIDIdImagesVideoModel.setImageType("image");
                        detailImagesArray.add(mIDIdImagesVideoModel);
                    }
                }
                if (!itemImagesObject.isNull("original_5")) {
                    IDItemImageModel.setOriginal_5(itemImagesObject.getString("original_5"));
                    if (itemImagesObject.getString("original_5").length() > 0) {
                        IDImagesVideoModel mIDIdImagesVideoModel = new IDImagesVideoModel();
                        mIDIdImagesVideoModel.setStrImageUrl(itemImagesObject.getString("original_5"));
                        mIDIdImagesVideoModel.setImageType("image");
                        detailImagesArray.add(mIDIdImagesVideoModel);
                    }
                }
                if (!itemImagesObject.isNull("original_6")) {
                    IDItemImageModel.setOriginal_6(itemImagesObject.getString("original_6"));
                    if (itemImagesObject.getString("original_6").length() > 0) {
                        IDImagesVideoModel mIDIdImagesVideoModel = new IDImagesVideoModel();
                        mIDIdImagesVideoModel.setStrImageUrl(itemImagesObject.getString("original_6"));
                        mIDIdImagesVideoModel.setImageType("image");
                        detailImagesArray.add(mIDIdImagesVideoModel);
                    }
                }
                itemDetailMoldel.setIDItemImageModel(IDItemImageModel);
            }

            if (!jsondetailObject.isNull("item_video")) {
                IDImagesVideoModel mIDIdImagesVideoModel = new IDImagesVideoModel();
                JSONObject itemVideoObject = jsondetailObject.getJSONObject("item_video");
                IDItemVideoModel = new IDItemVideoModel();
                if (!itemVideoObject.isNull("original_1")) {
                    IDItemVideoModel.setOriginal_1(itemVideoObject.getString("original_1"));
                    mIDIdImagesVideoModel.setVideoLink(itemVideoObject.getString("original_1"));
//                    detailImagesArray.add(mIDIdImagesVideoModel);
                }

                if (!itemVideoObject.isNull("original_videoimage")) {
                    IDItemVideoModel.setOriginal_videoimage(itemVideoObject.getString("original_videoimage"));
                    if (itemVideoObject.getString("original_videoimage").length() > 0) {
                        mIDIdImagesVideoModel.setStrImageUrl(itemVideoObject.getString("original_videoimage"));
                        mIDIdImagesVideoModel.setVideoLink(itemVideoObject.getString("original_1"));
                        mIDIdImagesVideoModel.setImageType("video");
                        detailImagesArray.add(mIDIdImagesVideoModel);
                    }
                }
//                Collections.reverse(detailImagesArray);
                itemDetailMoldel.setIDItemVideoModel(IDItemVideoModel);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*
         * Setting values in Constant_addItem
         */
        setDataOnWidgets(itemDetailMoldel);
        item_DetailSliding_Image();
    }

    private void setDataOnWidgets(ItemDetailMoldel itemDetailMoldel) {

        lat = itemDetailMoldel.getItemDetailLocationModel().getLat();
        log = itemDetailMoldel.getItemDetailLocationModel().getLng();

        if (itemDetailMoldel.getIDUserImageModel().getOriginal().length() > 0) {
            strPrfileUrl = itemDetailMoldel.getIDUserImageModel().getOriginal();
            uriProfile = Uri.parse(strPrfileUrl);
//            Picasso.with(getActivity()).load(uriProfile).into(profileIV);
            profileIV.setImageURI(uriProfile);
        }
        categorytv.setText(itemDetailMoldel.getCategory());

        if (itemDetailMoldel.getDelivery_type()) {
            dilerymethodtv.setText("Personal transfer");
            personaltransferGreen.setVisibility(View.VISIBLE);
        } else {
            dilerymethodtv.setText("Shipping");
            personaltransferGreen.setVisibility(View.GONE);
        }

        if (itemDetailMoldel.getIsAutographed()) {
            autographedtv.setText("Yes");
        } else {
            autographedtv.setText("No");
        }
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Bold.ttf");
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "Raleway-Light.ttf");

        Log.i(TAG, "itemDetailMoldel.getItem_name(): " + itemDetailMoldel.getItem_name());
        itemNameTv.setText(itemDetailMoldel.getItem_name());
        Spannable name = new SpannableString("Item Story: ");
        name.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.green)), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        name.setSpan(new CustomTypefaceSpan("", font), 0, name.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        itemstory.setText(name);

        Log.e(TAG, "itemDetailMoldel.getItem_story(): " + itemDetailMoldel.getItem_story());
        Spannable surName = new SpannableString(itemDetailMoldel.getItem_story());
        surName.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.gray_darkcoal)), 0, surName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        surName.setSpan(new CustomTypefaceSpan("", font2), 0, surName.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        itemstory.append(surName);

        String number = String.valueOf(itemDetailMoldel.getPrice());
        double amount = Double.parseDouble(number);
        DecimalFormat formatter = new DecimalFormat("#,###");
        String formatted = formatter.format(amount);
        itempricetv.setText("$" + formatted);

        usernameTV.setText(itemDetailMoldel.getUser_name());
        genreTV.setText(itemDetailMoldel.getGenre() + " -> " + itemDetailMoldel.getGenre_value());
        itenweighttv.setText(itemDetailMoldel.getWeight() + " lbs");

        Log.e(TAG, "issold: " + itemDetailMoldel.getIsSold());
        Log.e(TAG, "userid: " + HomeActivity.UserItself);
        Log.e(TAG, "second user : " + itemDetailMoldel.getUser_id());

        String sellerid = "";
        if (itemDetailMoldel.getSeller_id().contains("playpawn")) {
            sellerid = itemDetailMoldel.getSeller_id();
        } else {
            sellerid = itemDetailMoldel.getSeller_id() + "playpawn";
        }

        if (!itemDetailMoldel.getIsSold() && !HomeActivity.UserItself.equalsIgnoreCase(itemDetailMoldel.getUser_id())) {
            editTv.setText(getActivity().getResources().getString(R.string.BUY));
            strDetailsType = "BUY";
        } else if (itemDetailMoldel.getIsSold() && HomeActivity.UserItself.equalsIgnoreCase(sellerid)) {
            editTv.setText(getActivity().getResources().getString(R.string.OrderHistory));
            strDetailsType = "History";
        } else if (itemDetailMoldel.getIsSold() && HomeActivity.UserItself.equalsIgnoreCase(itemDetailMoldel.getUser_id())) {
            editTv.setText(getActivity().getResources().getString(R.string.OrderHistory));
            strDetailsType = "History";
        } else if (itemDetailMoldel.getIsSold() && !HomeActivity.UserItself.equalsIgnoreCase(itemDetailMoldel.getUser_id())) {
            editTv.setText(getActivity().getResources().getString(R.string.Sold));
            strDetailsType = "Sold";
        } else if (!itemDetailMoldel.getIsSold() && HomeActivity.UserItself.equalsIgnoreCase(itemDetailMoldel.getUser_id())) {
            editTv.setText(getActivity().getResources().getString(R.string.edit));
            strDetailsType = "EDIT";
        }

        if (!itemDetailMoldel.getCharity()) {
            ll12.setVisibility(View.GONE);
        } else {
            ll12.setVisibility(View.VISIBLE);
            String strcharity = "<u>" + "@" + "" + itemDetailMoldel.getCharity_name() + "</u>";
            CharityTV.setText(Html.fromHtml(strcharity));
        }

        ll12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String charityMsg = CharityTV.getText().toString();
                String finalmsg = charityMsg.replace("@", "");
                Toast.makeText(getActivity(), finalmsg, Toast.LENGTH_SHORT).show();
            }
        });

        if (!itemDetailMoldel.getPerson_transfertext().equalsIgnoreCase("")) {
            personalTransferTV.setVisibility(View.VISIBLE);
            Spannable personal = new SpannableString("Personal Transfer Message: ");
            personal.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.green)), 0, personal.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            personal.setSpan(new CustomTypefaceSpan("", font), 0, personal.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            personaltransferGreen.setText(personal);

            Spannable personalmsg = new SpannableString(itemDetailMoldel.getPerson_transfertext());
            personalmsg.setSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.gray_darkcoal)), 0, personalmsg.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            personalmsg.setSpan(new CustomTypefaceSpan("", font2), 0, personalmsg.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            personaltransferGreen.append(personalmsg);


        } else {
            personalTransferTV.setVisibility(View.GONE);
        }

        String Location = "<u>" + itemDetailLocationModel.getAdministrative_area_level_1() + "</u>";
        locationtv.setText(Html.fromHtml(Location));
        ratingBarRB.setStar(Math.round(Float.parseFloat(itemDetailMoldel.getRating())), true);
        ratingBarRB.setClickable(false);
        /* Location click event For Toast*************/
        locationtv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), itemDetailLocationModel.getAdministrative_area_level_1(), Toast.LENGTH_SHORT).show();
            }
        });
        String strTemp = itemDetailMoldel.getLast_used();
        String strArray[] = strTemp.split("T");
        String strDate = strArray[0];
        String strActualFormat = Utilities.getDate(strDate);
        lastusedtv.setText(strActualFormat);
        for (int s = 0; s < tagsArrayList.size(); s++) {
//            Tag tag = new Tag(tagsArrayList.get(s));
            newTag.add(tagsArrayList.get(s));

        }
        tagContainerLayout.setTags(newTag);
        tagContainerLayout.setOnTagClickListener(new co.lujun.androidtagview.TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                Fragments_Tags fragment_tags = new Fragments_Tags();
                Bundle bundle = new Bundle();
                bundle.putString("name", tagsArrayList.get(position));
                fragment_tags.setArguments(bundle);
                HomeActivity.switchFragment(getActivity(), new Fragments_Tags(), Constants.Fragments_Tags, true, bundle);

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {

            }
        });


        //Setting Up TAGS on RecyclerView
        recycler_viewTAGS.setHasFixedSize(true);
        recycler_viewTAGS.setNestedScrollingEnabled(false);
        StaggeredGridLayoutManager _sGridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        recycler_viewTAGS.setLayoutManager(_sGridLayoutManager);
        TagsAdapter rcAdapter = new TagsAdapter(getActivity(), tagsArrayList);
        recycler_viewTAGS.setAdapter(rcAdapter);

        if (tagsArrayList.size() == 0)
            tagTV.setVisibility(View.GONE);


        rl_details1.setVisibility(View.VISIBLE);
        rl_details2.setVisibility(View.VISIBLE);
        Constants_AddItem.ITEM_ADDER_ID = itemDetailMoldel.getUser_id();
        Constants_AddItem.ITEM_ID = itemDetailMoldel.getItem_id();
        Constants_AddItem.USER_ID = itemDetailMoldel.getUser_id();
        Constants_AddItem.SELLER_ID = itemDetailMoldel.getSeller_id();
        Constants_AddItem.CREATEDAT = itemDetailMoldel.getCreated();
        Constants_AddItem.ITEM_NAME = itemNameTv.getText().toString();
        Constants_AddItem.ITEM_PRICE = itempricetv.getText().toString();
        String itemweight;
        itemweight = itenweighttv.getText().toString();
        itemweight = itemweight.replace(" lbs", "");
        Constants_AddItem.ITEM_WEIGHT = itemweight;
        Constants_AddItem.ITEM_STORY = itemDetailMoldel.getItem_story();
        Constants_AddItem.ITEM_CAT = categorytv.getText().toString();
        Constants_AddItem.ITEM_GENRE = itemDetailMoldel.getGenre();
        Constants_AddItem.ITEM_GENREVALUE = itemDetailMoldel.getGenre_value();
        Constants_AddItem.ITEM_PERSONAL_TRANSFER = itemDetailMoldel.getPerson_transfertext();
        Constants_AddItem.ITEM_DELVIERYTYPE = String.valueOf(itemDetailMoldel.getDelivery_type());
        Constants_AddItem.ITEM_CHARITYNAME = itemDetailMoldel.getCharity_name();
        Constants_AddItem.ITEM_LOCATION = itemDetailMoldel.getItemDetailLocationModel().getAdministrative_area_level_1();
        Constants_AddItem.ITEM_COUNTRY = itemDetailMoldel.getItemDetailLocationModel().getCountry();

        Constants_AddItem.ITEM_AUTO = String.valueOf(itemDetailMoldel.getIsAutographed());
        Constants_AddItem.ITEM_CHARITY = String.valueOf(itemDetailMoldel.getCharity());
        Constants_AddItem.TAGS = "";
        for (int i = itemDetailMoldel.getIDTagModel().getTagsArrayList().size() - 1; i >= 0; i--) {
            Constants_AddItem.TAGS = itemDetailMoldel.getIDTagModel().getTagsArrayList().get(i).toString().trim() + "," + Constants_AddItem.TAGS;
        }
        if (itemDetailMoldel.getIDUserImageModel().getOriginal().length() > 0) {
            strPrfileUrl = itemDetailMoldel.getIDUserImageModel().getOriginal();
            Constants_AddItem.User_Profile_Image = strPrfileUrl;
            Log.e(TAG, "Constants_AddItem.User_Profile_Image: " + Constants_AddItem.User_Profile_Image);
        }
        /*Setting Up Date*/
        Constants_AddItem.ITEM_LASTUSED = strActualFormat;
        if (itemDetailMoldel.getIDItemImageModel().getOriginal_1() != null) {
            Constants_AddItem.imagepath1 = itemDetailMoldel.getIDItemImageModel().getOriginal_1();
        } else {
            Constants_AddItem.imagepath1 = "";
        }
        if (itemDetailMoldel.getIDItemImageModel().getOriginal_2() != null) {
            Constants_AddItem.imagepath2 = itemDetailMoldel.getIDItemImageModel().getOriginal_2();
        } else {
            Constants_AddItem.imagepath2 = "";
        }
        if (itemDetailMoldel.getIDItemImageModel().getOriginal_3() != null) {
            Constants_AddItem.imagepath3 = itemDetailMoldel.getIDItemImageModel().getOriginal_3();
        } else {
            Constants_AddItem.imagepath3 = "";
        }
        if (itemDetailMoldel.getIDItemImageModel().getOriginal_4() != null) {
            Constants_AddItem.imagepath4 = itemDetailMoldel.getIDItemImageModel().getOriginal_4();
        } else {
            Constants_AddItem.imagepath4 = "";
        }
        if (itemDetailMoldel.getIDItemImageModel().getOriginal_5() != null) {
            Constants_AddItem.imagepath5 = itemDetailMoldel.getIDItemImageModel().getOriginal_5();
        } else {
            Constants_AddItem.imagepath5 = "";
        }
        if (itemDetailMoldel.getIDItemImageModel().getOriginal_6() != null) {
            Constants_AddItem.imagepath6 = itemDetailMoldel.getIDItemImageModel().getOriginal_6();
        } else {
            Constants_AddItem.imagepath6 = "";
        }
        if (itemDetailMoldel.getIDItemVideoModel() != null) {
            Constants_AddItem.videopath = itemDetailMoldel.getIDItemVideoModel().getOriginal_1();
            Constants_AddItem.videoimage = itemDetailMoldel.getIDItemVideoModel().getOriginal_videoimage();
        } else {
            Constants_AddItem.videopath = "";
            Constants_AddItem.videoimage = "";
        }
    }




    private void item_DetailSliding_Image() {
        Viewpager.setAdapter(new Sliding_I_D_ImageAdapter(getActivity(), detailImagesArray));
        NUM_PAGES = detailImagesArray.size();
        // Auto start of viewpager
//        if (handler==null){
//            handler = new Handler();
//            update = new Runnable() {
//                public void run() {
//                    Log.e(TAG, "run: " + currentPage + "....." + NUM_PAGES);
//                    if (currentPage == NUM_PAGES) {
//                        currentPage = 0;
//                    }
//                    Viewpager.setCurrentItem(currentPage++, true);
//                }
//            };
//            new Timer().schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    handler.post(update);
//                }
//            }, 1000, 3000);
//        }
        handler = new Handler();
        update = new Runnable() {
            public void run() {
//                Log.e(TAG, "run: " + currentPage + "....." + NUM_PAGES);
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                Viewpager.setCurrentItem(currentPage++, true);
            }
        };
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 3000, 3000);

        pageIndicator.setupWithViewPager(Viewpager, true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setIndoorEnabled(true);
        mMap = googleMap;
        Double lat1;
        Double log2;
        if (!lat.equalsIgnoreCase("") && !log.equalsIgnoreCase("")) {
            lat1 = Double.parseDouble(lat);
            log2 = Double.parseDouble(log);
        } else {
            lat1 = 0.0;
            log2 = 0.0;
        }
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(lat1, log2);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15.0f));
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap bitmap) {
                imageviewmap.setImageBitmap(bitmap);
            }
        });
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                    @Override
                    public void onSnapshotReady(Bitmap bitmap) {
                        imageviewmap.setImageBitmap(bitmap);
                    }
                });
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Bundle bundle = new Bundle();
                bundle.putString("lat", lat);
                bundle.putString("log", log);
                HomeActivity.switchFragment(getActivity(), new Map_Fragment_NEW(), Constants.PROFILE_FRAGMENT, true, bundle);
            }
        });
    }
}