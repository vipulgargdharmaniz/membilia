package com.membilia.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.membilia.fonts.fontsclass.Raleway_Bold;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class ButtonRaleway_Bold extends Button {

    public ButtonRaleway_Bold(Context context, AttributeSet attrs) {
        super(context, attrs);
        try {
            this.setTypeface(new Raleway_Bold(context).getFontFamily());
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
}
