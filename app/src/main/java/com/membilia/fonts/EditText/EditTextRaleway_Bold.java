package com.membilia.fonts.EditText;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.membilia.fonts.fontsclass.Raleway_Bold;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class EditTextRaleway_Bold extends EditText{

    public EditTextRaleway_Bold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextRaleway_Bold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextRaleway_Bold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public EditTextRaleway_Bold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new Raleway_Bold(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
