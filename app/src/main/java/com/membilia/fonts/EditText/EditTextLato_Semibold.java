package com.membilia.fonts.EditText;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.membilia.fonts.fontsclass.Lato_Semibold;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class EditTextLato_Semibold extends EditText{

    public EditTextLato_Semibold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextLato_Semibold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextLato_Semibold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public EditTextLato_Semibold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new Lato_Semibold(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
