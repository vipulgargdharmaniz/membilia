package com.membilia.fonts.fontsclass;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class Raleway_Light {
    String path = "Raleway-Light.ttf";
    Context mContext;
    public static Typeface mTypeface;

    public Raleway_Light(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){

        try{
            mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mTypeface;
    }
}
