package com.membilia.fonts.fontsclass;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class Lato_Regular {
    String path = "Lato-Regular.ttf";
    Context mContext;
    public static Typeface mTypeface;

    public Lato_Regular(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){

        try{
            mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mTypeface;
    }
}
