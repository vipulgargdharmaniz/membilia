package com.membilia.fonts.fontsclass;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class Lato_Medium {
    String path = "Lato-Medium.ttf";
    Context mContext;
    public static Typeface mTypeface;

    public Lato_Medium(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){

        try{
            mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mTypeface;
    }
}
