package com.membilia.fonts.fontsclass;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by dharmaniapps on 8/12/17.
 */

public class HelveticaNeve_Medium {
    String path = "HelveticaNeue-Medium.ttf";
    Context mContext;
    public static Typeface mTypeface;

    public HelveticaNeve_Medium(Context context){
        mContext = context;
    }

    public Typeface getFontFamily(){

        try{
            mTypeface = Typeface.createFromAsset(mContext.getAssets(),path);
        }catch (Exception e){
            e.printStackTrace();
        }
        return mTypeface;
    }
}
