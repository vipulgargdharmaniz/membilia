package com.membilia.fonts.TextViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.membilia.fonts.fontsclass.Lato_Semibold;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class TextViewLato_Semibold extends TextView {
    public TextViewLato_Semibold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextViewLato_Semibold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextViewLato_Semibold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public TextViewLato_Semibold(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new Lato_Semibold(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
