package com.membilia.fonts.TextViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.membilia.fonts.fontsclass.Lato_Light;

/**
 * Created by Dharmani Apps on 5/9/2017.
 */

public class TextViewLato_Light extends TextView {
    public TextViewLato_Light(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextViewLato_Light(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextViewLato_Light(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public TextViewLato_Light(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new Lato_Light(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
