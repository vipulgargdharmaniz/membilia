package com.membilia.fonts.TextViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.membilia.fonts.fontsclass.Lato_Bold;

/**
 * Created by dharmaniapps on 8/12/17.
 */

public class TextViewHelvetica_Medium extends TextView {

    public TextViewHelvetica_Medium(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextViewHelvetica_Medium(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextViewHelvetica_Medium(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public TextViewHelvetica_Medium(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new Lato_Bold(context).getFontFamily());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
