package com.membilia.signleton;

import com.membilia.volley_models.ActModel;

import java.util.ArrayList;

/**
 * Created by dharmaniapps on 27/8/18.
 */

public class PlayerPawnSignleton {

    private static PlayerPawnSignleton uniqInstance;

    public ArrayList<ActModel> mActivityArrayList = new ArrayList<ActModel>();

    private PlayerPawnSignleton() {
    }

    public static PlayerPawnSignleton getInstance() {
        if (uniqInstance == null) {
            {
                if (uniqInstance == null)
                    uniqInstance = new PlayerPawnSignleton();
            }
        }
        return uniqInstance;
    }


    public ArrayList<ActModel> getmActivityArrayList() {
        return mActivityArrayList;
    }
}
